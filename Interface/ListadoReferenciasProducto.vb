﻿Imports MySql.Data.MySqlClient
Imports [Interface].PRODUCTOS
Imports [Interface].MagentoService
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.IO
Imports System.Net
Imports System.Web
Imports System.Text
Imports System
Imports System.Xml
Imports System.Threading
Imports MSXML2
Imports System.Drawing.Imaging.ImageFormat
Imports WinSCP

Public Class ListadoReferenciasProducto

    Dim conexion As New MySqlConnection("server = 82.98.146.91; user id= Navision;password = excelia=2009;database = qapp_production; port = 3306")
    Public conect As web = New web

    Private Sub btn_Cerrar_Click(sender As Object, e As EventArgs) Handles btn_Cerrar.Click
        Me.Close()
    End Sub

    Private Sub ListadoReferenciasProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'JOSE                            

        Dim sku As String = ModificarProducto.txt_Sku.Text

        'Leer producto simple
        conexion.Open()
        Dim consulta As String = "SELECT articulos.articulo_modelo_id, articulos.referencia, articulo_modelos.nombre FROM articulo_modelos INNER JOIN articulos ON articulo_modelos.id = articulos.articulo_modelo_id WHERE articulos.referencia LIKE '%" & sku & "%'"
        Dim cmd As MySqlCommand = New MySqlCommand(consulta, conexion)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader

        With Me.lst_Referencias
            .Clear()
            .View = View.Details
            .CheckBoxes = True
            .GridLines = True
            .FullRowSelect = True
            .HideSelection = False
            .MultiSelect = False
            .Columns.Add("Referencia", 200)
            .Columns.Add("ID Configurable", 100)
            .Columns.Add("Nombre", 400)
        End With

        While lector.Read <> False
            Dim nombre As String = ConvertUTF8toUnicode(CStr(lector("nombre").ToString))
            With Me.lst_Referencias.Items.Add(lector("referencia").ToString)
                .SubItems.Add(lector("articulo_modelo_id").ToString)
                .SubItems.Add(nombre)
            End With
        End While
        conexion.Close()
    End Sub

    Private Sub btn_Aceptar_Click(sender As Object, e As EventArgs) Handles btn_Aceptar.Click
        Dim referencia As String = ""

        Me.Close()

        For Each item As ListViewItem In Me.lst_Referencias.Items
            If item.Checked = True Then
                referencia = item.Text
                ModificarProducto.txt_Sku.Text = referencia
            End If
        Next

        If referencia <> "" Then
            conect.buscarSku(ModificarProducto.txt_Sku.Text)
        End If

    End Sub

    Public Function ConvertUTF8toUnicode(cadena As String) As String
        'JOSE

        'Convertir cadena UTF8 en Unicode (en los SELECT)
        Dim UTF8String As String = cadena
        Dim dstEncoding As Encoding = Encoding.Unicode
        Dim dstEncodingANSI As Encoding = Encoding.GetEncoding(1252)
        Dim srcEncoding As Encoding = Encoding.UTF8
        Dim srcBytes As Byte() = dstEncodingANSI.GetBytes(UTF8String)
        Dim dstBytes As Byte() = Encoding.Convert(srcEncoding, dstEncoding, srcBytes)
        Dim salida As String = dstEncoding.GetString(dstBytes)
        Return salida
    End Function

    Public Function ConvertUnicodetoUTF8(cadena As String) As String
        'JOSE

        'Convertir cadena Unicode en UTF8 (en los INSERT y UPDATE)
        Dim unicodeString As String = cadena
        Dim dstEncoding As Encoding = Encoding.UTF8
        Dim dstEncodingANSI As Encoding = Encoding.GetEncoding(1252)
        Dim srcEncoding As Encoding = Encoding.Unicode
        Dim srcBytes As Byte() = srcEncoding.GetBytes(unicodeString)
        Dim dstBytes As Byte() = Encoding.Convert(srcEncoding, dstEncoding, srcBytes)
        Dim salida As String = dstEncodingANSI.GetString(dstBytes)
        Return salida
    End Function


End Class