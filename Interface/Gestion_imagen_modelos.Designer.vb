﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Gestion_imagen_modelos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.abredirectorio = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.buscar = New System.Windows.Forms.Button()
        Me.modelobusqueda = New System.Windows.Forms.TextBox()
        Me.nombreimgen = New System.Windows.Forms.TextBox()
        Me.actualizar = New System.Windows.Forms.Button()
        Me.guardar_imagen = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Eliminar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.enviar = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'abredirectorio
        '
        Me.abredirectorio.Location = New System.Drawing.Point(6, 19)
        Me.abredirectorio.Name = "abredirectorio"
        Me.abredirectorio.Size = New System.Drawing.Size(75, 46)
        Me.abredirectorio.TabIndex = 13
        Me.abredirectorio.Text = "Seleccionar imagen"
        Me.abredirectorio.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(455, 66)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(794, 521)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 11
        Me.PictureBox1.TabStop = False
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.Location = New System.Drawing.Point(22, 92)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(129, 319)
        Me.CheckedListBox1.Sorted = True
        Me.CheckedListBox1.TabIndex = 10
        '
        'buscar
        '
        Me.buscar.Location = New System.Drawing.Point(128, 53)
        Me.buscar.Name = "buscar"
        Me.buscar.Size = New System.Drawing.Size(75, 23)
        Me.buscar.TabIndex = 9
        Me.buscar.Text = "Buscar"
        Me.buscar.UseVisualStyleBackColor = True
        '
        'modelobusqueda
        '
        Me.modelobusqueda.Location = New System.Drawing.Point(22, 56)
        Me.modelobusqueda.Name = "modelobusqueda"
        Me.modelobusqueda.Size = New System.Drawing.Size(100, 20)
        Me.modelobusqueda.TabIndex = 8
        '
        'nombreimgen
        '
        Me.nombreimgen.Location = New System.Drawing.Point(22, 417)
        Me.nombreimgen.Name = "nombreimgen"
        Me.nombreimgen.Size = New System.Drawing.Size(129, 20)
        Me.nombreimgen.TabIndex = 14
        Me.nombreimgen.Visible = False
        '
        'actualizar
        '
        Me.actualizar.Location = New System.Drawing.Point(99, 21)
        Me.actualizar.Name = "actualizar"
        Me.actualizar.Size = New System.Drawing.Size(84, 46)
        Me.actualizar.TabIndex = 15
        Me.actualizar.Text = "Actualizar"
        Me.actualizar.UseVisualStyleBackColor = True
        '
        'guardar_imagen
        '
        Me.guardar_imagen.Location = New System.Drawing.Point(99, 20)
        Me.guardar_imagen.Name = "guardar_imagen"
        Me.guardar_imagen.Size = New System.Drawing.Size(75, 54)
        Me.guardar_imagen.TabIndex = 66
        Me.guardar_imagen.Text = "Guardar Imagen"
        Me.guardar_imagen.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.abredirectorio)
        Me.GroupBox1.Controls.Add(Me.actualizar)
        Me.GroupBox1.Location = New System.Drawing.Point(168, 222)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(189, 81)
        Me.GroupBox1.TabIndex = 67
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Actualizar Imagenes"
        '
        'Eliminar
        '
        Me.Eliminar.Location = New System.Drawing.Point(14, 19)
        Me.Eliminar.Name = "Eliminar"
        Me.Eliminar.Size = New System.Drawing.Size(88, 43)
        Me.Eliminar.TabIndex = 68
        Me.Eliminar.Text = "Eliminar"
        Me.Eliminar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Eliminar)
        Me.GroupBox2.Location = New System.Drawing.Point(168, 330)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(108, 81)
        Me.GroupBox2.TabIndex = 69
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Eliminar"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(6, 21)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 52)
        Me.Button1.TabIndex = 70
        Me.Button1.Text = "Seleccionar imagen"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'enviar
        '
        Me.enviar.Location = New System.Drawing.Point(190, 20)
        Me.enviar.Name = "enviar"
        Me.enviar.Size = New System.Drawing.Size(75, 54)
        Me.enviar.TabIndex = 71
        Me.enviar.Text = "Enviar"
        Me.enviar.UseVisualStyleBackColor = True
        Me.enviar.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Controls.Add(Me.enviar)
        Me.GroupBox3.Controls.Add(Me.guardar_imagen)
        Me.GroupBox3.Location = New System.Drawing.Point(168, 122)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(271, 81)
        Me.GroupBox3.TabIndex = 72
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Añadir imagen"
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(412, 66)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(37, 50)
        Me.PictureBox2.TabIndex = 73
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Coral
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(1255, 66)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(120, 78)
        Me.Button2.TabIndex = 74
        Me.Button2.Text = "Cerrar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(22, 443)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 75
        Me.TextBox1.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(18, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(258, 24)
        Me.Label7.TabIndex = 86
        Me.Label7.Text = "IMÁGENES DE MODELOS"
        '
        'Gestion_imagen_modelos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1400, 637)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.nombreimgen)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.CheckedListBox1)
        Me.Controls.Add(Me.buscar)
        Me.Controls.Add(Me.modelobusqueda)
        Me.Name = "Gestion_imagen_modelos"
        Me.Text = "Gestion_imagen"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents abredirectorio As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents CheckedListBox1 As System.Windows.Forms.CheckedListBox
    Friend WithEvents buscar As System.Windows.Forms.Button
    Friend WithEvents modelobusqueda As System.Windows.Forms.TextBox
    Friend WithEvents nombreimgen As System.Windows.Forms.TextBox
    Friend WithEvents actualizar As System.Windows.Forms.Button
    Friend WithEvents guardar_imagen As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Eliminar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents enviar As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
