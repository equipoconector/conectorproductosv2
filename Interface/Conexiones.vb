﻿Imports MySql.Data.MySqlClient
Imports [Interface].PRODUCTOS
Imports [Interface].MagentoService
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.IO
Imports System.Net
Imports System.Web
Imports System.Text
Imports System
Imports System.Xml
Imports System.Threading
Imports MSXML2

'CLASE CONECTOR NAVISION ---------------------------------------------------------------------------------------------------------------------------------
Public Class CONECTOR_NAVISION
    Public conect As web = New web
    Public ws As New PRODUCTOS.Conexion
    'Public ws As New PRODUCTOS.clientews
    Public cadena As String
    Public cadena2 As String
    Public entrada As String
    Public retorno As String
    Public salidas As String
    Public referencia_busqueda As String = ""

    Public Sub navconectar()
        ws.Url = "http://192.168.2.6:7047/QUIRUMED/WS/QUIRUMED/Codeunit/Conexion"
        ws.Credentials = New System.Net.NetworkCredential("dynamics", "quirumed=2010", "QUIRUMED")
    End Sub

    Public Sub conversor_ascii()
        retorno = ""
        entrada = Modelo.nombremodelo.Text
        ws.ANSI_ASCII(retorno, entrada)
        salidas = retorno
    End Sub

    Public Sub BUSCAR()
        Dim ref = referencia_busqueda
        Dim nombre = ""
        Dim price As Decimal
        Dim valor As String = ""
        Dim iva As String = ""
        Dim Peso As Decimal
        Dim Alto As Decimal
        Dim Ancho As Decimal
        Dim Profundidad As Decimal
        Dim Proveedor As String = ""
        Dim Coste As Decimal
        Dim Ref_proveedor As String = ""
        Dim Marca As String = ""
        Dim EAN As String = ""
        Dim modelo As String = ""
        Dim preciodistrib As Decimal
        Dim estado As String = ""
        Dim plazo_entrega As String = ""
        Dim descatalogado As Boolean
        Dim ubicacion As String = ""
        Dim Peso_Portes As Decimal
        Dim ps1 As Boolean
        Dim ps2a As Boolean
        Dim ps2b As Boolean
        Dim ps3 As Boolean
        Dim psinvitro As Boolean
        Dim bajopedido As Boolean
        Dim enviodirecto As Boolean
        Dim oxigeno As Boolean
        Dim accesorio_repuesto As Boolean

        ws.Productosws(ref, nombre, price, valor, iva, Peso, Alto, Ancho, Profundidad, Proveedor, Ref_proveedor, Marca, EAN, modelo, estado, plazo_entrega, descatalogado, ubicacion, Peso_Portes, ps1, ps2a, ps2b, ps3, psinvitro, bajopedido, enviodirecto, oxigeno, accesorio_repuesto)
        ws.Costessws(ref, Coste)
        ws.Precio_distrib_sws(ref, preciodistrib)

        With ENLACE
            .Descripcion.Text = nombre
            .Precio.Text = CStr(price)
            .pvpAnterior.Text = CStr(price) 'Este campo es para controlar si se produce un cambio de PVP
            .Valor.Text = valor
            .iva.Text = iva
            .Peso.Text = CStr(Peso)
            .Alto.Text = CStr(Alto)
            .Ancho.Text = CStr(Ancho)
            .Profundidad.Text = CStr(Profundidad)
            .Proveedor.Text = Proveedor
            .Coste.Text = CStr(Coste)
            .Refproveedor.Text = Ref_proveedor
            .Marca.Text = Marca
            .ean.Text = EAN
            .modeloid.Text = modelo
            .pvp_distribuidor.Text = CStr(preciodistrib)
            .Estado.Text = estado
            .ubicaciones.Text = ubicacion
            .Plazo_entrega.Text = plazo_entrega

            If descatalogado = True Then
                .descatalogar.Checked = True
            ElseIf descatalogado = False Then
                .descatalogar.Checked = False
            End If

            If bajopedido = True Then
                .chkBajoPedido.Checked = True
            ElseIf bajopedido = False Then
                .chkBajoPedido.Checked = False
            End If

            If enviodirecto = True Then
                .chkEnvioDirecto.Checked = True
            ElseIf enviodirecto = False Then
                .chkEnvioDirecto.Checked = False
            End If

            If oxigeno = True Then
                .chkOxigeno.Checked = True
            ElseIf oxigeno = False Then
                .chkOxigeno.Checked = False
            End If

            If accesorio_repuesto = True Then
                .chkAccesorio.Checked = True
            ElseIf accesorio_repuesto = False Then
                .chkAccesorio.Checked = False
            End If

            If ps1 = True Then
                .psichk.Checked = True
            ElseIf ps1 = False Then
                .psichk.Checked = False
            End If

            If ps2a = True Then
                .psiiachk.Checked = True
            ElseIf ps2a = False Then
                .psiiachk.Checked = False
            End If

            If ps2b = True Then
                .psiibchk.Checked = True
            ElseIf ps2b = False Then
                .psiibchk.Checked = False
            End If

            If ps3 = True Then
                .psiiichk.Checked = True
            ElseIf ps3 = False Then
                .psiiichk.Checked = False
            End If

            If psinvitro = True Then
                .psinvchk.Checked = True
            ElseIf psinvitro = False Then
                .psinvchk.Checked = False
            End If
        End With

        ENLACE.Refresh()
        Dim mysqlc As web = New web()
        mysqlc.buscarfechasoferta()
    End Sub

    Public Sub BUSCAR_MODELO()
        Dim id_mod As String = referencia_busqueda
        Dim Nombre_mod As String = ""
        Dim Nombre_ampliado As String = ""
        Dim Categoria As String = ""
        Dim Estado As String = ""
        'Dim coste As Decimal
        Dim numero_rev As String = ""
        ws.Modelosws(id_mod, Nombre_mod, Nombre_ampliado, Categoria, Estado)

        With Modelo
            .nombremodelo.Text = Nombre_mod
            .nombre_ampliado.Text = Nombre_ampliado
            .Categoria.Text = Categoria
            .Estado.Text = Estado
            .nombrexrec.Text = Nombre_mod
        End With

        If ENLACE.modeloid.Text <> "" Then
            ENLACE.Descripcion.Text = Nombre_mod & " " & ENLACE.Valor.Text
        End If

        '  ENLACE.Coste.Text = CStr(coste)
        Dim revision As web = New web
        revision.revision()
    End Sub

    Public Sub recuperar_descripcion()
        Dim id_mod As String = ENLACE.modeloid.Text
        Dim Nombre_mod As String = ""
        Dim Nombre_ampliado As String = ""
        Dim Categoria As String = ""
        Dim Estado As String = ""
        Dim numerorev As String = "0"
        ws.Modelosws(id_mod, Nombre_mod, Nombre_ampliado, Categoria, Estado)
        ENLACE.Descripcion.Text = Nombre_mod
    End Sub

    Public Sub MODIFICAR()
        Dim ref As String = ENLACE.Referencia.Text
        Dim nombre As String = ENLACE.Descripcion.Text
        Dim price As Decimal = CDec(ENLACE.Precio.Text)
        Dim valor As String = ENLACE.Valor.Text
        Dim iva As String = ENLACE.iva.Text
        Dim Peso As Decimal = CDec(ENLACE.Peso.Text)
        Dim Alto As Decimal = CDec(ENLACE.Alto.Text)
        Dim Ancho As Decimal = CDec(ENLACE.Ancho.Text)
        Dim Profundidad As Decimal = CDec(ENLACE.Profundidad.Text)
        Dim Proveedor As String = ENLACE.Proveedor.Text
        Dim Ref_proveedor As String = ENLACE.Refproveedor.Text
        Dim Marca As String = ENLACE.Marca.Text
        Dim EAN As String = ENLACE.ean.Text
        Dim modelo As String = ENLACE.modeloid.Text
        Dim preciodistribuidor As Decimal = CDec(ENLACE.pvp_distribuidor.Text)
        Dim estatus As String = ENLACE.Estado.Text
        Dim plazo_entrega As String = ENLACE.Plazo_entrega.Text
        Dim ubicacion As String = ENLACE.ubicaciones.Text
        Dim visible_dist As Boolean
        Dim descatalogado As Boolean
        Dim Peso_Portes As Decimal = 0
        Dim ps1 As Boolean
        Dim ps2a As Boolean
        Dim ps2b As Boolean
        Dim ps3 As Boolean
        Dim psinvitro As Boolean
        Dim bajopedido As Boolean
        Dim enviodirecto As Boolean
        Dim oxigeno As Boolean
        Dim accesorio_repuesto As Boolean

        If ENLACE.chkBajoPedido.Checked = False Then
            bajopedido = False
        ElseIf ENLACE.chkBajoPedido.Checked = True Then
            bajopedido = True
        End If

        If ENLACE.chkEnvioDirecto.Checked = False Then
            enviodirecto = False
        ElseIf ENLACE.chkEnvioDirecto.Checked = True Then
            enviodirecto = True
        End If

        If ENLACE.chkOxigeno.Checked = False Then
            oxigeno = False
        ElseIf ENLACE.chkOxigeno.Checked = True Then
            oxigeno = True
        End If

        If ENLACE.chkAccesorio.Checked = False Then
            accesorio_repuesto = False
        ElseIf ENLACE.chkAccesorio.Checked = True Then
            accesorio_repuesto = True
        End If

        If ENLACE.pvp_distribuidor.Text <> ENLACE.Precio.Text And ENLACE.pvp_distribuidor.Text <> "0" Then
            visible_dist = True
        ElseIf ENLACE.pvp_distribuidor.Text = ENLACE.Precio.Text Or ENLACE.pvp_distribuidor.Text = "0" Then
            visible_dist = False
        End If

        If ENLACE.descatalogar.Checked = False Then
            descatalogado = False
        ElseIf ENLACE.descatalogar.Checked = True Then
            descatalogado = True
        End If

        With ENLACE
            If .psichk.Checked = True Then
                ps1 = True
            ElseIf .psichk.Checked = False Then
                ps1 = False
            End If

            If .psiiachk.Checked = True Then
                ps2a = True
            ElseIf .psiiachk.Checked = False Then
                ps2a = False
            End If

            If .psiibchk.Checked = True Then
                ps2b = True
            ElseIf .psiibchk.Checked = False Then
                ps2b = False
            End If

            If .psiiichk.Checked = True Then
                ps3 = True
            ElseIf .psiiichk.Checked = False Then
                ps3 = False
            End If

            If .psinvchk.Checked = True Then
                psinvitro = True
            ElseIf .psinvchk.Checked = False Then
                psinvitro = False
            End If
        End With

        Dim precio = CDec(ENLACE.importe_oferta.Text)

        If precio = 0 Then
            ws.ProductoswsMOD(ref, nombre, price, valor, iva, Peso, Alto, Ancho, Profundidad, Proveedor, Ref_proveedor, Marca, EAN, modelo, estatus, plazo_entrega, ubicacion, visible_dist, descatalogado, Peso_Portes, ps1, ps2a, ps2b, ps3, psinvitro, bajopedido, enviodirecto, oxigeno, accesorio_repuesto)
            If ENLACE.pvp_distribuidor.Text <> "0" Then
                ws.Precio_distrib_MOD(ref, preciodistribuidor)
            End If
            ws.Tarifa_MOD(ref, price)
        End If

        If precio <> 0 Then
            price = CDec(ENLACE.importe_oferta.Text)
            ws.ProductoswsMOD(ref, nombre, price, valor, iva, Peso, Alto, Ancho, Profundidad, Proveedor, Ref_proveedor, Marca, EAN, modelo, estatus, plazo_entrega, ubicacion, visible_dist, descatalogado, Peso_Portes, ps1, ps2a, ps2b, ps3, psinvitro, bajopedido, enviodirecto, oxigeno, accesorio_repuesto)
            If ENLACE.pvp_distribuidor.Text <> "0" Then
                ws.Precio_distrib_MOD(ref, preciodistribuidor)
            End If
            ws.Tarifa_MOD(ref, price)
        End If
    End Sub

    Public Sub MODIFICAR_MODELO()
        Dim id_modelo As String = Modelo.idmodelo.Text
        Dim Nombre_modelo As String = Modelo.nombremodelo.Text
        Dim Nombre_ampliado As String = Modelo.nombre_ampliado.Text
        Dim Categoria As String = Modelo.Categoria.Text
        Dim Estado As String = Modelo.Estado.Text
        Dim Numero_rev As String = CStr(Modelo.numero_rev)
        ws.ModeloswsMOD(id_modelo, Nombre_modelo, Nombre_ampliado, Categoria, Estado)
    End Sub

    Public Sub comprobar()
        'hay que usar interruptores
        Dim ref As String = ENLACE.Referencia.Text
        Dim usuario As String = ENLACE.txtUsuario.Text
        Dim i As Integer
        Dim control As String = ""
        ws.Controlproductos(ref, control)
        If control <> "" Then
            i = 0
            MessageBox.Show("YA EXISTE LA REFERENCIA")
        End If
        If control = "" Then
            i = 1
        End If
        If i = 1 Then
            navconectar()
            INSERTAR()
            MODIFICAR()
            INSERTAR_COSTES()
            'MYSQL
            conect.myinsertar()
            'Guardado en logs
            conect.savelogs(usuario, ref, "Creacion", "Se ha creado el producto simple con sku " & ref & ". Campos pcoste=" & ENLACE.Coste.Text & " y pvd=" & ENLACE.pvp_distribuidor.Text)

            'ENLACE.magento.crear_producto_magento_simple()
        End If
    End Sub

    Public Sub INSERTAR()
        Dim ref As String = ENLACE.Referencia.Text
        Dim nombre As String = ENLACE.Descripcion.Text
        Dim price As Decimal = CDec(ENLACE.Precio.Text)
        Dim valor As String = ENLACE.Valor.Text
        Dim iva As String = ENLACE.iva.Text
        Dim Peso As Decimal = CDec(ENLACE.Peso.Text)
        Dim Alto As Decimal = CDec(ENLACE.Alto.Text)
        Dim Ancho As Decimal = CDec(ENLACE.Ancho.Text)
        Dim Profundidad As Decimal = CDec(ENLACE.Profundidad.Text)
        Dim Proveedor As String = ENLACE.Proveedor.Text
        ' Dim Coste As Decimal = ENLACE.Coste.Text
        Dim Ref_proveedor As String = ENLACE.Refproveedor.Text
        Dim Marca As String = ENLACE.Marca.Text
        Dim EAN As String = ENLACE.ean.Text
        Dim modelo As String = ENLACE.modeloid.Text
        Dim preciodistrib As String = ENLACE.pvp_distribuidor.Text
        Dim plazo_entrega As String = ENLACE.Plazo_entrega.Text
        Dim ubicacion As String = ENLACE.ubicaciones.Text
        Dim Peso_Portes As Decimal = 0
        Dim ps1 As Boolean
        Dim ps2a As Boolean
        Dim ps2b As Boolean
        Dim ps3 As Boolean
        Dim psinvitro As Boolean
        Dim bajopedido As Boolean
        Dim enviodirecto As Boolean
        Dim oxigeno As Boolean
        Dim accesorio_repuesto As Boolean

        If ENLACE.chkBajoPedido.Checked = False Then
            bajopedido = False
        ElseIf ENLACE.chkBajoPedido.Checked = True Then
            bajopedido = True
        End If

        If ENLACE.chkEnvioDirecto.Checked = False Then
            enviodirecto = False
        ElseIf ENLACE.chkEnvioDirecto.Checked = True Then
            enviodirecto = True
        End If

        If ENLACE.chkOxigeno.Checked = False Then
            oxigeno = False
        ElseIf ENLACE.chkOxigeno.Checked = True Then
            oxigeno = True
        End If

        If ENLACE.chkAccesorio.Checked = False Then
            accesorio_repuesto = False
        ElseIf ENLACE.chkAccesorio.Checked = True Then
            accesorio_repuesto = True
        End If

        With ENLACE
            If .psichk.Checked = True Then
                ps1 = True
            ElseIf .psichk.Checked = False Then
                ps1 = False
            End If

            If .psiiachk.Checked = True Then
                ps2a = True
            ElseIf .psiiachk.Checked = False Then
                ps2a = False
            End If

            If .psiibchk.Checked = True Then
                ps2b = True
            ElseIf .psiibchk.Checked = False Then
                ps2b = False
            End If

            If .psiiichk.Checked = True Then
                ps3 = True
            ElseIf .psiiichk.Checked = False Then
                ps3 = False
            End If

            If .psinvchk.Checked = True Then
                psinvitro = True
            ElseIf .psinvchk.Checked = False Then
                psinvitro = False
            End If
        End With

        Try
            ws.Uds_insert(ref)
            ws.ProductoswsINSERT(ref, nombre, price, valor, iva, Peso, Alto, Ancho, Profundidad, Proveedor, Ref_proveedor, Marca, EAN, modelo, plazo_entrega, ubicacion, Peso_Portes, ps1, ps2a, ps2b, ps3, psinvitro, bajopedido, enviodirecto, oxigeno, accesorio_repuesto)
            ws.Precio_distrib_INSERT(ref, CDec(preciodistrib))
            ws.Tarifa_INSERT(ref, price)
        Catch ex As Exception
            ws.Uds_delete(ref)
            MessageBox.Show("Error AL INSERTAR EL PRODUCTO, REVISA EL MODELO")
        End Try
    End Sub

    Public Sub buscar_plazos()
        Dim controlplazo As String = ""
        ws.Plazos(controlplazo, ENLACE.Proveedor.Text)
        ENLACE.Plazo_entrega.Text = controlplazo
    End Sub

    Public Sub INSERTAR_COSTES()
        ws.CostesINSERT(ENLACE.Referencia.Text, ENLACE.Proveedor.Text, CDec(ENLACE.Coste.Text))
    End Sub

    Public Sub MODIFICAR_COSTES()
        ws.CostesMOD(ENLACE.Referencia.Text, ENLACE.Proveedor.Text, CDec(ENLACE.Coste.Text))
    End Sub

    Public Sub INSERTAR_MARCA()
        ws.MarcasINSERT(Marcas.Nombre_marca.Text)
    End Sub

    Public Sub eans()
        ws.EANS(ENLACE.ean.Text, ENLACE.Referencia.Text)
    End Sub

    Public Sub INSERTAR_MODELO()
        Dim id_modelo As String = Modelo.idmodelo.Text
        Dim Nombre_modelo As String = Modelo.nombremodelo.Text
        Dim Nombre_ampliado As String = Modelo.nombre_ampliado.Text
        Dim Categoria As String = Modelo.Categoria.Text
        Dim Estado As String = Modelo.Estado.Text
        Dim numero_rev As String = Modelo.revision.Text
        ws.ModeloswsINSERT(id_modelo, Nombre_modelo, Nombre_ampliado, Categoria, Estado)
    End Sub

    Public Sub ELIMINAR()
        Dim ref As String = ENLACE.Referencia.Text
        Dim nombre As String = ENLACE.Descripcion.Text
        Dim price As Decimal = CDec(ENLACE.Precio.Text)
        Dim valor As String = ENLACE.Valor.Text
        'Dim iva As String = ENLACE.IVA.Text
        Dim iva As String = ENLACE.iva.Text
        Dim Peso As Decimal = CDec(ENLACE.Peso.Text)
        Dim Alto As Decimal = CDec(ENLACE.Alto.Text)
        Dim Ancho As Decimal = CDec(ENLACE.Ancho.Text)
        Dim Profundidad As Decimal = CDec(ENLACE.Profundidad.Text)
        Dim Proveedor As String = ENLACE.Proveedor.Text
        Dim Coste As Decimal = CDec(ENLACE.Coste.Text)
        Dim Ref_proveedor As String = ENLACE.Refproveedor.Text
        Dim Marca As String = ENLACE.Marca.Text
        Dim EAN As String = ENLACE.ean.Text
        ws.ProductoswsBORRAR(ref, nombre, price, valor, iva, Peso, Alto, Ancho, Profundidad, Proveedor, Coste, Ref_proveedor, Marca, EAN)
    End Sub

    Public Sub ACTUALIZA_TARIFAS_EXCEL()
        'ws.CostesMOD(ENLACE.txt_referencia.Text, ENLACE.Proveedor.Text, CDec(ENLACE.txt_pcoste.Text))
        'Dim pvd = CDec(ENLACE.txt_pdistribuidor.Text)

        'If pvd <> 0 Then
        '    ws.Precio_distrib_MOD(ENLACE.txt_referencia.Text, CDec(ENLACE.txt_pdistribuidor.Text))
        'End If
        'Dim precio = CDec(ENLACE.importe_oferta.Text)

        'If precio = 0 Then
        '    ws.Tarifa_MOD(ENLACE.txt_referencia.Text, CDec(ENLACE.txt_precio.Text))
        'End If

        'If precio <> 0 Then
        '    ws.Tarifa_MOD(ENLACE.txt_referencia.Text, CDec(ENLACE.importe_oferta.Text))
        'End If
    End Sub

    'Conexion con Navision
    Public Sub ACTUALIZA_TARIFAS_EXCEL1()
        'ws.CostesMOD(ENLACE.txt_referencia.Text, ENLACE.Proveedor.Text, CDec(ENLACE.txt_pcoste.Text))
        Dim pvd = CDec(ENLACE.txt_pdistribuidor.Text)
        If pvd <> 0 Then
            ws.Precio_distrib_MOD(ENLACE.txt_referencia.Text, CDec(ENLACE.txt_pdistribuidor.Text))
        End If

        Dim precio = CDec(ENLACE.importe_oferta.Text)
        If precio = 0 Then
            ws.Tarifa_MOD(ENLACE.txt_referencia.Text, CDec(ENLACE.txt_precio.Text))
        End If

        If precio <> 0 Then
            ws.Tarifa_MOD(ENLACE.txt_referencia.Text, CDec(ENLACE.importe_oferta.Text))
        End If
    End Sub

    Public Sub ACTUALIZA_DIAS_PROVEEDOR()
        ws.Cambio_Dias_Prov(ENLACE.proveedorn.Text, ENLACE.diasprov.Text)
    End Sub

    Public Sub ACTUALIZA_PRODUCTOS_BAJO_PEDIDO()
        'ws.Cambio_Productos_BajoPedido(ENLACE.referencia.Text, 1)
    End Sub

    Public Sub ACTUALIZA_PRODUCTOS_ENVIO_DIRECTO()
        'ws.Cambio_Productos_EnvioDirecto(ENLACE.referencia.Text, 1)
    End Sub

    Public Sub ACTUALIZA_PRODUCTOS_PROVEEDOR_ID()
        'ws.Cambio_Productos_ProveedorId(ENLACE.referencia.Text, ENLACE.proveedor_Id.Text)
    End Sub

End Class

'FIN CONECTOR NAVISION -------------------------------------------------------------------------------------------------------------------------------------------------

'CLASE CONECTOR MYSQL -------------------------------------------------------------------------------------------------------------------------------------------------

Public Class web
    Public navision As CONECTOR_NAVISION
    Public mageconect As String = "https://quirumed.on-dev.com/scripts/pre/"
    Public actualiza_referencias As String
    Public referencia_cambio As String
    Public entity As String
    Public cadena As String
    Dim visible_dist As Integer
    Dim UrlStr As String
    Dim params As String
    Dim DomDoc As MSXML2.XMLHTTP
    Dim imagen As Images = New Images
    Dim conexion As New MySqlConnection("server = 192.168.2.28; user id= Navision;password = excelia=2009;database = qapp_production; port = 3306;Convert Zero Datetime=True")
    Dim conexion2 As New MySqlConnection("server = 192.168.2.28; user id= Navision;password = excelia=2009;database = qapp_production; port = 3306;Convert Zero Datetime=True")
    Dim conexionmag As New MySqlConnection("server = 185.105.223.9; user id= quirumed;password = debc7139448d440a905f1613ed0fdc9f;database = quirumed; port = 3306;Convert Zero Datetime=True")
    Dim conexionmag2 As New MySqlConnection("server = 185.105.223.9; user id= quirumed;password = debc7139448d440a905f1613ed0fdc9f;database = quirumed; port = 3306;Convert Zero Datetime=True")

    Public Function ConvertUnicodetoUTF8(cadena As String) As String
        'Convertir cadena Unicode en UTF8  para insert y update
        Dim unicodeString As String = cadena
        Dim dstEncoding As Encoding = Encoding.UTF8
        Dim dstEncodingANSI As Encoding = Encoding.GetEncoding(1252)
        Dim srcEncoding As Encoding = Encoding.Unicode
        Dim srcBytes As Byte() = srcEncoding.GetBytes(unicodeString)
        Dim dstBytes As Byte() = Encoding.Convert(srcEncoding, dstEncoding, srcBytes)
        Dim salida As String = dstEncodingANSI.GetString(dstBytes)
        Return salida
    End Function

    Public Function ConvertUTF8toUnicode(cadena2 As String) As String
        'Convertir cadena UTF8 en Unicode para los select
        Dim UTF8String As String = CStr(cadena2)
        Dim dstEncoding As Encoding = Encoding.Unicode
        Dim dstEncodingANSI As Encoding = Encoding.GetEncoding(1252)
        Dim srcEncoding As Encoding = Encoding.UTF8
        Dim srcBytes As Byte() = dstEncodingANSI.GetBytes(UTF8String)
        Dim dstBytes As Byte() = Encoding.Convert(srcEncoding, dstEncoding, srcBytes)
        Dim salida As String = dstEncoding.GetString(dstBytes)
        Return salida
    End Function

    Public Function login() As String
        Dim rol As String = ""
        Dim nombre As String = ""
        conexion.Open()
        Dim consultaprov As String = "select usuario.nombre, rol.rol from usuarios as usuario Left Join roles as rol on rol.id = usuario.rol_id  where usuario.user = '" & ENLACE.txtUsuario.Text & "' && usuario.password='" & ENLACE.txtPaswword.Text & "'"
        Dim cmd As MySqlCommand = New MySqlCommand(consultaprov, conexion)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader
        If lector.Read Then
            'rol = CStr(lector("roles.rol"))
            rol = CStr(lector("rol"))
            nombre = CStr(lector("nombre"))
            ENLACE.txtAcceso.ForeColor = Color.Blue
            ENLACE.txtAcceso.Text = "Bienvenido " + nombre
        Else
            rol = "FALSE"
            ENLACE.txtAcceso.ForeColor = Color.Red
            ENLACE.txtAcceso.Text = "Acceso denegado"
        End If
        conexion.Close()
        Return rol
    End Function

    Public Sub myinsertar()
        Dim bajopedido = 0
        Dim enviodirecto = 0
        Dim oxigeno = 0
        Dim accesorio_repuesto = 0

        If ENLACE.chkBajoPedido.Checked = True Then
            bajopedido = 1
        End If

        If ENLACE.chkEnvioDirecto.Checked = True Then
            enviodirecto = 1
        End If

        If ENLACE.chkOxigeno.Checked = True Then
            oxigeno = 1
        End If

        If ENLACE.chkAccesorio.Checked = True Then
            accesorio_repuesto = 1
        End If

        Try
            conexion.Open()
            Dim myinsert As New MySqlCommand("insert into `articulos`(referencia,articulo_modelo_id,valor,precio,tipo_iva,peso,medida_alt,medida_anc,medida_pro,estado,precio_distribuidor,precio_coste,proveedor_id,dias_pedido_compra,ubicacion,ean,marca,ref_proveedor,precio_oferta,fecha_oferta_desde,bajo_pedido,envio_directo,oxigeno,accesorio_repuesto) values ('" & ENLACE.Referencia.Text & "','" & ENLACE.modeloid.Text & "','" & ConvertUnicodetoUTF8(ENLACE.Valor.Text) & "','" & Replace(ENLACE.Precio.Text, ",", ".") & "','" & ENLACE.iva.Text & "','" & Replace(ENLACE.Peso.Text, ",", ".") & "','" & Replace(ENLACE.Alto.Text, ",", ".") & "','" & Replace(ENLACE.Ancho.Text, ",", ".") & "','" & Replace(ENLACE.Profundidad.Text, ",", ".") & "','" & ENLACE.Estado.Text & "','" & Replace(ENLACE.pvp_distribuidor.Text, ",", ".") & "','" & Replace(ENLACE.Coste.Text, ",", ".") & "','" & ENLACE.Proveedor.Text & "','" & ENLACE.Plazo_entrega.Text & "','" & ENLACE.ubicaciones.Text & "','" & ENLACE.ean.Text & "','" & ConvertUnicodetoUTF8(ENLACE.Marca.Text) & "','" & ENLACE.Refproveedor.Text & "','" & Replace(ENLACE.importe_oferta.Text, ",", ".") & "','" & ENLACE.fecha_inicio.Text & "'," & bajopedido & "," & enviodirecto & "," & oxigeno & "," & accesorio_repuesto & ")", conexion)
            myinsert.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    Public Sub savelogs(usuario As String, referencia As String, tipo As String, comentario As String)
        Try
            conexion.Open()
            Dim myinsert As New MySqlCommand("insert into `logs`(usuario,fecha,referencia,tipo,comentario) values ('" & usuario & "',NOW(),'" & referencia & "'" & ",'" & tipo & "','" & comentario & "')", conexion)
            myinsert.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    'OBTENER EL AUTONUMERICO DEL MODELO
    Public Sub idmodelo()
        conexion.Open()
        Dim consulta As String = "select * from `articulo_modelos` order by id desc limit 0,1"
        Dim cmd As MySqlCommand = New MySqlCommand(consulta, conexion)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader
        If lector.Read Then
            ENLACE.modeloid.Text = CStr(lector("id"))
            ENLACE.Descripcion.Text = CStr(lector("nombre"))
            Modelo.idmodelo.Text = CStr(lector("id"))
        End If
        conexion.Close()
        'ENLACE.magento.crear_producto_magento_Agrupado()
    End Sub
    'FIN OBTENER EL AUTONUMERICO DEL MODELO

    'OBTENER NUMERO REVISION PARA TRADUCCIÓN
    Public Sub revision()
        conexion.Open()
        Dim consulta As String = "select nombre_rev,nombre_ampliacion,nombre_ampliacion_rev from `articulo_modelos`  where id = '" & Modelo.idmodelo.Text & "'"
        Dim cmd As MySqlCommand = New MySqlCommand(consulta, conexion)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader
        If lector.Read Then
            Modelo.revision.Text = CStr(lector("nombre_rev"))
            Modelo.nombreampliacionrev.Text = ConvertUTF8toUnicode(CStr(lector("nombre_ampliacion_rev")))
            Modelo.descripcioncortat.Text = ConvertUTF8toUnicode(CStr(lector("nombre_ampliacion")))
        End If
        conexion.Close()
    End Sub

    'FIN OBTENER NUMERO DE REVISION PARA TRADUCCIÓN
    Public Sub myinsertar_mod()
        If ENLACE.pvp_distribuidor.Text <> ENLACE.Precio.Text Then
            visible_dist = 1
        ElseIf ENLACE.pvp_distribuidor.Text = ENLACE.Precio.Text Then
            visible_dist = 0
        End If
        Try
            conexion.Open()
            If Modelo.descripcioncortat.Text = "" Then
                Dim myinsert As New MySqlCommand("insert into `articulo_modelos`(nombre,categoria_id,estado,visible_distribuidor) values ('" & ConvertUnicodetoUTF8(Modelo.nombremodelo.Text) & "','" & Modelo.Categoria.Text & "','" & Modelo.Estado.Text & "'," & visible_dist & ")", conexion)
                myinsert.ExecuteNonQuery()
            Else
                Dim myinsert As New MySqlCommand("insert into `articulo_modelos`(nombre,categoria_id,estado,visible_distribuidor,nombre_ampliacion) values ('" & ConvertUnicodetoUTF8(Modelo.nombremodelo.Text) & "','" & Modelo.Categoria.Text & "','" & Modelo.Estado.Text & "'," & visible_dist & ",'" & ConvertUnicodetoUTF8(Modelo.descripcioncortat.Text) & "')", conexion)
                myinsert.ExecuteNonQuery()
            End If
            conexion.Close()
        Catch ex As Exception
            MessageBox.Show("Error de insercion de modelo en web")
            conexion.Close()
        End Try
    End Sub

    Public Sub myactualizar()
        Dim bajopedido = 0
        Dim enviodirecto = 0
        Dim oxigeno = 0
        Dim accesorio_repuesto = 0

        If ENLACE.chkBajoPedido.Checked = True Then
            bajopedido = 1
        End If

        If ENLACE.chkEnvioDirecto.Checked = True Then
            enviodirecto = 1
        End If

        If ENLACE.chkOxigeno.Checked = True Then
            oxigeno = 1
        End If

        If ENLACE.chkAccesorio.Checked = True Then
            accesorio_repuesto = 1
        End If

        Try
            conexion.Open()
            Dim myupdate As New MySqlCommand("update `articulos` set bajo_pedido = " & bajopedido & ", envio_directo = " & enviodirecto & ", oxigeno =" & oxigeno & ", accesorio_repuesto =" & accesorio_repuesto & ", articulo_modelo_id ='" & ENLACE.modeloid.Text & "',valor = '" & ConvertUnicodetoUTF8(ENLACE.Valor.Text) & "',dias_pedido_compra = '" & ENLACE.Plazo_entrega.Text & "',ref_proveedor = '" & ENLACE.Refproveedor.Text & "',precio_distribuidor = '" & Replace(ENLACE.pvp_distribuidor.Text, ",", ".") & "',precio = '" & Replace(ENLACE.Precio.Text, ",", ".") & "',tipo_iva = '" & ENLACE.iva.Text & "',peso = '" & Replace(ENLACE.Peso.Text, ",", ".") & "',medida_alt = '" & Replace(ENLACE.Alto.Text, ",", ".") & "',medida_anc = '" & Replace(ENLACE.Ancho.Text, ",", ".") & "',medida_pro = '" & Replace(ENLACE.Profundidad.Text, ",", ".") & "',estado = '" & ENLACE.Estado.Text & "',precio_coste = '" & Replace(ENLACE.Coste.Text, ",", ".") & "',proveedor_id = '" & ENLACE.Proveedor.Text & "',dias_pedido_compra = '" & ENLACE.Plazo_entrega.Text & "',ubicacion= '" & ENLACE.ubicaciones.Text & "',ean = '" & ENLACE.ean.Text & "',marca = '" & ENLACE.Marca.Text & "',precio_oferta = '" & Replace(ENLACE.importe_oferta.Text, ",", ".") & "',fecha_oferta_desde = '" & ENLACE.fecha_inicio.Text & "' where Referencia ='" & ENLACE.Referencia.Text & "'", conexion)
            myupdate.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    Public Sub myactualizar_sin_pvd()
        Dim bajopedido = 0
        Dim enviodirecto = 0
        Dim oxigeno = 0
        Dim accesorio_repuesto = 0

        If ENLACE.chkBajoPedido.Checked = True Then
            bajopedido = 1
        End If

        If ENLACE.chkEnvioDirecto.Checked = True Then
            enviodirecto = 1
        End If

        If ENLACE.chkOxigeno.Checked = True Then
            oxigeno = 1
        End If

        If ENLACE.chkAccesorio.Checked = True Then
            accesorio_repuesto = 1
        End If

        Try
            conexion.Open()
            Dim myupdate As New MySqlCommand("update `articulos` set bajo_pedido = " & bajopedido & ", envio_directo = " & enviodirecto & ", oxigeno =" & oxigeno & ", accesorio_repuesto =" & accesorio_repuesto & ", articulo_modelo_id ='" & ENLACE.modeloid.Text & "',valor = '" & ConvertUnicodetoUTF8(ENLACE.Valor.Text) & "',dias_pedido_compra = '" & ENLACE.Plazo_entrega.Text & "',ref_proveedor = '" & ENLACE.Refproveedor.Text & "',precio = '" & Replace(ENLACE.Precio.Text, ",", ".") & "',tipo_iva = '" & ENLACE.iva.Text & "',peso = '" & Replace(ENLACE.Peso.Text, ",", ".") & "',medida_alt = '" & Replace(ENLACE.Alto.Text, ",", ".") & "',medida_anc = '" & Replace(ENLACE.Ancho.Text, ",", ".") & "',medida_pro = '" & Replace(ENLACE.Profundidad.Text, ",", ".") & "',estado = '" & ENLACE.Estado.Text & "',precio_coste = '" & Replace(ENLACE.Coste.Text, ",", ".") & "',proveedor_id = '" & ENLACE.Proveedor.Text & "',dias_pedido_compra = '" & ENLACE.Plazo_entrega.Text & "',ubicacion= '" & ENLACE.ubicaciones.Text & "',ean = '" & ENLACE.ean.Text & "',marca = '" & ENLACE.Marca.Text & "',precio_oferta = '" & Replace(ENLACE.importe_oferta.Text, ",", ".") & "',fecha_oferta_desde = '" & ENLACE.fecha_inicio.Text & "' where Referencia ='" & ENLACE.Referencia.Text & "'", conexion)
            myupdate.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    Public Sub myactualizar_mod()
        Dim conversor As CONECTOR_NAVISION = New CONECTOR_NAVISION
        If ENLACE.pvp_distribuidor.Text <> ENLACE.Precio.Text Then
            visible_dist = 1
        ElseIf ENLACE.pvp_distribuidor.Text = ENLACE.Precio.Text Then
            visible_dist = 0
        End If
        Try
            conexion.Open()
            Dim myupdate As New MySqlCommand("update `articulo_modelos` set nombre ='" & ConvertUnicodetoUTF8(Modelo.nombremodelo.Text) & "',visible_distribuidor = " & visible_dist & ",categoria_id = '" & Modelo.Categoria.Text & "', nombre_rev = '" & Modelo.revision.Text & "',estado = '" & Modelo.Estado.Text & "',nombre_ampliacion = '" & ConvertUnicodetoUTF8(Modelo.descripcioncortat.Text) & "',nombre_ampliacion_rev = '" & Modelo.nombreampliacionrev.Text & "' where id = '" & Modelo.idmodelo.Text & "'", conexion)
            myupdate.ExecuteNonQuery()
            conexion.Close()
            If Modelo.Estado.Text = "OCULTO" Then
                bloquea_simples()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub
    ''BLOQUEAMOS TODOS LOS SIMPLES ASOCIADOS CON EL MODELO
    Public Sub bloquea_simples()
        Try
            conexion.Open()
            Dim myupdatebloq As New MySqlCommand("update `articulos` set estado = '" & Modelo.Estado.Text & "' where  articulo_modelo_id  ='" & Modelo.idmodelo.Text & "'", conexion)
            myupdatebloq.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    '''BUSQUEDA OFERTAS 
    Public Sub buscarfechasoferta()
        conexion.Open()
        Dim consultaoferta As String = "select precio,precio_oferta,fecha_oferta_desde,fecha_oferta_hasta,precio_distribuidor from `articulos`  where referencia = '" & ENLACE.Referencia.Text & "'"
        Dim cmd As MySqlCommand = New MySqlCommand(consultaoferta, conexion)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader

        ENLACE.fecha_inicio.Text = ""
        ENLACE.importe_oferta.Text = ""
        ENLACE.Precio.Text = ""
        ENLACE.Refresh()

        While lector.Read <> False
            ENLACE.fecha_inicio.Text = CStr(CDate(lector("fecha_oferta_desde")))
            ENLACE.importe_oferta.Text = CStr(lector("precio_oferta"))
            ENLACE.Precio.Text = CStr(lector("precio"))
        End While

        ENLACE.Refresh()
        conexion.Close()
    End Sub
    '''IMPROVISACION DE ACTUALIZAR DIAS DE PROVEEDOR
    Public Sub actualizar_dias_prov()
        conexion.Open()
        Dim consultaprov As String = "select referencia from `articulos`  where proveedor_id = '" & ENLACE.proveedorn.Text & "'"
        Dim cmd As MySqlCommand = New MySqlCommand(consultaprov, conexion)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader
        Try
            While lector.Read <> False
                referencia_cambio = CStr(lector("referencia"))
                actualiza_web_plazos()
            End While
        Catch ex As Exception
            Console.WriteLine(ex.Source)
            Console.WriteLine(ex.Message)
        Finally
        End Try
        conexion.Close()
    End Sub

    Public Sub recuperar_simple_navision()
        Try
            conexion.Open()
            Dim referencia = ENLACE.txtRecuperarReferencia.Text
            Dim id_modelo As Integer = CInt(ENLACE.txtRecuperarModelo.Text)
            Dim myinsert As New MySqlCommand("insert into `articulos`(articulo_modelo_id, referencia) values (" & id_modelo & ",'" & referencia & "')", conexion)
            myinsert.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MessageBox.Show("Error de insercion")
            conexion.Close()
        End Try
    End Sub

    Public Sub actualiza_web_plazos()
        conexion2.Open()
        Dim myupdateprov As New MySqlCommand("update `articulos` set dias_pedido_compra = '" & ENLACE.diasprov.Text & "' where referencia = '" & referencia_cambio & "'", conexion2)
        myupdateprov.ExecuteNonQuery()
        conexion2.Close()
        Try
            MAGENTO_ACTUALIZA_PROV()
        Catch ex As Exception
            Console.WriteLine(ex.Source)
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Public Sub MAGENTO_ACTUALIZA_PROV()
        'Actualizar en Magento
        Dim strHtml As String = ""
        UrlStr = mageconect & "mageProveedorDias.php"
        DomDoc = New XMLHTTP
        params = "referencia=" & referencia_cambio & "&dias=" & ENLACE.diasprov.Text & ""
        DomDoc.open("POST", UrlStr, False)
        DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
        DomDoc.setRequestHeader("Connection", "close")
        DomDoc.send(params)
    End Sub

    Public Sub myeliminar()
        Try
            conexion.Open()
            Dim mydelete As New MySqlCommand("delete from `articulos` where referencia ='" & ENLACE.Referencia.Text & "'", conexion)
            mydelete.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    '-----------------INSERTAR IMAGENES
    '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    'INSERTAR IMAGENES EN EL MODELO
    Public Sub imagen_modelo()
        Try
            conexion.Open()
            Dim nombre_imagen As String = Modelo.idmodelo.Text & "_" & Gestion_imagen_modelos.n & ".jpg"
            Dim z As Integer = Modelo.i
            Dim id_modelo As Integer = CInt(Modelo.idmodelo.Text)
            Dim myinsert As New MySqlCommand("insert into `articulo_imagenes`(articulo_modelo_id,articulo_imagenes.order,file_name) values ('" & id_modelo & "'," & Gestion_imagen_modelos.n & ",'" & ConvertUnicodetoUTF8(nombre_imagen) & "')", conexion)
            myinsert.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MessageBox.Show("Error de insercion de imagen de modelo en web")
            conexion.Close()
        End Try
    End Sub
    'FIN INSERTAR IMAGENES EN MODELOS ---------------------------------------------------------------------------------------------------------------------------------------------------

    'INSERTAR IMAGENES EN PRODUCTOS ----------------------------------------------------------------------------------------------------------------------------------------------------
    Public Sub imagen_producto()
        Try
            conexion.Open()
            Dim nombre_imagen As String = ENLACE.Referencia.Text & "_" & ENLACE.z - 1 & ".jpg"
            Dim z As Integer = ENLACE.z
            Dim id_modelo As Integer = CInt(ENLACE.modeloid.Text)
            Dim myinsert As New MySqlCommand("insert into `articulo_imagenes`(articulo_modelo_id,articulo_imagenes.order,file_name) values ('" & id_modelo & "'," & z & ",'" & ConvertUnicodetoUTF8(nombre_imagen) & "')", conexion)
            myinsert.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MessageBox.Show("Error de insercion de imagen de modelo en web")
            ' MessageBox.Show(ex.Message)
            conexion.Close()
        End Try
    End Sub

    'Recuperar id de la imagen
    Public id_imagen As Integer
    Public Sub recuperar_id_imagen()
        conexion.Open()
        Dim consulta As String = "select * from `articulo_imagenes` where articulo_modelo_id ='" & ENLACE.modeloid.Text & "' order by id desc limit 0,1"
        Dim cmd As MySqlCommand = New MySqlCommand(consulta, conexion)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader
        If lector.Read Then
            id_imagen = CInt(lector("id"))
        End If
        conexion.Close()
    End Sub
    'Fin recuperar imagen

    'Insertar imagen en tabla articulo
    Public Sub update_img_articulo()
        Try
            conexion.Open()
            Dim myupdate As New MySqlCommand("update `articulos` set articulo_imagen_id = '" & id_imagen & "' where Referencia = '" & ENLACE.Referencia.Text & "'", conexion)
            myupdate.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub
    'FIN INSERTAR IMAGEN EN TABLA ARTICULO

    'FIN IMAGENES ARTICULOS -------------------------------------------------------------------------------------------------------------


    ' CATEGORIAS    
    Public Sub nuevaCategoria()
        conexion.Open()
        Dim consulta As String = "INSERT INTO categorias(nombre, parent_id, nivel,meta_title,meta_keywords,meta_description,descripcion) VALUES ('" & ConvertUnicodetoUTF8(FCategorias.nombrecat.Text) & "'," & FCategorias.Catpadre.Text & ",'" & FCategorias.nivelcat.Text & "','" & ConvertUnicodetoUTF8(FCategorias.Metatitle.Text) & "','" & ConvertUnicodetoUTF8(FCategorias.metakeywords.Text) & "','" & ConvertUnicodetoUTF8(FCategorias.metadescription.Text) & "','" & ConvertUnicodetoUTF8(FCategorias.descripcion.Text) & "')"
        Dim cmd As MySqlCommand = New MySqlCommand(consulta, conexion)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader
        conexion.Close()

        conexion.Open()
        Dim consulta1 As String = "SELECT * FROM `categorias` ORDER BY id DESC limit 0,1"
        Dim cmd1 As MySqlCommand = New MySqlCommand(consulta1, conexion)
        Dim lector1 As MySql.Data.MySqlClient.MySqlDataReader = cmd1.ExecuteReader
        Dim categoria_id As String = ""

        If lector1.Read Then
            categoria_id = CStr(lector1("id"))
        End If
        conexion.Close()

        FCategorias.categoriaid.Text = categoria_id

        'Actualizar en Magento
        Dim strHtml As String = ""
        UrlStr = mageconect & "mageCategoriaNuevo.php"
        DomDoc = New XMLHTTP
        params = "categoria_id=" & categoria_id
        DomDoc.open("POST", UrlStr, False)
        DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
        DomDoc.setRequestHeader("Connection", "close")
        DomDoc.send(params)

        'Actualizar Navision
        Dim navision As CONECTOR_NAVISION = New CONECTOR_NAVISION
        navision.navconectar()
        navision.ws.Categorias(FCategorias.categoriaid.Text, FCategorias.Catpadre.Text, FCategorias.nombrecat.Text, FCategorias.nivelcat.Text)

        'TRADUCCIONES
        Dim strHtml2 As String = ""
        Dim UrlStr2 As String
        Dim DomDoc2 As MSXML2.XMLHTTP
        Dim params2 As String
        UrlStr2 = mageconect & "mageTranslateCategory.php"
        DomDoc2 = New XMLHTTP
        params2 = "categoria_id=" & FCategorias.categoriaid.Text
        DomDoc2.open("POST", UrlStr2, False)
        DomDoc2.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        DomDoc2.setRequestHeader("Content-length", CStr(Len(params2)))
        DomDoc2.setRequestHeader("Connection", "close")
        DomDoc2.send(params2)
        'FIN TRADUCCIONES
        MsgBox("Categoria Nueva")
    End Sub

    Public rev As Integer
    Public Sub buscarCategoria(categoria_id As String)
        conexion.Open()
        Dim consulta As String = "SELECT id, nombre, parent_id, nivel, id_magento, nombre_rev,meta_title,meta_keywords,meta_description,descripcion,descripcion_rev FROM categorias WHERE id=" & categoria_id & ""
        Dim cmd As MySqlCommand = New MySqlCommand(consulta, conexion)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader
        If lector.Read Then
            ' FCategorias.categoriaid.Text = CStr(lector("id"))
            FCategorias.nombrecat.Text = ConvertUTF8toUnicode(CStr(lector("nombre")))
            FCategorias.Catpadre.Text = CStr(lector("parent_id"))
            FCategorias.nivelcat.Text = CStr(lector("nivel"))
            FCategorias.Idmagento.Text = CStr(lector("id_magento"))
            FCategorias.metakeywords.Text = ConvertUTF8toUnicode(CStr(lector("meta_keywords")))
            FCategorias.Metatitle.Text = ConvertUTF8toUnicode(CStr(lector("meta_title")))
            FCategorias.metadescription.Text = ConvertUTF8toUnicode(CStr(lector("meta_description")))
            FCategorias.descripcion.Text = ConvertUTF8toUnicode(CStr(lector("descripcion")))
            rev = CInt(CStr(lector("nombre_rev")))
            FCategorias.rev_descripcion = CInt(lector("descripcion_rev"))
            rev = rev + 1
        End If
        conexion.Close()
    End Sub

    Public Sub obtiene_padre_magento()
        Try
            conexion.Open()
            Dim consulta As String = "SELECT parent_id_magento FROM categorias WHERE parent_id=" & FCategorias.Catpadre.Text & ""
            Dim cmd As MySqlCommand = New MySqlCommand(consulta, conexion)
            Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader
            If lector.Read Then
                FCategorias.catpadremag.Text = CStr(lector("parent_id_magento"))
                FCategorias.cat_padre_magento = CStr(lector("parent_id_magento"))
            End If
        Catch ex As Exception
            MsgBox("NO EXISTE LA CATEGORIA")
            conexion.Close()
        End Try
        conexion.Close()
    End Sub

    Public Sub modificarCategoria(categoria_id As String)
        Try
            'Actualizar en MySql 
            conexion.Open()
            Dim consulta As String = "UPDATE categorias SET nombre='" & ConvertUnicodetoUTF8(FCategorias.nombrecat.Text) & "',parent_id_magento=" & FCategorias.cat_padre_magento & ", parent_id=" & FCategorias.Catpadre.Text & ", nombre_rev=" & rev & ",descripcion_rev =" & FCategorias.rev_descripcion & ", nivel=" & FCategorias.nivelcat.Text & ", meta_title='" & ConvertUnicodetoUTF8(FCategorias.Metatitle.Text) & "', meta_keywords='" & ConvertUnicodetoUTF8(FCategorias.metakeywords.Text) & "',meta_description='" & ConvertUnicodetoUTF8(FCategorias.metadescription.Text) & "',descripcion='" & ConvertUnicodetoUTF8(FCategorias.descripcion.Text) & "' WHERE id=" & categoria_id & ""
            Dim myinsert As New MySqlCommand(consulta, conexion)
            myinsert.ExecuteNonQuery()
            conexion.Close()

            'Actualizar en Magento
            Dim strHtml As String = ""
            UrlStr = mageconect & "mageCategoriaModificar.php"
            DomDoc = New XMLHTTP
            params = "categoria_id=" & categoria_id
            DomDoc.open("POST", UrlStr, False)
            DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
            DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
            DomDoc.setRequestHeader("Connection", "close")
            DomDoc.send(params)

        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
        MsgBox("Categoria Actualizada")
    End Sub

    Public Sub actualiza_cat_navision(categorias_id As String)
        'Actualizar Navision
        'Dim navision As CONECTOR_NAVISION = New CONECTOR_NAVISION
        'navision.navconectar()
        ' PRUEBAS DE PACO navision.ws.Categorias_Mod(FCategorias.nombrecat.Text, FCategorias.nivelcat.Text, categorias_id)
    End Sub

    ' INSERTAR IMAGEN CATEGORIAS
    Public Sub imagen_categoria()
        conexion.Open()
        Try
            'ELIMINAMOS ANTES DE INSERTAR PARA QUE NO SE REPITA LA IMAGEN
            Dim consultacat As New MySqlCommand("delete from `categoria_imagenes` where categoria_id ='" & Gestion_imagen_categorias.idcatweb.Text & "'", conexion)
            consultacat.ExecuteNonQuery()
            'AHORA INSERTAMOS LA IMAGEN
            Dim myinsert As New MySqlCommand("insert into `categoria_imagenes`(categoria_id, file_name) values ('" & Gestion_imagen_categorias.idcatweb.Text & "','" & Gestion_imagen_categorias.idcatweb.Text & ".jpg')", conexion)
            myinsert.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MessageBox.Show("Error de insercion de imagen de categoria en web")
            ' MessageBox.Show(ex.Message)
            conexion.Close()
        End Try
    End Sub

    'FIN INSERTAR IMAGEN CATEGORIAS

    'ACTUALIZA IMAGEN CATEGORIAS
    Public Sub actualiza_imagen_categoria()
        conexion.Open()
        Try
            Dim myupdate As New MySqlCommand("UPDATE `categoria_imagenes` SET categoria_id='" & Gestion_imagen_categorias.idcatweb.Text & "', file_name='" & Gestion_imagen_categorias.nombreimgen.Text & "' WHERE id=" & Gestion_imagen_categorias.idcatweb.Text & "")
            myupdate.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            ' MessageBox.Show("Error de actualizacion de imagen de categoria en web")
            ' MessageBox.Show(ex.Message)
            conexion.Close()
        End Try
    End Sub

    'FIN ACTUALIZA IMAGEN CATEGORIAS

    'FIN CATEGORIAS

    Public Sub busca_sku()
        'conexionmag.Open()
        'Dim consulta As String = ("select entity_id from `catalog_product_entity` where sku = '" & ENLACE.Referencia.Text & "'")
        'Dim cmd As MySqlCommand = New MySqlCommand(consulta, conexionmag)
        'Dim lector As MySqlDataReader = cmd.ExecuteReader
        'If lector.Read Then
        '    entity = CStr(lector("entity_id"))
        'End If
        'ACTUALIZAR_GRUPO_PRECIOS()
        'conexionmag.Close()
    End Sub

    Public Sub ACTUALIZAR_GRUPO_PRECIOS()
        'conexionmag2.Open()
        'Dim myupdate As New MySqlCommand("update `catalog_product_entity_group_price` set value = '" & Replace(ENLACE.pvp_distribuidor.Text, ",", ".") & "' where entity_id  = '" & entity & "' and customer_group_id = 4;update `catalog_product_entity_group_price` set value = '" & Replace(ENLACE.pvp_distribuidor.Text, ",", ".") & "' where entity_id  = '" & entity & "' and customer_group_id = 7", conexionmag2)
        'myupdate.ExecuteNonQuery()
        'conexionmag2.Close()
    End Sub

    Public Sub updateTarifaProductoCsv()
        Try
            'Consultar el ide de modelo
            conexion.Open()
            Dim consulta1 As String = "SELECT articulo_modelo_id, referencia, proveedor_id FROM `articulos` WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
            Dim cmd1 As MySqlCommand = New MySqlCommand(consulta1, conexion)
            Dim lector1 As MySql.Data.MySqlClient.MySqlDataReader = cmd1.ExecuteReader
            Dim modelo_id As String = ""
            Dim proveedor_id As String = ""
            If lector1.Read Then
                modelo_id = CStr(lector1("articulo_modelo_id"))
                proveedor_id = CStr(lector1("proveedor_id"))
            End If
            conexion.Close()

            If ENLACE.txt_pcoste.Text = "" Then
                ENLACE.txt_pcoste.Text = "0"
            End If

            If ENLACE.txt_pdistribuidor.Text = "" Then
                ENLACE.txt_pdistribuidor.Text = "0"
            End If

            If ENLACE.importe_oferta.Text = "" Then
                ENLACE.importe_oferta.Text = "0"
            End If

            Dim importoferta As Decimal
            importoferta = CDec(ENLACE.importe_oferta.Text)

            If importoferta = 0 Then
                If ENLACE.txt_pcoste.Text = "0" Then
                    ENLACE.txt_pdistribuidor.Text = ENLACE.txt_precio.Text
                End If

                'Actualizar en MySql con precio normal
                conexion2.Open()
                Dim consulta As String
                consulta = "UPDATE articulos SET precio=" & Replace(ENLACE.txt_precio.Text, ",", ".") & ",precio_oferta=" & Replace(ENLACE.importe_oferta.Text, ",", ".") & ",precio_distribuidor=" & Replace(ENLACE.txt_pdistribuidor.Text, ",", ".") & ",precio_coste=" & Replace(ENLACE.txt_pcoste.Text, ",", ".") & " WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
                Dim myinsert As New MySqlCommand(consulta, conexion2)
                myinsert.ExecuteNonQuery()
                conexion2.Close()
            End If

            If importoferta <> 0 Then
                If ENLACE.txt_pcoste.Text = "0" Then
                    ENLACE.txt_pdistribuidor.Text = ENLACE.importe_oferta.Text
                End If
                'Actualizar en MySql con precio oferta
                conexion2.Open()
                Dim consulta As String
                consulta = "UPDATE articulos SET precio=" & Replace(ENLACE.txt_precio.Text, ",", ".") & ",fecha_oferta_desde='" & ENLACE.fecha_inicio.Text & "',precio_oferta=" & Replace(ENLACE.importe_oferta.Text, ",", ".") & ",precio_distribuidor=" & Replace(ENLACE.txt_pdistribuidor.Text, ",", ".") & ",precio_coste=" & Replace(ENLACE.txt_pcoste.Text, ",", ".") & " WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
                Dim myinsert As New MySqlCommand(consulta, conexion2)
                myinsert.ExecuteNonQuery()
                conexion2.Close()
            End If

            'Conexion con Magento
            Dim strHtml As String = ""
            Dim UrlStr As String
            Dim DomDoc As MSXML2.XMLHTTP
            Dim params As String
            UrlStr = mageconect & "mageProductoModificar.php"
            DomDoc = New XMLHTTP
            params = "producto_id=" & modelo_id
            DomDoc.open("POST", UrlStr, False)
            DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
            DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
            DomDoc.setRequestHeader("Connection", "close")
            DomDoc.send(params)
            'MsgBox("El producto ha sido actualizado")
            'ModificarProducto.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    Public Sub preciosActual()
        Try
            conexion.Open()
            Dim consulta1 As String = "SELECT precio, precio_oferta, precio_distribuidor FROM `articulos` WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
            Dim cmd1 As MySqlCommand = New MySqlCommand(consulta1, conexion)
            Dim lector1 As MySql.Data.MySqlClient.MySqlDataReader = cmd1.ExecuteReader
            If lector1.Read Then
                ENLACE.pvdAnterior.Text = CStr(lector1("precio_distribuidor"))
                ENLACE.pvpAnterior.Text = CStr(lector1("precio"))
                ENLACE.pvoAnterior.Text = CStr(lector1("precio_oferta"))
                ENLACE.pvdAnterior.Text = Replace(ENLACE.pvdAnterior.Text, ".", ",")
                ENLACE.pvpAnterior.Text = Replace(ENLACE.pvpAnterior.Text, ".", ",")
                ENLACE.pvoAnterior.Text = Replace(ENLACE.pvdAnterior.Text, ".", ",")
            End If
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    Public Sub preciosActualDirecto()
        'Try
        '    conexion.Open()
        '    Dim consulta1 As String = "SELECT precio, precio_oferta, precio_distribuidor FROM `articulos` WHERE referencia='" & ENLACE.Referencia.Text & "'"
        '    Dim cmd1 As MySqlCommand = New MySqlCommand(consulta1, conexion)
        '    Dim lector1 As MySql.Data.MySqlClient.MySqlDataReader = cmd1.ExecuteReader
        '    If lector1.Read Then
        '        ENLACE.pvdAnterior.Text = CStr(lector1("precio_distribuidor"))
        '        ENLACE.pvpAnterior.Text = CStr(lector1("precio"))
        '        ENLACE.pvoAnterior.Text = CStr(lector1("precio_oferta"))
        '        ENLACE.pvdAnterior.Text = Replace(ENLACE.pvdAnterior.Text, ".", ",")
        '        ENLACE.pvpAnterior.Text = Replace(ENLACE.pvpAnterior.Text, ".", ",")
        '        ENLACE.pvoAnterior.Text = Replace(ENLACE.pvdAnterior.Text, ".", ",")
        '    End If
        '    conexion.Close()
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        '    conexion.Close()
        'End Try
    End Sub

    Public Sub updateTarifaProductoDistribuidorCsv()
        Try
            'Consultar el ide de modelo
            conexion.Open()
            Dim consulta1 As String = "SELECT articulo_modelo_id, referencia, proveedor_id FROM `articulos` WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
            Dim cmd1 As MySqlCommand = New MySqlCommand(consulta1, conexion)
            Dim lector1 As MySql.Data.MySqlClient.MySqlDataReader = cmd1.ExecuteReader
            Dim modelo_id As String = ""
            Dim proveedor_id As String = ""
            If lector1.Read Then
                modelo_id = CStr(lector1("articulo_modelo_id"))
                proveedor_id = CStr(lector1("proveedor_id"))
            End If
            conexion.Close()

            If ENLACE.txt_pcoste.Text = "" Then
                ENLACE.txt_pcoste.Text = "0"
            End If

            If ENLACE.txt_pdistribuidor.Text = "" Then
                ENLACE.txt_pdistribuidor.Text = "0"
            End If

            If ENLACE.importe_oferta.Text = "" Then
                ENLACE.importe_oferta.Text = "0"
            End If

            Dim importoferta As Decimal
            importoferta = CDec(ENLACE.importe_oferta.Text)

            If importoferta = 0 Then
                'If ENLACE.txt_pcoste.Text = "0" Then
                'ENLACE.txt_pdistribuidor.Text = ENLACE.txt_precio.Text
                'End If

                'Actualizar en MySql con precio normal
                conexion2.Open()
                Dim consulta As String
                If ENLACE.txt_pdistribuidor.Text <> "0" Then
                    consulta = "UPDATE articulos SET precio=" & Replace(ENLACE.txt_precio.Text, ",", ".") & ",precio_oferta=" & Replace(ENLACE.importe_oferta.Text, ",", ".") & ",precio_distribuidor=" & Replace(ENLACE.txt_pdistribuidor.Text, ",", ".") & ",precio_coste=" & Replace(ENLACE.txt_pcoste.Text, ",", ".") & " WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
                Else
                    consulta = "UPDATE articulos SET precio=" & Replace(ENLACE.txt_precio.Text, ",", ".") & ",precio_oferta=" & Replace(ENLACE.importe_oferta.Text, ",", ".") & ",precio_coste=" & Replace(ENLACE.txt_pcoste.Text, ",", ".") & " WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
                End If

                Dim myinsert As New MySqlCommand(consulta, conexion2)
                myinsert.ExecuteNonQuery()
                conexion2.Close()
            End If

            If importoferta <> 0 Then
                'If ENLACE.txt_pcoste.Text = "0" Then
                'ENLACE.txt_pdistribuidor.Text = ENLACE.importe_oferta.Text
                'End If

                'Actualizar en MySql con precio oferta
                conexion2.Open()
                Dim consulta As String
                If ENLACE.txt_pdistribuidor.Text <> "0" Then
                    consulta = "UPDATE articulos SET precio=" & Replace(ENLACE.txt_precio.Text, ",", ".") & ",fecha_oferta_desde='" & ENLACE.fecha_inicio.Text & "',precio_oferta=" & Replace(ENLACE.importe_oferta.Text, ",", ".") & ",precio_distribuidor=" & Replace(ENLACE.txt_pdistribuidor.Text, ",", ".") & ",precio_coste=" & Replace(ENLACE.txt_pcoste.Text, ",", ".") & " WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
                Else
                    consulta = "UPDATE articulos SET precio=" & Replace(ENLACE.txt_precio.Text, ",", ".") & ",fecha_oferta_desde='" & ENLACE.fecha_inicio.Text & "',precio_oferta=" & Replace(ENLACE.importe_oferta.Text, ",", ".") & ",precio_coste=" & Replace(ENLACE.txt_pcoste.Text, ",", ".") & " WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
                End If
                Dim myinsert As New MySqlCommand(consulta, conexion2)
                myinsert.ExecuteNonQuery()
                conexion2.Close()
            End If

            'Conexion con Magento
            Dim strHtml As String = ""
            Dim UrlStr As String
            Dim DomDoc As MSXML2.XMLHTTP
            Dim params As String
            UrlStr = mageconect & "mageProductoModificar.php"
            DomDoc = New XMLHTTP
            params = "producto_id=" & modelo_id
            DomDoc.open("POST", UrlStr, False)
            DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
            DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
            DomDoc.setRequestHeader("Connection", "close")
            DomDoc.send(params)
            'MsgBox("El producto ha sido actualizado")
            'ModificarProducto.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub


    Public Sub updateProductosBajoPedidoCsv()
        Try
            conexion.Open()
            Dim consulta As String
            consulta = "UPDATE articulos SET bajo_pedido=1 WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
            Dim myinsert As New MySqlCommand(consulta, conexion)
            myinsert.ExecuteNonQuery()
            conexion.Close()

            Threading.Thread.Sleep(2000)

            'Conexion con Magento
            Dim strHtml As String = ""
            Dim UrlStr As String
            Dim DomDoc As MSXML2.XMLHTTP
            Dim params As String
            UrlStr = mageconect & "mageProductoBajoPedido.php"
            DomDoc = New XMLHTTP
            params = "referencia=" & ENLACE.txt_referencia.Text
            DomDoc.open("POST", UrlStr, False)
            DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
            DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
            DomDoc.setRequestHeader("Connection", "close")
            DomDoc.send(params)
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    Public Sub updateProductosEnvioDirectoCsv()
        'Try
        '    conexion.Open()
        '    Dim consulta As String
        '    consulta = "UPDATE articulos SET envio_directo=1 WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
        '    Dim myinsert As New MySqlCommand(consulta, conexion)
        '    myinsert.ExecuteNonQuery()
        '    conexion.Close()

        '    Threading.Thread.Sleep(2000)

        '    'Conexion con Magento
        '    Dim strHtml As String = ""
        '    Dim UrlStr As String
        '    Dim DomDoc As MSXML2.XMLHTTP
        '    Dim params As String
        '    UrlStr = mageconect & "mageProductoEnvioDirecto.php"
        '    DomDoc = New XMLHTTP
        '    params = "referencia=" & ENLACE.txt_referencia.Text
        '    DomDoc.open("POST", UrlStr, False)
        '    DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        '    DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
        '    DomDoc.setRequestHeader("Connection", "close")
        '    DomDoc.send(params)
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        '    conexion.Close()
        'End Try
    End Sub

    Public Sub updateProductosProveedorIdCsv()
        'Try
        '    conexion.Open()
        '    Dim consulta As String
        '    consulta = "UPDATE articulos SET proveedor_id = '" & ENLACE.txt_proveedorId.Text & "' WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
        '    Dim myinsert As New MySqlCommand(consulta, conexion)
        '    myinsert.ExecuteNonQuery()
        '    conexion.Close()

        '    'Threading.Thread.Sleep(2000)

        '    'conexion con Magento
        '    Dim strHtml As String = ""
        '    Dim UrlStr As String
        '    Dim DomDoc As MSXML2.XMLHTTP
        '    Dim params As String
        '    UrlStr = mageconect & "mageProductoProveedorId.php"
        '    DomDoc = New XMLHTTP
        '    params = "referencia=" & ENLACE.txt_referencia.Text & "&proveedor_id=" & ENLACE.txt_proveedorId.Text & ""
        '    DomDoc.open("POST", UrlStr, False)
        '    DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        '    DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
        '    DomDoc.setRequestHeader("Connection", "close")
        '    DomDoc.send(params)
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        '    conexion.Close()
        'End Try
    End Sub

    Public Sub updateOcultarProductoCsv()
        'Try
        '    'Actualizar en MySql con precio normal
        '    conexion2.Open()
        '    Dim consulta As String = "UPDATE articulos SET estado='OCULTO' WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
        '    Dim myinsert As New MySqlCommand(consulta, conexion2)
        '    myinsert.ExecuteNonQuery()
        '    conexion2.Close()
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        '    conexion.Close()
        'End Try
    End Sub

    Public Sub updateValorMultidiomaCsv()
        Dim valor = ENLACE.txtValorMultidioma.Text.Replace("'", "´")
        Try
            'Actualizar en MySql con precio normal
            conexion2.Open()
            Dim consulta As String = "UPDATE articulos SET valor_" & ENLACE.txtLang.Text & "='" & valor & "', volcado=1 WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
            Dim myinsert As New MySqlCommand(consulta, conexion2)
            myinsert.ExecuteNonQuery()
            conexion2.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    Public Sub updateNombreCategoriaMultidiomaCsv()
        Dim nombre = ENLACE.txtNombreMultidioma.Text.Replace("'", "´")
        Try
            'Actualizar en MySql con precio normal
            conexion2.Open()
            Dim consulta As String = "UPDATE categorias SET nombre_" & ENLACE.txtLang.Text & "='" & nombre & "', volcado=1 WHERE id=" & ENLACE.txt_referencia.Text & ""
            Dim myinsert As New MySqlCommand(consulta, conexion2)
            myinsert.ExecuteNonQuery()
            conexion2.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    Public Sub updatePVDProductoCsv()
        Try
            'Actualizar en MySql el PVD
            conexion.Open()
            Dim consulta As String = "UPDATE articulos SET precio_distribuidor=" & Replace(ENLACE.txt_pdistribuidor.Text, ",", ".") & " WHERE referencia='" & ENLACE.txt_referencia.Text & "'"
            Dim myinsert As New MySqlCommand(consulta, conexion)
            myinsert.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexion.Close()
        End Try
    End Sub

    Public Sub verLogs()
        conexion.Open()
        Dim consulta As String = "SELECT id, usuario, fecha, referencia, tipo, comentario FROM `logs` ORDER BY id DESC"
        Dim cmd As MySqlCommand = New MySqlCommand(consulta, conexion)
        Dim lector As New MySqlDataAdapter(cmd)
        Dim ds = New DataSet()
        lector.Fill(ds)
        Logs.DataGridView1.DataSource = ds.Tables(0).DefaultView
        Logs.DataGridView1.Columns(5).Width = 400 'cambio tamaño columna comentarios
        conexion.Close()
    End Sub
End Class


'FIN CLASE CONECTOR WEB
'-----------------------------------------------------------------------------------------------------------------------------------------

'CLASE VISUALIZACIÓN DE MARCAS---------------------------------------------------------------------------------------------------------------


Public Class SQLCONECTAR
    Public cadena As String
    Public conectar As SqlConnection
    'Public sqlconecta As SQLCONECTAR = New SQLCONECTAR

    Public Sub sqlconectar()
        Try
            cadena = "Data Source = PROLIANT2008\NAVISION;Initial Catalog = QUIRUMED;user id = conector;password = quirumed2015"
            'cadena = "Data Source = PROLIANT2008\NAVISION;Initial Catalog = QUIRUMED;user id = javier;password = quirumed2012"
            conectar = New SqlConnection
            conectar.ConnectionString = cadena
            conectar.Open()
        Catch ex As Exception
            conectar.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub VISUALIZAR_MARCAS()
        Dim sqlconecta As SQLCONECTAR = New SQLCONECTAR
        sqlconecta.sqlconectar()
        Dim sqlbuscar As New SqlClient.SqlCommand
        sqlbuscar.CommandType = System.Data.CommandType.Text
        sqlbuscar.CommandText = "select QUIRUMED.dbo.[QUIRUMED$Dimension Value].Code from QUIRUMED.dbo.[QUIRUMED$Dimension Value]" &
        "where QUIRUMED.dbo.[QUIRUMED$Dimension Value].[Dimension Code] = 'MARCA'"
        sqlbuscar.Connection = sqlconecta.conectar
        Dim cmd As SqlCommand = New SqlCommand(sqlbuscar.CommandText, sqlconecta.conectar)
        Dim lector As SqlDataReader = cmd.ExecuteReader
        'ENLACE.RichTextBox1.Text = sqlbuscar.CommandText
        While lector.Read <> False
            ENLACE.Marca.Items.Add(lector("Code"))
        End While
    End Sub

    Public Sub VISUALIZAR_EAN()
        Dim sqlconecta As SQLCONECTAR = New SQLCONECTAR
        sqlconecta.sqlconectar()
        Dim sqlbuscar As New SqlClient.SqlCommand
        sqlbuscar.CommandType = System.Data.CommandType.Text
        sqlbuscar.CommandText = "select QUIRUMED.dbo.[QUIRUMED$Codigos Ean].EAN from QUIRUMED.dbo.[QUIRUMED$Codigos Ean]" &
        "where QUIRUMED.dbo.[QUIRUMED$Codigos Ean].Ocupado = 0"
        sqlbuscar.Connection = sqlconecta.conectar
        Dim cmd As SqlCommand = New SqlCommand(sqlbuscar.CommandText, sqlconecta.conectar)
        Dim lector As SqlDataReader = cmd.ExecuteReader
        'ENLACE.RichTextBox1.Text = sqlbuscar.CommandText
        While lector.Read <> False
            ENLACE.EANS.Items.Add(lector("EAN"))
        End While
    End Sub
End Class

Public Class MAGENTO

    'Public row As String
    'Public row1 As String

    Public Sub crear_producto_magento_simple()
        'Dim sesion As String
        'Using proxy As New MagentoService.MagentoService
        '    Dim sessionId As String = proxy.login("jose", "Quirumed2012")
        '    sesion = sessionId
        '    Dim productData As catalogProductCreateEntity = New catalogProductCreateEntity
        '    Dim a As catalogProductEntity = New catalogProductEntity()
        '    Dim sku As String = ENLACE.Referencia.Text
        '    Dim type As String = "simple" 'simple - grouped - configurable
        '    Dim categorias() As String = {"4"}
        '    Dim atrset As String = ""
        '    Dim attributesets = proxy.catalogProductAttributeSetList(sessionId)
        '    For Each attributeset In attributesets
        '        atrset = CStr(attributeset.set_id)
        '    Next
        '    'Dim valor As associativeEntity = New associativeEntity
        '    'valor.key = "grouped_child_text"
        '    'valor.value = "Color Rojo"
        '    With productData
        '        .name = ENLACE.Descripcion.Text
        '        .url_key = ""
        '        .description = "ESTO ES UNA PRUEBA DE CONECTOR2"
        '        .category_ids = categorias
        '        .short_description = .name
        '        .weight = ENLACE.Peso.Text 'ENLACE.PESO
        '        .status = CStr(1)
        '        .url_path = ""
        '        .visibility = CStr(1) ' 4 para producto agrupado y 1 para simple
        '        .price = ENLACE.Precio.Text 'ENLACE.PRECIO
        '        .tax_class_id = CStr(2) 'PRIMERO HAY QUE VER LO QUE HAY EN MAGENTO (Numeración) y hacer una equivalencia con los que usamos (ejem 1= gen)
        '        .meta_title = ""
        '        .meta_keyword = ""
        '        .meta_description = ""
        '        '.additional_attributes.single_data.SetValue(valor, 0)
        '    End With
        '    Dim product As Integer = proxy.catalogProductCreate(sessionId, type, atrset, sku, productData, "default")
        '    Dim url As String = "http://aledplus.com/catalog/product/view/id/" & product
        '    ' proxy.endSession(sessionId)
        'End Using
        'Agrupar()
    End Sub

    Public Sub crear_producto_magento_Agrupado()
        'Dim sesion As String
        'Using proxy As New MagentoService.MagentoService
        '    Dim sessionId As String = proxy.login("jose", "Quirumed2012")
        '    sesion = sessionId
        '    Dim productData As catalogProductCreateEntity = New catalogProductCreateEntity
        '    Dim a As catalogProductEntity = New catalogProductEntity()
        '    Dim sku As String = Modelo.idmodelo.Text
        '    Dim type As String = "grouped" 'simple - grouped - configurable
        '    Dim categorias() As String = {"4"}
        '    Dim atrset As String = ""
        '    Dim attributesets = proxy.catalogProductAttributeSetList(sessionId)
        '    For Each attributeset In attributesets
        '        atrset = CStr(attributeset.set_id)
        '    Next
        '    Dim valor As associativeEntity = New associativeEntity
        '    valor.key = "grouped_child_text"
        '    valor.value = "Color Rojo"
        '    With productData
        '        .name = Modelo.nombremodelo.Text
        '        .url_key = ""
        '        .description = "ESTO ES UNA PRUEBA DE CONECTOR 26042013"
        '        .category_ids = categorias
        '        .short_description = .name
        '        .weight = CStr(1) 'ENLACE.PESO
        '        .status = CStr(1)
        '        .url_path = ""
        '        .visibility = CStr(4) ' 4 para producto agrupado y 1 para simple
        '        .price = CStr(13) 'ENLACE.PRECIO
        '        .tax_class_id = CStr(2) 'PRIMERO HAY QUE VER LO QUE HAY EN MAGENTO (Numeración) y hacer una equivalencia con los que usamos (ejem 1= gen)
        '        .meta_title = ""
        '        .meta_keyword = ""
        '        .meta_description = ""
        '        .additional_attributes.single_data.SetValue(valor, 0)
        '    End With
        '    Dim product As Integer = proxy.catalogProductCreate(sessionId, type, atrset, sku, productData, "default")
        '    Dim url As String = "http://aledplus.com/catalog/product/view/id/" & product
        '    proxy.endSession(sessionId)
        'End Using
        'Agrupar()
    End Sub

    Public Sub Agrupar()
        'Using proxy As New MagentoService.MagentoService
        '    Dim sessionId As String = proxy.login("jose", "Quirumed2012")
        '    ' Sesion.Text = sessionId
        '    Dim productData As catalogProductLinkEntity = New catalogProductLinkEntity()
        '    Dim sku As String = ENLACE.modeloid.Text
        '    Dim type As String = "grouped" 'related, cross-sell, up-sell
        '    Dim productIdentifierType As String = "sku"
        '    Dim linkedProduct As String = ENLACE.Referencia.Text
        '    With productData
        '        .position = CStr(0)
        '        .sku = ENLACE.modeloid.Text
        '        .type = "grouped"
        '    End With
        '    Dim result As Boolean = proxy.catalogProductLinkAssign(sessionId, type, sku, linkedProduct, productData, productIdentifierType)
        '    MessageBox.Show(CStr(result))
        '    'proxy.endSession(sessionId)
        'End Using
    End Sub

    Public Sub modifificar_producto_simple_magento()
        'Using proxy As New MagentoService.MagentoService
        '    Dim sessionId As String = proxy.login("jose", "Quirumed2012")
        '    ' Sesion.Text = sessionId
        '    Dim productData As catalogProductCreateEntity = New catalogProductCreateEntity()
        '    Dim a As catalogProductEntity = New catalogProductEntity()  'sku
        '    Dim sku As String = ENLACE.Referencia.Text 'Referencia
        '    Dim product_id As Integer = 6
        '    Dim type As String = "simple" 'simple - grouped
        '    Dim categorias() As String = {"4"}
        '    Dim productIdentifierType As String = "sku" 'Define si es el id o sku el que se envia como parametro
        '    'get attribute set
        '    Dim atrset As String
        '    Dim attributeSets = proxy.catalogProductAttributeSetList(sessionId)
        '    For Each attributeSet In attributeSets
        '        atrset = CStr(attributeSet.set_id)
        '    Next
        '    With productData
        '        .name = ENLACE.Descripcion.Text
        '        .url_key = ""
        '        .description = "ESTO ES UNA PRUEBA DE CONECTOR2"
        '        .category_ids = categorias
        '        .short_description = .name
        '        .weight = ENLACE.Peso.Text
        '        .status = CStr(1)
        '        .url_path = ""
        '        .visibility = CStr(1)  '1 si se trata de un producto simple
        '        .price = ENLACE.Precio.Text
        '        .tax_class_id = CStr(2) 'IVA
        '        .meta_title = ""
        '        .meta_keyword = ""
        '        .meta_description = ""
        '    End With
        '    Dim product As Boolean = proxy.catalogProductUpdate(sessionId, sku, productData, "default", productIdentifierType)
        '    'MessageBox.Show(product)
        '    'proxy.endSession(sessionId)
        'End Using
    End Sub
End Class

'--------------------------------------------------------------------------------------------------------------------------------------------------------------





