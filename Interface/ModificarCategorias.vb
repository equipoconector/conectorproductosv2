﻿Public Class ModificarCategorias

    Public navconectar As CONECTOR_NAVISION = New CONECTOR_NAVISION
    Public conect As web = New web
    Public sqlconexion As SQLCONECTAR = New SQLCONECTAR

    Private Sub btn_Cerrar_Click(sender As Object, e As EventArgs) Handles btn_Cerrar.Click
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        If Me.txt_Id.Text <> "" Then
            'btn_Activar.Enabled = True
            conect.buscarCategoria(Me.txt_Id.Text)
        Else
            MsgBox("Debes de introducir la ID de la categoria")
        End If
    End Sub

    Private Sub btn_Modificar_Click(sender As Object, e As EventArgs) Handles btn_Modificar.Click
        conect.updateCategoria(Me.txt_Id.Text)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btn_Upload.Click
        conect.uploadImagenCategoria()
    End Sub

    Private Sub btn_Examinar_Click(sender As Object, e As EventArgs) Handles btn_Examinar.Click
        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            txtFile.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub btn_Activar_Click(sender As Object, e As EventArgs) Handles btn_Activar.Click
        If txtPassword.Text = "qmpaco" Then
            If btn_Activar.Text = "Activar" Then
                btn_Activar.Text = "Desactivar"
                txtNivel.Enabled = True
                txt_Categoria_Padre.Enabled = True
                txt_Categoria_Padre_Magento.Enabled = True
                btn_Actualizar.Enabled = True
                GroupBox1.BackColor = Color.DarkKhaki
            Else
                btn_Activar.Text = "Activar"
                txtNivel.Enabled = False
                txt_Categoria_Padre.Enabled = False
                txt_Categoria_Padre_Magento.Enabled = False
                btn_Actualizar.Enabled = False
                'GroupBox1.BackColor = Color.DarkGray
                GroupBox1.BackColor = Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
            End If
        End If
    End Sub

    Private Sub btn_Actualizar_Click(sender As Object, e As EventArgs) Handles btn_Actualizar.Click
        btn_Actualizar.Enabled = False
        txtNivel.Enabled = False
        txt_Categoria_Padre.Enabled = False
        txt_Categoria_Padre_Magento.Enabled = False
        btn_Activar.Text = "Activar"
        btn_Activar.Enabled = True
        conect.cambioLugarCategoria(Me.txt_Id.Text)
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles btn_categorias.Click
        Arbol_Categorias_CModificar.Tree.Enabled = True
        Arbol_Categorias_CModificar.Dispose()
        Arbol_Categorias_CModificar.ShowDialog()
    End Sub

    Private Sub txt_Id_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_Id.Leave
        If Me.txt_Id.Text <> "" Then
            conect.buscarCategoria(Me.txt_Id.Text)
        End If
    End Sub

End Class