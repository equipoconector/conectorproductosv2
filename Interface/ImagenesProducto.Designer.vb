﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImagenesProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_Cerrar = New System.Windows.Forms.Button()
        Me.lblUploadExito = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btn_Examinar = New System.Windows.Forms.Button()
        Me.txtFile = New System.Windows.Forms.TextBox()
        Me.btn_Upload = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.txt_NumeroImagen = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btn_Cerrar
        '
        Me.btn_Cerrar.Location = New System.Drawing.Point(946, 703)
        Me.btn_Cerrar.Name = "btn_Cerrar"
        Me.btn_Cerrar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cerrar.TabIndex = 20
        Me.btn_Cerrar.Text = "Cerrar"
        Me.btn_Cerrar.UseVisualStyleBackColor = True
        '
        'lblUploadExito
        '
        Me.lblUploadExito.AutoSize = True
        Me.lblUploadExito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUploadExito.Location = New System.Drawing.Point(755, 696)
        Me.lblUploadExito.Name = "lblUploadExito"
        Me.lblUploadExito.Size = New System.Drawing.Size(124, 13)
        Me.lblUploadExito.TabIndex = 93
        Me.lblUploadExito.Text = "¡¡¡ Upload Imágen !!!"
        Me.lblUploadExito.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(49, 692)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(42, 13)
        Me.Label15.TabIndex = 92
        Me.Label15.Text = "Imágen"
        '
        'btn_Examinar
        '
        Me.btn_Examinar.Location = New System.Drawing.Point(434, 690)
        Me.btn_Examinar.Name = "btn_Examinar"
        Me.btn_Examinar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Examinar.TabIndex = 91
        Me.btn_Examinar.Text = "Examinar"
        Me.btn_Examinar.UseVisualStyleBackColor = True
        '
        'txtFile
        '
        Me.txtFile.Location = New System.Drawing.Point(104, 690)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.Size = New System.Drawing.Size(324, 20)
        Me.txtFile.TabIndex = 90
        '
        'btn_Upload
        '
        Me.btn_Upload.Enabled = False
        Me.btn_Upload.Location = New System.Drawing.Point(673, 691)
        Me.btn_Upload.Name = "btn_Upload"
        Me.btn_Upload.Size = New System.Drawing.Size(75, 23)
        Me.btn_Upload.TabIndex = 89
        Me.btn_Upload.Text = "Upload File"
        Me.btn_Upload.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'txt_NumeroImagen
        '
        Me.txt_NumeroImagen.Location = New System.Drawing.Point(601, 691)
        Me.txt_NumeroImagen.Name = "txt_NumeroImagen"
        Me.txt_NumeroImagen.Size = New System.Drawing.Size(44, 20)
        Me.txt_NumeroImagen.TabIndex = 94
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(538, 697)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 95
        Me.Label1.Text = "Nº Imagen"
        '
        'ImagenesProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1067, 738)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_NumeroImagen)
        Me.Controls.Add(Me.lblUploadExito)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.btn_Examinar)
        Me.Controls.Add(Me.txtFile)
        Me.Controls.Add(Me.btn_Upload)
        Me.Controls.Add(Me.btn_Cerrar)
        Me.Name = "ImagenesProducto"
        Me.Text = "Imagenes Producto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Cerrar As System.Windows.Forms.Button
    Friend WithEvents lblUploadExito As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents btn_Examinar As System.Windows.Forms.Button
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents btn_Upload As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txt_NumeroImagen As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
