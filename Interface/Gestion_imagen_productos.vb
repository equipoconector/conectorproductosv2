﻿Imports System
Imports System.IO
Imports WinSCP
Public Class Gestion_imagen_productos
    Public carpeta As New DirectoryInfo("\\192.168.2.6\imagenes\img_simple")
    Public act_imagen As New Images
    Public i As Integer = 0
    Private Sub buscar_Click(sender As Object, e As EventArgs) Handles buscar.Click
        CheckedListBox1.Items.Clear()

        For Each f As FileInfo In carpeta.GetFiles
            If f.Name.Contains(productobusqueda.Text) Then
                CheckedListBox1.Items.Add(f.Name)
            End If
        Next
    End Sub

    Private Sub CheckedListBox1_Click(sender As Object, e As EventArgs) Handles CheckedListBox1.Click
        Dim cadena As String = CStr(CheckedListBox1.SelectedItem)
        Using stream As New StreamReader("\\192.168.2.6\imagenes\img_simple\" & cadena)
            PictureBox1.Image = Image.FromStream(stream.BaseStream)
            nombreimgen.Text = cadena
        End Using
    End Sub

    Private Sub Gestion_imagen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For Each f As FileInfo In carpeta.GetFiles
            If f.Name.StartsWith(ENLACE.Referencia.Text & "_") Then
                CheckedListBox1.Items.Add(f.Name)
                i = i + 1
            End If
        Next
    End Sub

    Private Sub abredirectorio_Click(sender As Object, e As EventArgs) Handles abredirectorio.Click
        PictureBox1.Image = Nothing
        CheckedListBox1.ClearSelected()
        Dim OpenFile As New OpenFileDialog()
        If OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.PictureBox1.Image = Image.FromFile(OpenFile.FileName)
        End If
    End Sub

    Private Sub actualizar_Click(sender As Object, e As EventArgs) Handles actualizar.Click
        act_imagen.actualizar_img_articulo()
    End Sub

    Private Sub Eliminar_Click(sender As Object, e As EventArgs) Handles Eliminar.Click
        act_imagen.eliminar_imagen_articulo()
    End Sub

    Private Sub guardar_imagen_Click(sender As Object, e As EventArgs) Handles guardar_imagen.Click
        Dim conexion As web = New web
        act_imagen.referencia = ENLACE.Referencia.Text
        act_imagen.incremento = ENLACE.Referencia.Text & "_" & i
        act_imagen.Abrir_carpeta_Producto()

        conexion.imagen_producto()
        conexion.recuperar_id_imagen() 'bloqueado por magento
        conexion.update_img_articulo()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        PictureBox1.Image = Nothing
        CheckedListBox1.ClearSelected()
        Dim OpenFile As New OpenFileDialog()
        If OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.PictureBox1.Image = Image.FromFile(OpenFile.FileName)
        End If
    End Sub

    Private Sub enviar_Click(sender As Object, e As EventArgs) Handles enviar.Click

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class