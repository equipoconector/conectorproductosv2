﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CATEGORIAS_SIMPLES
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ID = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.busqueda = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lista = New System.Windows.Forms.ListView()
        Me.lista2 = New System.Windows.Forms.ListView()
        Me.lista3 = New System.Windows.Forms.ListView()
        Me.lista4 = New System.Windows.Forms.ListView()
        Me.lista5 = New System.Windows.Forms.ListView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ID
        '
        Me.ID.Location = New System.Drawing.Point(213, 51)
        Me.ID.Name = "ID"
        Me.ID.Size = New System.Drawing.Size(108, 20)
        Me.ID.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(62, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(132, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "PRODUCTO AGRUPADO"
        '
        'busqueda
        '
        Me.busqueda.Location = New System.Drawing.Point(370, 42)
        Me.busqueda.Name = "busqueda"
        Me.busqueda.Size = New System.Drawing.Size(94, 30)
        Me.busqueda.TabIndex = 2
        Me.busqueda.Text = "BUSCAR"
        Me.busqueda.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(489, 42)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(100, 29)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "ACTUALIZAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lista
        '
        Me.lista.CheckBoxes = True
        Me.lista.Location = New System.Drawing.Point(65, 129)
        Me.lista.Name = "lista"
        Me.lista.Size = New System.Drawing.Size(314, 615)
        Me.lista.TabIndex = 4
        Me.lista.UseCompatibleStateImageBehavior = False
        '
        'lista2
        '
        Me.lista2.CheckBoxes = True
        Me.lista2.Location = New System.Drawing.Point(412, 129)
        Me.lista2.Name = "lista2"
        Me.lista2.Size = New System.Drawing.Size(317, 615)
        Me.lista2.TabIndex = 5
        Me.lista2.UseCompatibleStateImageBehavior = False
        '
        'lista3
        '
        Me.lista3.CheckBoxes = True
        Me.lista3.Location = New System.Drawing.Point(766, 129)
        Me.lista3.Name = "lista3"
        Me.lista3.Size = New System.Drawing.Size(328, 615)
        Me.lista3.TabIndex = 6
        Me.lista3.UseCompatibleStateImageBehavior = False
        '
        'lista4
        '
        Me.lista4.CheckBoxes = True
        Me.lista4.Location = New System.Drawing.Point(1120, 129)
        Me.lista4.Name = "lista4"
        Me.lista4.Size = New System.Drawing.Size(362, 615)
        Me.lista4.TabIndex = 7
        Me.lista4.UseCompatibleStateImageBehavior = False
        '
        'lista5
        '
        Me.lista5.CheckBoxes = True
        Me.lista5.Location = New System.Drawing.Point(1512, 129)
        Me.lista5.Name = "lista5"
        Me.lista5.Size = New System.Drawing.Size(344, 615)
        Me.lista5.TabIndex = 8
        Me.lista5.UseCompatibleStateImageBehavior = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(62, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "PRIMER NIVEL"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(409, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "SEGUNDO NIVEL"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(773, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "TERCER NIVEL"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(1117, 96)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "CUARTO NIVEL"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(1509, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(83, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "QUINTO NIVEL"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Coral
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(1712, 17)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(144, 82)
        Me.Button2.TabIndex = 14
        Me.Button2.Text = "CERRAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(61, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(234, 24)
        Me.Label7.TabIndex = 86
        Me.Label7.Text = "CATEGORÍAS SIMPLES"
        '
        'CATEGORIAS_SIMPLES
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1885, 826)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lista5)
        Me.Controls.Add(Me.lista4)
        Me.Controls.Add(Me.lista3)
        Me.Controls.Add(Me.lista2)
        Me.Controls.Add(Me.lista)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.busqueda)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ID)
        Me.Name = "CATEGORIAS_SIMPLES"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CATEGORIAS_SIMPLES"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents busqueda As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lista As System.Windows.Forms.ListView
    Friend WithEvents lista2 As System.Windows.Forms.ListView
    Friend WithEvents lista3 As System.Windows.Forms.ListView
    Friend WithEvents lista4 As System.Windows.Forms.ListView
    Friend WithEvents lista5 As System.Windows.Forms.ListView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
