﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class Service
    Private _key As String

    Private Const BaseUrl = "https://www.googleapis.com/language/translate/v2"
    Private Const keyParamName = "key"

    Sub New(ByVal key As String)
        _key = key
    End Sub

    'Public Function Translate(ByVal sources As System.Collections.Generic.IEnumerable(Of String)) As String
    Public Function Translate(source As String) As String
        Dim ub = New System.Text.StringBuilder(BaseUrl)

        Dim parameters = New System.Collections.Generic.List(Of String)

        parameters.Add(String.Format("{0}={1}", keyParamName, _key))
        parameters.Add(String.Format("{0}={1}", "source", "es"))
        parameters.Add(String.Format("{0}={1}", "target", "en"))
        'For Each source In sources
        parameters.Add(String.Format("{0}={1}", "q", source))
        'Next

        ub.AppendFormat("?{0}", String.Join("&", parameters))

        Dim r = System.Net.HttpWebRequest.Create(ub.ToString)

        Dim sr As New System.IO.StreamReader(r.GetResponse.GetResponseStream)

        Dim json As String = sr.ReadToEnd
        Dim deserializedProduct As JObject = JsonConvert.DeserializeObject(Of JObject)(json)
        Dim traslated As JToken = deserializedProduct.SelectToken("data.translations[0].translatedText")

        Return CStr(traslated)
    End Function
End Class
