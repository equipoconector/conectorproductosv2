﻿Imports System.IO
'Imports Google.API.Translate

Imports System.Collections.Generic
Imports System.ComponentModel
Imports Google.Apis.Translate.v2
Imports Google.Apis.Translate.v2.Data
Imports TranslationsResource = Google.Apis.Translate.v2.Data.TranslationsResource


Public Class CrearProducto

    Public navconectar As CONECTOR_NAVISION = New CONECTOR_NAVISION
    Public conect As web = New web
    Public sqlconexion As SQLCONECTAR = New SQLCONECTAR

    Public trans As Service = New Service("AIzaSyA3wV4rphhHftZW7XydTMmuxudadFIKy3Q")

    Private Sub btn_Cerrar_Click(sender As Object, e As EventArgs) Handles btn_Cerrar.Click
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub btn_Crear_Click(sender As Object, e As EventArgs) Handles btn_Crear.Click
        If Me.txt_Id_categoria.Text <> "" And Me.txt_Nombre.Text <> "" And Me.combo_Estado.Text <> "" Then
            conect.nuevoModelo()
        Else
            MsgBox("Faltan datos para crear el Producto")
        End If
    End Sub

    Private Sub btn_Editor_Click(sender As Object, e As EventArgs) Handles btn_Editor.Click
        Editorweb.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Arbol_Categorias_PNueva.Tree.Enabled = True
        Arbol_Categorias_PNueva.Dispose()
        Arbol_Categorias_PNueva.ShowDialog()
    End Sub

    Private Sub txt_Id_categoria_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_Id_categoria.Leave
        If Me.txt_Id_categoria.Text <> "" Then
            conect.consultarNombreCategoria(Me.txt_Id_categoria.Text)
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        conect.ponerCategoriasprincipales()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnCambiarIva.Click
        'El txt abrirlo con el Notepad++ y codificarlo con UTF-8 sin BOM
        Dim ruta As String = "C:\TestFolder\iva_red.txt"
        If File.Exists(ruta) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim referencia As String = ""
                        Dim iva As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                referencia = currentField
                            End If
                            If n = 1 Then
                                iva = currentField
                            End If
                            n = n + 1
                        Next
                        conect.updateIvaProductoCsv(referencia, iva)
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If
    End Sub

    Private Sub Button3_Click_1(sender As Object, e As EventArgs) Handles Button3.Click
        'referencia, pvp, precio_coste  --> ejemplo: qm-6754    1675,80     1300,76     (separador: TAB)
        'Dim ruta As String = "C:\TestFolder\tarifas_producto_csv.txt"
        Dim ruta As String = Me.txtFile.Text
        If File.Exists(ruta) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim referencia As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                txt_referencia.Text = currentField
                            End If
                            If n = 1 Then
                                txt_precio.Text = currentField
                            End If
                            If n = 2 Then
                                txt_pcoste.Text = currentField
                            End If                            
                            n = n + 1
                        Next
                        Me.CalculoPvd()
                        conect.updateTarifaProductoCsv()

                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If
    End Sub

    Private Sub CalculoPvd()

        Dim porcentaje As Decimal
        Dim resultado As Decimal

        With Me

            If CDec(.txt_precio.Text) <> 0 And CDec(Me.txt_pcoste.Text) <> 0 Then

                porcentaje = (CDec(.txt_precio.Text) - CDec(.txt_pcoste.Text)) * 100 / CDec(.txt_precio.Text)

                If (porcentaje >= 33.33) And (porcentaje < 37.5) Then
                    resultado = 5
                End If

                If (porcentaje >= 37.4) And (porcentaje < 41.18) Then
                    resultado = 10
                End If

                If (porcentaje >= 41.18) And (porcentaje < 44.44) Then
                    resultado = 15
                End If

                If (porcentaje >= 44.44) And (porcentaje < 50) Then
                    resultado = 20
                End If

                If (porcentaje >= 50) And (porcentaje < 60) Then
                    resultado = 25
                End If

                If (porcentaje >= 60) And (porcentaje < 66.67) Then
                    resultado = 30
                End If

                If (porcentaje >= 66.67) And (porcentaje <= 71.43) Then
                    resultado = 35
                End If

                If porcentaje > 71.43 Then
                    resultado = 40
                End If

                .txt_pdistribuidor.Text = CStr(CDec(.txt_precio.Text) - ((CDec(.txt_precio.Text) * resultado)) / 100)
            End If
         End With

    End Sub

    Private Sub btn_Examinar_Click(sender As Object, e As EventArgs) Handles btn_Examinar.Click
        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            txtFile.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim ruta As String = Me.txtFile.Text
        If File.Exists(ruta) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim email As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                txt_Email.Text = currentField
                            End If
                            n = 0
                        Next

                        conect.updateGrupoClienteCsv()

                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If

    End Sub

    Private Sub btn_Traducir_Click(sender As Object, e As EventArgs) Handles btn_Traducir.Click
        'Dim gTranslator As New TranslateClient("AIzaSyA3wV4rphhHftZW7XydTMmuxudadFIKy3Q")
        'txtTO.Text = gTranslator.Translate(txtFrom.Text, "es", "en")
        'Try
        'Me.txtTO.Text = gTranslator.Translate(Me.txtFrom.Text, Language.Spanish, Language.English)
        'Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
        'MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
        'End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        'El txt abrirlo con el Notepad++ y codificarlo con UTF-8 sin BOM
        Dim ruta As String = "C:\TestFolder\valor_ingles_2.txt"
        If File.Exists(ruta) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim referencia As String = ""
                        Dim ingles As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                referencia = currentField
                            End If
                            If n = 1 Then
                                ingles = currentField
                            End If
                            n = n + 1
                        Next
                        conect.updateValorInglesCsv(referencia, ingles)
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        'El txt abrirlo con el Notepad++ y codificarlo con UTF-8 sin BOM
        Dim ruta As String = "C:\TestFolder\borrar_productos.txt"
        If File.Exists(ruta) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim referencia As String = ""
                        Dim tipo As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                referencia = currentField
                            End If
                            If n = 1 Then
                                tipo = currentField
                            End If
                            n = n + 1
                        Next
                        conect.updateBorrarProductoCsv(referencia, tipo)
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        'El txt abrirlo con el Notepad++ y codificarlo con UTF-8 sin BOM
        Dim ruta As String = "C:\TestFolder\precios_distribuidor.txt"
        If File.Exists(ruta) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim referencia As String = ""
                        Dim precio As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                referencia = currentField
                            End If
                            If n = 1 Then
                                precio = currentField
                            End If
                            n = n + 1
                        Next
                        conect.updatePreciosDistribuidorCsv(referencia, precio)
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        'Dim ruta As String = "C:\TestFolder\tarifas_producto_csv.txt"
        Dim ruta As String = Me.txtFile.Text
        If File.Exists(ruta) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim referencia As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                referencia = currentField
                            End If
                        Next
                        conect.updateEansVacioCsv(referencia)

                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If

    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click

        'result.Text = trans.Translate({"Este texto quiero traducirlo a inglés", ""})
        Dim origen As String = txtOrigen.Text
        result.Text = trans.Translate(origen)

    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        'El txt abrirlo con el Notepad++ y codificarlo con UTF-8 sin BOM
        Dim ruta As String = "C:\TestFolder\precios.txt"
        If File.Exists(ruta) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim referencia As String = ""
                        Dim precio As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                referencia = currentField
                            End If
                            If n = 1 Then
                                precio = currentField
                            End If
                            n = n + 1
                        Next
                        conect.updatePreciosCsv(referencia, precio)
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If

    End Sub

    Private Sub btn_Idioma_Click(sender As Object, e As EventArgs) Handles btn_Idioma.Click
        conect.importarIdioma(Me.txtIdioma.Text)
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        'El txt abrirlo con el Notepad++ y codificarlo con UTF-8 sin BOM
        Dim ruta As String = "C:\TestFolder\Urls\urls_productos_" & Me.txtIdioma.Text & ".txt"
        If File.Exists(ruta) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim sku As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                sku = currentField
                            End If
                        Next
                        conect.viewUrl(sku)
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If

    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        'El txt abrirlo con el Notepad++ y codificarlo con UTF-8 sin BOM
        'Dim ruta As String = "C:\TestFolder\nombres_categoria_cz.txt"
        Dim ruta As String = "C:\TestFolder\nombre_producto_pl_csv.txt"
        If File.Exists(ruta) Then
            Using MyReader As New Microsoft.VisualBasic.
                                FileIO.TextFieldParser(ruta)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim referencia As String = ""
                        Dim nombre As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                referencia = currentField
                            End If
                            If n = 1 Then
                                nombre = currentField
                            End If
                            n = n + 1
                        Next
                        conect.updateChecoCsv(referencia, nombre)
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        Dim valor As Double = OptionsChecked()
        MsgBox(valor)

        Dim binario As String = DecToBin(valor)
        MsgBox(binario)
    End Sub

    Public Function DecToBin(ByVal DecNum As Double) As String
        Dim BinStr As String
        BinStr = ""
        Do While DecNum <> 0
            If (DecNum Mod 2) = 1 Then
                BinStr = "1" & BinStr
            Else
                BinStr = "0" & BinStr
            End If
            DecNum = CLng(DecNum) \ 2
        Loop
        'If BinStr = "" Then BinStr = "000000"
        DecToBin = BinStr
        'Return StrReverse(DecToBin)        
    End Function

    Private Function OptionsChecked() As Double
        Dim i As Double = 0
        For Each chk As CheckBox In GroupBox1.Controls
            If chk.Name = "CheckBox1" Then
                If chk.Checked = True Then
                    i = i + 2 ^ 0
                End If
            End If
            If chk.Name = "CheckBox2" Then
                If chk.Checked = True Then
                    i = i + 2 ^ 1
                End If
            End If
            If chk.Name = "CheckBox3" Then
                If chk.Checked = True Then
                    i = i + 2 ^ 2
                End If
            End If
            If chk.Name = "CheckBox4" Then
                If chk.Checked = True Then
                    i = i + 2 ^ 3
                End If
            End If
            If chk.Name = "CheckBox5" Then
                If chk.Checked = True Then
                    i = i + 2 ^ 4
                End If
            End If
            If chk.Name = "CheckBox6" Then
                If chk.Checked = True Then
                    i = i + 2 ^ 5
                End If
            End If
        Next
        Return i

    End Function

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click
        conect.updateValorCsv(txtIdiomaValor.Text)
    End Sub

    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click
        'El txt abrirlo con el Notepad++ y codificarlo con UTF-8 sin BOM
        Dim ruta As String = "C:\TestFolder\desglose\listado_optporfolio_19.txt"  'Del 1 al 19
        If File.Exists(ruta) Then
            Using MyReader As New Microsoft.VisualBasic.
                                FileIO.TextFieldParser(ruta)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim referencia As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                referencia = currentField
                            End If
                        Next
                        conect.borrarProductos(referencia)
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If
    End Sub
End Class