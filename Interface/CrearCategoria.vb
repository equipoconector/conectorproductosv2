﻿Public Class CrearCategoria

    Public navconectar As CONECTOR_NAVISION = New CONECTOR_NAVISION
    Public conect As web = New web
    Public sqlconexion As SQLCONECTAR = New SQLCONECTAR

    Private Sub btn_Cerrar_Click(sender As Object, e As EventArgs) Handles btn_Cerrar.Click
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub btn_Crear_Click(sender As Object, e As EventArgs) Handles btn_Crear.Click
        If Me.txt_Categoria_Padre.Text <> "" And Me.txtNombre.Text <> "" Then
            conect.nuevaCategoria()
        Else
            MsgBox("Faltan datos para crear la Categoría")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Arbol_Categorias_CNueva.Tree.Enabled = True
        Arbol_Categorias_CNueva.Dispose()
        Arbol_Categorias_CNueva.ShowDialog()
    End Sub

    Private Sub txt_Categoria_Padre_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_Categoria_Padre.Leave
        If Me.txt_Categoria_Padre.Text <> "" Then
            conect.consultarNivelCategoria(Me.txt_Categoria_Padre.Text)
        End If
    End Sub

End Class