﻿Imports MySql.Data.MySqlClient
Imports [Interface].PRODUCTOS
Imports [Interface].MagentoService
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.IO
Imports System.Net
Imports System.Web
Imports System.Text
Imports System
Imports System.Xml
Imports System.Threading
Imports MSXML2

Public Class busqueda
    Public cadena As String
    Public conectar As SqlConnection

    'Public sqlconecta As SQLCONECTAR = New SQLCONECTAR
    Dim conectmysql As New MySqlConnection("server = 192.168.2.28; user id= Navision;password = excelia=2009;database = qapp_production; port = 3306")

    Public Function ConvertUTF8toUnicode(cadena2 As String) As String
        'Convertir cadena UTF8 en Unicode para los select
        Dim UTF8String As String = CStr(cadena2)
        Dim dstEncoding As Encoding = Encoding.Unicode
        Dim dstEncodingANSI As Encoding = Encoding.GetEncoding(1252)
        Dim srcEncoding As Encoding = Encoding.UTF8
        Dim srcBytes As Byte() = dstEncodingANSI.GetBytes(UTF8String)
        Dim dstBytes As Byte() = Encoding.Convert(srcEncoding, dstEncoding, srcBytes)
        Dim salida As String = dstEncoding.GetString(dstBytes)
        Return salida
    End Function

    Public Sub sqlconectar()
        Try
            cadena = "Data Source = PROLIANT2008\NAVISION;Initial Catalog = QUIRUMED;user id = conector;password = quirumed2015"
            conectar = New SqlConnection
            conectar.ConnectionString = cadena
            conectar.Open()
        Catch ex As Exception
            conectar.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    '*************************************************************************   BUSQUEDA PRODUCTOS SIMPLES ******************************************************************************************
    Public Sub busqueda_simples()
        BUSCADOR_PRODUCTOS_SIMPLES.Show()

        Dim sqlconecta As SQLCONECTAR = New SQLCONECTAR
        sqlconecta.sqlconectar()

        Dim sqlbuscar As New SqlClient.SqlCommand

        sqlbuscar.CommandType = System.Data.CommandType.Text
        sqlbuscar.CommandText = "select  QUIRUMED.dbo.Item.No_,QUIRUMED.dbo.Item.Description from QUIRUMED.dbo.Item where QUIRUMED.dbo.Item.Description like upper ('%" & ENLACE.Descripcion.Text & "%') or QUIRUMED.dbo.Item.Description like '%" & ENLACE.Descripcion.Text & "%'"
        sqlbuscar.Connection = sqlconecta.conectar
        Dim cmd As SqlCommand = New SqlCommand(sqlbuscar.CommandText, sqlconecta.conectar)
        Dim lector As SqlDataReader = cmd.ExecuteReader

        With BUSCADOR_PRODUCTOS_SIMPLES.listaproductossimples
            .Clear()
            .View = View.Details
            .Columns.Add("Referencia", 100)
            .Columns.Add("Descripcion", 250)
        End With

        While lector.Read <> False
            With BUSCADOR_PRODUCTOS_SIMPLES.listaproductossimples.Items.Add(lector("No_").ToString)
                .SubItems.Add(lector("Description").ToString)
            End With
        End While
    End Sub

    Public Sub busqueda_simple_referencia()
        BUSCADOR_PRODUCTOS_SIMPLES.Show()

        Dim sqlconecta As SQLCONECTAR = New SQLCONECTAR
        sqlconecta.sqlconectar()

        Dim sqlbuscar As New SqlClient.SqlCommand

        sqlbuscar.CommandType = System.Data.CommandType.Text
        sqlbuscar.CommandText = "select  QUIRUMED.dbo.Item.No_,QUIRUMED.dbo.Item.Description from QUIRUMED.dbo.Item where QUIRUMED.dbo.Item.No_ like upper ('%" & ENLACE.Referencia.Text & "%') or QUIRUMED.dbo.Item.No_ like '%" & ENLACE.Referencia.Text & "%'"
        sqlbuscar.Connection = sqlconecta.conectar
        Dim cmd As SqlCommand = New SqlCommand(sqlbuscar.CommandText, sqlconecta.conectar)
        Dim lector As SqlDataReader = cmd.ExecuteReader

        With BUSCADOR_PRODUCTOS_SIMPLES.listaproductossimples
            .Clear()
            .View = View.Details
            .Columns.Add("Referencia", 100)
            .Columns.Add("Descripcion", 250)
        End With

        While lector.Read <> False
            With BUSCADOR_PRODUCTOS_SIMPLES.listaproductossimples.Items.Add(lector("No_").ToString)
                .SubItems.Add(lector("Description").ToString)
            End With
        End While
    End Sub

    '**************************************************************************** FIN BUSQUEDA PRODUCTOS SIMPLES **********************************************************************************************************

   

    '**************************************************************************** BUSQUEDA PRODUCTOS CONFIGURABLES ********************************************************************************************************

    Public Sub busqueda_configurable_nombre()
        BUSCADOR_PRODUCTOS_CONFIGURABLES.Show()

        Dim sqlconecta As SQLCONECTAR = New SQLCONECTAR
        sqlconecta.sqlconectar()

        Dim sqlbuscar As New SqlClient.SqlCommand

        sqlbuscar.CommandType = System.Data.CommandType.Text
        sqlbuscar.CommandText = "select  QUIRUMED.dbo.[Modelos Articulos].No_, QUIRUMED.dbo.[Modelos Articulos].Nombre from  QUIRUMED.dbo.[Modelos Articulos]" &
        "where  QUIRUMED.dbo.[Modelos Articulos].Idioma = 'ES' AND QUIRUMED.dbo.[Modelos Articulos].Nombre like upper ('%" & Modelo.nombremodelo.Text & "%') or QUIRUMED.dbo.[Modelos Articulos].Nombre like '%" & Modelo.nombremodelo.Text & "%'"
        sqlbuscar.Connection = sqlconecta.conectar
        Dim cmd As SqlCommand = New SqlCommand(sqlbuscar.CommandText, sqlconecta.conectar)
        Dim lector As SqlDataReader = cmd.ExecuteReader

        With BUSCADOR_PRODUCTOS_CONFIGURABLES.listabusquedaconfi
            .Clear()
            .View = View.Details
            .Columns.Add("Referencia", 100)
            .Columns.Add("Descripcion", 250)
        End With

        While lector.Read <> False
            With BUSCADOR_PRODUCTOS_CONFIGURABLES.listabusquedaconfi.Items.Add(lector("No_").ToString)
                .SubItems.Add(lector("Nombre").ToString)
            End With
        End While
    End Sub

    Public Sub busqueda_configurable_referencia()
        BUSCADOR_PRODUCTOS_CONFIGURABLES.Show()

        Dim sqlconecta As SQLCONECTAR = New SQLCONECTAR
        sqlconecta.sqlconectar()

        Dim sqlbuscar As New SqlClient.SqlCommand

        sqlbuscar.CommandType = System.Data.CommandType.Text
        sqlbuscar.CommandText = "select  QUIRUMED.dbo.[Modelos Articulos].No_, QUIRUMED.dbo.[Modelos Articulos].Nombre from  QUIRUMED.dbo.[Modelos Articulos] where  QUIRUMED.dbo.[Modelos Articulos].Idioma = 'ES' AND  QUIRUMED.dbo.[Modelos Articulos].No_ like '%" & Modelo.idmodelo.Text & "%'order by QUIRUMED.dbo.[Modelos Articulos].No_"
        sqlbuscar.Connection = sqlconecta.conectar
        Dim cmd As SqlCommand = New SqlCommand(sqlbuscar.CommandText, sqlconecta.conectar)
        Dim lector As SqlDataReader = cmd.ExecuteReader

        With BUSCADOR_PRODUCTOS_CONFIGURABLES.listabusquedaconfi
            .Clear()
            .View = View.Details
            .Columns.Add("Referencia", 100)
            .Columns.Add("Descripcion", 250)
        End With

        While lector.Read <> False
            With BUSCADOR_PRODUCTOS_CONFIGURABLES.listabusquedaconfi.Items.Add(lector("No_").ToString)
                .SubItems.Add(lector("Nombre").ToString)
            End With
        End While
    End Sub


    '**************************************************************************** FIN BUSQUEDA PRODUCTOS CONFIGURABLES ****************************************************************************************************

    '**************************************************************************** BUSQUEDA CATEGORIAS *********************************************************************************************************************
    Public Sub categorias_referencia_simples()
        BUSCADOR_CATEGORIAS_SIMPLES.Show()

        Dim sqlconecta As SQLCONECTAR = New SQLCONECTAR
        sqlconecta.sqlconectar()

        Dim sqlbuscar As New SqlClient.SqlCommand

        sqlbuscar.CommandType = System.Data.CommandType.Text
        sqlbuscar.CommandText = "select  QUIRUMED.dbo.[Modelos Articulos].No_, QUIRUMED.dbo.[Modelos Articulos].Nombre from  QUIRUMED.dbo.[Modelos Articulos] where  QUIRUMED.dbo.[Modelos Articulos].Idioma = 'ES' AND  QUIRUMED.dbo.[Modelos Articulos].No_ like '%" & CATEGORIAS_SIMPLES.ID.Text & "%'order by QUIRUMED.dbo.[Modelos Articulos].No_"
        sqlbuscar.Connection = sqlconecta.conectar
        Dim cmd As SqlCommand = New SqlCommand(sqlbuscar.CommandText, sqlconecta.conectar)
        Dim lector As SqlDataReader = cmd.ExecuteReader

        With BUSCADOR_CATEGORIAS_SIMPLES.listacatsimples
            .Clear()
            .View = View.Details
            .Columns.Add("Referencia", 100)
            .Columns.Add(ConvertUTF8toUnicode("Descripcion"), 250)
        End With

        While lector.Read <> False
            With BUSCADOR_CATEGORIAS_SIMPLES.listacatsimples.Items.Add(lector("No_").ToString)
                .SubItems.Add(ConvertUTF8toUnicode(CStr(lector("Nombre").ToString)))
            End With
        End While
    End Sub

    Public Sub categorias_modificar_referencia()
        BUSCADOR_CATEGORIAS.Show()

        conectmysql.Open()
        Dim consultacat As New MySqlCommand("select id,nombre,nivel,parent_id,id_magento,parent_id_magento from categorias where nivel > 0 and id like '%" & FCategorias.busqcat.Text & "%'order by id", conectmysql)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = consultacat.ExecuteReader()

        With BUSCADOR_CATEGORIAS.listacategorias
            .Clear()
            .View = View.Details
            .Columns.Add("Referencia", 100)
            .Columns.Add(ConvertUTF8toUnicode("Descripcion"), 250)
        End With

        While lector.Read <> False
            With BUSCADOR_CATEGORIAS.listacategorias.Items.Add(lector("id").ToString)
                .SubItems.Add(lector("nombre").ToString)
            End With
        End While
        conectmysql.Close()
    End Sub

    Public Sub categorias_modificar_nombre()
        BUSCADOR_CATEGORIAS.Show()

        conectmysql.Open()
        Dim consultacat As New MySqlCommand("select id,nombre,nivel,parent_id,id_magento,parent_id_magento from categorias where nivel > 0 and nombre like '%" & FCategorias.nombrecat.Text & "%'order by id", conectmysql)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = consultacat.ExecuteReader()

        With BUSCADOR_CATEGORIAS.listacategorias
            .Clear()
            .View = View.Details
            .Columns.Add("Referencia", 100)
            .Columns.Add(ConvertUTF8toUnicode("Descripcion"), 250)
        End With

        While lector.Read <> False
            With BUSCADOR_CATEGORIAS.listacategorias.Items.Add(lector("id").ToString)
                .SubItems.Add(ConvertUTF8toUnicode(lector("nombre").ToString))
            End With
        End While
        conectmysql.Close()
    End Sub

    '**************************************************************************** FIN BUSQUEDA CATEGORIAS ******************************************************************************************************************
End Class
