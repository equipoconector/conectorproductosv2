﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FCategorias
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.nuevacat = New System.Windows.Forms.Button()
        Me.modcat = New System.Windows.Forms.Button()
        Me.busqcat = New System.Windows.Forms.TextBox()
        Me.buscarcat = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.categoriaid = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nombrecat = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Catpadre = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.nivelcat = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Idmagento = New System.Windows.Forms.TextBox()
        Me.cerrar = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.enviar_modelo = New System.Windows.Forms.Button()
        Me.guardar_imagen = New System.Windows.Forms.Button()
        Me.abre_imagen = New System.Windows.Forms.Button()
        Me.gestionimg = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Metatitle = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.metakeywords = New System.Windows.Forms.TextBox()
        Me.metadescription = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.descripcion = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.catpadremag = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'nuevacat
        '
        Me.nuevacat.Location = New System.Drawing.Point(247, 91)
        Me.nuevacat.Name = "nuevacat"
        Me.nuevacat.Size = New System.Drawing.Size(75, 23)
        Me.nuevacat.TabIndex = 0
        Me.nuevacat.Text = "Nueva"
        Me.nuevacat.UseVisualStyleBackColor = True
        '
        'modcat
        '
        Me.modcat.Location = New System.Drawing.Point(341, 91)
        Me.modcat.Name = "modcat"
        Me.modcat.Size = New System.Drawing.Size(75, 23)
        Me.modcat.TabIndex = 1
        Me.modcat.Text = "Modificar"
        Me.modcat.UseVisualStyleBackColor = True
        '
        'busqcat
        '
        Me.busqcat.Location = New System.Drawing.Point(26, 51)
        Me.busqcat.Name = "busqcat"
        Me.busqcat.Size = New System.Drawing.Size(130, 20)
        Me.busqcat.TabIndex = 2
        '
        'buscarcat
        '
        Me.buscarcat.Location = New System.Drawing.Point(169, 48)
        Me.buscarcat.Name = "buscarcat"
        Me.buscarcat.Size = New System.Drawing.Size(75, 23)
        Me.buscarcat.TabIndex = 3
        Me.buscarcat.Text = "Buscar"
        Me.buscarcat.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 101)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "ID"
        '
        'categoriaid
        '
        Me.categoriaid.Enabled = False
        Me.categoriaid.Location = New System.Drawing.Point(112, 94)
        Me.categoriaid.Name = "categoriaid"
        Me.categoriaid.Size = New System.Drawing.Size(118, 20)
        Me.categoriaid.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 127)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Nombre"
        '
        'nombrecat
        '
        Me.nombrecat.Location = New System.Drawing.Point(112, 120)
        Me.nombrecat.Name = "nombrecat"
        Me.nombrecat.Size = New System.Drawing.Size(389, 20)
        Me.nombrecat.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 153)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Categoria Padre"
        '
        'Catpadre
        '
        Me.Catpadre.Location = New System.Drawing.Point(112, 146)
        Me.Catpadre.Name = "Catpadre"
        Me.Catpadre.Size = New System.Drawing.Size(100, 20)
        Me.Catpadre.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 177)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(79, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Nivel Categoria"
        '
        'nivelcat
        '
        Me.nivelcat.Location = New System.Drawing.Point(112, 172)
        Me.nivelcat.Name = "nivelcat"
        Me.nivelcat.Size = New System.Drawing.Size(48, 20)
        Me.nivelcat.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(166, 177)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "(De 1 a 5)"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(23, 200)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "ID Magento"
        '
        'Idmagento
        '
        Me.Idmagento.Enabled = False
        Me.Idmagento.Location = New System.Drawing.Point(112, 197)
        Me.Idmagento.Name = "Idmagento"
        Me.Idmagento.Size = New System.Drawing.Size(118, 20)
        Me.Idmagento.TabIndex = 14
        '
        'cerrar
        '
        Me.cerrar.BackColor = System.Drawing.Color.Coral
        Me.cerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cerrar.Location = New System.Drawing.Point(1085, 37)
        Me.cerrar.Name = "cerrar"
        Me.cerrar.Size = New System.Drawing.Size(94, 99)
        Me.cerrar.TabIndex = 15
        Me.cerrar.Text = "Cerrar"
        Me.cerrar.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(1104, 268)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(60, 79)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 16
        Me.PictureBox1.TabStop = False
        '
        'enviar_modelo
        '
        Me.enviar_modelo.Location = New System.Drawing.Point(1104, 550)
        Me.enviar_modelo.Name = "enviar_modelo"
        Me.enviar_modelo.Size = New System.Drawing.Size(68, 23)
        Me.enviar_modelo.TabIndex = 69
        Me.enviar_modelo.Text = "ENVIAR"
        Me.enviar_modelo.UseVisualStyleBackColor = True
        Me.enviar_modelo.Visible = False
        '
        'guardar_imagen
        '
        Me.guardar_imagen.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guardar_imagen.Location = New System.Drawing.Point(1085, 484)
        Me.guardar_imagen.Name = "guardar_imagen"
        Me.guardar_imagen.Size = New System.Drawing.Size(97, 54)
        Me.guardar_imagen.TabIndex = 68
        Me.guardar_imagen.Text = "Guardar Imagen"
        Me.guardar_imagen.UseVisualStyleBackColor = True
        Me.guardar_imagen.Visible = False
        '
        'abre_imagen
        '
        Me.abre_imagen.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.abre_imagen.Location = New System.Drawing.Point(1085, 400)
        Me.abre_imagen.Name = "abre_imagen"
        Me.abre_imagen.Size = New System.Drawing.Size(97, 67)
        Me.abre_imagen.TabIndex = 67
        Me.abre_imagen.Text = "Seleccionar Imagen"
        Me.abre_imagen.UseVisualStyleBackColor = True
        Me.abre_imagen.Visible = False
        '
        'gestionimg
        '
        Me.gestionimg.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gestionimg.Location = New System.Drawing.Point(290, 156)
        Me.gestionimg.Name = "gestionimg"
        Me.gestionimg.Size = New System.Drawing.Size(143, 78)
        Me.gestionimg.TabIndex = 70
        Me.gestionimg.Text = "Imágenes"
        Me.gestionimg.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(440, 91)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 71
        Me.Button1.Text = "Limpiar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Metatitle
        '
        Me.Metatitle.Location = New System.Drawing.Point(29, 400)
        Me.Metatitle.Multiline = True
        Me.Metatitle.Name = "Metatitle"
        Me.Metatitle.Size = New System.Drawing.Size(336, 138)
        Me.Metatitle.TabIndex = 72
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(26, 384)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(60, 13)
        Me.Label7.TabIndex = 73
        Me.Label7.Text = "Meta Titulo"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(386, 384)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 13)
        Me.Label8.TabIndex = 74
        Me.Label8.Text = "Meta Keywords"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(727, 384)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(90, 13)
        Me.Label9.TabIndex = 75
        Me.Label9.Text = "Meta Descripcion"
        '
        'metakeywords
        '
        Me.metakeywords.Location = New System.Drawing.Point(379, 400)
        Me.metakeywords.Multiline = True
        Me.metakeywords.Name = "metakeywords"
        Me.metakeywords.Size = New System.Drawing.Size(336, 138)
        Me.metakeywords.TabIndex = 76
        '
        'metadescription
        '
        Me.metadescription.Location = New System.Drawing.Point(730, 400)
        Me.metadescription.Multiline = True
        Me.metadescription.Name = "metadescription"
        Me.metadescription.Size = New System.Drawing.Size(336, 138)
        Me.metadescription.TabIndex = 77
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(244, 384)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(101, 13)
        Me.Label10.TabIndex = 78
        Me.Label10.Text = "Max 250 caracteres"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(600, 384)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(101, 13)
        Me.Label11.TabIndex = 79
        Me.Label11.Text = "Max 250 caracteres"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(965, 384)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(101, 13)
        Me.Label12.TabIndex = 80
        Me.Label12.Text = "Max 250 caracteres"
        '
        'descripcion
        '
        Me.descripcion.Location = New System.Drawing.Point(503, 150)
        Me.descripcion.Multiline = True
        Me.descripcion.Name = "descripcion"
        Me.descripcion.Size = New System.Drawing.Size(563, 216)
        Me.descripcion.TabIndex = 81
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(754, 123)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(63, 13)
        Me.Label13.TabIndex = 82
        Me.Label13.Text = "Descripcion"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(23, 234)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(107, 13)
        Me.Label14.TabIndex = 83
        Me.Label14.Text = "Categoria Padre Mag"
        '
        'catpadremag
        '
        Me.catpadremag.Enabled = False
        Me.catpadremag.Location = New System.Drawing.Point(138, 231)
        Me.catpadremag.Name = "catpadremag"
        Me.catpadremag.Size = New System.Drawing.Size(82, 20)
        Me.catpadremag.TabIndex = 84
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(25, 9)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(142, 24)
        Me.Label15.TabIndex = 86
        Me.Label15.Text = "CATEGORÍAS"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Enabled = False
        Me.Label16.Location = New System.Drawing.Point(878, 13)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(0, 13)
        Me.Label16.TabIndex = 87
        Me.Label16.Visible = False
        '
        'FCategorias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1209, 599)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.catpadremag)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.descripcion)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.metadescription)
        Me.Controls.Add(Me.metakeywords)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Metatitle)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.gestionimg)
        Me.Controls.Add(Me.enviar_modelo)
        Me.Controls.Add(Me.guardar_imagen)
        Me.Controls.Add(Me.abre_imagen)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cerrar)
        Me.Controls.Add(Me.Idmagento)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.nivelcat)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Catpadre)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.nombrecat)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.categoriaid)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.buscarcat)
        Me.Controls.Add(Me.busqcat)
        Me.Controls.Add(Me.modcat)
        Me.Controls.Add(Me.nuevacat)
        Me.Name = "FCategorias"
        Me.Text = "Categorias"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents nuevacat As System.Windows.Forms.Button
    Friend WithEvents modcat As System.Windows.Forms.Button
    Friend WithEvents busqcat As System.Windows.Forms.TextBox
    Friend WithEvents buscarcat As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents categoriaid As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents nombrecat As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Catpadre As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents nivelcat As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Idmagento As System.Windows.Forms.TextBox
    Friend WithEvents cerrar As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents enviar_modelo As System.Windows.Forms.Button
    Friend WithEvents guardar_imagen As System.Windows.Forms.Button
    Friend WithEvents abre_imagen As System.Windows.Forms.Button
    Friend WithEvents gestionimg As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Metatitle As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents metakeywords As System.Windows.Forms.TextBox
    Friend WithEvents metadescription As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents descripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents catpadremag As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As Label
End Class
