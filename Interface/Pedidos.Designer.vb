﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Pedidos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtNumPedido = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_Buscar = New System.Windows.Forms.Button()
        Me.txtFecha = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_Cerrar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtClienteID = New System.Windows.Forms.TextBox()
        Me.btn_Cliente = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtVendedor = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtIdioma = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtFormaPago = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtZonaEnvio = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtEstadoPedido = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtComentarioFacturacion = New System.Windows.Forms.TextBox()
        Me.txtComentarioInterno = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtDatosPago = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtFacturacionNombre = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtFacturacionApellidos = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtFacturacionNif = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtFacturacionDireccion = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtFacturacionPais = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtFacturacionProvincia = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtFacturacionPostal = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtFacturacionPoblacion = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtFacturacionFax = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtFacturacionTelefono2 = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtFacturacionTelefono = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtEnvioPoblacion = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtEnvioDireccion = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtEnvioApellidos = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtEnvioNombre = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtEnvioTelefono = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtEnvioPais = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtEnvioProvincia = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtEnvioPostal = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.DataGridProductos = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtImporteTotal = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtImporteArticulos = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtIvaTotal = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.txtPortes = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtDtoArticulos = New System.Windows.Forms.TextBox()
        Me.DataGridEventos = New System.Windows.Forms.DataGridView()
        Me.txtImporteTotalMoneda = New System.Windows.Forms.TextBox()
        Me.lbl_moneda = New System.Windows.Forms.Label()
        CType(Me.DataGridProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridEventos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtNumPedido
        '
        Me.txtNumPedido.Location = New System.Drawing.Point(121, 27)
        Me.txtNumPedido.Name = "txtNumPedido"
        Me.txtNumPedido.Size = New System.Drawing.Size(100, 20)
        Me.txtNumPedido.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(35, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Número Pedido"
        '
        'btn_Buscar
        '
        Me.btn_Buscar.Location = New System.Drawing.Point(233, 25)
        Me.btn_Buscar.Name = "btn_Buscar"
        Me.btn_Buscar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Buscar.TabIndex = 2
        Me.btn_Buscar.Text = "Buscar"
        Me.btn_Buscar.UseVisualStyleBackColor = True
        '
        'txtFecha
        '
        Me.txtFecha.Location = New System.Drawing.Point(424, 27)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Size = New System.Drawing.Size(160, 20)
        Me.txtFecha.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(358, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Fecha"
        '
        'btn_Cerrar
        '
        Me.btn_Cerrar.Location = New System.Drawing.Point(1245, 895)
        Me.btn_Cerrar.Name = "btn_Cerrar"
        Me.btn_Cerrar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cerrar.TabIndex = 5
        Me.btn_Cerrar.Text = "Cerrar"
        Me.btn_Cerrar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(39, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "ID Cliente"
        '
        'txtClienteID
        '
        Me.txtClienteID.Location = New System.Drawing.Point(97, 74)
        Me.txtClienteID.Name = "txtClienteID"
        Me.txtClienteID.Size = New System.Drawing.Size(158, 20)
        Me.txtClienteID.TabIndex = 6
        '
        'btn_Cliente
        '
        Me.btn_Cliente.Location = New System.Drawing.Point(274, 72)
        Me.btn_Cliente.Name = "btn_Cliente"
        Me.btn_Cliente.Size = New System.Drawing.Size(143, 23)
        Me.btn_Cliente.TabIndex = 8
        Me.btn_Cliente.Text = "Ver Ficha del Cliente"
        Me.btn_Cliente.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(650, 31)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Vendedor"
        '
        'txtVendedor
        '
        Me.txtVendedor.Location = New System.Drawing.Point(716, 28)
        Me.txtVendedor.Name = "txtVendedor"
        Me.txtVendedor.Size = New System.Drawing.Size(91, 20)
        Me.txtVendedor.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(851, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(38, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Idioma"
        '
        'txtIdioma
        '
        Me.txtIdioma.Location = New System.Drawing.Point(900, 29)
        Me.txtIdioma.Name = "txtIdioma"
        Me.txtIdioma.Size = New System.Drawing.Size(54, 20)
        Me.txtIdioma.TabIndex = 11
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(39, 147)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(70, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Tipo de pago"
        '
        'txtFormaPago
        '
        Me.txtFormaPago.Location = New System.Drawing.Point(140, 145)
        Me.txtFormaPago.Name = "txtFormaPago"
        Me.txtFormaPago.Size = New System.Drawing.Size(200, 20)
        Me.txtFormaPago.TabIndex = 15
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(488, 114)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(79, 13)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Zona de Envío"
        '
        'txtZonaEnvio
        '
        Me.txtZonaEnvio.Location = New System.Drawing.Point(580, 110)
        Me.txtZonaEnvio.Name = "txtZonaEnvio"
        Me.txtZonaEnvio.Size = New System.Drawing.Size(227, 20)
        Me.txtZonaEnvio.TabIndex = 17
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(474, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(93, 13)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Estado del Pedido"
        '
        'txtEstadoPedido
        '
        Me.txtEstadoPedido.Location = New System.Drawing.Point(580, 75)
        Me.txtEstadoPedido.Name = "txtEstadoPedido"
        Me.txtEstadoPedido.Size = New System.Drawing.Size(227, 20)
        Me.txtEstadoPedido.TabIndex = 19
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(36, 180)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(119, 13)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Comentario Facturación"
        '
        'txtComentarioFacturacion
        '
        Me.txtComentarioFacturacion.Location = New System.Drawing.Point(164, 174)
        Me.txtComentarioFacturacion.Multiline = True
        Me.txtComentarioFacturacion.Name = "txtComentarioFacturacion"
        Me.txtComentarioFacturacion.Size = New System.Drawing.Size(486, 67)
        Me.txtComentarioFacturacion.TabIndex = 22
        '
        'txtComentarioInterno
        '
        Me.txtComentarioInterno.Location = New System.Drawing.Point(806, 180)
        Me.txtComentarioInterno.Multiline = True
        Me.txtComentarioInterno.Name = "txtComentarioInterno"
        Me.txtComentarioInterno.Size = New System.Drawing.Size(488, 61)
        Me.txtComentarioInterno.TabIndex = 24
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(676, 183)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(111, 13)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Comentario CallCenter"
        '
        'txtDatosPago
        '
        Me.txtDatosPago.Location = New System.Drawing.Point(346, 144)
        Me.txtDatosPago.Name = "txtDatosPago"
        Me.txtDatosPago.Size = New System.Drawing.Size(948, 20)
        Me.txtDatosPago.TabIndex = 27
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(39, 278)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(44, 13)
        Me.Label13.TabIndex = 30
        Me.Label13.Text = "Nombre"
        '
        'txtFacturacionNombre
        '
        Me.txtFacturacionNombre.Location = New System.Drawing.Point(145, 274)
        Me.txtFacturacionNombre.Name = "txtFacturacionNombre"
        Me.txtFacturacionNombre.Size = New System.Drawing.Size(467, 20)
        Me.txtFacturacionNombre.TabIndex = 29
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(40, 302)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(49, 13)
        Me.Label14.TabIndex = 32
        Me.Label14.Text = "Apellidos"
        '
        'txtFacturacionApellidos
        '
        Me.txtFacturacionApellidos.Location = New System.Drawing.Point(146, 300)
        Me.txtFacturacionApellidos.Name = "txtFacturacionApellidos"
        Me.txtFacturacionApellidos.Size = New System.Drawing.Size(466, 20)
        Me.txtFacturacionApellidos.TabIndex = 31
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(39, 331)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(45, 13)
        Me.Label15.TabIndex = 34
        Me.Label15.Text = "NIF/CIF"
        '
        'txtFacturacionNif
        '
        Me.txtFacturacionNif.Location = New System.Drawing.Point(145, 326)
        Me.txtFacturacionNif.Name = "txtFacturacionNif"
        Me.txtFacturacionNif.Size = New System.Drawing.Size(227, 20)
        Me.txtFacturacionNif.TabIndex = 33
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(39, 356)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(52, 13)
        Me.Label16.TabIndex = 36
        Me.Label16.Text = "Dirección"
        '
        'txtFacturacionDireccion
        '
        Me.txtFacturacionDireccion.Location = New System.Drawing.Point(145, 352)
        Me.txtFacturacionDireccion.Name = "txtFacturacionDireccion"
        Me.txtFacturacionDireccion.Size = New System.Drawing.Size(402, 20)
        Me.txtFacturacionDireccion.TabIndex = 35
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(39, 456)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(29, 13)
        Me.Label17.TabIndex = 44
        Me.Label17.Text = "País"
        '
        'txtFacturacionPais
        '
        Me.txtFacturacionPais.Location = New System.Drawing.Point(145, 454)
        Me.txtFacturacionPais.Name = "txtFacturacionPais"
        Me.txtFacturacionPais.Size = New System.Drawing.Size(227, 20)
        Me.txtFacturacionPais.TabIndex = 43
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(39, 431)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(89, 13)
        Me.Label18.TabIndex = 42
        Me.Label18.Text = "Provincia/Estado"
        '
        'txtFacturacionProvincia
        '
        Me.txtFacturacionProvincia.Location = New System.Drawing.Point(145, 428)
        Me.txtFacturacionProvincia.Name = "txtFacturacionProvincia"
        Me.txtFacturacionProvincia.Size = New System.Drawing.Size(227, 20)
        Me.txtFacturacionProvincia.TabIndex = 41
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(39, 405)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(72, 13)
        Me.Label19.TabIndex = 40
        Me.Label19.Text = "Código Postal"
        '
        'txtFacturacionPostal
        '
        Me.txtFacturacionPostal.Location = New System.Drawing.Point(145, 402)
        Me.txtFacturacionPostal.Name = "txtFacturacionPostal"
        Me.txtFacturacionPostal.Size = New System.Drawing.Size(117, 20)
        Me.txtFacturacionPostal.TabIndex = 39
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(39, 379)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(54, 13)
        Me.Label20.TabIndex = 38
        Me.Label20.Text = "Población"
        '
        'txtFacturacionPoblacion
        '
        Me.txtFacturacionPoblacion.Location = New System.Drawing.Point(145, 376)
        Me.txtFacturacionPoblacion.Name = "txtFacturacionPoblacion"
        Me.txtFacturacionPoblacion.Size = New System.Drawing.Size(272, 20)
        Me.txtFacturacionPoblacion.TabIndex = 37
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(23, 279)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(24, 13)
        Me.Label22.TabIndex = 50
        Me.Label22.Text = "Fax"
        '
        'txtFacturacionFax
        '
        Me.txtFacturacionFax.Location = New System.Drawing.Point(145, 528)
        Me.txtFacturacionFax.Name = "txtFacturacionFax"
        Me.txtFacturacionFax.Size = New System.Drawing.Size(227, 20)
        Me.txtFacturacionFax.TabIndex = 49
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(39, 506)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(55, 13)
        Me.Label23.TabIndex = 48
        Me.Label23.Text = "Teléfono2"
        '
        'txtFacturacionTelefono2
        '
        Me.txtFacturacionTelefono2.Location = New System.Drawing.Point(145, 502)
        Me.txtFacturacionTelefono2.Name = "txtFacturacionTelefono2"
        Me.txtFacturacionTelefono2.Size = New System.Drawing.Size(227, 20)
        Me.txtFacturacionTelefono2.TabIndex = 47
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(39, 479)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(49, 13)
        Me.Label24.TabIndex = 46
        Me.Label24.Text = "Teléfono"
        '
        'txtFacturacionTelefono
        '
        Me.txtFacturacionTelefono.Location = New System.Drawing.Point(145, 476)
        Me.txtFacturacionTelefono.Name = "txtFacturacionTelefono"
        Me.txtFacturacionTelefono.Size = New System.Drawing.Size(227, 20)
        Me.txtFacturacionTelefono.TabIndex = 45
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(715, 355)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(54, 13)
        Me.Label21.TabIndex = 58
        Me.Label21.Text = "Población"
        '
        'txtEnvioPoblacion
        '
        Me.txtEnvioPoblacion.Location = New System.Drawing.Point(821, 352)
        Me.txtEnvioPoblacion.Name = "txtEnvioPoblacion"
        Me.txtEnvioPoblacion.Size = New System.Drawing.Size(227, 20)
        Me.txtEnvioPoblacion.TabIndex = 57
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(715, 329)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(99, 13)
        Me.Label25.TabIndex = 56
        Me.Label25.Text = "Dirección de Envío"
        '
        'txtEnvioDireccion
        '
        Me.txtEnvioDireccion.Location = New System.Drawing.Point(821, 326)
        Me.txtEnvioDireccion.Name = "txtEnvioDireccion"
        Me.txtEnvioDireccion.Size = New System.Drawing.Size(351, 20)
        Me.txtEnvioDireccion.TabIndex = 55
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(715, 303)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(49, 13)
        Me.Label26.TabIndex = 54
        Me.Label26.Text = "Apellidos"
        '
        'txtEnvioApellidos
        '
        Me.txtEnvioApellidos.Location = New System.Drawing.Point(821, 300)
        Me.txtEnvioApellidos.Name = "txtEnvioApellidos"
        Me.txtEnvioApellidos.Size = New System.Drawing.Size(420, 20)
        Me.txtEnvioApellidos.TabIndex = 53
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(715, 277)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(44, 13)
        Me.Label27.TabIndex = 52
        Me.Label27.Text = "Nombre"
        '
        'txtEnvioNombre
        '
        Me.txtEnvioNombre.Location = New System.Drawing.Point(821, 274)
        Me.txtEnvioNombre.Name = "txtEnvioNombre"
        Me.txtEnvioNombre.Size = New System.Drawing.Size(420, 20)
        Me.txtEnvioNombre.TabIndex = 51
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(715, 457)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(49, 13)
        Me.Label28.TabIndex = 66
        Me.Label28.Text = "Teléfono"
        '
        'txtEnvioTelefono
        '
        Me.txtEnvioTelefono.Location = New System.Drawing.Point(821, 454)
        Me.txtEnvioTelefono.Name = "txtEnvioTelefono"
        Me.txtEnvioTelefono.Size = New System.Drawing.Size(227, 20)
        Me.txtEnvioTelefono.TabIndex = 65
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(715, 431)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(29, 13)
        Me.Label29.TabIndex = 64
        Me.Label29.Text = "País"
        '
        'txtEnvioPais
        '
        Me.txtEnvioPais.Location = New System.Drawing.Point(821, 428)
        Me.txtEnvioPais.Name = "txtEnvioPais"
        Me.txtEnvioPais.Size = New System.Drawing.Size(227, 20)
        Me.txtEnvioPais.TabIndex = 63
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(715, 405)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(89, 13)
        Me.Label30.TabIndex = 62
        Me.Label30.Text = "Provincia/Estado"
        '
        'txtEnvioProvincia
        '
        Me.txtEnvioProvincia.Location = New System.Drawing.Point(821, 402)
        Me.txtEnvioProvincia.Name = "txtEnvioProvincia"
        Me.txtEnvioProvincia.Size = New System.Drawing.Size(227, 20)
        Me.txtEnvioProvincia.TabIndex = 61
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(715, 379)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(72, 13)
        Me.Label31.TabIndex = 60
        Me.Label31.Text = "Código Postal"
        '
        'txtEnvioPostal
        '
        Me.txtEnvioPostal.Location = New System.Drawing.Point(821, 376)
        Me.txtEnvioPostal.Name = "txtEnvioPostal"
        Me.txtEnvioPostal.Size = New System.Drawing.Size(147, 20)
        Me.txtEnvioPostal.TabIndex = 59
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(139, 331)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(104, 24)
        Me.Label32.TabIndex = 68
        Me.Label32.Text = "Productos"
        '
        'DataGridProductos
        '
        Me.DataGridProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridProductos.Location = New System.Drawing.Point(25, 601)
        Me.DataGridProductos.Name = "DataGridProductos"
        Me.DataGridProductos.Size = New System.Drawing.Size(1158, 150)
        Me.DataGridProductos.TabIndex = 67
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label32)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Location = New System.Drawing.Point(21, 252)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(655, 310)
        Me.GroupBox1.TabIndex = 69
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de facturación"
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(699, 252)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(595, 310)
        Me.GroupBox2.TabIndex = 70
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos de envío"
        '
        'txtImporteTotal
        '
        Me.txtImporteTotal.Location = New System.Drawing.Point(1083, 877)
        Me.txtImporteTotal.Name = "txtImporteTotal"
        Me.txtImporteTotal.Size = New System.Drawing.Size(100, 20)
        Me.txtImporteTotal.TabIndex = 71
        Me.txtImporteTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(998, 881)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 13)
        Me.Label6.TabIndex = 72
        Me.Label6.Text = "Importe Total"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(1187, 880)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(13, 13)
        Me.Label12.TabIndex = 73
        Me.Label12.Text = "€"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(988, 773)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(85, 13)
        Me.Label33.TabIndex = 75
        Me.Label33.Text = "Importe Articulos"
        '
        'txtImporteArticulos
        '
        Me.txtImporteArticulos.Location = New System.Drawing.Point(1083, 768)
        Me.txtImporteArticulos.Name = "txtImporteArticulos"
        Me.txtImporteArticulos.Size = New System.Drawing.Size(100, 20)
        Me.txtImporteArticulos.TabIndex = 74
        Me.txtImporteArticulos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(1042, 852)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(24, 13)
        Me.Label34.TabIndex = 77
        Me.Label34.Text = "IVA"
        '
        'txtIvaTotal
        '
        Me.txtIvaTotal.Location = New System.Drawing.Point(1083, 848)
        Me.txtIvaTotal.Name = "txtIvaTotal"
        Me.txtIvaTotal.Size = New System.Drawing.Size(100, 20)
        Me.txtIvaTotal.TabIndex = 76
        Me.txtIvaTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(1187, 773)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(13, 13)
        Me.Label35.TabIndex = 78
        Me.Label35.Text = "€"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(1187, 854)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(13, 13)
        Me.Label36.TabIndex = 79
        Me.Label36.Text = "€"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(25, 582)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(55, 13)
        Me.Label37.TabIndex = 80
        Me.Label37.Text = "Productos"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(1188, 798)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(13, 13)
        Me.Label38.TabIndex = 83
        Me.Label38.Text = "€"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(1029, 798)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(37, 13)
        Me.Label39.TabIndex = 82
        Me.Label39.Text = "Portes"
        '
        'txtPortes
        '
        Me.txtPortes.Location = New System.Drawing.Point(1084, 793)
        Me.txtPortes.Name = "txtPortes"
        Me.txtPortes.Size = New System.Drawing.Size(100, 20)
        Me.txtPortes.TabIndex = 81
        Me.txtPortes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(1188, 826)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(13, 13)
        Me.Label40.TabIndex = 86
        Me.Label40.Text = "€"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.ForeColor = System.Drawing.Color.Red
        Me.Label41.Location = New System.Drawing.Point(967, 826)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(107, 13)
        Me.Label41.TabIndex = 85
        Me.Label41.Text = "Descuento articulos -"
        '
        'txtDtoArticulos
        '
        Me.txtDtoArticulos.Location = New System.Drawing.Point(1084, 821)
        Me.txtDtoArticulos.Name = "txtDtoArticulos"
        Me.txtDtoArticulos.Size = New System.Drawing.Size(100, 20)
        Me.txtDtoArticulos.TabIndex = 84
        Me.txtDtoArticulos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DataGridEventos
        '
        Me.DataGridEventos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridEventos.Location = New System.Drawing.Point(827, 55)
        Me.DataGridEventos.Name = "DataGridEventos"
        Me.DataGridEventos.Size = New System.Drawing.Size(467, 72)
        Me.DataGridEventos.TabIndex = 87
        '
        'txtImporteTotalMoneda
        '
        Me.txtImporteTotalMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteTotalMoneda.Location = New System.Drawing.Point(1083, 898)
        Me.txtImporteTotalMoneda.Name = "txtImporteTotalMoneda"
        Me.txtImporteTotalMoneda.Size = New System.Drawing.Size(100, 22)
        Me.txtImporteTotalMoneda.TabIndex = 88
        Me.txtImporteTotalMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbl_moneda
        '
        Me.lbl_moneda.AutoSize = True
        Me.lbl_moneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_moneda.Location = New System.Drawing.Point(1187, 901)
        Me.lbl_moneda.Name = "lbl_moneda"
        Me.lbl_moneda.Size = New System.Drawing.Size(0, 16)
        Me.lbl_moneda.TabIndex = 89
        '
        'Pedidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1344, 930)
        Me.ControlBox = False
        Me.Controls.Add(Me.lbl_moneda)
        Me.Controls.Add(Me.txtImporteTotalMoneda)
        Me.Controls.Add(Me.DataGridEventos)
        Me.Controls.Add(Me.Label40)
        Me.Controls.Add(Me.Label41)
        Me.Controls.Add(Me.txtDtoArticulos)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.Label39)
        Me.Controls.Add(Me.txtPortes)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.txtIvaTotal)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.txtImporteArticulos)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtImporteTotal)
        Me.Controls.Add(Me.DataGridProductos)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.txtEnvioTelefono)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.txtEnvioPais)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.txtEnvioProvincia)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.txtEnvioPostal)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.txtEnvioPoblacion)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.txtEnvioDireccion)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.txtEnvioApellidos)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.txtEnvioNombre)
        Me.Controls.Add(Me.txtFacturacionFax)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.txtFacturacionTelefono2)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.txtFacturacionTelefono)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtFacturacionPais)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.txtFacturacionProvincia)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.txtFacturacionPostal)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.txtFacturacionPoblacion)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.txtFacturacionDireccion)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.txtFacturacionNif)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.txtFacturacionApellidos)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.txtFacturacionNombre)
        Me.Controls.Add(Me.txtDatosPago)
        Me.Controls.Add(Me.txtComentarioInterno)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtComentarioFacturacion)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtEstadoPedido)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtZonaEnvio)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtFormaPago)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtIdioma)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtVendedor)
        Me.Controls.Add(Me.btn_Cliente)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtClienteID)
        Me.Controls.Add(Me.btn_Cerrar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtFecha)
        Me.Controls.Add(Me.btn_Buscar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNumPedido)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "Pedidos"
        Me.Text = "Pedidos"
        CType(Me.DataGridProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridEventos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtNumPedido As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_Buscar As System.Windows.Forms.Button
    Friend WithEvents txtFecha As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btn_Cerrar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtClienteID As System.Windows.Forms.TextBox
    Friend WithEvents btn_Cliente As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtVendedor As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtIdioma As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtFormaPago As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtZonaEnvio As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtEstadoPedido As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtComentarioFacturacion As System.Windows.Forms.TextBox
    Friend WithEvents txtComentarioInterno As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtDatosPago As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacionNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacionApellidos As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacionNif As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacionDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacionPais As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacionProvincia As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacionPostal As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacionPoblacion As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacionFax As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacionTelefono2 As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtFacturacionTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioPoblacion As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioApellidos As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioPais As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioProvincia As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioPostal As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents DataGridProductos As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtImporteTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtImporteArticulos As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtIvaTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents txtPortes As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txtDtoArticulos As System.Windows.Forms.TextBox
    Friend WithEvents DataGridEventos As System.Windows.Forms.DataGridView
    Friend WithEvents txtImporteTotalMoneda As System.Windows.Forms.TextBox
    Friend WithEvents lbl_moneda As System.Windows.Forms.Label
End Class
