﻿Imports System
Imports System.IO
Imports WinSCP

Public Class Gestion_imagen_categorias
    Public carpeta As New DirectoryInfo("\\192.168.2.6\imagenes\categorias\")
    Public act_imagen As New Images
    Dim conect As New web
    Private Sub buscar_Click(sender As Object, e As EventArgs) Handles buscar.Click
        CheckedListBox1.Items.Clear()

        For Each f As FileInfo In carpeta.GetFiles
            If f.Name.Contains(categoriabusqueda.Text) Then
                CheckedListBox1.Items.Add(f.Name)
            End If
        Next
    End Sub

    Private Sub abredirectorio_Click(sender As Object, e As EventArgs) Handles abredirectorio.Click
        Dim OpenFile As New OpenFileDialog()
        OpenFile.InitialDirectory = "\\192.168.2.6\imagenes\categorias\" & FCategorias.categoriaid.Text
        If OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.PictureBox1.Image = Image.FromFile(OpenFile.FileName)
        End If
    End Sub

    Private Sub CheckedListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CheckedListBox1.SelectedIndexChanged
        Dim cadena As String = CStr(CheckedListBox1.SelectedItem)
        Using stream As New StreamReader("\\192.168.2.6\imagenes\categorias\" & cadena)
            PictureBox1.Image = Image.FromStream(stream.BaseStream)
            nombreimgen.Text = cadena
        End Using
    End Sub

    Private Sub actualizar_Click(sender As Object, e As EventArgs) Handles actualizar.Click
        act_imagen.actualizar_categorias()
        conect.actualiza_imagen_categoria()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim OpenFile As New OpenFileDialog()
        If OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
            PictureBox1.Image = Image.FromFile(OpenFile.FileName)
        End If
    End Sub

    Private Sub guardar_imagen_Click(sender As Object, e As EventArgs) Handles guardar_imagen.Click
        act_imagen.guardar_categorias()
    End Sub

    Private Sub enviar_Click(sender As Object, e As EventArgs) Handles enviar.Click
        act_imagen.subir_categorias()
        conect.imagen_categoria()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Gestion_imagen_categorias_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For Each f As FileInfo In carpeta.GetFiles
            If f.Name.Contains(FCategorias.categoriaid.Text) Then
                CheckedListBox1.Items.Add(f.Name)
            End If
        Next
    End Sub

    Private Sub Eliminar_Click(sender As Object, e As EventArgs) Handles Eliminar.Click
        act_imagen.eliminar_imagen_categoria()
    End Sub
End Class