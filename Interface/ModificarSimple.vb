﻿Public Class ModificarSimple

    Public conect As web = New web
    Public sqlconexion As SQLCONECTAR = New SQLCONECTAR

    Private Sub btn_Cerrar_Click(sender As Object, e As EventArgs) Handles btn_Cerrar.Click
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub btn_Modificar_Click(sender As Object, e As EventArgs) Handles btn_Modificar.Click
        conect.updateSimple()
    End Sub

    Private Sub btn_Calcular_Click(sender As Object, e As EventArgs) Handles btn_Calcular.Click

        Dim porcentaje As Decimal
        Dim resultado As Decimal

        With Me
            If CDec(.txtPvp.Text) <> 0 And CDec(.txtCoste.Text) <> 0 Then

                porcentaje = (CDec(.txtPvp.Text) - CDec(.txtCoste.Text)) * 100 / CDec(.txtPvp.Text)

                If (porcentaje >= 33.33) And (porcentaje < 37.5) Then
                    resultado = 5
                End If

                If (porcentaje >= 37.4) And (porcentaje < 41.18) Then
                    resultado = 10
                End If

                If (porcentaje >= 41.18) And (porcentaje < 44.44) Then
                    resultado = 15
                End If

                If (porcentaje >= 44.44) And (porcentaje < 50) Then
                    resultado = 20
                End If

                If (porcentaje >= 50) And (porcentaje < 60) Then
                    resultado = 25
                End If

                If (porcentaje >= 60) And (porcentaje < 66.67) Then
                    resultado = 30
                End If

                If (porcentaje >= 66.67) And (porcentaje <= 71.43) Then
                    resultado = 35
                End If

                If porcentaje > 71.43 Then
                    resultado = 40
                End If

                .txtPvd.Text = CStr(CDec(.txtPvp.Text) - ((CDec(.txtPvp.Text) * resultado)) / 100)

                '.txtPvd.Text = FormatNumber(.txtPvd.Text, 2)

            End If
        End With

    End Sub

    Private Sub btn_Examinar_Click(sender As Object, e As EventArgs) Handles btn_Examinar.Click
        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            txtFile.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub btn_Upload_Click(sender As Object, e As EventArgs) Handles btn_Upload.Click
        If Me.txtFile.Text <> "" Then
            conect.uploadImagenSimple()
        Else
            MsgBox("Hay que seleccionar una imagen")
        End If
    End Sub

    Private Sub ModificarSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        sqlconexion.listado_Marcas()
        sqlconexion.listado_Eans()
        Me.Combo_marcas.SelectedItem = Me.txtMarca.Text
        Me.Combo_eans.SelectedItem = Me.txtEan.Text
    End Sub

    Private Sub Combo_eans_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Combo_eans.SelectedIndexChanged
        Me.txtEan.Text = CStr(Me.Combo_eans.SelectedItem)
    End Sub

    Private Sub Combo_marcas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Combo_marcas.SelectedIndexChanged
        Me.txtMarca.Text = CStr(Me.Combo_marcas.SelectedItem)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Marcas.ShowDialog()
    End Sub
End Class