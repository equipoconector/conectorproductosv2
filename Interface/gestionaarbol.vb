﻿Imports System.Data
Imports System.Windows.Forms
Imports System.Text
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports MySql.Data.MySqlClient
Imports System
Imports MSXML2

Public Class gestionaarbol
    Public ds As DataSet
    Public tabla As DataTable = New DataTable()
    Public tabla1 As DataTable = New DataTable()
    Public tempagrega As DataTable = New DataTable()
    Public tempelimina As DataTable = New DataTable()
    Public interruptor As Integer
    Public nivel1 As String
    Public nivel2 As String
    Public nivel3 As String
    Public nivel4 As String
    Public nivel5 As String
    Public idtabla As String
    Public idtabla2 As String
    Public vEncontrado As Boolean
    Public envio_agregar As String = ""
    Dim conectmysql As New MySqlConnection("server = 192.168.2.28; user id= Navision;password = excelia=2009;database = qapp_production; port = 3306")
    Dim conectmysql2 As New MySqlConnection("server = 192.168.2.28; user id= Navision;password = excelia=2009;database = qapp_production; port = 3306")
    Dim conect As New web

    '*********************************************** INSERTAMOS EN LOS LISTVIEW ***************************************************  

    Public Sub crea_tabla1()
        Dim item As New ListViewItem
        Dim condicion As String = "nivel = 1 "
        Dim i As Integer
        Dim encuentra As DataRow()
        encuentra = tabla.Select(condicion)

        With ARBOL_CATEGORIAS.lista
            .Clear()
            .View = View.Details
            .Columns.Add("Id ", 50)
            .Columns.Add("Nombre ", 500)
        End With

        For i = 0 To (encuentra.Length - 1)
            With ARBOL_CATEGORIAS.lista.Items.Add(encuentra(i)(0).ToString)
                .SubItems.Add(encuentra(i)(2).ToString)
                If encuentra(i)(4).ToString = "True" Then
                    .Checked = True
                End If
            End With
        Next
    End Sub

    Public Sub crea_tabla2()
        Dim item As New ListViewItem
        Dim condicion As String = "nivel = 2 "
        Dim i As Integer
        Dim encuentra As DataRow()

        encuentra = tabla.Select(condicion)

        With ARBOL_CATEGORIAS.lista2
            .Clear()
            .View = View.Details
            .Columns.Add("Id ", 50)
            .Columns.Add("Nombre ", 500)
        End With

        For i = 0 To (encuentra.Length - 1)
            With ARBOL_CATEGORIAS.lista2.Items.Add(encuentra(i)(0).ToString)
                .SubItems.Add(encuentra(i)(2).ToString)
                If encuentra(i)(4).ToString = "True" Then
                    .Checked = True
                End If
            End With
        Next
    End Sub

    Public Sub crea_tabla3()
        Dim item As New ListViewItem
        Dim condicion As String = "nivel = 3 "
        Dim i As Integer
        Dim encuentra As DataRow()

        encuentra = tabla.Select(condicion)

        With ARBOL_CATEGORIAS.lista3
            .Clear()
            .View = View.Details
            .Columns.Add("Id ", 50)
            .Columns.Add("Nombre ", 500)
        End With

        For i = 0 To (encuentra.Length - 1)
            With ARBOL_CATEGORIAS.lista3.Items.Add(encuentra(i)(0).ToString)
                .SubItems.Add(encuentra(i)(2).ToString)
                If encuentra(i)(4).ToString = "True" Then
                    .Checked = True
                End If
            End With
        Next
    End Sub
    Public Sub crea_tabla4()
        Dim item As New ListViewItem
        Dim condicion As String = "nivel = 4 "
        Dim i As Integer
        Dim encuentra As DataRow()

        encuentra = tabla.Select(condicion)

        With ARBOL_CATEGORIAS.lista4
            .Clear()
            .View = View.Details
            .Columns.Add("Id ", 50)
            .Columns.Add("Nombre ", 500)
        End With

        For i = 0 To (encuentra.Length - 1)
            With ARBOL_CATEGORIAS.lista4.Items.Add(encuentra(i)(0).ToString)
                .SubItems.Add(encuentra(i)(2).ToString)
                If encuentra(i)(4).ToString = "True" Then
                    .Checked = True
                End If
            End With
        Next
    End Sub
    Public Sub crea_tabla5()
        Dim item As New ListViewItem
        Dim condicion As String = "nivel = 5 "
        Dim i As Integer
        Dim encuentra As DataRow()

        encuentra = tabla.Select(condicion)

        With ARBOL_CATEGORIAS.lista5
            .Clear()
            .View = View.Details
            .Columns.Add("Id ", 50)
            .Columns.Add("Nombre ", 500)
        End With

        For i = 0 To (encuentra.Length - 1)
            With ARBOL_CATEGORIAS.lista5.Items.Add(encuentra(i)(0).ToString)
                .SubItems.Add(encuentra(i)(2).ToString)
                If encuentra(i)(4).ToString = "True" Then
                    .Checked = True
                End If
            End With
        Next
    End Sub
    '************************************************* FIN INSERCION EN LISTVIEW ******************************************************************

    '***************************************************** CARGA DE DATATABLES ************************************************************************
    Public Sub inicio()
        Try
            'CATEGORIAS ASOCIADAS
            conectmysql2.Open()

            Dim consultacat2 As New MySqlCommand("select * from categorias_arbol where Asociado ='" & ARBOL_CATEGORIAS.id.Text & "'order by id_categoria", conectmysql2)
            Dim lector2 As MySql.Data.MySqlClient.MySqlDataReader = consultacat2.ExecuteReader()

            tabla1.Columns.Add("Id", Type.GetType("System.Int32"))
            tabla1.Columns.Add("nivel", Type.GetType("System.Int32"))
            tabla1.Columns.Add("Nombre", Type.GetType("System.String"))
            tabla1.Columns.Add("parent_id", Type.GetType("System.Int32"))
            tabla1.Columns.Add("Asociados", Type.GetType("System.Int32"))
            tabla1.Columns.Add("Seleccion", Type.GetType("System.Boolean"))
            tabla1.Columns.Add("id_magento", Type.GetType("System.Int32"))
            tabla1.Columns.Add("parent_id_magento", Type.GetType("System.Int32"))

            While lector2.Read
                Dim dtregistro2 As DataRow = tabla1.NewRow()

                dtregistro2("Id") = lector2("id_categoria").ToString()
                dtregistro2("nivel") = Convert.ToInt32(lector2("nivel"))
                dtregistro2("Nombre") = lector2("nombre_categoria").ToString()
                dtregistro2("parent_id") = lector2("parent_id").ToString()
                dtregistro2("Asociados") = lector2("asociado").ToString()
                dtregistro2("id_magento") = lector2("id_magento").ToString()
                dtregistro2("parent_id_magento") = lector2("parent_id_magento").ToString()
                tabla1.Rows.Add(dtregistro2)
            End While

            conectmysql2.Close()
            conectmysql.Open()

            Dim consultacat As New MySqlCommand("select id,nombre,nivel,parent_id,id_magento,parent_id_magento from categorias where nivel > 0 order by id", conectmysql)
            Dim lector As MySql.Data.MySqlClient.MySqlDataReader = consultacat.ExecuteReader()

            tabla.Columns.Add("Id", Type.GetType("System.Int32"))
            tabla.Columns.Add("nivel", Type.GetType("System.Int32"))
            tabla.Columns.Add("Nombre", Type.GetType("System.String"))
            tabla.Columns.Add("parent_id", Type.GetType("System.Int32"))
            tabla.Columns.Add("Seleccion", Type.GetType("System.Boolean"))
            tabla.Columns.Add("id_magento", Type.GetType("System.Int32"))
            tabla.Columns.Add("parent_id_magento", Type.GetType("System.Int32"))

            Dim id As Integer
            Dim id2 As String = ""
            Dim encuentra1 As DataRow()

            While lector.Read
                id = CInt(lector("id"))
                Dim condicion As String = "Id= " & id

                encuentra1 = tabla1.Select(condicion)

                For i = 0 To (encuentra1.Length - 1)
                    id2 = encuentra1(i)(0).ToString
                    If id2 <> "" Then
                        interruptor = CInt(True)
                    End If
                Next

                Dim dtregistro As DataRow = tabla.NewRow()

                dtregistro("Id") = Convert.ToInt32(lector("id"))
                dtregistro("nivel") = Convert.ToInt32(lector("nivel"))
                dtregistro("Nombre") = lector("nombre").ToString()
                dtregistro("parent_id") = Convert.ToInt32(lector("parent_id"))
                dtregistro("id_magento") = Convert.ToInt32(lector("id_magento"))
                dtregistro("parent_id_magento") = Convert.ToInt32(lector("parent_id_magento"))
                dtregistro("Seleccion") = interruptor

                tabla.Rows.Add(dtregistro)
                interruptor = CInt(False)
            End While
            lector.Close()
            conectmysql.Close()

            ' LLAMADA A LAS CARGAS DE LISTVIEW
            crea_tabla1()
            crea_tabla2()
            crea_tabla3()
            crea_tabla4()
            crea_tabla5()
            ' FIN 
        Catch ex As Exception
            conectmysql.Close()
            conectmysql2.Close()
            MessageBox.Show("HA OCURRIDO UN ERROR CON LA CONEXIÓN, PRUEBA DE NUEVO")
        End Try
    End Sub
    '********************************************************************* FIN DE LA CARGA DE DATATABLES **************************************************

    ' ********************************************** PROCESO DE ELIMINACIÓN DE LAS CATEGORIAS EN MOVIMIENTOS *****************************************************
    Public Sub elimina_movimientos()
        tempelimina.Columns.Add("Id", Type.GetType("System.String"))
        tempelimina.Columns.Add("nivel", Type.GetType("System.Int16"))
        tempelimina.Columns.Add("Nombre", Type.GetType("System.String"))
        tempelimina.Columns.Add("Nivel_superior", Type.GetType("System.String"))
        tempelimina.Columns.Add("Asociados", Type.GetType("System.String"))
        tempelimina.Columns.Add("Seleccion", Type.GetType("System.Boolean"))
        tempelimina.Columns.Add("id_magento", Type.GetType("System.Int32"))
        tempelimina.Columns.Add("parent_id_magento", Type.GetType("System.Int32"))

        Dim condicion As String = "Id <> 10000000"
        Dim i As Integer
        Dim encuentra4 As DataRow()
        encuentra4 = tabla1.Select(condicion)

        For i = 0 To (encuentra4.Length - 1)
            For Each item As ListViewItem In ARBOL_CATEGORIAS.lista.Items
                If item.Checked = False Then
                    If encuentra4(i)(0).ToString = item.Text Then
                        Dim dtregistro2 As DataRow = tempelimina.NewRow()

                        dtregistro2("Id") = encuentra4(i)(0).ToString
                        dtregistro2("nivel") = encuentra4(i)(1).ToString
                        dtregistro2("Nombre") = encuentra4(i)(2).ToString
                        dtregistro2("Nivel_superior") = encuentra4(i)(3).ToString
                        dtregistro2("Asociados") = ARBOL_CATEGORIAS.id.Text
                        dtregistro2("id_magento") = encuentra4(i)(7).ToString
                        tempelimina.Rows.Add(dtregistro2)
                    End If
                End If
            Next
        Next

        For i = 0 To (encuentra4.Length - 1)
            For Each item As ListViewItem In ARBOL_CATEGORIAS.lista2.Items
                If item.Checked = False Then
                    If encuentra4(i)(0).ToString = item.Text Then
                        Dim dtregistro2 As DataRow = tempelimina.NewRow()

                        dtregistro2("Id") = encuentra4(i)(0).ToString
                        dtregistro2("nivel") = encuentra4(i)(1).ToString
                        dtregistro2("Nombre") = encuentra4(i)(2).ToString
                        dtregistro2("Nivel_superior") = encuentra4(i)(3).ToString
                        dtregistro2("Asociados") = ARBOL_CATEGORIAS.id.Text
                        dtregistro2("id_magento") = encuentra4(i)(7).ToString
                        tempelimina.Rows.Add(dtregistro2)
                    End If
                End If
            Next
        Next

        For i = 0 To (encuentra4.Length - 1)
            For Each item As ListViewItem In ARBOL_CATEGORIAS.lista3.Items
                If item.Checked = False Then
                    If encuentra4(i)(0).ToString = item.Text Then
                        Dim dtregistro2 As DataRow = tempelimina.NewRow()

                        dtregistro2("Id") = encuentra4(i)(0).ToString
                        dtregistro2("nivel") = encuentra4(i)(1).ToString
                        dtregistro2("Nombre") = encuentra4(i)(2).ToString
                        dtregistro2("Nivel_superior") = encuentra4(i)(3).ToString
                        dtregistro2("Asociados") = ARBOL_CATEGORIAS.id.Text
                        dtregistro2("id_magento") = encuentra4(i)(7).ToString
                        tempelimina.Rows.Add(dtregistro2)
                    End If
                End If
            Next
        Next

        For i = 0 To (encuentra4.Length - 1)
            For Each item As ListViewItem In ARBOL_CATEGORIAS.lista4.Items
                If item.Checked = False Then
                    If encuentra4(i)(0).ToString = item.Text Then
                        Dim dtregistro2 As DataRow = tempelimina.NewRow()

                        dtregistro2("Id") = encuentra4(i)(0).ToString
                        dtregistro2("nivel") = encuentra4(i)(1).ToString
                        dtregistro2("Nombre") = encuentra4(i)(2).ToString
                        dtregistro2("Nivel_superior") = encuentra4(i)(3).ToString
                        dtregistro2("Asociados") = ARBOL_CATEGORIAS.id.Text
                        dtregistro2("id_magento") = encuentra4(i)(7).ToString
                        tempelimina.Rows.Add(dtregistro2)
                    End If
                End If
            Next
        Next

        For i = 0 To (encuentra4.Length - 1)
            For Each item As ListViewItem In ARBOL_CATEGORIAS.lista5.Items
                If item.Checked = False Then
                    If encuentra4(i)(0).ToString = item.Text Then
                        Dim dtregistro2 As DataRow = tempelimina.NewRow()

                        dtregistro2("Id") = encuentra4(i)(0).ToString
                        dtregistro2("nivel") = encuentra4(i)(1).ToString
                        dtregistro2("Nombre") = encuentra4(i)(2).ToString
                        dtregistro2("Nivel_superior") = encuentra4(i)(3).ToString
                        dtregistro2("Asociados") = ARBOL_CATEGORIAS.id.Text
                        dtregistro2("id_magento") = encuentra4(i)(7).ToString
                        tempelimina.Rows.Add(dtregistro2)
                    End If
                End If
            Next
        Next
        ' LLAMADA AL LISTVIEW QUE MUESTRA LOS ELEMENTOS ELIMINADOS
        tabla4()
        ' FIN
    End Sub

    '************************************************************************ PROCESO DE INSERCIÓN DE NUEVOS CATEGORIAS ********************************************
    Public Sub nuevos_movimientos()
        tempagrega.Columns.Add("Id", Type.GetType("System.String"))
        tempagrega.Columns.Add("nivel", Type.GetType("System.Int16"))
        tempagrega.Columns.Add("Nombre", Type.GetType("System.String"))
        tempagrega.Columns.Add("Nivel_superior", Type.GetType("System.String"))
        tempagrega.Columns.Add("Asociados", Type.GetType("System.String"))
        tempagrega.Columns.Add("Seleccion", Type.GetType("System.Boolean"))
        tempagrega.Columns.Add("id_magento", Type.GetType("System.Int32"))
        tempagrega.Columns.Add("parent_id_magento", Type.GetType("System.Int32"))

        Dim i As Integer
        Dim encuentra5 As DataRow()
        Dim nivel1 As String = ""
        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista.Items
            Dim interruptor As String = ""

            If item.Checked = True Then
                Dim condicion As String = "Id=" & item.Text
                nivel1 = item.Text
                encuentra5 = tabla1.Select(condicion)
                For i = 0 To (encuentra5.Length - 1)
                    interruptor = encuentra5(i)(0).ToString

                Next

                If interruptor = "" Then
                    Dim condicion2 As String = "Id =" & item.Text
                    Dim encuentra6 As DataRow()
                    encuentra6 = tabla.Select(condicion)

                    For i = 0 To (encuentra6.Length - 1)
                        Dim dtregistro2 As DataRow = tempagrega.NewRow()

                        dtregistro2("Id") = encuentra6(i)(0).ToString
                        dtregistro2("nivel") = encuentra6(i)(1).ToString
                        dtregistro2("Nombre") = encuentra6(i)(2).ToString
                        dtregistro2("Nivel_superior") = encuentra6(i)(3).ToString
                        dtregistro2("id_magento") = encuentra6(i)(5).ToString
                        dtregistro2("parent_id_magento") = encuentra6(i)(6).ToString
                        dtregistro2("Asociados") = ARBOL_CATEGORIAS.id.Text

                        tempagrega.Rows.Add(dtregistro2)
                    Next
                End If
            End If
        Next

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista2.Items
            Dim interruptor As String = ""

            If item.Checked = True Then
                Dim condicion As String = "Id=" & item.Text
                nivel2 = item.Text
                encuentra5 = tabla1.Select(condicion)
                For i = 0 To (encuentra5.Length - 1)
                    interruptor = encuentra5(i)(0).ToString
                Next

                If interruptor = "" Then
                    Dim condicion2 As String = "Id =" & item.Text
                    Dim encuentra6 As DataRow()
                    encuentra6 = tabla.Select(condicion)

                    For i = 0 To (encuentra6.Length - 1)
                        Dim dtregistro2 As DataRow = tempagrega.NewRow()

                        dtregistro2("Id") = encuentra6(i)(0).ToString
                        dtregistro2("nivel") = encuentra6(i)(1).ToString
                        dtregistro2("Nombre") = encuentra6(i)(2).ToString
                        dtregistro2("Nivel_superior") = encuentra6(i)(3).ToString
                        dtregistro2("id_magento") = encuentra6(i)(5).ToString
                        dtregistro2("parent_id_magento") = encuentra6(i)(6).ToString
                        dtregistro2("Asociados") = encuentra6(i)(4).ToString

                        tempagrega.Rows.Add(dtregistro2)
                    Next
                End If
            End If
        Next

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista3.Items
            Dim interruptor As String = ""

            If item.Checked = True Then
                Dim condicion As String = "Id=" & item.Text
                nivel3 = item.Text
                encuentra5 = tabla1.Select(condicion)
                For i = 0 To (encuentra5.Length - 1)
                    interruptor = encuentra5(i)(0).ToString
                Next

                If interruptor = "" Then
                    Dim condicion2 As String = "Id =" & item.Text
                    Dim encuentra6 As DataRow()
                    encuentra6 = tabla.Select(condicion)

                    For i = 0 To (encuentra6.Length - 1)
                        Dim dtregistro2 As DataRow = tempagrega.NewRow()

                        dtregistro2("Id") = encuentra6(i)(0).ToString
                        dtregistro2("nivel") = encuentra6(i)(1).ToString
                        dtregistro2("Nombre") = encuentra6(i)(2).ToString
                        dtregistro2("Nivel_superior") = encuentra6(i)(3).ToString
                        dtregistro2("id_magento") = encuentra6(i)(5).ToString
                        dtregistro2("parent_id_magento") = encuentra6(i)(6).ToString
                        dtregistro2("Asociados") = encuentra6(i)(4).ToString

                        tempagrega.Rows.Add(dtregistro2)
                    Next
                End If
            End If
        Next

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista4.Items
            Dim interruptor As String = ""

            If item.Checked = True Then
                Dim condicion As String = "Id=" & item.Text
                nivel4 = item.Text
                encuentra5 = tabla1.Select(condicion)
                For i = 0 To (encuentra5.Length - 1)
                    interruptor = encuentra5(i)(0).ToString
                Next

                If interruptor = "" Then
                    Dim condicion2 As String = "Id =" & item.Text
                    Dim encuentra6 As DataRow()
                    encuentra6 = tabla.Select(condicion)

                    For i = 0 To (encuentra6.Length - 1)
                        Dim dtregistro2 As DataRow = tempagrega.NewRow()

                        dtregistro2("Id") = encuentra6(i)(0).ToString
                        dtregistro2("nivel") = encuentra6(i)(1).ToString
                        dtregistro2("Nombre") = encuentra6(i)(2).ToString
                        dtregistro2("Nivel_superior") = encuentra6(i)(3).ToString
                        dtregistro2("id_magento") = encuentra6(i)(5).ToString
                        dtregistro2("parent_id_magento") = encuentra6(i)(6).ToString
                        dtregistro2("Asociados") = encuentra6(i)(4).ToString

                        tempagrega.Rows.Add(dtregistro2)
                    Next
                End If
            End If
        Next

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista5.Items
            Dim interruptor As String = ""

            If item.Checked = True Then
                Dim condicion As String = "Id=" & item.Text
                nivel5 = item.Text
                encuentra5 = tabla1.Select(condicion)
                For i = 0 To (encuentra5.Length - 1)
                    interruptor = encuentra5(i)(0).ToString

                Next

                If interruptor = "" Then
                    Dim condicion2 As String = "Id =" & item.Text
                    Dim encuentra6 As DataRow()
                    encuentra6 = tabla.Select(condicion)

                    For i = 0 To (encuentra6.Length - 1)
                        Dim dtregistro2 As DataRow = tempagrega.NewRow()

                        dtregistro2("Id") = encuentra6(i)(0).ToString
                        dtregistro2("nivel") = encuentra6(i)(1).ToString
                        dtregistro2("Nombre") = encuentra6(i)(2).ToString
                        dtregistro2("Nivel_superior") = encuentra6(i)(3).ToString
                        dtregistro2("id_magento") = encuentra6(i)(5).ToString
                        dtregistro2("parent_id_magento") = encuentra6(i)(6).ToString
                        dtregistro2("Asociados") = encuentra6(i)(4).ToString

                        tempagrega.Rows.Add(dtregistro2)
                    Next
                End If
            End If
        Next

        If nivel1 <> "" Then
            Modelo.Categoria.Text = nivel1
        End If

        If nivel2 <> "" Then
            Modelo.Categoria.Text = nivel2
        End If

        If nivel3 <> "" Then
            Modelo.Categoria.Text = nivel3
        End If

        If nivel4 <> "" Then
            Modelo.Categoria.Text = nivel4
        End If

        If nivel5 <> "" Then
            Modelo.Categoria.Text = nivel5
        End If

        tabla5()
        ' FIN 
    End Sub

    '******************************************************** FIN DEL PROCESO DE AGREGADO DE NUEVOS MOVIMIENTOS DE CATEGORIAS ******************************************


    '***************************** MOVIMIENTOS EN TABLAS *********************************************************************************************
    Public Sub tabla4()
        Dim item5 As New ListViewItem
        Dim condicion As String = "Id <> 110000000 "
        Dim i As Integer
        Dim encuentra5 As DataRow()
        Dim envio_eliminar As String = ""
        encuentra5 = tempelimina.Select(condicion)
        conectmysql.Open()

        For i = 0 To (encuentra5.Length - 1)
            Dim consultacat As New MySqlCommand("delete from categorias_arbol where id_categoria ='" & encuentra5(i)(0).ToString & "' and Asociado ='" & ARBOL_CATEGORIAS.id.Text & "'", conectmysql)
            consultacat.ExecuteNonQuery()

            Dim consultacat2 As New MySqlCommand("delete from categorias_configurables where id_categoria ='" & encuentra5(i)(0).ToString & "' and Asociado ='" & ARBOL_CATEGORIAS.id.Text & "'", conectmysql)
            consultacat2.ExecuteNonQuery()
        Next

        conectmysql.Close()
        tempelimina.Reset()
    End Sub
    Public Sub tabla5()
        Dim item5 As New ListViewItem
        Dim condicion As String = "Id <> 110000000 "
        Dim i As Integer
        Dim encuentra5 As DataRow()

        encuentra5 = tempagrega.Select(condicion)
        conectmysql2.Open()

        For i = 0 To (encuentra5.Length - 1)
            Dim consultacat As New MySqlCommand("insert into `categorias_arbol`(id_categoria,nivel,Nombre_categoria,Asociado,parent_id,id_magento,parent_id_magento) values ('" & encuentra5(i)(0).ToString & "'," & encuentra5(i)(1).ToString & ",'" & encuentra5(i)(2).ToString & "','" & ARBOL_CATEGORIAS.id.Text & "','" & encuentra5(i)(3).ToString & "','" & encuentra5(i)(6).ToString & "','" & encuentra5(i)(7).ToString & "')", conectmysql2)
            consultacat.ExecuteNonQuery()
            Dim consultacat2 As New MySqlCommand("insert into `categorias_configurables`(id_categoria,nivel,Nombre_categoria,Asociado,parent_id,id_magento,parent_id_magento) values ('" & encuentra5(i)(0).ToString & "'," & encuentra5(i)(1).ToString & ",'" & encuentra5(i)(2).ToString & "','" & ARBOL_CATEGORIAS.id.Text & "','" & encuentra5(i)(3).ToString & "','" & encuentra5(i)(6).ToString & "','" & encuentra5(i)(7).ToString & "')", conectmysql2)
            consultacat2.ExecuteNonQuery()
        Next

        conectmysql2.Close()
        tempagrega.Reset()
    End Sub

    ' ******************************************************************* FIN DE LISTIVEWS QUE SIMULAN INSERCIÓN EN TABLAS ******************************************************

    '************************************************************************** MOVIMIENTOS EN ARBOL SEGÚN SE SELECCIONA ***********************************************************
    Public Sub quinto_nivel()
        Dim i As Integer
        Dim nivel2 As DataRow()
        Dim parent_id As String = ""

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista5.Items
            Dim id As String = ""
            If item.Checked = True Then
                Dim condicion As String = "Id =" & item.Text
                nivel2 = tabla.Select(condicion)

                For i = 0 To (nivel2.Length - 1)
                    id = nivel2(i)(0).ToString
                    parent_id = nivel2(i)(3).ToString

                    For Each item2 As ListViewItem In ARBOL_CATEGORIAS.lista4.Items
                        If item2.Text = parent_id Then
                            item2.Checked = True
                        End If
                    Next
                Next
            End If
        Next

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista4.Items
            Dim id As String = ""
            If item.Checked = True Then
                Dim condicion As String = "Id =" & item.Text
                nivel2 = tabla.Select(condicion)

                For i = 0 To (nivel2.Length - 1)
                    id = nivel2(i)(0).ToString
                    parent_id = nivel2(i)(3).ToString

                    For Each item2 As ListViewItem In ARBOL_CATEGORIAS.lista3.Items
                        If item2.Text = parent_id Then
                            item2.Checked = True
                        End If
                    Next
                Next
            End If
        Next

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista3.Items
            Dim id As String = ""
            If item.Checked = True Then
                Dim condicion As String = "Id =" & item.Text
                nivel2 = tabla.Select(condicion)

                For i = 0 To (nivel2.Length - 1)
                    id = nivel2(i)(0).ToString
                    parent_id = nivel2(i)(3).ToString

                    For Each item2 As ListViewItem In ARBOL_CATEGORIAS.lista2.Items
                        If item2.Text = parent_id Then
                            item2.Checked = True
                        End If
                    Next
                Next
            End If
        Next

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista2.Items
            Dim id As String = ""
            If item.Checked = True Then
                Dim condicion As String = "Id =" & item.Text
                nivel2 = tabla.Select(condicion)

                For i = 0 To (nivel2.Length - 1)
                    id = nivel2(i)(0).ToString
                    parent_id = nivel2(i)(3).ToString

                    For Each item2 As ListViewItem In ARBOL_CATEGORIAS.lista.Items
                        If item2.Text = parent_id Then
                            item2.Checked = True
                        End If
                    Next
                Next
            End If
        Next
    End Sub

    Public Sub cuarto_nivel()
        Dim i As Integer
        Dim nivel2 As DataRow()
        Dim parent_id As String = ""

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista4.Items
            Dim id As String = ""
            If item.Checked = True Then
                Dim condicion As String = "Id =" & item.Text
                nivel2 = tabla.Select(condicion)

                For i = 0 To (nivel2.Length - 1)
                    id = nivel2(i)(0).ToString
                    parent_id = nivel2(i)(3).ToString

                    For Each item2 As ListViewItem In ARBOL_CATEGORIAS.lista3.Items
                        If item2.Text = parent_id Then
                            item2.Checked = True
                        End If
                    Next
                Next
            End If
        Next

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista3.Items
            Dim id As String = ""
            If item.Checked = True Then
                Dim condicion As String = "Id =" & item.Text
                nivel2 = tabla.Select(condicion)

                For i = 0 To (nivel2.Length - 1)
                    id = nivel2(i)(0).ToString
                    parent_id = nivel2(i)(3).ToString

                    For Each item2 As ListViewItem In ARBOL_CATEGORIAS.lista2.Items
                        If item2.Text = parent_id Then
                            item2.Checked = True
                        End If
                    Next
                Next
            End If
        Next

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista2.Items
            Dim id As String = ""
            If item.Checked = True Then
                Dim condicion As String = "Id =" & item.Text
                nivel2 = tabla.Select(condicion)

                For i = 0 To (nivel2.Length - 1)
                    id = nivel2(i)(0).ToString
                    parent_id = nivel2(i)(3).ToString

                    For Each item2 As ListViewItem In ARBOL_CATEGORIAS.lista.Items
                        If item2.Text = parent_id Then
                            item2.Checked = True
                        End If
                    Next
                Next
            End If
        Next
    End Sub

    Public Sub tercer_nivel()
        Dim i As Integer
        Dim nivel2 As DataRow()
        Dim parent_id As String = ""

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista3.Items
            Dim id As String = ""
            If item.Checked = True Then
                Dim condicion As String = "Id =" & item.Text
                nivel2 = tabla.Select(condicion)

                For i = 0 To (nivel2.Length - 1)
                    id = nivel2(i)(0).ToString
                    parent_id = nivel2(i)(3).ToString

                    For Each item2 As ListViewItem In ARBOL_CATEGORIAS.lista2.Items
                        If item2.Text = parent_id Then
                            item2.Checked = True
                        End If
                    Next
                Next
            End If
        Next

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista2.Items
            Dim id As String = ""
            If item.Checked = True Then
                Dim condicion As String = "Id =" & item.Text
                nivel2 = tabla.Select(condicion)

                For i = 0 To (nivel2.Length - 1)
                    id = nivel2(i)(0).ToString
                    parent_id = nivel2(i)(3).ToString

                    For Each item2 As ListViewItem In ARBOL_CATEGORIAS.lista.Items
                        If item2.Text = parent_id Then
                            item2.Checked = True
                        End If
                    Next
                Next
            End If
        Next
    End Sub

    Public Sub segundo_nivel()
        Dim i As Integer
        Dim nivel2 As DataRow()
        Dim parent_id As String = ""

        For Each item As ListViewItem In ARBOL_CATEGORIAS.lista2.Items
            Dim id As String = ""
            If item.Checked = True Then
                Dim condicion As String = "Id =" & item.Text
                nivel2 = tabla.Select(condicion)

                For i = 0 To (nivel2.Length - 1)
                    id = nivel2(i)(0).ToString
                    parent_id = nivel2(i)(3).ToString

                    For Each item2 As ListViewItem In ARBOL_CATEGORIAS.lista.Items
                        If item2.Text = parent_id Then
                            item2.Checked = True
                        End If
                    Next
                Next
            End If
        Next
    End Sub
    ' ****************************************************************** FIN MOVIMIENTOS DE ARBOL ****************************************************************************************
End Class

