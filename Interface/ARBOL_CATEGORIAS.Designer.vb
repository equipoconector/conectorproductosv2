﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ARBOL_CATEGORIAS
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lista2 = New System.Windows.Forms.ListView()
        Me.id = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lista = New System.Windows.Forms.ListView()
        Me.busqueda = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lista5 = New System.Windows.Forms.ListView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lista4 = New System.Windows.Forms.ListView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lista3 = New System.Windows.Forms.ListView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(57, 28)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(132, 13)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "PRODUCTO AGRUPADO"
        '
        'lista2
        '
        Me.lista2.CheckBoxes = True
        Me.lista2.GridLines = True
        Me.lista2.Location = New System.Drawing.Point(406, 121)
        Me.lista2.Name = "lista2"
        Me.lista2.Size = New System.Drawing.Size(328, 652)
        Me.lista2.TabIndex = 31
        Me.lista2.UseCompatibleStateImageBehavior = False
        '
        'id
        '
        Me.id.Location = New System.Drawing.Point(195, 25)
        Me.id.Name = "id"
        Me.id.Size = New System.Drawing.Size(118, 20)
        Me.id.TabIndex = 30
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(466, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 36)
        Me.Button1.TabIndex = 29
        Me.Button1.Text = "ACTUALIZAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lista
        '
        Me.lista.CheckBoxes = True
        Me.lista.GridLines = True
        Me.lista.Location = New System.Drawing.Point(60, 121)
        Me.lista.Name = "lista"
        Me.lista.Size = New System.Drawing.Size(329, 652)
        Me.lista.TabIndex = 28
        Me.lista.UseCompatibleStateImageBehavior = False
        '
        'busqueda
        '
        Me.busqueda.Location = New System.Drawing.Point(336, 16)
        Me.busqueda.Name = "busqueda"
        Me.busqueda.Size = New System.Drawing.Size(90, 36)
        Me.busqueda.TabIndex = 27
        Me.busqueda.Text = "BUSCAR"
        Me.busqueda.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(1506, 90)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(83, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "QUINTO NIVEL"
        '
        'lista5
        '
        Me.lista5.CheckBoxes = True
        Me.lista5.GridLines = True
        Me.lista5.Location = New System.Drawing.Point(1509, 121)
        Me.lista5.Name = "lista5"
        Me.lista5.Size = New System.Drawing.Size(332, 652)
        Me.lista5.TabIndex = 25
        Me.lista5.UseCompatibleStateImageBehavior = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(1128, 90)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 13)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "CUARTO NIVEL"
        '
        'lista4
        '
        Me.lista4.CheckBoxes = True
        Me.lista4.GridLines = True
        Me.lista4.Location = New System.Drawing.Point(1131, 121)
        Me.lista4.Name = "lista4"
        Me.lista4.Size = New System.Drawing.Size(360, 652)
        Me.lista4.TabIndex = 23
        Me.lista4.UseCompatibleStateImageBehavior = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(748, 90)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 13)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "TERCER NIVEL"
        '
        'lista3
        '
        Me.lista3.CheckBoxes = True
        Me.lista3.GridLines = True
        Me.lista3.Location = New System.Drawing.Point(751, 121)
        Me.lista3.Name = "lista3"
        Me.lista3.Size = New System.Drawing.Size(356, 652)
        Me.lista3.TabIndex = 21
        Me.lista3.UseCompatibleStateImageBehavior = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(415, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "SEGUNDO NIVEL"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(57, 90)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 13)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "PRIMER NIVEL"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(1725, 16)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(116, 36)
        Me.Button2.TabIndex = 33
        Me.Button2.Text = "CERRAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'ARBOL_CATEGORIAS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1885, 826)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lista2)
        Me.Controls.Add(Me.id)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lista)
        Me.Controls.Add(Me.busqueda)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lista5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lista4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lista3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "ARBOL_CATEGORIAS"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ARBOL CATEGORIAS"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lista2 As System.Windows.Forms.ListView
    Friend WithEvents id As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lista As System.Windows.Forms.ListView
    Friend WithEvents busqueda As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lista5 As System.Windows.Forms.ListView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lista4 As System.Windows.Forms.ListView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lista3 As System.Windows.Forms.ListView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
