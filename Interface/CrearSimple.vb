﻿Public Class CrearSimple

    Public navconectar As CONECTOR_NAVISION = New CONECTOR_NAVISION
    Public conect As web = New web
    Public sqlconexion As SQLCONECTAR = New SQLCONECTAR

    Private Sub btn_Cerrar_Click(sender As Object, e As EventArgs)
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub btn_Cerrar_Click_1(sender As Object, e As EventArgs) Handles btn_Cerrar.Click
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub btn_Crear_Click(sender As Object, e As EventArgs) Handles btn_Crear.Click
        If Me.txtSku.Text <> "" And Me.txtPvp.Text <> "" And Me.ComboEstado.Text <> "" And Me.ComboIva.Text <> "" And Me.txtPeso.Text <> "" And Me.txtAlto.Text <> "" And Me.txtAncho.Text <> "" And Me.txtProfundo.Text <> "" Then
            conect.nuevoSimple()
        Else
            MsgBox("Faltan datos para crear el Producto Simple")
        End If
    End Sub

    Private Sub btn_Calcular_Click(sender As Object, e As EventArgs) Handles btn_Calcular.Click
        Dim porcentaje As Decimal
        Dim resultado As Decimal

        With Me
            If CDec(.txtPvp.Text) <> 0 And CDec(.txtCoste.Text) <> 0 Then

                porcentaje = (CDec(.txtPvp.Text) - CDec(.txtCoste.Text)) * 100 / CDec(.txtPvp.Text)

                If (porcentaje >= 33.33) And (porcentaje < 37.5) Then
                    resultado = 5
                End If

                If (porcentaje >= 37.4) And (porcentaje < 41.18) Then
                    resultado = 10
                End If

                If (porcentaje >= 41.18) And (porcentaje < 44.44) Then
                    resultado = 15
                End If

                If (porcentaje >= 44.44) And (porcentaje < 50) Then
                    resultado = 20
                End If

                If (porcentaje >= 50) And (porcentaje < 60) Then
                    resultado = 25
                End If

                If (porcentaje >= 60) And (porcentaje < 66.67) Then
                    resultado = 30
                End If

                If (porcentaje >= 66.67) And (porcentaje <= 71.43) Then
                    resultado = 35
                End If

                If porcentaje > 71.43 Then
                    resultado = 40
                End If

                .txtPvd.Text = CStr(CDec(.txtPvp.Text) - ((CDec(.txtPvp.Text) * resultado)) / 100)

            End If
        End With
    End Sub

    Private Sub CrearSimple_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        sqlconexion.listado_Marcas_Nuevo()
        sqlconexion.listado_Eans_Nuevo()
    End Sub

    Private Sub Combo_eans_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Combo_eans.SelectedIndexChanged
        Me.txtEan.Text = CStr(Me.Combo_eans.SelectedItem)
    End Sub

    Private Sub Combo_marcas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Combo_marcas.SelectedIndexChanged
        Me.txtMarca.Text = CStr(Me.Combo_marcas.SelectedItem)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Marcas.ShowDialog()
    End Sub
End Class