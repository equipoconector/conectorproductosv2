﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ModificarSimple
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_Modificar = New System.Windows.Forms.Button()
        Me.ComboIva = New System.Windows.Forms.ComboBox()
        Me.txtValor = New System.Windows.Forms.TextBox()
        Me.txtPvp = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtSku = New System.Windows.Forms.TextBox()
        Me.btn_Cerrar = New System.Windows.Forms.Button()
        Me.lbl_Estado = New System.Windows.Forms.Label()
        Me.lbl_Id_Categoria = New System.Windows.Forms.Label()
        Me.lbl_Desc_Corta = New System.Windows.Forms.Label()
        Me.lbl_Nombre = New System.Windows.Forms.Label()
        Me.lbl_Sku = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtProfundo = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtAncho = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtAlto = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPeso = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCoste = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtPvd = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btn_Calcular = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtEan = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtMarca = New System.Windows.Forms.TextBox()
        Me.ComboEstado = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.chkDescatalogar = New System.Windows.Forms.CheckBox()
        Me.txtProveedor = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtRefProveedor = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtPlazoEntrega = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtUbicacion = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Combo_marcas = New System.Windows.Forms.ComboBox()
        Me.Combo_eans = New System.Windows.Forms.ComboBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtValor_Anterior = New System.Windows.Forms.TextBox()
        Me.txtValor_Rev = New System.Windows.Forms.TextBox()
        Me.Image = New System.Windows.Forms.PictureBox()
        Me.lblUploadExito = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btn_Examinar = New System.Windows.Forms.Button()
        Me.txtFile = New System.Windows.Forms.TextBox()
        Me.btn_Upload = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.Image, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(36, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(161, 24)
        Me.Label1.TabIndex = 57
        Me.Label1.Text = "Producto simple"
        '
        'btn_Modificar
        '
        Me.btn_Modificar.Enabled = False
        Me.btn_Modificar.Location = New System.Drawing.Point(740, 688)
        Me.btn_Modificar.Name = "btn_Modificar"
        Me.btn_Modificar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Modificar.TabIndex = 53
        Me.btn_Modificar.Text = "Modificar"
        Me.btn_Modificar.UseVisualStyleBackColor = True
        '
        'ComboIva
        '
        Me.ComboIva.FormattingEnabled = True
        Me.ComboIva.Items.AddRange(New Object() {"GEN", "RED", "SRE"})
        Me.ComboIva.Location = New System.Drawing.Point(198, 246)
        Me.ComboIva.Name = "ComboIva"
        Me.ComboIva.Size = New System.Drawing.Size(121, 21)
        Me.ComboIva.TabIndex = 52
        '
        'txtValor
        '
        Me.txtValor.Location = New System.Drawing.Point(177, 144)
        Me.txtValor.Name = "txtValor"
        Me.txtValor.Size = New System.Drawing.Size(420, 20)
        Me.txtValor.TabIndex = 51
        '
        'txtPvp
        '
        Me.txtPvp.Location = New System.Drawing.Point(198, 207)
        Me.txtPvp.Name = "txtPvp"
        Me.txtPvp.Size = New System.Drawing.Size(100, 20)
        Me.txtPvp.TabIndex = 50
        '
        'txtNombre
        '
        Me.txtNombre.Enabled = False
        Me.txtNombre.Location = New System.Drawing.Point(176, 107)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(731, 20)
        Me.txtNombre.TabIndex = 49
        '
        'txtSku
        '
        Me.txtSku.Enabled = False
        Me.txtSku.Location = New System.Drawing.Point(176, 70)
        Me.txtSku.Name = "txtSku"
        Me.txtSku.Size = New System.Drawing.Size(204, 20)
        Me.txtSku.TabIndex = 48
        '
        'btn_Cerrar
        '
        Me.btn_Cerrar.Location = New System.Drawing.Point(832, 688)
        Me.btn_Cerrar.Name = "btn_Cerrar"
        Me.btn_Cerrar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cerrar.TabIndex = 47
        Me.btn_Cerrar.Text = "Cerrar"
        Me.btn_Cerrar.UseVisualStyleBackColor = True
        '
        'lbl_Estado
        '
        Me.lbl_Estado.AutoSize = True
        Me.lbl_Estado.Location = New System.Drawing.Point(96, 249)
        Me.lbl_Estado.Name = "lbl_Estado"
        Me.lbl_Estado.Size = New System.Drawing.Size(24, 13)
        Me.lbl_Estado.TabIndex = 45
        Me.lbl_Estado.Text = "IVA"
        '
        'lbl_Id_Categoria
        '
        Me.lbl_Id_Categoria.AutoSize = True
        Me.lbl_Id_Categoria.Location = New System.Drawing.Point(101, 144)
        Me.lbl_Id_Categoria.Name = "lbl_Id_Categoria"
        Me.lbl_Id_Categoria.Size = New System.Drawing.Size(31, 13)
        Me.lbl_Id_Categoria.TabIndex = 44
        Me.lbl_Id_Categoria.Text = "Valor"
        '
        'lbl_Desc_Corta
        '
        Me.lbl_Desc_Corta.AutoSize = True
        Me.lbl_Desc_Corta.Location = New System.Drawing.Point(95, 210)
        Me.lbl_Desc_Corta.Name = "lbl_Desc_Corta"
        Me.lbl_Desc_Corta.Size = New System.Drawing.Size(28, 13)
        Me.lbl_Desc_Corta.TabIndex = 42
        Me.lbl_Desc_Corta.Text = "PVP"
        '
        'lbl_Nombre
        '
        Me.lbl_Nombre.AutoSize = True
        Me.lbl_Nombre.Location = New System.Drawing.Point(98, 110)
        Me.lbl_Nombre.Name = "lbl_Nombre"
        Me.lbl_Nombre.Size = New System.Drawing.Size(44, 13)
        Me.lbl_Nombre.TabIndex = 41
        Me.lbl_Nombre.Text = "Nombre"
        '
        'lbl_Sku
        '
        Me.lbl_Sku.AutoSize = True
        Me.lbl_Sku.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Sku.Location = New System.Drawing.Point(73, 73)
        Me.lbl_Sku.Name = "lbl_Sku"
        Me.lbl_Sku.Size = New System.Drawing.Size(98, 13)
        Me.lbl_Sku.TabIndex = 40
        Me.lbl_Sku.Text = "Referencia-SKU"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtProfundo)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtAncho)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtAlto)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtPeso)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(66, 303)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(830, 78)
        Me.GroupBox1.TabIndex = 58
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Peso y medidas"
        '
        'txtProfundo
        '
        Me.txtProfundo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProfundo.Location = New System.Drawing.Point(567, 32)
        Me.txtProfundo.Name = "txtProfundo"
        Me.txtProfundo.Size = New System.Drawing.Size(73, 20)
        Me.txtProfundo.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(494, 36)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Profundo"
        '
        'txtAncho
        '
        Me.txtAncho.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAncho.Location = New System.Drawing.Point(367, 33)
        Me.txtAncho.Name = "txtAncho"
        Me.txtAncho.Size = New System.Drawing.Size(89, 20)
        Me.txtAncho.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(306, 36)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Ancho"
        '
        'txtAlto
        '
        Me.txtAlto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAlto.Location = New System.Drawing.Point(190, 34)
        Me.txtAlto.Name = "txtAlto"
        Me.txtAlto.Size = New System.Drawing.Size(85, 20)
        Me.txtAlto.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(159, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(25, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Alto"
        '
        'txtPeso
        '
        Me.txtPeso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeso.Location = New System.Drawing.Point(69, 37)
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.Size = New System.Drawing.Size(74, 20)
        Me.txtPeso.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(31, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Peso"
        '
        'txtCoste
        '
        Me.txtCoste.Location = New System.Drawing.Point(408, 206)
        Me.txtCoste.Name = "txtCoste"
        Me.txtCoste.Size = New System.Drawing.Size(100, 20)
        Me.txtCoste.TabIndex = 59
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(334, 209)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 60
        Me.Label6.Text = "Precio Coste"
        '
        'txtPvd
        '
        Me.txtPvd.Location = New System.Drawing.Point(681, 206)
        Me.txtPvd.Name = "txtPvd"
        Me.txtPvd.Size = New System.Drawing.Size(100, 20)
        Me.txtPvd.TabIndex = 61
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(566, 209)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(92, 13)
        Me.Label7.TabIndex = 62
        Me.Label7.Text = "Precio Distribuidor"
        '
        'btn_Calcular
        '
        Me.btn_Calcular.Location = New System.Drawing.Point(806, 204)
        Me.btn_Calcular.Name = "btn_Calcular"
        Me.btn_Calcular.Size = New System.Drawing.Size(75, 23)
        Me.btn_Calcular.TabIndex = 63
        Me.btn_Calcular.Text = "Calcular"
        Me.btn_Calcular.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(101, 415)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(29, 13)
        Me.Label8.TabIndex = 64
        Me.Label8.Text = "EAN"
        '
        'txtEan
        '
        Me.txtEan.Location = New System.Drawing.Point(134, 411)
        Me.txtEan.Name = "txtEan"
        Me.txtEan.Size = New System.Drawing.Size(177, 20)
        Me.txtEan.TabIndex = 65
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(542, 418)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 13)
        Me.Label9.TabIndex = 66
        Me.Label9.Text = "Marca"
        '
        'txtMarca
        '
        Me.txtMarca.Location = New System.Drawing.Point(716, 472)
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.Size = New System.Drawing.Size(164, 20)
        Me.txtMarca.TabIndex = 67
        Me.txtMarca.Visible = False
        '
        'ComboEstado
        '
        Me.ComboEstado.FormattingEnabled = True
        Me.ComboEstado.Items.AddRange(New Object() {"OCULTO", "VISIBLE"})
        Me.ComboEstado.Location = New System.Drawing.Point(179, 575)
        Me.ComboEstado.Name = "ComboEstado"
        Me.ComboEstado.Size = New System.Drawing.Size(121, 21)
        Me.ComboEstado.TabIndex = 68
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(105, 575)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 13)
        Me.Label10.TabIndex = 69
        Me.Label10.Text = "Estado"
        '
        'chkDescatalogar
        '
        Me.chkDescatalogar.AutoSize = True
        Me.chkDescatalogar.Location = New System.Drawing.Point(354, 579)
        Me.chkDescatalogar.Name = "chkDescatalogar"
        Me.chkDescatalogar.Size = New System.Drawing.Size(97, 17)
        Me.chkDescatalogar.TabIndex = 70
        Me.chkDescatalogar.Text = "A descatalogar"
        Me.chkDescatalogar.UseVisualStyleBackColor = True
        '
        'txtProveedor
        '
        Me.txtProveedor.Location = New System.Drawing.Point(181, 453)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Size = New System.Drawing.Size(100, 20)
        Me.txtProveedor.TabIndex = 71
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(101, 453)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(56, 13)
        Me.Label11.TabIndex = 72
        Me.Label11.Text = "Proveedor"
        '
        'txtRefProveedor
        '
        Me.txtRefProveedor.Location = New System.Drawing.Point(426, 450)
        Me.txtRefProveedor.Name = "txtRefProveedor"
        Me.txtRefProveedor.Size = New System.Drawing.Size(108, 20)
        Me.txtRefProveedor.TabIndex = 73
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(299, 453)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(110, 13)
        Me.Label12.TabIndex = 74
        Me.Label12.Text = "Referencia proveedor"
        '
        'txtPlazoEntrega
        '
        Me.txtPlazoEntrega.Location = New System.Drawing.Point(196, 494)
        Me.txtPlazoEntrega.Name = "txtPlazoEntrega"
        Me.txtPlazoEntrega.Size = New System.Drawing.Size(100, 20)
        Me.txtPlazoEntrega.TabIndex = 75
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(99, 496)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 13)
        Me.Label13.TabIndex = 76
        Me.Label13.Text = "Plazo entrega"
        '
        'txtUbicacion
        '
        Me.txtUbicacion.Location = New System.Drawing.Point(413, 494)
        Me.txtUbicacion.Name = "txtUbicacion"
        Me.txtUbicacion.Size = New System.Drawing.Size(100, 20)
        Me.txtUbicacion.TabIndex = 77
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(336, 497)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(55, 13)
        Me.Label14.TabIndex = 78
        Me.Label14.Text = "Ubicación"
        '
        'GroupBox2
        '
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(66, 178)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(841, 100)
        Me.GroupBox2.TabIndex = 79
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Precios"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Controls.Add(Me.Combo_marcas)
        Me.GroupBox3.Controls.Add(Me.Combo_eans)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(65, 387)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(831, 138)
        Me.GroupBox3.TabIndex = 80
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Almacen"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(727, 26)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(98, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Crear Marca"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Combo_marcas
        '
        Me.Combo_marcas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo_marcas.FormattingEnabled = True
        Me.Combo_marcas.Location = New System.Drawing.Point(525, 26)
        Me.Combo_marcas.Name = "Combo_marcas"
        Me.Combo_marcas.Size = New System.Drawing.Size(183, 21)
        Me.Combo_marcas.TabIndex = 1
        '
        'Combo_eans
        '
        Me.Combo_eans.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Combo_eans.FormattingEnabled = True
        Me.Combo_eans.Location = New System.Drawing.Point(258, 25)
        Me.Combo_eans.Name = "Combo_eans"
        Me.Combo_eans.Size = New System.Drawing.Size(171, 21)
        Me.Combo_eans.TabIndex = 0
        '
        'GroupBox4
        '
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(66, 543)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(520, 72)
        Me.GroupBox4.TabIndex = 81
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Estado"
        '
        'txtValor_Anterior
        '
        Me.txtValor_Anterior.Location = New System.Drawing.Point(604, 144)
        Me.txtValor_Anterior.Name = "txtValor_Anterior"
        Me.txtValor_Anterior.Size = New System.Drawing.Size(10, 20)
        Me.txtValor_Anterior.TabIndex = 82
        Me.txtValor_Anterior.Visible = False
        '
        'txtValor_Rev
        '
        Me.txtValor_Rev.Location = New System.Drawing.Point(621, 144)
        Me.txtValor_Rev.Name = "txtValor_Rev"
        Me.txtValor_Rev.Size = New System.Drawing.Size(11, 20)
        Me.txtValor_Rev.TabIndex = 83
        Me.txtValor_Rev.Visible = False
        '
        'Image
        '
        Me.Image.Location = New System.Drawing.Point(65, 624)
        Me.Image.Name = "Image"
        Me.Image.Size = New System.Drawing.Size(156, 87)
        Me.Image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Image.TabIndex = 89
        Me.Image.TabStop = False
        '
        'lblUploadExito
        '
        Me.lblUploadExito.AutoSize = True
        Me.lblUploadExito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUploadExito.Location = New System.Drawing.Point(775, 634)
        Me.lblUploadExito.Name = "lblUploadExito"
        Me.lblUploadExito.Size = New System.Drawing.Size(124, 13)
        Me.lblUploadExito.TabIndex = 88
        Me.lblUploadExito.Text = "¡¡¡ Upload Imágen !!!"
        Me.lblUploadExito.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(227, 630)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(42, 13)
        Me.Label15.TabIndex = 87
        Me.Label15.Text = "Imágen"
        '
        'btn_Examinar
        '
        Me.btn_Examinar.Location = New System.Drawing.Point(612, 628)
        Me.btn_Examinar.Name = "btn_Examinar"
        Me.btn_Examinar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Examinar.TabIndex = 86
        Me.btn_Examinar.Text = "Examinar"
        Me.btn_Examinar.UseVisualStyleBackColor = True
        '
        'txtFile
        '
        Me.txtFile.Location = New System.Drawing.Point(282, 628)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.Size = New System.Drawing.Size(324, 20)
        Me.txtFile.TabIndex = 85
        '
        'btn_Upload
        '
        Me.btn_Upload.Location = New System.Drawing.Point(693, 629)
        Me.btn_Upload.Name = "btn_Upload"
        Me.btn_Upload.Size = New System.Drawing.Size(75, 23)
        Me.btn_Upload.TabIndex = 84
        Me.btn_Upload.Text = "Upload File"
        Me.btn_Upload.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ModificarSimple
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(936, 723)
        Me.ControlBox = False
        Me.Controls.Add(Me.Image)
        Me.Controls.Add(Me.lblUploadExito)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.btn_Examinar)
        Me.Controls.Add(Me.txtFile)
        Me.Controls.Add(Me.btn_Upload)
        Me.Controls.Add(Me.txtValor_Rev)
        Me.Controls.Add(Me.txtValor_Anterior)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.txtUbicacion)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.txtPlazoEntrega)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtRefProveedor)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtProveedor)
        Me.Controls.Add(Me.chkDescatalogar)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.ComboEstado)
        Me.Controls.Add(Me.txtMarca)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtEan)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.btn_Calcular)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtPvd)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtCoste)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_Modificar)
        Me.Controls.Add(Me.ComboIva)
        Me.Controls.Add(Me.txtValor)
        Me.Controls.Add(Me.txtPvp)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.txtSku)
        Me.Controls.Add(Me.btn_Cerrar)
        Me.Controls.Add(Me.lbl_Estado)
        Me.Controls.Add(Me.lbl_Id_Categoria)
        Me.Controls.Add(Me.lbl_Desc_Corta)
        Me.Controls.Add(Me.lbl_Nombre)
        Me.Controls.Add(Me.lbl_Sku)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "ModificarSimple"
        Me.Text = "Modificar Producto Simple"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.Image, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_Modificar As System.Windows.Forms.Button
    Friend WithEvents ComboIva As System.Windows.Forms.ComboBox
    Friend WithEvents txtValor As System.Windows.Forms.TextBox
    Friend WithEvents txtPvp As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtSku As System.Windows.Forms.TextBox
    Friend WithEvents btn_Cerrar As System.Windows.Forms.Button
    Friend WithEvents lbl_Estado As System.Windows.Forms.Label
    Friend WithEvents lbl_Id_Categoria As System.Windows.Forms.Label
    Friend WithEvents lbl_Desc_Corta As System.Windows.Forms.Label
    Friend WithEvents lbl_Nombre As System.Windows.Forms.Label
    Friend WithEvents lbl_Sku As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtAncho As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtAlto As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPeso As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtProfundo As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCoste As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPvd As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btn_Calcular As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtEan As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtMarca As System.Windows.Forms.TextBox
    Friend WithEvents ComboEstado As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents chkDescatalogar As System.Windows.Forms.CheckBox
    Friend WithEvents txtProveedor As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtRefProveedor As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtPlazoEntrega As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtUbicacion As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtValor_Anterior As System.Windows.Forms.TextBox
    Friend WithEvents txtValor_Rev As System.Windows.Forms.TextBox
    Friend WithEvents Image As System.Windows.Forms.PictureBox
    Friend WithEvents lblUploadExito As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents btn_Examinar As System.Windows.Forms.Button
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents btn_Upload As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Combo_eans As System.Windows.Forms.ComboBox
    Friend WithEvents Combo_marcas As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
