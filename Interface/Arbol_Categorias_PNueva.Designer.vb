﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Arbol_Categorias_PNueva
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_Cerrar = New System.Windows.Forms.Button()
        Me.Tree = New System.Windows.Forms.TreeView()
        Me.txt_Categorias = New System.Windows.Forms.TextBox()
        Me.txt_nLevel = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btn_Cerrar
        '
        Me.btn_Cerrar.Location = New System.Drawing.Point(739, 564)
        Me.btn_Cerrar.Name = "btn_Cerrar"
        Me.btn_Cerrar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cerrar.TabIndex = 0
        Me.btn_Cerrar.Text = "Cerrar"
        Me.btn_Cerrar.UseVisualStyleBackColor = True
        '
        'Tree
        '
        Me.Tree.CheckBoxes = True
        Me.Tree.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tree.Location = New System.Drawing.Point(31, 24)
        Me.Tree.Name = "Tree"
        Me.Tree.Size = New System.Drawing.Size(623, 563)
        Me.Tree.TabIndex = 1
        '
        'txt_Categorias
        '
        Me.txt_Categorias.Enabled = False
        Me.txt_Categorias.Location = New System.Drawing.Point(672, 62)
        Me.txt_Categorias.Multiline = True
        Me.txt_Categorias.Name = "txt_Categorias"
        Me.txt_Categorias.Size = New System.Drawing.Size(164, 77)
        Me.txt_Categorias.TabIndex = 3
        Me.txt_Categorias.Visible = False
        '
        'txt_nLevel
        '
        Me.txt_nLevel.Enabled = False
        Me.txt_nLevel.Location = New System.Drawing.Point(768, 145)
        Me.txt_nLevel.Name = "txt_nLevel"
        Me.txt_nLevel.Size = New System.Drawing.Size(68, 20)
        Me.txt_nLevel.TabIndex = 4
        Me.txt_nLevel.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(672, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Path"
        Me.Label1.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(727, 148)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Nivel"
        Me.Label2.Visible = False
        '
        'Arbol_Categorias_PNueva
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(848, 607)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_nLevel)
        Me.Controls.Add(Me.txt_Categorias)
        Me.Controls.Add(Me.Tree)
        Me.Controls.Add(Me.btn_Cerrar)
        Me.Name = "Arbol_Categorias_PNueva"
        Me.Text = "Categorias"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Cerrar As System.Windows.Forms.Button
    Friend WithEvents Tree As System.Windows.Forms.TreeView
    Friend WithEvents txt_Categorias As System.Windows.Forms.TextBox
    Friend WithEvents txt_nLevel As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
