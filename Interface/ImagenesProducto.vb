﻿Public Class ImagenesProducto

    Public conect As web = New web
    Public Pictureboxes As List(Of PictureBox) = New List(Of PictureBox)

    Private Sub btn_Cerrar_Click(sender As Object, e As EventArgs) Handles btn_Cerrar.Click
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub ImagenesProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Dim Pictureboxes As List(Of PictureBox) = New List(Of PictureBox)
        Dim Labels As List(Of Label) = New List(Of Label)

        Dim contador As Integer = 0
        For N As Integer = 0 To 3
            For I As Integer = 0 To 4
                Dim P As PictureBox = New PictureBox
                Dim L As Label = New Label
                P.Size = New Size(200, 150)
                P.Location = New Point(I * 200 + 20, N * 150 + 20)
                P.BorderStyle = BorderStyle.FixedSingle
                P.SizeMode = PictureBoxSizeMode.StretchImage
                Pictureboxes.Add(P)
                L.Size = New Size(80, 20)
                L.Location = New Point(I * 200 + 20, (N + 1) * 150 + 20)
                L.Text = ModificarProducto.txtId.Text & "_" & contador & ".jpg"
                contador = contador + 1
                Labels.Add(L)
                Me.Controls.Add(P)
                Me.Controls.Add(L)                
            Next I
        Next N

        Dim carpeta As String = Mid(ModificarProducto.txtId.Text, 1, 1)
        Dim subcarpeta As String = Mid(ModificarProducto.txtId.Text, 2, 1)
        For H As Integer = 0 To 19
            Pictureboxes(H).ImageLocation = "http://quirumed.dev.onestic.com/media/catalog/product/" & carpeta & "/" & subcarpeta & "/" & ModificarProducto.txtId.Text & "_" & H & ".jpg"
            Labels(H).Show()
        Next

    End Sub

    Private Sub btn_Examinar_Click(sender As Object, e As EventArgs) Handles btn_Examinar.Click
        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            txtFile.Text = OpenFileDialog1.FileName            
        End If
        If (txtFile.Text <> "" And txt_NumeroImagen.Text <> "") Then
            Me.btn_Upload.Enabled = True
        End If
    End Sub

    Private Sub btn_Upload_Click(sender As Object, e As EventArgs) Handles btn_Upload.Click
        If Me.txtFile.Text <> "" And (CInt(Me.txt_NumeroImagen.Text) >= 0 And CInt(Me.txt_NumeroImagen.Text) <= 19) Then
            conect.uploadImagenProducto()
        Else
            MsgBox("Hay que introducir un numero válido de imagen")
        End If
        Me.Refresh()
    End Sub

    Private Sub txt_NumeroImagen_TextChanged(sender As Object, e As EventArgs) Handles txt_NumeroImagen.TextChanged
        Me.btn_Upload.Enabled = True
    End Sub
End Class