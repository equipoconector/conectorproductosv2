﻿Imports MySql.Data.MySqlClient
Imports [Interface].PRODUCTOS
Imports [Interface].MagentoService
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.IO
Imports System.Net
Imports System.Web
Imports System.Text
Imports System
Imports System.Xml
Imports System.Threading
Imports MSXML2
Imports System.Drawing.Imaging.ImageFormat
Imports WinSCP

Public Class Arbol_Categorias_PNueva

    Public conect As web = New web
    Dim conexion As MySqlConnection

    Dim aIndex(5) As Integer
    Dim nLevel As Integer
    Dim oNode As TreeNode
    Dim primer_check As Integer = 0

    Private Sub btn_Cerrar_Click(sender As Object, e As EventArgs) Handles btn_Cerrar.Click
        primer_check = 0
        Me.Close()
    End Sub

    Private Sub Arbol_Categorias_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Tree.Nodes.Clear()

        conexion = conect.conexion
        conexion.Open()
        Dim consulta As String = "SELECT id, nombre, parent_id FROM categorias WHERE parent_id=0 and no_exportar=0 and link_categoria_id=0"
        Dim cmd As MySqlCommand = New MySqlCommand(consulta, conexion)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader

        'Pasar los datos del resultado de la consulta a un array
        Dim index As Integer = 0
        Dim id(100) As String
        Dim nombre(100) As String
        Dim parent_id(100) As String
        While lector.Read <> False
            id(index) = CStr(lector("id"))
            nombre(index) = CStr(lector("nombre"))
            parent_id(index) = CStr(lector("parent_id"))
            index += 1
        End While
        conexion.Close()

        Dim i As Integer
        For n = 0 To index - 1
            aIndex(nLevel) = i
            oNode = New TreeNode
            oNode.Text = ConvertUTF8toUnicode(nombre(n))
            oNode.Tag = id(n)
            Tree.Nodes.Add(oNode)

            'si quiero que tenga mas de un nivel mando a llamar la siguiente funcion 
            Find_Items(CInt(id(n)))
            i += 1
        Next

        Tree.EndUpdate()

    End Sub

    Private Sub Find_Items(ByVal nId As Integer)

        nLevel += 1

        conexion = conect.conexion
        conexion.Open()
        Dim consulta As String = "SELECT id, nombre, parent_id FROM categorias WHERE parent_id = " & nId & " and no_exportar=0 and link_categoria_id=0"
        Dim cmd As MySqlCommand = New MySqlCommand(consulta, conexion)
        Dim lector As MySql.Data.MySqlClient.MySqlDataReader = cmd.ExecuteReader

        'Pasar los datos del resultado de la consulta a un array
        Dim index As Integer = 0
        Dim id(100) As String
        Dim nombre(100) As String
        Dim parent_id(100) As String
        While lector.Read <> False
            id(index) = CStr(lector("id"))
            nombre(index) = CStr(lector("nombre"))
            parent_id(index) = CStr(lector("parent_id"))
            index += 1
        End While
        conexion.Close()

        Dim oNode As TreeNode
        Dim i As Integer
        For n = 0 To index - 1
            aIndex(nLevel) = i
            oNode = New TreeNode
            oNode.Text = ConvertUTF8toUnicode(nombre(n))
            oNode.Tag = id(n)
            oNode.Expand()

            Select Case nLevel
                Case 1
                    Tree.Nodes(aIndex(0)).Nodes.Add(oNode)
                Case 2
                    Tree.Nodes(aIndex(0)).Nodes(aIndex(1)).Nodes.Add(oNode)
                Case 3
                    Tree.Nodes(aIndex(0)).Nodes(aIndex(1)).Nodes(aIndex(2)).Nodes.Add(oNode)
                Case 4
                    Tree.Nodes(aIndex(0)).Nodes(aIndex(1)).Nodes(aIndex(2)).Nodes(aIndex(3)).Nodes.Add(oNode)
                Case 5
                    Tree.Nodes(aIndex(0)).Nodes(aIndex(1)).Nodes(aIndex(2)).Nodes(aIndex(3)).Nodes(aIndex(4)).Nodes.Add(oNode)
            End Select

            'Hago la recursividad para llenar mis datos de la tabla en n niveles llamando la funcion
            Find_Items(CInt(id(n)))

            i += 1
        Next
        nLevel -= 1

    End Sub

    Public Function ConvertUTF8toUnicode(cadena As String) As String
        'JOSE

        'Convertir cadena UTF8 en Unicode (en los SELECT)
        Dim UTF8String As String = cadena
        Dim dstEncoding As Encoding = Encoding.Unicode
        Dim dstEncodingANSI As Encoding = Encoding.GetEncoding(1252)
        Dim srcEncoding As Encoding = Encoding.UTF8
        Dim srcBytes As Byte() = dstEncodingANSI.GetBytes(UTF8String)
        Dim dstBytes As Byte() = Encoding.Convert(srcEncoding, dstEncoding, srcBytes)
        Dim salida As String = dstEncoding.GetString(dstBytes)
        Return salida
    End Function

    Private Sub Tree_AfterCheck(sender As Object, e As TreeViewEventArgs) Handles Tree.AfterCheck
        'MsgBox("La categoría " & e.Node.Text & " con ID: " & CInt(e.Node.Tag) & " la has puesto a " & e.Node.Checked & " y su nivel es " & e.Node.Level + 1 & " y su rama padre es " & e.Node.FullPath)

        If primer_check = 0 Then
            Dim id As Integer = CInt(e.Node.Tag)
            Dim path As String = e.Node.FullPath
            CrearProducto.txt_Id_categoria.Text = CStr(id)
            CrearProducto.txt_Nombre_Categoria.Text = path
            txt_nLevel.Text = CStr(e.Node.Level + 1)
        End If

        If Not e.Node.Parent Is Nothing Then
            primer_check = 1
            e.Node.Parent.Checked = True
        End If

        If e.Node.Level = 0 Then
            If e.Node.Checked = True Then
                If txt_Categorias.Text = "" Then
                    CallRecursive(Tree)
                Else
                    txt_Categorias.Text = ""
                    CallRecursive(Tree)
                End If
                'eliminar la última coma de la cadena
                If txt_Categorias.Text <> "" Then
                    txt_Categorias.Text = Mid(txt_Categorias.Text, 1, Len(txt_Categorias.Text) - 1)
                End If
            End If
            primer_check = 0
            Tree.Enabled = False
        End If
    End Sub

    Private Sub Tree_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles Tree.AfterSelect
        'Tree.SelectedNode.Nodes.Add(oNode)
    End Sub

    Private Sub PrintRecursive(ByVal n As TreeNode)
        '*** Es aqui donde añado lo que necesito guardar de cada nodo ***  
        Dim aNode As TreeNode
        'Por cada nodo de la raiz
        For Each aNode In n.Nodes
            PrintRecursive(aNode)
            If aNode.Checked Then
                aNode.ForeColor = Color.Red
                txt_Categorias.Text += CStr(aNode.Tag) & ","
            End If
        Next
    End Sub

    Private Sub CallRecursive(ByVal aTreeView As TreeView)
        Dim n As TreeNode
        'Por cada raíz
        For Each n In aTreeView.Nodes
            PrintRecursive(n)
            If n.Checked Then
                n.ForeColor = Color.Red
                txt_Categorias.Text += CStr(n.Tag) & ","
            End If
        Next
    End Sub

End Class