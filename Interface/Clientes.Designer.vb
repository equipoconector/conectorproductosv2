﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Clientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtClienteID = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtApellidos = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtPoblacion = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCpostal = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtProvincia = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtPais = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtTelefono2 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtFax = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtTipoEmpresa = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtTipoCliente = New System.Windows.Forms.TextBox()
        Me.btn_Cerrar = New System.Windows.Forms.Button()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtEnvioPais = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtEnvioProvincia = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtEnvioCpostal = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtEnvioPoblacion = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtEnvioDireccion = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtEnvioApellidos = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtEnvioNombre = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtDistribuidor = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtTipo = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'txtClienteID
        '
        Me.txtClienteID.Location = New System.Drawing.Point(109, 49)
        Me.txtClienteID.Name = "txtClienteID"
        Me.txtClienteID.Size = New System.Drawing.Size(100, 20)
        Me.txtClienteID.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(47, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "ID Cliente"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(281, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Correo electrónico"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(379, 50)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(345, 20)
        Me.txtEmail.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(47, 114)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Nombre"
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(145, 112)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(543, 20)
        Me.txtNombre.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(47, 144)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Apellidos"
        '
        'txtApellidos
        '
        Me.txtApellidos.Location = New System.Drawing.Point(145, 142)
        Me.txtApellidos.Name = "txtApellidos"
        Me.txtApellidos.Size = New System.Drawing.Size(675, 20)
        Me.txtApellidos.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(47, 178)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Dirección"
        '
        'txtDireccion
        '
        Me.txtDireccion.Location = New System.Drawing.Point(145, 176)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(619, 20)
        Me.txtDireccion.TabIndex = 8
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(47, 212)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Población"
        '
        'txtPoblacion
        '
        Me.txtPoblacion.Location = New System.Drawing.Point(145, 210)
        Me.txtPoblacion.Name = "txtPoblacion"
        Me.txtPoblacion.Size = New System.Drawing.Size(345, 20)
        Me.txtPoblacion.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(532, 213)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Código Postal"
        '
        'txtCpostal
        '
        Me.txtCpostal.Location = New System.Drawing.Point(630, 211)
        Me.txtCpostal.Name = "txtCpostal"
        Me.txtCpostal.Size = New System.Drawing.Size(166, 20)
        Me.txtCpostal.TabIndex = 12
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(47, 243)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(51, 13)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "Provincia"
        '
        'txtProvincia
        '
        Me.txtProvincia.Location = New System.Drawing.Point(145, 241)
        Me.txtProvincia.Name = "txtProvincia"
        Me.txtProvincia.Size = New System.Drawing.Size(345, 20)
        Me.txtProvincia.TabIndex = 14
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(532, 248)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(29, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "País"
        '
        'txtPais
        '
        Me.txtPais.Location = New System.Drawing.Point(630, 246)
        Me.txtPais.Name = "txtPais"
        Me.txtPais.Size = New System.Drawing.Size(345, 20)
        Me.txtPais.TabIndex = 16
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(47, 281)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(49, 13)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Teléfono"
        '
        'txtTelefono
        '
        Me.txtTelefono.Location = New System.Drawing.Point(145, 279)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(229, 20)
        Me.txtTelefono.TabIndex = 18
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(403, 286)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(55, 13)
        Me.Label11.TabIndex = 21
        Me.Label11.Text = "Telefono2"
        '
        'txtTelefono2
        '
        Me.txtTelefono2.Location = New System.Drawing.Point(476, 283)
        Me.txtTelefono2.Name = "txtTelefono2"
        Me.txtTelefono2.Size = New System.Drawing.Size(174, 20)
        Me.txtTelefono2.TabIndex = 20
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(700, 286)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(24, 13)
        Me.Label12.TabIndex = 23
        Me.Label12.Text = "Fax"
        '
        'txtFax
        '
        Me.txtFax.Location = New System.Drawing.Point(747, 283)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(155, 20)
        Me.txtFax.TabIndex = 22
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(47, 83)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 13)
        Me.Label13.TabIndex = 25
        Me.Label13.Text = "Tipo Empresa"
        '
        'txtTipoEmpresa
        '
        Me.txtTipoEmpresa.Location = New System.Drawing.Point(145, 81)
        Me.txtTipoEmpresa.Name = "txtTipoEmpresa"
        Me.txtTipoEmpresa.Size = New System.Drawing.Size(345, 20)
        Me.txtTipoEmpresa.TabIndex = 24
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(557, 84)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(63, 13)
        Me.Label14.TabIndex = 27
        Me.Label14.Text = "Tipo Cliente"
        '
        'txtTipoCliente
        '
        Me.txtTipoCliente.Location = New System.Drawing.Point(655, 82)
        Me.txtTipoCliente.Name = "txtTipoCliente"
        Me.txtTipoCliente.Size = New System.Drawing.Size(345, 20)
        Me.txtTipoCliente.TabIndex = 26
        '
        'btn_Cerrar
        '
        Me.btn_Cerrar.Location = New System.Drawing.Point(965, 578)
        Me.btn_Cerrar.Name = "btn_Cerrar"
        Me.btn_Cerrar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cerrar.TabIndex = 28
        Me.btn_Cerrar.Text = "Cerrar"
        Me.btn_Cerrar.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(532, 515)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(29, 13)
        Me.Label15.TabIndex = 42
        Me.Label15.Text = "País"
        '
        'txtEnvioPais
        '
        Me.txtEnvioPais.Location = New System.Drawing.Point(630, 513)
        Me.txtEnvioPais.Name = "txtEnvioPais"
        Me.txtEnvioPais.Size = New System.Drawing.Size(345, 20)
        Me.txtEnvioPais.TabIndex = 41
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(47, 510)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(51, 13)
        Me.Label16.TabIndex = 40
        Me.Label16.Text = "Provincia"
        '
        'txtEnvioProvincia
        '
        Me.txtEnvioProvincia.Location = New System.Drawing.Point(145, 508)
        Me.txtEnvioProvincia.Name = "txtEnvioProvincia"
        Me.txtEnvioProvincia.Size = New System.Drawing.Size(345, 20)
        Me.txtEnvioProvincia.TabIndex = 39
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(532, 480)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(72, 13)
        Me.Label17.TabIndex = 38
        Me.Label17.Text = "Código Postal"
        '
        'txtEnvioCpostal
        '
        Me.txtEnvioCpostal.Location = New System.Drawing.Point(630, 478)
        Me.txtEnvioCpostal.Name = "txtEnvioCpostal"
        Me.txtEnvioCpostal.Size = New System.Drawing.Size(166, 20)
        Me.txtEnvioCpostal.TabIndex = 37
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(47, 479)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(54, 13)
        Me.Label18.TabIndex = 36
        Me.Label18.Text = "Población"
        '
        'txtEnvioPoblacion
        '
        Me.txtEnvioPoblacion.Location = New System.Drawing.Point(145, 477)
        Me.txtEnvioPoblacion.Name = "txtEnvioPoblacion"
        Me.txtEnvioPoblacion.Size = New System.Drawing.Size(345, 20)
        Me.txtEnvioPoblacion.TabIndex = 35
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(47, 445)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(52, 13)
        Me.Label19.TabIndex = 34
        Me.Label19.Text = "Dirección"
        '
        'txtEnvioDireccion
        '
        Me.txtEnvioDireccion.Location = New System.Drawing.Point(145, 443)
        Me.txtEnvioDireccion.Name = "txtEnvioDireccion"
        Me.txtEnvioDireccion.Size = New System.Drawing.Size(619, 20)
        Me.txtEnvioDireccion.TabIndex = 33
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(47, 411)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(49, 13)
        Me.Label20.TabIndex = 32
        Me.Label20.Text = "Apellidos"
        '
        'txtEnvioApellidos
        '
        Me.txtEnvioApellidos.Location = New System.Drawing.Point(145, 409)
        Me.txtEnvioApellidos.Name = "txtEnvioApellidos"
        Me.txtEnvioApellidos.Size = New System.Drawing.Size(675, 20)
        Me.txtEnvioApellidos.TabIndex = 31
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(47, 381)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(44, 13)
        Me.Label21.TabIndex = 30
        Me.Label21.Text = "Nombre"
        '
        'txtEnvioNombre
        '
        Me.txtEnvioNombre.Location = New System.Drawing.Point(145, 379)
        Me.txtEnvioNombre.Name = "txtEnvioNombre"
        Me.txtEnvioNombre.Size = New System.Drawing.Size(543, 20)
        Me.txtEnvioNombre.TabIndex = 29
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(31, 363)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1009, 201)
        Me.GroupBox1.TabIndex = 43
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de Envío"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.ForeColor = System.Drawing.Color.Red
        Me.Label22.Location = New System.Drawing.Point(48, 18)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(82, 13)
        Me.Label22.TabIndex = 45
        Me.Label22.Text = "Este cliente es: "
        '
        'txtDistribuidor
        '
        Me.txtDistribuidor.Location = New System.Drawing.Point(143, 16)
        Me.txtDistribuidor.Name = "txtDistribuidor"
        Me.txtDistribuidor.Size = New System.Drawing.Size(183, 20)
        Me.txtDistribuidor.TabIndex = 44
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(386, 23)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(28, 13)
        Me.Label23.TabIndex = 47
        Me.Label23.Text = "Tipo"
        '
        'txtTipo
        '
        Me.txtTipo.Location = New System.Drawing.Point(448, 21)
        Me.txtTipo.Name = "txtTipo"
        Me.txtTipo.Size = New System.Drawing.Size(142, 20)
        Me.txtTipo.TabIndex = 46
        '
        'Clientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1078, 623)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.txtTipo)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.txtDistribuidor)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.txtEnvioPais)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.txtEnvioProvincia)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtEnvioCpostal)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.txtEnvioPoblacion)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.txtEnvioDireccion)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.txtEnvioApellidos)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.txtEnvioNombre)
        Me.Controls.Add(Me.btn_Cerrar)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.txtTipoCliente)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.txtTipoEmpresa)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtFax)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtTelefono2)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtTelefono)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtPais)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtProvincia)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtCpostal)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtPoblacion)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtApellidos)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtEmail)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtClienteID)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.Name = "Clientes"
        Me.Text = "Clientes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtClienteID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtApellidos As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPoblacion As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCpostal As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtProvincia As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtPais As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono2 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtFax As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtTipoEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtTipoCliente As System.Windows.Forms.TextBox
    Friend WithEvents btn_Cerrar As System.Windows.Forms.Button
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioPais As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioProvincia As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioCpostal As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioPoblacion As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioApellidos As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtEnvioNombre As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtDistribuidor As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtTipo As System.Windows.Forms.TextBox
End Class
