﻿Imports MySql.Data.MySqlClient
Imports [Interface].PRODUCTOS
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.IO
Imports System.Net
Imports System.Web
Imports System.Text
Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Xml
Imports MSXML2
Imports System.Threading
Imports System.Globalization
Imports System.Deployment.Application


Public Class ENLACE
    Public cadena As String

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Dim version As String = "2.0.0.66" 'version nueva que se va a generar
        'Try
        'Label44.Text = "Version: " & ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString
        'If version <> ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString Then
        'MsgBox("Has de actualizar el Conector de Productos a la última Versión para poder utilizarlo")
        'End
        'End If
        'Catch ex As Exception
        'MsgBox("Versión no instalada")
        'End Try

        Dim myVersion As Version

        If ApplicationDeployment.IsNetworkDeployed Then
            myVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion
        End If

        Label44.Text = String.Concat("Versión: ", myVersion)

        sqlconexion.VISUALIZAR_MARCAS()

        'Ocultar controles
        Disable_Groupboxes(CType(Me.Controls, ControlCollection), False)
        Label37.Visible = False
        Label38.Visible = False
        categorias.Visible = False
        gestionimagen.Visible = False
        Nueva.Visible = False
        MODIFICAR.Visible = False
        Label34.Visible = False
        Button2.Visible = False
        Label35.Visible = False
        Label33.Visible = False
        Descripcion.Visible = False
        Label3.Visible = False
        Valor.Visible = False
        Label1.Visible = False
        Estado.Visible = False
        Label18.Visible = False
        descatalogar.Visible = False
        Referencia.Visible = False
        Label2.Visible = False
        busqueda.Visible = False
        busqueda1.Visible = False
        Insertar.Visible = False
        Label14.Visible = False
        modeloid.Visible = False
        Modelos.Visible = False
        chkAccesorio.Visible = False
    End Sub

    Public imagen As Imagenes = New Imagenes
    Public navconectar As CONECTOR_NAVISION = New CONECTOR_NAVISION
    Public conect As web = New web
    Public preciodistribuidor As Calculo_pvp_distribuidor = New Calculo_pvp_distribuidor
    Public sqlconexion As SQLCONECTAR = New SQLCONECTAR
    Public z As Integer
    Public enviar As Images = New Images
    Public magento As MAGENTO = New MAGENTO
    Public a As Integer
    Public calculovolumen As Volumetrico = New Volumetrico
    Public busquedasimple As busqueda = New busqueda

    '----------------------------------------------------------------------------------------------------------------------------------------

    Private Sub busqueda_Click(sender As Object, e As EventArgs) Handles busqueda.Click
        GroupBoxPrecios.Enabled = False
        GroupBoxImportadorTarifas.Enabled = False
        GroupBoxImportadorTarifas1.Enabled = False
        GroupBoxImportadorTarifas2.Enabled = False

        a = 1

        If Me.Referencia.Text <> "" Then
            busquedasimple.busqueda_simple_referencia()
        End If

        If Me.Referencia.Text = "" And Me.Descripcion.Text <> "" Then
            busquedasimple.busqueda_simples()
        End If
    End Sub

    Private Sub MODIFICAR_Click(sender As Object, e As EventArgs) Handles MODIFICAR.Click
        Button2.Enabled = True 'Boton de actualizar magento

        conect.preciosActual()

        'NAVISION
        navconectar.navconectar()
        navconectar.MODIFICAR()

        'MYSQL
        If Me.pvp_distribuidor.Text <> "0" Then
            conect.myactualizar()
        Else
            conect.myactualizar_sin_pvd()
        End If

        'conect.busca_sku()

        conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "Modificacion", "Se ha modificado el producto simple en Mysql/Navision con sku: " & Referencia.Text)

        If descatalogar.Checked Then
            conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "Descatalogado", "El producto " & Referencia.Text & " se ha descatalogado.")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Insertar.Click
        If Referencia.Text = "" Then
            MessageBox.Show("No se puede dejar el campo Referencia en blanco")
        End If
        ''NAVISION
        navconectar.navconectar()
        navconectar.comprobar()
    End Sub

    Private Sub BORRAR_Click(sender As Object, e As EventArgs) Handles BORRAR.Click
        'NAVISION
        navconectar.navconectar()
        navconectar.ELIMINAR()
        'MYSQL
        conect.myeliminar()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        My.Settings.Save()
        Me.Close()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Modelos.Click
        Modelo.Show()
        If Me.modeloid.Text <> "" Then
            Modelo.idmodelo.Text = Me.modeloid.Text
            navconectar.referencia_busqueda = Me.modeloid.Text
            navconectar.BUSCAR_MODELO()
            conect.revision()
        End If
    End Sub

    Private Sub Nueva_Click(sender As Object, e As EventArgs) Handles Nueva.Click
        sqlconexion.VISUALIZAR_EAN()

        With Me
            .Referencia.Text = ""
            .modeloid.Text = ""
            .Descripcion.Text = ""
            .Valor.Text = ""
            .Precio.Text = CStr(0)
            .Coste.Text = CStr(0)
            .pvp_distribuidor.Text = CStr(0)
            .importe_oferta.Text = CStr(0)
            .fecha_inicio.Text = ""
            .fecha_fin.Text = ""
            .Peso.Text = CStr(0)
            .Alto.Text = CStr(0)
            .Ancho.Text = CStr(0)
            .Profundidad.Text = CStr(0)
            .Proveedor.Text = ""
            .Refproveedor.Text = ""
            .ubicaciones.Text = ""
            .Plazo_entrega.Text = ""
            .diaspedido.Text = CStr(0)
            .ean.Text = ""
            .descatalogar.Checked = False
            .chkBajoPedido.Checked = False
            .chkEnvioDirecto.Checked = False
            .chkOxigeno.Checked = False
            .chkAccesorio.Checked = False
            .psichk.Checked = False
            .psiiachk.Checked = False
            .psiibchk.Checked = False
            .psiiichk.Checked = False
            .psinvchk.Checked = False
        End With
    End Sub
 
    Private Sub idmodelo_TextChanged(sender As Object, e As EventArgs) Handles modeloid.TextChanged
        Try
            navconectar.navconectar()
            navconectar.recuperar_descripcion()
        Catch ex As Exception
            MessageBox.Show("No existe el modelo")
        End Try
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Calculo_distribuidor.Click
        Label15.ForeColor = Color.Black
        Me.pvp_distribuidor.Text = CStr(0)
        preciodistribuidor.precio_distribuidor()
    End Sub

    Private Sub Button1_Click_2(sender As Object, e As EventArgs) Handles cargar_img_producto.Click
        'Dim OpenFile As New OpenFileDialog()
        'OpenFile.InitialDirectory = "\\192.168.2.6\images\imagenes\" & Me.modeloid.Text
        'If OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
        'Me.PictureBox1.Image = Image.FromFile(OpenFile.FileName)
        'End If
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles guardarimagen.Click
        Dim conexion As web = New web
        enviar.referencia = Me.Referencia.Text
        enviar.incremento = Me.Referencia.Text & "_" & z
        enviar.Abrir_carpeta_Producto()
        'enviar.imagen_big_producto()
        'enviar.imagen_large_producto()
        'enviar.imagen_medium_producto()
        'enviar.imagen_small_producto()

        conexion.imagen_producto()
        conexion.recuperar_id_imagen()
        conexion.update_img_articulo()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles cmarca.Click
        Marcas.Show()
    End Sub

    Private Sub EANS_DropDown(sender As Object, e As EventArgs) Handles EANS.DropDown
        EANS.Items.Clear()
        sqlconexion.VISUALIZAR_EAN()
    End Sub

    Private Sub EANS_SelectedIndexChanged(sender As Object, e As EventArgs) Handles EANS.SelectedIndexChanged
        Me.EANS.Text = ""
        Me.ean.Text = CStr(Me.EANS.SelectedItem)
        Me.EANS.Items.Clear()
    End Sub

    Private Sub Referencia_TextChanged(sender As Object, e As EventArgs) Handles Referencia.TextChanged
        z = 0
    End Sub

    Private Sub enviarproductos_Click(sender As Object, e As EventArgs) Handles enviarproductos.Click
        'enviar.ftp_modelos_small()
        'enviar.ftp_modelos_big()
        'enviar.ftp_modelos_large()
        'enviar.ftp_modelos_medium() 'bloqueado por magento
    End Sub

    Private Sub Precio_GotFocus(sender As Object, e As EventArgs) Handles Precio.GotFocus
        Label15.ForeColor = Color.Red
    End Sub

    Private Sub Proveedor_TextChanged(sender As Object, e As EventArgs) Handles Proveedor.TextChanged
        If a = 1 Then
            If Proveedor.Text <> "" Then
                navconectar.navconectar()
                navconectar.buscar_plazos()
            End If
        End If
    End Sub

    Private Sub Proveedor_Validated(sender As Object, e As EventArgs) Handles Proveedor.Validated
        If a <> 1 Then
            If Proveedor.Text <> "" Then
                navconectar.navconectar()
                navconectar.buscar_plazos()
            End If
        End If
    End Sub

    Private Sub Coste_GotFocus(sender As Object, e As EventArgs) Handles Coste.GotFocus
        Label15.ForeColor = Color.Red
    End Sub

    Private Sub ean_TextChanged(sender As Object, e As EventArgs) Handles ean.TextChanged
        navconectar.navconectar()
        navconectar.eans()
    End Sub

    Private Sub categorias_Click(sender As Object, e As EventArgs) Handles categorias.Click
        MENU_CATEGORIAS.Show()
    End Sub

    Private Sub Button1_Click_3(sender As Object, e As EventArgs)
        'LLamada a código Mage para definir el atributo configurable en el producto
        Dim strHtml As String = ""
        Dim UrlStr As String
        Dim DomDoc As MSXML2.XMLHTTP
        Dim params As String
        Dim mageconect = "https://quirumed.on-dev.com/scripts/pre/"
        UrlStr = mageconect & "mageProductoNuevo.php"
        DomDoc = New XMLHTTP
        params = "producto_id=" & Me.modeloid.Text
        DomDoc.open("POST", UrlStr, False)
        DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
        DomDoc.setRequestHeader("Connection", "close")
        DomDoc.send(params)
    End Sub

    Private Sub Button2_Click_2(sender As Object, e As EventArgs) Handles Button2.Click
        Dim strHtml As String = ""
        Dim UrlStr As String
        Dim DomDoc As MSXML2.XMLHTTP
        Dim params As String
        Dim mageconect = "https://quirumed.on-dev.com/scripts/pre/"
        UrlStr = mageconect & "mageProductoModificar.php"
        DomDoc = New XMLHTTP
        params = "producto_id=" & Me.modeloid.Text
        DomDoc.open("POST", UrlStr, False)
        DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
        DomDoc.setRequestHeader("Connection", "close")
        DomDoc.send(params)

        GroupBoxPrecios.Enabled = True
        GroupBoxImportadorTarifas.Enabled = True
        GroupBoxImportadorTarifas1.Enabled = True
        GroupBoxImportadorTarifas2.Enabled = True

        conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "Modificacion", "Se ha modificado el producto simple en Magento con sku: " & Referencia.Text)

        If descatalogar.Checked Then
            conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "Descatalogado", "El producto " & Referencia.Text & " se ha descatalogado.")
        End If

        If importe_oferta.Text = "" Or importe_oferta.Text = "0" Or importe_oferta.Text = "0,00" Then
            If Convert.ToDecimal(pvpAnterior.Text) > Convert.ToDecimal(Precio.Text) Then
                conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "PVP-DOWN", Referencia.Text & " ha bajado el PVP de " & pvpAnterior.Text & " a " & Precio.Text)
                If Convert.ToDecimal(pvdAnterior.Text) > Convert.ToDecimal(pvp_distribuidor.Text) Then
                    conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "PVD-DOWN", Referencia.Text & " ha bajado el PVD de " & pvdAnterior.Text & " a " & pvp_distribuidor.Text)
                End If
            End If
            If Convert.ToDecimal(pvpAnterior.Text) < Convert.ToDecimal(Precio.Text) Then
                conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "PVP-UP", Referencia.Text & " ha subido el PVP de " & pvpAnterior.Text & " a " & Precio.Text)
                If Convert.ToDecimal(pvdAnterior.Text) < Convert.ToDecimal(pvp_distribuidor.Text) Then
                    conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "PVD-UP", Referencia.Text & " ha subido el PVD de " & pvdAnterior.Text & " a " & pvp_distribuidor.Text)
                End If
            End If
        Else
            If Convert.ToDecimal(pvpAnterior.Text) > Convert.ToDecimal(importe_oferta.Text) Then
                conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "PVP-DOWN", Referencia.Text & " ha bajado el PVP de " & pvpAnterior.Text & " a " & importe_oferta.Text)
                If Convert.ToDecimal(pvdAnterior.Text) > Convert.ToDecimal(pvp_distribuidor.Text) Then
                    conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "PVD-DOWN", Referencia.Text & " ha bajado el PVD de " & pvdAnterior.Text & " a " & pvp_distribuidor.Text)
                End If
            End If
            If Convert.ToDecimal(pvpAnterior.Text) < Convert.ToDecimal(importe_oferta.Text) Then
                conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "PVP-UP", Referencia.Text & " ha subido el PVP de " & pvpAnterior.Text & " a " & importe_oferta.Text)
                If Convert.ToDecimal(pvdAnterior.Text) < Convert.ToDecimal(pvp_distribuidor.Text) Then
                    conect.savelogs(Me.txtUsuario.Text, Referencia.Text, "PVD-UP", Referencia.Text & " ha subido el PVD de " & pvdAnterior.Text & " a " & pvp_distribuidor.Text)
                End If
            End If
        End If

        pvpAnterior.Text = "0"
        pvoAnterior.Text = "0"
        pvdAnterior.Text = "0"

        With Me
            .Referencia.Text = ""
            .Descripcion.Text = ""
            .Valor.Text = ""
            .Precio.Text = CStr(0)
            .Peso.Text = CStr(0)
            .Alto.Text = CStr(0)
            .Ancho.Text = CStr(0)
            .Coste.Text = CStr(0)
            .Profundidad.Text = CStr(0)
            .pvp_distribuidor.Text = CStr(0)
            .diaspedido.Text = CStr(0)
            .ean.Text = ""
            .importe_oferta.Text = CStr(0)
            .Proveedor.Text = ""
            .Refproveedor.Text = ""
            .Plazo_entrega.Text = ""
            .ubicaciones.Text = ""
        End With
    End Sub

    Private Sub gestionimagen_Click(sender As Object, e As EventArgs) Handles gestionimagen.Click
        Gestion_imagen_productos.Show()
    End Sub

    Private Sub btn_Examinar_Click(sender As Object, e As EventArgs) Handles btn_Examinar.Click
        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            txtFile.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub Button3_Click_1(sender As Object, e As EventArgs) Handles Button3.Click
        'Dim ruta As String = Me.txtFile.Text
        'If File.Exists(ruta) Then
        '    Using MyReader As New Microsoft.VisualBasic.
        '                            FileIO.TextFieldParser(ruta)
        '        MyReader.TextFieldType = FileIO.FieldType.Delimited
        '        MyReader.SetDelimiters(ControlChars.Tab)
        '        Dim currentRow As String()

        '        While Not MyReader.EndOfData
        '            Try
        '                currentRow = MyReader.ReadFields()
        '                Dim currentField As String
        '                Dim referencia As String = ""
        '                Dim n As Integer = 0
        '                For Each currentField In currentRow
        '                    If n = 0 Then
        '                        txt_referencia.Text = currentField
        '                    End If
        '                    If n = 1 Then
        '                        txt_precio.Text = currentField
        '                    End If
        '                    If n = 2 Then
        '                        txt_pcoste.Text = currentField
        '                    End If
        '                    If n = 3 Then
        '                        importe_oferta.Text = currentField
        '                    End If
        '                    If n = 4 Then
        '                        fecha_inicio.Text = currentField
        '                    End If
        '                    n = n + 1
        '                Next
        '                Me.CalculoPvd()

        '                conect.updateTarifaProductoCsv()
        '                navconectar.navconectar()
        '                navconectar.ACTUALIZA_TARIFAS_EXCEL()

        '            Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
        '                MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
        '            End Try
        '        End While
        '    End Using
        '    MsgBox("Proceso terminado")
        'Else
        '    MsgBox("No existe el archivo")
        'End If
    End Sub

    Private Sub btnPVD_Click(sender As Object, e As EventArgs) Handles btnPVD.Click
        'Dim ruta As String = "C:\tarifas\PVD.txt"
        'Using MyReader As New Microsoft.VisualBasic.
        '                        FileIO.TextFieldParser(ruta)
        '    MyReader.TextFieldType = FileIO.FieldType.Delimited
        '    MyReader.SetDelimiters(ControlChars.Tab)
        '    Dim currentRow As String()

        '    While Not MyReader.EndOfData
        '        Try
        '            currentRow = MyReader.ReadFields()
        '            Dim currentField As String
        '            Dim referencia As String = ""
        '            Dim n As Integer = 0
        '            For Each currentField In currentRow
        '                If n = 0 Then
        '                    txt_referencia.Text = currentField
        '                End If
        '                If n = 1 Then
        '                    txt_pdistribuidor.Text = currentField
        '                End If
        '                n = n + 1
        '            Next

        '            conect.updatePVDProductoCsv()

        '        Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
        '            MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
        '        End Try
        '    End While
        'End Using
        'MsgBox("Proceso terminado")
    End Sub

    Private Sub CalculoPvd()
        Dim porcentaje As Decimal
        Dim resultado As Decimal

        Dim importoferta As Decimal
        importoferta = CDec(Me.importe_oferta.Text)

        If importoferta = 0 Then
            With Me
                If CDec(.txt_precio.Text) <> 0 And CDec(Me.txt_pcoste.Text) <> 0 Then

                    porcentaje = (CDec(.txt_precio.Text) - CDec(.txt_pcoste.Text)) * 100 / CDec(.txt_precio.Text)

                    If (porcentaje >= 37.5) And (porcentaje < 41.1764706) Then
                        resultado = 5
                    End If

                    If (porcentaje >= 41.1764706) And (porcentaje < 44.44) Then
                        resultado = 10
                    End If

                    If (porcentaje >= 44.44) And (porcentaje < 50) Then
                        resultado = 15
                    End If

                    If (porcentaje >= 50) And (porcentaje < 60) Then
                        resultado = 20
                    End If

                    If (porcentaje >= 60) And (porcentaje < 66.6666667) Then
                        resultado = 25
                    End If

                    If porcentaje >= 66.67 Then
                        resultado = 30
                    End If
                    .txt_pdistribuidor.Text = CStr(CDec(.txt_precio.Text) - ((CDec(.txt_precio.Text) * resultado)) / 100)
                End If
            End With
        End If

        If importoferta <> 0 Then
            With Me
                If CDec(.importe_oferta.Text) <> 0 And CDec(Me.txt_pcoste.Text) <> 0 Then

                    porcentaje = (CDec(.importe_oferta.Text) - CDec(.txt_pcoste.Text)) * 100 / CDec(.importe_oferta.Text)

                    If (porcentaje >= 37.5) And (porcentaje < 41.1764706) Then
                        resultado = 5
                    End If

                    If (porcentaje >= 41.1764706) And (porcentaje < 44.44) Then
                        resultado = 10
                    End If

                    If (porcentaje >= 44.44) And (porcentaje < 50) Then
                        resultado = 15
                    End If

                    If (porcentaje >= 50) And (porcentaje < 60) Then
                        resultado = 20
                    End If

                    If (porcentaje >= 60) And (porcentaje < 66.6666667) Then
                        resultado = 25
                    End If

                    If porcentaje >= 66.67 Then
                        resultado = 30
                    End If
                    .txt_pdistribuidor.Text = CStr(CDec(.importe_oferta.Text) - ((CDec(.importe_oferta.Text) * resultado)) / 100)
                End If
            End With
        End If
    End Sub

    Private Sub modificardias_Click(sender As Object, e As EventArgs) Handles modificardias.Click
        Try
            navconectar.navconectar()
            navconectar.ACTUALIZA_DIAS_PROVEEDOR()
            conect.actualizar_dias_prov()
            conect.savelogs(Me.txtUsuario.Text, proveedorn.Text, "Modificacion", "Upload y modificación de los dias a servir del proveedor " & proveedorn.Text)
        Catch ex As Exception
            Console.WriteLine(ex.Source)
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub fecha_inicio_Enter(sender As Object, e As EventArgs)
        Dim dt As DateTime
        Dim bln As Boolean = DateTime.TryParse(fecha_inicio.Text, dt)

        If (bln) Then
            fecha_inicio.Text = String.Format("{0:yyyy/MM/dd}", dt)
        Else
            fecha_inicio.Clear()
        End If
    End Sub

    Private Sub Coste_TextChanged(sender As Object, e As EventArgs) Handles Coste.TextChanged
        If Coste.Text = "0" Then
            Label10.ForeColor = Color.Red
        Else
            Label10.ForeColor = Color.Black
        End If
    End Sub

    Private Sub btnCrearEnMysql_Click(sender As Object, e As EventArgs) Handles btnCrearEnMysql.Click
        conect.recuperar_simple_navision()
    End Sub

    Private Sub rol_false()
        Disable_Groupboxes(CType(Me.Controls, ControlCollection), False)
        Label37.Visible = False
        Label38.Visible = False
        categorias.Visible = False
        gestionimagen.Visible = False
        Nueva.Visible = False
        MODIFICAR.Visible = False
        Label34.Visible = False
        Button2.Visible = False
        Label35.Visible = False
        Label33.Visible = False
        Descripcion.Visible = False
        Label3.Visible = False
        Valor.Visible = False
        Label1.Visible = False
        Estado.Visible = False
        Label18.Visible = False
        descatalogar.Visible = False
        Referencia.Visible = False
        Label2.Visible = False
        busqueda.Visible = False
        busqueda1.Visible = False
        Insertar.Visible = False
        Label14.Visible = False
        modeloid.Visible = False
        Modelos.Visible = False
        GroupBox1.Visible = False
        Label40.Visible = False
        txtArchivo.Visible = False
        btnOcultarExaminar.Visible = False
        btn_Importar.Visible = False
        chkAccesorio.Visible = False
    End Sub

    Private Sub btnAcceder_Click(sender As Object, e As EventArgs) Handles btnAcceder.Click
        'Login de Acceso
        'txtUsuario.Text , txtPassword.Text
        'Acceder a la tablas usuarios, roles Mysql
        'Devuelve el Rol
        'Segun el rol bloquear u ocultar controles
        If txtUsuario.Text = "" Or txtPaswword.Text = "" Then
            txtAcceso.ForeColor = Color.Red
            txtAcceso.Text = "Falta el usuario o el password"
        Else
            'GroupBox creados
            'GroupBoxProveedores        --> Importador provevedor dias
            'GroupBoxRecuperar          --> Recuperar simple de Navsion
            'GroupBoxImportadorTarifas  --> Importador de tarifas
            'GroupBoxAlmacen y Label38  --> Almacen y portes
            'GroupBoxPrecios y Label37  --> Precios
            'GroupBoxClase              --> Clase

            'Otros controles creados
            'categorias                 --> Botón categorías
            'gestionimagen              --> Botón de imágenes
            'Nueva                      --> Botón limpiar formulario
            'MODIFICAR y Label34        --> Botón Actualizar Mysql/Navision
            'Button2 y Label35          --> Botón Actualizar Magento
            'Label33                    --> texto HERRAMIENTAS

            'Descripcion y Label1       --> Nombre
            'Valor y Label3             --> valor
            'Estado y Label18           --> Estado
            'descatalogar               --> Descatalogar

            'Referencia, Label2 y busqueda  --> Buscador Referencia
            'Insertar                       --> Botón nueva referencia

            'Label14, modeloid y Modelos    --> Modelos


            Dim rol As String = conect.login()

            If rol = "FALSE" Then                
                'Todo oculto
                rol_false()
            End If

            If rol = "PRECIOS" Then
                txtUsuario.Enabled = False
                txtPaswword.Enabled = False
                rol_false()
                'Precios
                Label37.Visible = True
                GroupBoxPrecios.Visible = True
                'Herramientas
                Label33.Visible = True
                GroupBoxImportadorTarifas.Visible = False
                GroupBoxImportadorTarifas1.Visible = True
                GroupBoxImportadorTarifas2.Visible = True
                'Referencia - buscador
                Label2.Visible = True
                Referencia.Visible = True
                busqueda.Visible = True
                'Nombre-valor enabled
                Label1.Visible = True
                Descripcion.Visible = True
                Descripcion.Enabled = False
                Label3.Visible = True
                Valor.Visible = True
                Valor.Enabled = False
                'botones actualizar
                Label34.Visible = True
                MODIFICAR.Visible = True
                Label35.Visible = True
                Button2.Visible = True
                busqueda.Visible = False
                busqueda1.Visible = True
                categorias.Visible = True
            End If

            If rol = "GENERAL" Then
                txtUsuario.Enabled = False
                txtPaswword.Enabled = False
                'Todo visible 
                Disable_Groupboxes(CType(Me.Controls, ControlCollection), True)
                Label37.Visible = True
                Label38.Visible = True
                categorias.Visible = True
                gestionimagen.Visible = True
                Nueva.Visible = True
                MODIFICAR.Visible = True
                Label34.Visible = True
                Button2.Visible = True
                Label35.Visible = True
                Label33.Visible = True
                Descripcion.Visible = True
                Label1.Visible = True
                Valor.Visible = True
                Label3.Visible = True
                Estado.Visible = True
                Label18.Visible = True
                descatalogar.Visible = True
                Referencia.Visible = True
                Label2.Visible = True
                busqueda.Visible = True
                Insertar.Visible = True
                Label14.Visible = True
                modeloid.Visible = True
                Modelos.Visible = True
                'Ocultar simples
                GroupBox1.Visible = False
                Label40.Visible = False
                txtArchivo.Visible = False
                btnOcultarExaminar.Visible = False
                btn_Importar.Visible = False
                chkAccesorio.Visible = True
                GroupBoxImportadorTarifas.Enabled = False
                GroupBoxImportadorTarifas.Visible = False
                GroupBoxImportadorTarifas1.Enabled = False
                GroupBoxImportadorTarifas1.Visible = False
                GroupBoxImportadorTarifas2.Enabled = False
                GroupBoxImportadorTarifas2.Visible = False
                busqueda.Visible = True
                busqueda1.Visible = False
            End If

            If rol = "ADMIN" Then
                'Todo visible 
                Disable_Groupboxes(CType(Me.Controls, ControlCollection), True)
                Label37.Visible = True
                Label38.Visible = True
                categorias.Visible = True
                gestionimagen.Visible = True
                Nueva.Visible = True
                MODIFICAR.Visible = True
                Label34.Visible = True
                Button2.Visible = True
                Label35.Visible = True
                Label33.Visible = True
                Descripcion.Visible = True
                Label1.Visible = True
                Valor.Visible = True
                Label3.Visible = True
                Estado.Visible = True
                Label18.Visible = True
                descatalogar.Visible = True
                Referencia.Visible = True
                Label2.Visible = True
                busqueda.Visible = True
                Insertar.Visible = True
                Label14.Visible = True
                modeloid.Visible = True
                Modelos.Visible = True
                'Ocultar simples
                GroupBox1.Visible = True
                Label40.Visible = True
                txtArchivo.Visible = True
                btnOcultarExaminar.Visible = True
                btn_Importar.Visible = True
                chkAccesorio.Visible = True
                txtLogs.Enabled = True
                txtLogs.Visible = True
                'Importadores proveedor id, bajo pedido y envio directo
                Label43.Enabled = True
                Label43.Visible = True
                Button5.Enabled = True
                Button5.Visible = True
                txtFileImportar.Enabled = True
                txtFileImportar.Visible = True
                ImportarProductoProveedorID.Enabled = False
                ImportarProductoProveedorID.Visible = False
                ImportarBajoPedido.Enabled = True
                ImportarBajoPedido.Visible = True
                ImportarEnvioDirecto.Enabled = False
                ImportarEnvioDirecto.Visible = False
                GroupBoxImportadorTarifas.Visible = False
                GroupBox1.Visible = False
            End If
        End If
    End Sub

    Public Sub Disable_Groupboxes(ByVal Container As ControlCollection, ByVal Visible As Boolean)
        For Each Control As Control In Container
            If TypeOf Control Is GroupBox Then Control.Visible = Visible
        Next
    End Sub

    Private Sub Cerrar_Click(sender As Object, e As EventArgs) Handles Cerrar.Click
        Me.Close()
    End Sub

    Private Sub btn_Examinar1_Click(sender As Object, e As EventArgs) Handles btn_Examinar1.Click
        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            txtFile1.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub Button3a_Click(sender As Object, e As EventArgs) Handles Button3a.Click
        Dim ruta1 As String = Me.txtFile1.Text
        If File.Exists(ruta1) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta1)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim referencia As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                txt_referencia.Text = currentField
                            End If
                            If n = 1 Then
                                txt_precio.Text = currentField
                            End If
                            If n = 2 Then
                                txt_pcoste.Text = currentField
                            End If
                            If n = 3 Then
                                importe_oferta.Text = currentField
                            End If
                            If n = 4 Then
                                fecha_inicio.Text = currentField
                            End If
                            n = n + 1
                        Next

                        conect.preciosActual()
                        Me.CalculoPvd()
                        conect.updateTarifaProductoCsv()
                        navconectar.navconectar()
                        navconectar.ACTUALIZA_TARIFAS_EXCEL1()
                        conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "Modificacion", "Importación de tarifas (Importación Excel) del sku: " & txt_referencia.Text)

                        If importe_oferta.Text = "" Or importe_oferta.Text = "0" Or importe_oferta.Text = "0,00" Then
                            If Convert.ToDecimal(pvpAnterior.Text) > Convert.ToDecimal(txt_precio.Text) Then
                                conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVP-DOWN", txt_referencia.Text & " ha bajado el PVP de " & pvpAnterior.Text & " a " & txt_precio.Text)
                                If Convert.ToDecimal(pvdAnterior.Text) > Convert.ToDecimal(txt_pdistribuidor.Text) Then
                                    conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVD-DOWN", txt_referencia.Text & " ha bajado el PVD de " & pvdAnterior.Text & " a " & txt_pdistribuidor.Text)
                                End If
                            End If
                            If Convert.ToDecimal(pvpAnterior.Text) < Convert.ToDecimal(txt_precio.Text) Then
                                conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVP-UP", txt_referencia.Text & " ha subido el PVP de " & pvpAnterior.Text & " a " & txt_precio.Text)
                                If Convert.ToDecimal(pvdAnterior.Text) < Convert.ToDecimal(txt_pdistribuidor.Text) Then
                                    conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVD-UP", txt_referencia.Text & " ha subido el PVD de " & pvdAnterior.Text & " a " & txt_pdistribuidor.Text)
                                End If
                            End If
                        Else
                            If Convert.ToDecimal(pvpAnterior.Text) > Convert.ToDecimal(importe_oferta.Text) Then
                                conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVP-DOWN", txt_referencia.Text & " ha bajado el PVP de " & pvpAnterior.Text & " a " & importe_oferta.Text)
                                If Convert.ToDecimal(pvdAnterior.Text) > Convert.ToDecimal(txt_pdistribuidor.Text) Then
                                    conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVD-DOWN", txt_referencia.Text & " ha bajado el PVD de " & pvdAnterior.Text & " a " & txt_pdistribuidor.Text)
                                End If
                            End If
                            If Convert.ToDecimal(pvpAnterior.Text) < Convert.ToDecimal(importe_oferta.Text) Then
                                conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVP-UP", txt_referencia.Text & " ha subido el PVP de " & pvpAnterior.Text & " a " & importe_oferta.Text)
                                If Convert.ToDecimal(pvdAnterior.Text) < Convert.ToDecimal(txt_pdistribuidor.Text) Then
                                    conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVD-UP", txt_referencia.Text & " ha subido el PVD de " & pvdAnterior.Text & " a " & txt_pdistribuidor.Text)
                                End If
                            End If
                        End If
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If
    End Sub

    Private Sub btnOcultarExaminar_Click(sender As Object, e As EventArgs) Handles btnOcultarExaminar.Click
        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            txtArchivo.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub btn_Importar_Click(sender As Object, e As EventArgs) Handles btn_Importar.Click
        'Dim ruta As String = Me.txtArchivo.Text
        'If File.Exists(ruta) Then
        '    Using MyReader As New Microsoft.VisualBasic.
        '                            FileIO.TextFieldParser(ruta)
        '        MyReader.TextFieldType = FileIO.FieldType.Delimited
        '        MyReader.SetDelimiters(ControlChars.Tab)
        '        Dim currentRow As String()

        '        While Not MyReader.EndOfData
        '            Try
        '                currentRow = MyReader.ReadFields()
        '                Dim currentField As String
        '                Dim referencia As String = ""
        '                Dim n As Integer = 0
        '                For Each currentField In currentRow
        '                    If n = 0 Then
        '                        txt_referencia.Text = currentField
        '                    End If
        '                    n = n + 1
        '                Next
        '                conect.updateOcultarProductoCsv()
        '                conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "Modificacion", "Ocultar productos (Importación Excel) del sku " & txt_referencia.Text)
        '            Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
        '                MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
        '            End Try
        '        End While
        '    End Using
        '    MsgBox("Proceso terminado")
        'Else
        '    MsgBox("No existe el archivo")
        'End If
    End Sub

    Private Sub btnValorMultidioma_Click(sender As Object, e As EventArgs) Handles btnValorMultidioma.Click
        'Dim ruta As String = Me.txtArchivo.Text
        'If File.Exists(ruta) Then
        '    Using MyReader As New Microsoft.VisualBasic.
        '                            FileIO.TextFieldParser(ruta)
        '        MyReader.TextFieldType = FileIO.FieldType.Delimited
        '        MyReader.SetDelimiters(ControlChars.Tab)
        '        Dim currentRow As String()

        '        While Not MyReader.EndOfData
        '            Try
        '                currentRow = MyReader.ReadFields()
        '                Dim currentField As String
        '                Dim referencia As String = ""
        '                Dim n As Integer = 0
        '                For Each currentField In currentRow
        '                    If n = 0 Then
        '                        txt_referencia.Text = currentField
        '                    End If
        '                    If n = 1 Then
        '                        txtValorMultidioma.Text = currentField
        '                    End If
        '                    n = n + 1
        '                Next
        '                conect.updateValorMultidiomaCsv()

        '            Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
        '                MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
        '            End Try
        '        End While
        '    End Using
        '    MsgBox("Proceso terminado")
        'Else
        '    MsgBox("No existe el archivo")
        'End If
    End Sub

    Private Sub btnCategoriaMultidioma_Click(sender As Object, e As EventArgs) Handles btnCategoriaMultidioma.Click
        'Dim ruta As String = Me.txtArchivo.Text
        'If File.Exists(ruta) Then
        '    Using MyReader As New Microsoft.VisualBasic.
        '                            FileIO.TextFieldParser(ruta)
        '        MyReader.TextFieldType = FileIO.FieldType.Delimited
        '        MyReader.SetDelimiters(ControlChars.Tab)
        '        Dim currentRow As String()

        '        While Not MyReader.EndOfData
        '            Try
        '                currentRow = MyReader.ReadFields()
        '                Dim currentField As String
        '                Dim referencia As String = ""
        '                Dim n As Integer = 0
        '                For Each currentField In currentRow
        '                    If n = 0 Then
        '                        txt_referencia.Text = currentField
        '                    End If
        '                    If n = 1 Then
        '                        txtNombreMultidioma.Text = currentField
        '                    End If
        '                    n = n + 1
        '                Next
        '                conect.updateNombreCategoriaMultidiomaCsv()

        '            Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
        '                MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
        '            End Try
        '        End While
        '    End Using
        '    MsgBox("Proceso terminado")
        'Else
        '    MsgBox("No existe el archivo")
        'End If
    End Sub

    Private Sub Button1_Click_4(sender As Object, e As EventArgs) Handles Button1.Click
        Dim ruta2 As String = Me.txtFile2.Text
        If File.Exists(ruta2) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta2)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        Dim referencia As String = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                txt_referencia.Text = currentField
                            End If
                            If n = 1 Then
                                txt_precio.Text = currentField
                            End If
                            If n = 2 Then
                                txt_pdistribuidor.Text = currentField
                            End If
                            If n = 3 Then
                                importe_oferta.Text = currentField
                            End If
                            If n = 4 Then
                                fecha_inicio.Text = currentField
                            End If
                            n = n + 1
                        Next

                        'mira el PVD actual
                        conect.preciosActual()

                        conect.updateTarifaProductoDistribuidorCsv()
                        navconectar.navconectar()
                        navconectar.ACTUALIZA_TARIFAS_EXCEL1()

                        conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "Modificacion", "Importación de tarifas distribuidor (Importación Excel) del sku " & txt_referencia.Text)

                        If txt_pdistribuidor.Text <> "" And txt_pdistribuidor.Text <> "0" And txt_pdistribuidor.Text <> "0,00" Then
                            If Convert.ToDecimal(pvdAnterior.Text) > Convert.ToDecimal(txt_pdistribuidor.Text) Then
                                conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVD-DOWN", txt_referencia.Text & " ha bajado el PVD de " & pvdAnterior.Text & " a " & txt_pdistribuidor.Text)
                                If Convert.ToDecimal(pvpAnterior.Text) > Convert.ToDecimal(txt_precio.Text) Then
                                    conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVP-DOWN", txt_referencia.Text & " ha bajado el PVP de " & pvpAnterior.Text & " a " & txt_precio.Text)
                                End If
                            End If
                            If Convert.ToDecimal(pvdAnterior.Text) < Convert.ToDecimal(txt_pdistribuidor.Text) Then
                                conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVD-UP", txt_referencia.Text & " ha subido el PVD de " & pvdAnterior.Text & " a " & txt_pdistribuidor.Text)
                                If Convert.ToDecimal(pvpAnterior.Text) < Convert.ToDecimal(txt_precio.Text) Then
                                    conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "PVP-UP", txt_referencia.Text & " ha subido el PVP de " & pvpAnterior.Text & " a " & txt_precio.Text)
                                End If
                            End If
                        End If
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If
    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles Button4.Click
        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            txtFile2.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub ImportarBajoPedido_Click(sender As Object, e As EventArgs) Handles ImportarBajoPedido.Click
        Dim ruta2 As String = Me.txtFileImportar.Text
        If File.Exists(ruta2) Then
            Using MyReader As New Microsoft.VisualBasic.
                                    FileIO.TextFieldParser(ruta2)
                MyReader.TextFieldType = FileIO.FieldType.Delimited
                MyReader.SetDelimiters(ControlChars.Tab)
                Dim currentRow As String()

                While Not MyReader.EndOfData
                    Try
                        currentRow = MyReader.ReadFields()
                        Dim currentField As String
                        txt_referencia.Text = ""
                        Dim n As Integer = 0
                        For Each currentField In currentRow
                            If n = 0 Then
                                txt_referencia.Text = currentField
                            End If
                            n = n + 1
                        Next
                        conect.updateProductosBajoPedidoCsv()

                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
                    End Try
                End While
            End Using
            MsgBox("Proceso terminado")
        Else
            MsgBox("No existe el archivo")
        End If
    End Sub

    Private Sub ImportarEnvioDirecto_Click(sender As Object, e As EventArgs) Handles ImportarEnvioDirecto.Click
        'Dim ruta2 As String = Me.txtFileImportar.Text
        'If File.Exists(ruta2) Then
        '    Using MyReader As New Microsoft.VisualBasic.
        '                            FileIO.TextFieldParser(ruta2)
        '        MyReader.TextFieldType = FileIO.FieldType.Delimited
        '        MyReader.SetDelimiters(ControlChars.Tab)
        '        Dim currentRow As String()

        '        While Not MyReader.EndOfData
        '            Try
        '                currentRow = MyReader.ReadFields()
        '                Dim currentField As String
        '                txt_referencia.Text = ""
        '                Dim n As Integer = 0
        '                For Each currentField In currentRow
        '                    If n = 0 Then
        '                        txt_referencia.Text = currentField
        '                    End If
        '                    n = n + 1
        '                Next
        '                conect.updateProductosEnvioDirectoCsv()

        '            Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
        '                MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
        '            End Try
        '        End While
        '    End Using
        '    MsgBox("Proceso terminado")
        'Else
        '    MsgBox("No existe el archivo")
        'End If
    End Sub

    Private Sub ImportarProductoProveedorID_Click(sender As Object, e As EventArgs) Handles ImportarProductoProveedorID.Click
        'Dim ruta2 As String = Me.txtFileImportar.Text
        'If File.Exists(ruta2) Then
        '    Using MyReader As New Microsoft.VisualBasic.
        '                            FileIO.TextFieldParser(ruta2)
        '        MyReader.TextFieldType = FileIO.FieldType.Delimited
        '        MyReader.SetDelimiters(ControlChars.Tab)
        '        'MyReader.SetDelimiters(";")
        '        Dim currentRow As String()

        '        While Not MyReader.EndOfData
        '            Try
        '                currentRow = MyReader.ReadFields()
        '                Dim currentField As String
        '                txt_referencia.Text = ""
        '                txt_proveedorId.Text = ""

        '                Dim n As Integer = 0
        '                For Each currentField In currentRow
        '                    If n = 0 Then
        '                        txt_referencia.Text = currentField
        '                    End If
        '                    If n = 1 Then
        '                        txt_proveedorId.Text = currentField
        '                    End If
        '                    n = n + 1
        '                Next
        '                conect.updateProductosProveedorIdCsv()

        '                conect.savelogs(Me.txtUsuario.Text, txt_referencia.Text, "Modificacion", "Importación de proveedor " & txt_proveedorId.Text & " asociado a producto (Importación Excel) del sku " & txt_referencia.Text)

        '            Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
        '                MsgBox("Linea " & ex.Message & " no tiene un valor válido.")
        '            End Try
        '        End While
        '    End Using
        '    MsgBox("Proceso terminado")
        'Else
        '    MsgBox("No existe el archivo")
        'End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            txtFileImportar.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub txtLogs_Click(sender As Object, e As EventArgs) Handles txtLogs.Click
        Logs.Show()
        conect.verLogs()
    End Sub

    Private Sub Buscar1_Click(sender As Object, e As EventArgs) Handles busqueda1.Click

        GroupBoxPrecios.Enabled = True
        GroupBoxImportadorTarifas.Enabled = True
        GroupBoxImportadorTarifas1.Enabled = True
        GroupBoxImportadorTarifas2.Enabled = True

        a = 1

        If Me.Referencia.Text <> "" Then
            busquedasimple.busqueda_simple_referencia()
        End If

        If Me.Referencia.Text = "" And Me.Descripcion.Text <> "" Then
            busquedasimple.busqueda_simples()
        End If
    End Sub
End Class