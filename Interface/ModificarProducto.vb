﻿Imports MySql.Data.MySqlClient
Imports [Interface].PRODUCTOS
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.IO
Imports System.Net
'Imports CKEditor.NET
Imports System.Web
Imports System.Text
Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Xml
Imports MSXML2
Imports System.Threading

Public Class ModificarProducto

    Public imagen As Imagenes = New Imagenes
    Public navconectar As CONECTOR_NAVISION = New CONECTOR_NAVISION
    Public conect As web = New web
    Public preciodistribuidor As Calculo_pvp_distribuidor = New Calculo_pvp_distribuidor
    Public sqlconexion As SQLCONECTAR = New SQLCONECTAR
    Public z As Integer
    Public enviar As Images = New Images
    Public magento As MAGENTO = New MAGENTO
    Public a As Integer

    Private Sub btn_Cerrar_Click(sender As Object, e As EventArgs) Handles btn_Cerrar.Click
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        txt_Ids_Categorias.Text = ""
        If Me.txtId.Text <> "" Then
            conect.buscarModelo(Me.txtId.Text)
        Else
            MsgBox("Debes de introducir la ID de un producto")
        End If
    End Sub

    Private Sub btn_Editor_Click(sender As Object, e As EventArgs) Handles btn_Editor.Click
        Editorweb.ShowDialog()
    End Sub

    Private Sub btn_Modificar_Click(sender As Object, e As EventArgs) Handles btn_Modificar.Click
        'With ENLACE
        '.Descripcion.Text = Me.nombremodelo.Text
        'navision
        '.navconectar.navconectar()
        '.navconectar.MODIFICAR_MODELO()

        'mysql
        '.conect.myactualizar_mod()
        conect.updateModelo()
        'End With
    End Sub

    Private Sub DataGridProductosSimples_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridProductosSimples.CellContentClick
        Dim nFila = e.RowIndex
        Dim nColumna = e.ColumnIndex
        Me.DataGridProductosSimples.CurrentCell = Me.DataGridProductosSimples.Rows(nFila).Cells(nColumna)
        Dim sku As String = Convert.ToString(DataGridProductosSimples.CurrentRow.Cells(0).Value)
        Me.txtSku.Text = sku
        If Me.txtSku.Text <> "" Then
            conect.buscarSimple(Me.txtSku.Text)
            ModificarSimple.ShowDialog()
        Else
            MsgBox("No se ha seleccionado una referencia válida de producto simple")
        End If
    End Sub

    Private Sub btn_Nuevo_Click(sender As Object, e As EventArgs) Handles btn_Nuevo.Click
        CrearSimple.ShowDialog()
    End Sub

    Private Sub btnBuscarSku_Click(sender As Object, e As EventArgs) Handles btnBuscarSku.Click
        If Me.txt_Sku.Text <> "" Then
            'conect.buscarSku(Me.txt_Sku.Text)
            conect.buscarSkuLike(Me.txt_Sku.Text)
        Else
            MsgBox("Debes de introducir la Referencia/Sku de un producto")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_Ver.Click
        ImagenesProducto.ShowDialog()
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        Arbol_Categorias_PModificar.Dispose()
        Arbol_Categorias_PModificar.ShowDialog()
    End Sub
End Class