﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ModificarCategorias
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_Meta_Description = New System.Windows.Forms.Label()
        Me.txt_Meta_Keywords = New System.Windows.Forms.TextBox()
        Me.lbl_Meta_Keywords = New System.Windows.Forms.Label()
        Me.txt_Meta_Title = New System.Windows.Forms.TextBox()
        Me.lbl_Meta_Title = New System.Windows.Forms.Label()
        Me.txt_Categoria_Padre_Magento = New System.Windows.Forms.TextBox()
        Me.lbl_Categoria_Padre_Magento = New System.Windows.Forms.Label()
        Me.txt_Categoria_Padre = New System.Windows.Forms.TextBox()
        Me.lbl_Categoria_Padre = New System.Windows.Forms.Label()
        Me.txt_Id_Magento = New System.Windows.Forms.TextBox()
        Me.btn_Modificar = New System.Windows.Forms.Button()
        Me.txtNivel = New System.Windows.Forms.TextBox()
        Me.txt_Descripcion = New System.Windows.Forms.TextBox()
        Me.txt_Nombre = New System.Windows.Forms.TextBox()
        Me.txt_Id = New System.Windows.Forms.TextBox()
        Me.btn_Cerrar = New System.Windows.Forms.Button()
        Me.lbl_Producto_Configurable = New System.Windows.Forms.Label()
        Me.lbl_Id_Magento = New System.Windows.Forms.Label()
        Me.lbl_Nivel = New System.Windows.Forms.Label()
        Me.lbl_Descripcion = New System.Windows.Forms.Label()
        Me.lbl_Nombre = New System.Windows.Forms.Label()
        Me.lbl_Id = New System.Windows.Forms.Label()
        Me.btn_Buscar = New System.Windows.Forms.Button()
        Me.btn_Upload = New System.Windows.Forms.Button()
        Me.txtFile = New System.Windows.Forms.TextBox()
        Me.btn_Examinar = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.txt_Meta_Description = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblUploadExito = New System.Windows.Forms.Label()
        Me.Image = New System.Windows.Forms.PictureBox()
        Me.txt_Nombre_Anterior = New System.Windows.Forms.TextBox()
        Me.txt_Descripcion_Anterior = New System.Windows.Forms.TextBox()
        Me.txt_Nombre_Rev = New System.Windows.Forms.TextBox()
        Me.txt_Descripcion_Rev = New System.Windows.Forms.TextBox()
        Me.btn_Activar = New System.Windows.Forms.Button()
        Me.btn_Actualizar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.btn_categorias = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_Path_Categoria = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        CType(Me.Image, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_Meta_Description
        '
        Me.lbl_Meta_Description.AutoSize = True
        Me.lbl_Meta_Description.Location = New System.Drawing.Point(52, 476)
        Me.lbl_Meta_Description.Name = "lbl_Meta_Description"
        Me.lbl_Meta_Description.Size = New System.Drawing.Size(87, 13)
        Me.lbl_Meta_Description.TabIndex = 68
        Me.lbl_Meta_Description.Text = "Meta Description"
        '
        'txt_Meta_Keywords
        '
        Me.txt_Meta_Keywords.Location = New System.Drawing.Point(154, 416)
        Me.txt_Meta_Keywords.Multiline = True
        Me.txt_Meta_Keywords.Name = "txt_Meta_Keywords"
        Me.txt_Meta_Keywords.Size = New System.Drawing.Size(731, 45)
        Me.txt_Meta_Keywords.TabIndex = 67
        '
        'lbl_Meta_Keywords
        '
        Me.lbl_Meta_Keywords.AutoSize = True
        Me.lbl_Meta_Keywords.Location = New System.Drawing.Point(51, 416)
        Me.lbl_Meta_Keywords.Name = "lbl_Meta_Keywords"
        Me.lbl_Meta_Keywords.Size = New System.Drawing.Size(80, 13)
        Me.lbl_Meta_Keywords.TabIndex = 66
        Me.lbl_Meta_Keywords.Text = "Meta Keywords"
        '
        'txt_Meta_Title
        '
        Me.txt_Meta_Title.Location = New System.Drawing.Point(154, 380)
        Me.txt_Meta_Title.Name = "txt_Meta_Title"
        Me.txt_Meta_Title.Size = New System.Drawing.Size(731, 20)
        Me.txt_Meta_Title.TabIndex = 65
        '
        'lbl_Meta_Title
        '
        Me.lbl_Meta_Title.AutoSize = True
        Me.lbl_Meta_Title.Location = New System.Drawing.Point(51, 380)
        Me.lbl_Meta_Title.Name = "lbl_Meta_Title"
        Me.lbl_Meta_Title.Size = New System.Drawing.Size(54, 13)
        Me.lbl_Meta_Title.TabIndex = 64
        Me.lbl_Meta_Title.Text = "Meta Title"
        '
        'txt_Categoria_Padre_Magento
        '
        Me.txt_Categoria_Padre_Magento.Enabled = False
        Me.txt_Categoria_Padre_Magento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Categoria_Padre_Magento.Location = New System.Drawing.Point(454, 284)
        Me.txt_Categoria_Padre_Magento.Name = "txt_Categoria_Padre_Magento"
        Me.txt_Categoria_Padre_Magento.Size = New System.Drawing.Size(100, 20)
        Me.txt_Categoria_Padre_Magento.TabIndex = 63
        '
        'lbl_Categoria_Padre_Magento
        '
        Me.lbl_Categoria_Padre_Magento.AutoSize = True
        Me.lbl_Categoria_Padre_Magento.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.lbl_Categoria_Padre_Magento.ForeColor = System.Drawing.Color.Black
        Me.lbl_Categoria_Padre_Magento.Location = New System.Drawing.Point(307, 284)
        Me.lbl_Categoria_Padre_Magento.Name = "lbl_Categoria_Padre_Magento"
        Me.lbl_Categoria_Padre_Magento.Size = New System.Drawing.Size(141, 13)
        Me.lbl_Categoria_Padre_Magento.TabIndex = 62
        Me.lbl_Categoria_Padre_Magento.Text = "ID Categoria padre Magento"
        '
        'txt_Categoria_Padre
        '
        Me.txt_Categoria_Padre.Enabled = False
        Me.txt_Categoria_Padre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Categoria_Padre.Location = New System.Drawing.Point(454, 246)
        Me.txt_Categoria_Padre.Name = "txt_Categoria_Padre"
        Me.txt_Categoria_Padre.Size = New System.Drawing.Size(100, 20)
        Me.txt_Categoria_Padre.TabIndex = 61
        '
        'lbl_Categoria_Padre
        '
        Me.lbl_Categoria_Padre.AutoSize = True
        Me.lbl_Categoria_Padre.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.lbl_Categoria_Padre.ForeColor = System.Drawing.Color.Black
        Me.lbl_Categoria_Padre.Location = New System.Drawing.Point(307, 249)
        Me.lbl_Categoria_Padre.Name = "lbl_Categoria_Padre"
        Me.lbl_Categoria_Padre.Size = New System.Drawing.Size(96, 13)
        Me.lbl_Categoria_Padre.TabIndex = 60
        Me.lbl_Categoria_Padre.Text = "ID Categoria padre"
        '
        'txt_Id_Magento
        '
        Me.txt_Id_Magento.Enabled = False
        Me.txt_Id_Magento.Location = New System.Drawing.Point(154, 281)
        Me.txt_Id_Magento.Name = "txt_Id_Magento"
        Me.txt_Id_Magento.Size = New System.Drawing.Size(100, 20)
        Me.txt_Id_Magento.TabIndex = 59
        '
        'btn_Modificar
        '
        Me.btn_Modificar.Enabled = False
        Me.btn_Modificar.Location = New System.Drawing.Point(713, 617)
        Me.btn_Modificar.Name = "btn_Modificar"
        Me.btn_Modificar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Modificar.TabIndex = 58
        Me.btn_Modificar.Text = "Modificar"
        Me.btn_Modificar.UseVisualStyleBackColor = True
        '
        'txtNivel
        '
        Me.txtNivel.Enabled = False
        Me.txtNivel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNivel.Location = New System.Drawing.Point(154, 242)
        Me.txtNivel.Name = "txtNivel"
        Me.txtNivel.Size = New System.Drawing.Size(53, 20)
        Me.txtNivel.TabIndex = 57
        '
        'txt_Descripcion
        '
        Me.txt_Descripcion.Location = New System.Drawing.Point(154, 134)
        Me.txt_Descripcion.Multiline = True
        Me.txt_Descripcion.Name = "txt_Descripcion"
        Me.txt_Descripcion.Size = New System.Drawing.Size(731, 57)
        Me.txt_Descripcion.TabIndex = 56
        '
        'txt_Nombre
        '
        Me.txt_Nombre.Location = New System.Drawing.Point(154, 101)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.Size = New System.Drawing.Size(731, 20)
        Me.txt_Nombre.TabIndex = 55
        '
        'txt_Id
        '
        Me.txt_Id.Location = New System.Drawing.Point(154, 64)
        Me.txt_Id.Name = "txt_Id"
        Me.txt_Id.Size = New System.Drawing.Size(100, 20)
        Me.txt_Id.TabIndex = 54
        '
        'btn_Cerrar
        '
        Me.btn_Cerrar.Location = New System.Drawing.Point(810, 617)
        Me.btn_Cerrar.Name = "btn_Cerrar"
        Me.btn_Cerrar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cerrar.TabIndex = 53
        Me.btn_Cerrar.Text = "Cerrar"
        Me.btn_Cerrar.UseVisualStyleBackColor = True
        '
        'lbl_Producto_Configurable
        '
        Me.lbl_Producto_Configurable.AutoSize = True
        Me.lbl_Producto_Configurable.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Producto_Configurable.Location = New System.Drawing.Point(8, 14)
        Me.lbl_Producto_Configurable.Name = "lbl_Producto_Configurable"
        Me.lbl_Producto_Configurable.Size = New System.Drawing.Size(99, 24)
        Me.lbl_Producto_Configurable.TabIndex = 52
        Me.lbl_Producto_Configurable.Text = "Categoría"
        '
        'lbl_Id_Magento
        '
        Me.lbl_Id_Magento.AutoSize = True
        Me.lbl_Id_Magento.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.lbl_Id_Magento.ForeColor = System.Drawing.Color.Black
        Me.lbl_Id_Magento.Location = New System.Drawing.Point(51, 281)
        Me.lbl_Id_Magento.Name = "lbl_Id_Magento"
        Me.lbl_Id_Magento.Size = New System.Drawing.Size(63, 13)
        Me.lbl_Id_Magento.TabIndex = 51
        Me.lbl_Id_Magento.Text = "ID Magento"
        '
        'lbl_Nivel
        '
        Me.lbl_Nivel.AutoSize = True
        Me.lbl_Nivel.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.lbl_Nivel.ForeColor = System.Drawing.Color.Black
        Me.lbl_Nivel.Location = New System.Drawing.Point(52, 242)
        Me.lbl_Nivel.Name = "lbl_Nivel"
        Me.lbl_Nivel.Size = New System.Drawing.Size(31, 13)
        Me.lbl_Nivel.TabIndex = 50
        Me.lbl_Nivel.Text = "Nivel"
        '
        'lbl_Descripcion
        '
        Me.lbl_Descripcion.AutoSize = True
        Me.lbl_Descripcion.Location = New System.Drawing.Point(51, 137)
        Me.lbl_Descripcion.Name = "lbl_Descripcion"
        Me.lbl_Descripcion.Size = New System.Drawing.Size(63, 13)
        Me.lbl_Descripcion.TabIndex = 49
        Me.lbl_Descripcion.Text = "Descripción"
        '
        'lbl_Nombre
        '
        Me.lbl_Nombre.AutoSize = True
        Me.lbl_Nombre.Location = New System.Drawing.Point(51, 104)
        Me.lbl_Nombre.Name = "lbl_Nombre"
        Me.lbl_Nombre.Size = New System.Drawing.Size(44, 13)
        Me.lbl_Nombre.TabIndex = 48
        Me.lbl_Nombre.Text = "Nombre"
        '
        'lbl_Id
        '
        Me.lbl_Id.AutoSize = True
        Me.lbl_Id.Location = New System.Drawing.Point(51, 67)
        Me.lbl_Id.Name = "lbl_Id"
        Me.lbl_Id.Size = New System.Drawing.Size(68, 13)
        Me.lbl_Id.TabIndex = 47
        Me.lbl_Id.Text = "ID Categoría"
        '
        'btn_Buscar
        '
        Me.btn_Buscar.Location = New System.Drawing.Point(317, 62)
        Me.btn_Buscar.Name = "btn_Buscar"
        Me.btn_Buscar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Buscar.TabIndex = 70
        Me.btn_Buscar.Text = "Buscar"
        Me.btn_Buscar.UseVisualStyleBackColor = True
        '
        'btn_Upload
        '
        Me.btn_Upload.Enabled = False
        Me.btn_Upload.Location = New System.Drawing.Point(682, 566)
        Me.btn_Upload.Name = "btn_Upload"
        Me.btn_Upload.Size = New System.Drawing.Size(75, 23)
        Me.btn_Upload.TabIndex = 71
        Me.btn_Upload.Text = "Upload File"
        Me.btn_Upload.UseVisualStyleBackColor = True
        '
        'txtFile
        '
        Me.txtFile.Enabled = False
        Me.txtFile.Location = New System.Drawing.Point(271, 565)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.Size = New System.Drawing.Size(324, 20)
        Me.txtFile.TabIndex = 72
        '
        'btn_Examinar
        '
        Me.btn_Examinar.Enabled = False
        Me.btn_Examinar.Location = New System.Drawing.Point(601, 565)
        Me.btn_Examinar.Name = "btn_Examinar"
        Me.btn_Examinar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Examinar.TabIndex = 73
        Me.btn_Examinar.Text = "Examinar"
        Me.btn_Examinar.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'txt_Meta_Description
        '
        Me.txt_Meta_Description.Location = New System.Drawing.Point(154, 476)
        Me.txt_Meta_Description.Multiline = True
        Me.txt_Meta_Description.Name = "txt_Meta_Description"
        Me.txt_Meta_Description.Size = New System.Drawing.Size(731, 58)
        Me.txt_Meta_Description.TabIndex = 69
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(216, 567)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 74
        Me.Label1.Text = "Imágen"
        '
        'lblUploadExito
        '
        Me.lblUploadExito.AutoSize = True
        Me.lblUploadExito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUploadExito.Location = New System.Drawing.Point(764, 571)
        Me.lblUploadExito.Name = "lblUploadExito"
        Me.lblUploadExito.Size = New System.Drawing.Size(124, 13)
        Me.lblUploadExito.TabIndex = 75
        Me.lblUploadExito.Text = "¡¡¡ Upload Imágen !!!"
        Me.lblUploadExito.Visible = False
        '
        'Image
        '
        Me.Image.Location = New System.Drawing.Point(54, 561)
        Me.Image.Name = "Image"
        Me.Image.Size = New System.Drawing.Size(156, 87)
        Me.Image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Image.TabIndex = 76
        Me.Image.TabStop = False
        '
        'txt_Nombre_Anterior
        '
        Me.txt_Nombre_Anterior.Location = New System.Drawing.Point(890, 100)
        Me.txt_Nombre_Anterior.Name = "txt_Nombre_Anterior"
        Me.txt_Nombre_Anterior.Size = New System.Drawing.Size(20, 20)
        Me.txt_Nombre_Anterior.TabIndex = 77
        Me.txt_Nombre_Anterior.Visible = False
        '
        'txt_Descripcion_Anterior
        '
        Me.txt_Descripcion_Anterior.Location = New System.Drawing.Point(892, 144)
        Me.txt_Descripcion_Anterior.Name = "txt_Descripcion_Anterior"
        Me.txt_Descripcion_Anterior.Size = New System.Drawing.Size(20, 20)
        Me.txt_Descripcion_Anterior.TabIndex = 78
        Me.txt_Descripcion_Anterior.Visible = False
        '
        'txt_Nombre_Rev
        '
        Me.txt_Nombre_Rev.Location = New System.Drawing.Point(914, 99)
        Me.txt_Nombre_Rev.Name = "txt_Nombre_Rev"
        Me.txt_Nombre_Rev.Size = New System.Drawing.Size(17, 20)
        Me.txt_Nombre_Rev.TabIndex = 79
        Me.txt_Nombre_Rev.Visible = False
        '
        'txt_Descripcion_Rev
        '
        Me.txt_Descripcion_Rev.Location = New System.Drawing.Point(914, 144)
        Me.txt_Descripcion_Rev.Name = "txt_Descripcion_Rev"
        Me.txt_Descripcion_Rev.Size = New System.Drawing.Size(17, 20)
        Me.txt_Descripcion_Rev.TabIndex = 80
        Me.txt_Descripcion_Rev.Visible = False
        '
        'btn_Activar
        '
        Me.btn_Activar.Location = New System.Drawing.Point(780, 246)
        Me.btn_Activar.Name = "btn_Activar"
        Me.btn_Activar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Activar.TabIndex = 81
        Me.btn_Activar.Text = "Activar"
        Me.btn_Activar.UseVisualStyleBackColor = True
        '
        'btn_Actualizar
        '
        Me.btn_Actualizar.Enabled = False
        Me.btn_Actualizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_Actualizar.ForeColor = System.Drawing.Color.Black
        Me.btn_Actualizar.Location = New System.Drawing.Point(739, 67)
        Me.btn_Actualizar.Name = "btn_Actualizar"
        Me.btn_Actualizar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Actualizar.TabIndex = 82
        Me.btn_Actualizar.Text = "Actualizar"
        Me.btn_Actualizar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtPassword)
        Me.GroupBox1.Controls.Add(Me.btn_Actualizar)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Red
        Me.GroupBox1.Location = New System.Drawing.Point(41, 212)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(844, 123)
        Me.GroupBox1.TabIndex = 83
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Utilizar solamente por persona autorizada,  para cambios de ubicación de categori" & _
    "a"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(572, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 84
        Me.Label3.Text = "Contraseña"
        '
        'txtPassword
        '
        Me.txtPassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.Location = New System.Drawing.Point(641, 36)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(90, 20)
        Me.txtPassword.TabIndex = 83
        '
        'btn_categorias
        '
        Me.btn_categorias.Location = New System.Drawing.Point(259, 63)
        Me.btn_categorias.Name = "btn_categorias"
        Me.btn_categorias.Size = New System.Drawing.Size(55, 23)
        Me.btn_categorias.TabIndex = 84
        Me.btn_categorias.Text = "Arbol"
        Me.btn_categorias.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(409, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 13)
        Me.Label2.TabIndex = 85
        Me.Label2.Text = "Path"
        '
        'txt_Path_Categoria
        '
        Me.txt_Path_Categoria.Enabled = False
        Me.txt_Path_Categoria.Location = New System.Drawing.Point(444, 63)
        Me.txt_Path_Categoria.Name = "txt_Path_Categoria"
        Me.txt_Path_Categoria.Size = New System.Drawing.Size(444, 20)
        Me.txt_Path_Categoria.TabIndex = 86
        '
        'GroupBox2
        '
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.GroupBox2.Location = New System.Drawing.Point(35, 352)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(865, 192)
        Me.GroupBox2.TabIndex = 87
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Metas"
        '
        'ModificarCategorias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(937, 670)
        Me.ControlBox = False
        Me.Controls.Add(Me.txt_Path_Categoria)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btn_categorias)
        Me.Controls.Add(Me.btn_Activar)
        Me.Controls.Add(Me.txt_Descripcion_Rev)
        Me.Controls.Add(Me.txt_Nombre_Rev)
        Me.Controls.Add(Me.txt_Descripcion_Anterior)
        Me.Controls.Add(Me.txt_Nombre_Anterior)
        Me.Controls.Add(Me.Image)
        Me.Controls.Add(Me.lblUploadExito)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_Examinar)
        Me.Controls.Add(Me.txtFile)
        Me.Controls.Add(Me.btn_Upload)
        Me.Controls.Add(Me.btn_Buscar)
        Me.Controls.Add(Me.txt_Meta_Description)
        Me.Controls.Add(Me.lbl_Meta_Description)
        Me.Controls.Add(Me.txt_Meta_Keywords)
        Me.Controls.Add(Me.lbl_Meta_Keywords)
        Me.Controls.Add(Me.txt_Meta_Title)
        Me.Controls.Add(Me.lbl_Meta_Title)
        Me.Controls.Add(Me.txt_Categoria_Padre_Magento)
        Me.Controls.Add(Me.lbl_Categoria_Padre_Magento)
        Me.Controls.Add(Me.txt_Categoria_Padre)
        Me.Controls.Add(Me.lbl_Categoria_Padre)
        Me.Controls.Add(Me.txt_Id_Magento)
        Me.Controls.Add(Me.btn_Modificar)
        Me.Controls.Add(Me.txtNivel)
        Me.Controls.Add(Me.txt_Descripcion)
        Me.Controls.Add(Me.txt_Nombre)
        Me.Controls.Add(Me.txt_Id)
        Me.Controls.Add(Me.btn_Cerrar)
        Me.Controls.Add(Me.lbl_Producto_Configurable)
        Me.Controls.Add(Me.lbl_Id_Magento)
        Me.Controls.Add(Me.lbl_Nivel)
        Me.Controls.Add(Me.lbl_Descripcion)
        Me.Controls.Add(Me.lbl_Nombre)
        Me.Controls.Add(Me.lbl_Id)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "ModificarCategorias"
        Me.Text = "Modificar Categoria"
        CType(Me.Image, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_Meta_Description As System.Windows.Forms.Label
    Friend WithEvents txt_Meta_Keywords As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Meta_Keywords As System.Windows.Forms.Label
    Friend WithEvents txt_Meta_Title As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Meta_Title As System.Windows.Forms.Label
    Friend WithEvents txt_Categoria_Padre_Magento As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Categoria_Padre_Magento As System.Windows.Forms.Label
    Friend WithEvents txt_Categoria_Padre As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Categoria_Padre As System.Windows.Forms.Label
    Friend WithEvents txt_Id_Magento As System.Windows.Forms.TextBox
    Friend WithEvents btn_Modificar As System.Windows.Forms.Button
    Friend WithEvents txtNivel As System.Windows.Forms.TextBox
    Friend WithEvents txt_Descripcion As System.Windows.Forms.TextBox
    Friend WithEvents txt_Nombre As System.Windows.Forms.TextBox
    Friend WithEvents txt_Id As System.Windows.Forms.TextBox
    Friend WithEvents btn_Cerrar As System.Windows.Forms.Button
    Friend WithEvents lbl_Producto_Configurable As System.Windows.Forms.Label
    Friend WithEvents lbl_Id_Magento As System.Windows.Forms.Label
    Friend WithEvents lbl_Nivel As System.Windows.Forms.Label
    Friend WithEvents lbl_Descripcion As System.Windows.Forms.Label
    Friend WithEvents lbl_Nombre As System.Windows.Forms.Label
    Friend WithEvents lbl_Id As System.Windows.Forms.Label
    Friend WithEvents btn_Buscar As System.Windows.Forms.Button
    Friend WithEvents btn_Upload As System.Windows.Forms.Button
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents btn_Examinar As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txt_Meta_Description As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblUploadExito As System.Windows.Forms.Label
    Friend WithEvents Image As System.Windows.Forms.PictureBox
    Friend WithEvents txt_Nombre_Anterior As System.Windows.Forms.TextBox
    Friend WithEvents txt_Descripcion_Anterior As System.Windows.Forms.TextBox
    Friend WithEvents txt_Nombre_Rev As System.Windows.Forms.TextBox
    Friend WithEvents txt_Descripcion_Rev As System.Windows.Forms.TextBox
    Friend WithEvents btn_Activar As System.Windows.Forms.Button
    Friend WithEvents btn_Actualizar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_categorias As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_Path_Categoria As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
End Class
