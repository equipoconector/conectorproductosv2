﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Modelo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Editor = New System.Windows.Forms.Button()
        Me.editormodelos = New System.Windows.Forms.RichTextBox()
        Me.Modificar = New System.Windows.Forms.Button()
        Me.Buscar = New System.Windows.Forms.Button()
        Me.Estado = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Categoria = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.nombre_ampliado = New System.Windows.Forms.TextBox()
        Me.Cerrar = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.idmodelo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.guardar = New System.Windows.Forms.Button()
        Me.nombremodelo = New System.Windows.Forms.TextBox()
        Me.abre_imagen = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.guardar_imagen = New System.Windows.Forms.Button()
        Me.enviar_modelo = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.gestionimagen = New System.Windows.Forms.Button()
        Me.nombrexrec = New System.Windows.Forms.TextBox()
        Me.revision = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.descripcioncortat = New System.Windows.Forms.TextBox()
        Me.nombreampliacionrev = New System.Windows.Forms.TextBox()
        Me.nombrecortoxrec = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Editor
        '
        Me.Editor.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Editor.Location = New System.Drawing.Point(584, 272)
        Me.Editor.Name = "Editor"
        Me.Editor.Size = New System.Drawing.Size(125, 69)
        Me.Editor.TabIndex = 61
        Me.Editor.Text = "Editor"
        Me.Editor.UseVisualStyleBackColor = True
        '
        'editormodelos
        '
        Me.editormodelos.Location = New System.Drawing.Point(1286, 228)
        Me.editormodelos.Name = "editormodelos"
        Me.editormodelos.Size = New System.Drawing.Size(10, 19)
        Me.editormodelos.TabIndex = 60
        Me.editormodelos.Text = ""
        Me.editormodelos.Visible = False
        '
        'Modificar
        '
        Me.Modificar.BackColor = System.Drawing.Color.CadetBlue
        Me.Modificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Modificar.Location = New System.Drawing.Point(445, 478)
        Me.Modificar.Name = "Modificar"
        Me.Modificar.Size = New System.Drawing.Size(149, 40)
        Me.Modificar.TabIndex = 59
        Me.Modificar.Text = "Actualizar"
        Me.Modificar.UseVisualStyleBackColor = False
        '
        'Buscar
        '
        Me.Buscar.Location = New System.Drawing.Point(319, 68)
        Me.Buscar.Name = "Buscar"
        Me.Buscar.Size = New System.Drawing.Size(75, 23)
        Me.Buscar.TabIndex = 58
        Me.Buscar.Text = "Buscar"
        Me.Buscar.UseVisualStyleBackColor = True
        '
        'Estado
        '
        Me.Estado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Estado.Items.AddRange(New Object() {"VISIBLE", "OCULTO", "BLOQUEADO"})
        Me.Estado.Location = New System.Drawing.Point(102, 112)
        Me.Estado.Name = "Estado"
        Me.Estado.Size = New System.Drawing.Size(121, 21)
        Me.Estado.TabIndex = 57
        Me.Estado.Tag = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(33, 112)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 13)
        Me.Label5.TabIndex = 56
        Me.Label5.Text = "Estado"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(35, 272)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 55
        Me.Label4.Text = "Categoria"
        '
        'Categoria
        '
        Me.Categoria.Enabled = False
        Me.Categoria.Location = New System.Drawing.Point(104, 269)
        Me.Categoria.Name = "Categoria"
        Me.Categoria.Size = New System.Drawing.Size(82, 20)
        Me.Categoria.TabIndex = 54
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(1206, 153)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 13)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "Nombre Ampliado"
        Me.Label3.Visible = False
        '
        'nombre_ampliado
        '
        Me.nombre_ampliado.Location = New System.Drawing.Point(1302, 150)
        Me.nombre_ampliado.Name = "nombre_ampliado"
        Me.nombre_ampliado.Size = New System.Drawing.Size(31, 20)
        Me.nombre_ampliado.TabIndex = 52
        Me.nombre_ampliado.Visible = False
        '
        'Cerrar
        '
        Me.Cerrar.BackColor = System.Drawing.Color.Coral
        Me.Cerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cerrar.Location = New System.Drawing.Point(909, 34)
        Me.Cerrar.Name = "Cerrar"
        Me.Cerrar.Size = New System.Drawing.Size(112, 91)
        Me.Cerrar.TabIndex = 51
        Me.Cerrar.Text = "Cerrar"
        Me.Cerrar.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(33, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 50
        Me.Label2.Text = "Id Modelo"
        '
        'idmodelo
        '
        Me.idmodelo.Location = New System.Drawing.Point(102, 70)
        Me.idmodelo.Name = "idmodelo"
        Me.idmodelo.Size = New System.Drawing.Size(82, 20)
        Me.idmodelo.TabIndex = 49
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(33, 152)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 48
        Me.Label1.Text = "Nombre"
        '
        'guardar
        '
        Me.guardar.Location = New System.Drawing.Point(203, 67)
        Me.guardar.Name = "guardar"
        Me.guardar.Size = New System.Drawing.Size(110, 23)
        Me.guardar.TabIndex = 47
        Me.guardar.Text = "Nuevo Modelo"
        Me.guardar.UseVisualStyleBackColor = True
        '
        'nombremodelo
        '
        Me.nombremodelo.Location = New System.Drawing.Point(102, 149)
        Me.nombremodelo.Name = "nombremodelo"
        Me.nombremodelo.Size = New System.Drawing.Size(919, 20)
        Me.nombremodelo.TabIndex = 46
        '
        'abre_imagen
        '
        Me.abre_imagen.Location = New System.Drawing.Point(1222, 295)
        Me.abre_imagen.Name = "abre_imagen"
        Me.abre_imagen.Size = New System.Drawing.Size(75, 46)
        Me.abre_imagen.TabIndex = 62
        Me.abre_imagen.Text = "Seleccionar Imagen"
        Me.abre_imagen.UseVisualStyleBackColor = True
        Me.abre_imagen.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(1213, 234)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(37, 34)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 63
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(1233, 478)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(64, 50)
        Me.PictureBox2.TabIndex = 64
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'guardar_imagen
        '
        Me.guardar_imagen.Location = New System.Drawing.Point(1222, 365)
        Me.guardar_imagen.Name = "guardar_imagen"
        Me.guardar_imagen.Size = New System.Drawing.Size(75, 54)
        Me.guardar_imagen.TabIndex = 65
        Me.guardar_imagen.Text = "Guardar Imagen"
        Me.guardar_imagen.UseVisualStyleBackColor = True
        Me.guardar_imagen.Visible = False
        '
        'enviar_modelo
        '
        Me.enviar_modelo.Location = New System.Drawing.Point(1222, 449)
        Me.enviar_modelo.Name = "enviar_modelo"
        Me.enviar_modelo.Size = New System.Drawing.Size(75, 23)
        Me.enviar_modelo.TabIndex = 66
        Me.enviar_modelo.Text = "ENVIAR"
        Me.enviar_modelo.UseVisualStyleBackColor = True
        Me.enviar_modelo.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.CadetBlue
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(676, 477)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(113, 41)
        Me.Button1.TabIndex = 67
        Me.Button1.Text = "Actualizar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.CadetBlue
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(804, 477)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(217, 40)
        Me.Button2.TabIndex = 68
        Me.Button2.Text = "Nuevo Configurable"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'gestionimagen
        '
        Me.gestionimagen.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gestionimagen.Location = New System.Drawing.Point(742, 272)
        Me.gestionimagen.Name = "gestionimagen"
        Me.gestionimagen.Size = New System.Drawing.Size(126, 69)
        Me.gestionimagen.TabIndex = 70
        Me.gestionimagen.Text = "Imágenes"
        Me.gestionimagen.UseVisualStyleBackColor = True
        '
        'nombrexrec
        '
        Me.nombrexrec.Location = New System.Drawing.Point(1279, 564)
        Me.nombrexrec.Name = "nombrexrec"
        Me.nombrexrec.Size = New System.Drawing.Size(35, 20)
        Me.nombrexrec.TabIndex = 71
        Me.nombrexrec.Visible = False
        '
        'revision
        '
        Me.revision.Location = New System.Drawing.Point(1279, 597)
        Me.revision.Name = "revision"
        Me.revision.Size = New System.Drawing.Size(34, 20)
        Me.revision.TabIndex = 72
        Me.revision.Visible = False
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(895, 272)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(126, 69)
        Me.Button3.TabIndex = 73
        Me.Button3.Text = "Categorías Iniciales"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(33, 207)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(90, 13)
        Me.Label6.TabIndex = 74
        Me.Label6.Text = "Descripcion corta"
        '
        'descripcioncortat
        '
        Me.descripcioncortat.Location = New System.Drawing.Point(129, 180)
        Me.descripcioncortat.MaxLength = 250
        Me.descripcioncortat.Multiline = True
        Me.descripcioncortat.Name = "descripcioncortat"
        Me.descripcioncortat.Size = New System.Drawing.Size(892, 64)
        Me.descripcioncortat.TabIndex = 75
        '
        'nombreampliacionrev
        '
        Me.nombreampliacionrev.Location = New System.Drawing.Point(1225, 597)
        Me.nombreampliacionrev.Name = "nombreampliacionrev"
        Me.nombreampliacionrev.Size = New System.Drawing.Size(14, 20)
        Me.nombreampliacionrev.TabIndex = 76
        Me.nombreampliacionrev.Visible = False
        '
        'nombrecortoxrec
        '
        Me.nombrecortoxrec.Location = New System.Drawing.Point(1222, 564)
        Me.nombrecortoxrec.Name = "nombrecortoxrec"
        Me.nombrecortoxrec.Size = New System.Drawing.Size(17, 20)
        Me.nombrecortoxrec.TabIndex = 77
        Me.nombrecortoxrec.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(32, 22)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(111, 24)
        Me.Label7.TabIndex = 78
        Me.Label7.Text = "MODELOS"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(763, 435)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(126, 31)
        Me.Label35.TabIndex = 164
        Me.Label35.Text = "Magento"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(412, 435)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(213, 31)
        Me.Label34.TabIndex = 165
        Me.Label34.Text = "Mysql-Navision"
        '
        'Modelo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1058, 540)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.nombrecortoxrec)
        Me.Controls.Add(Me.nombreampliacionrev)
        Me.Controls.Add(Me.descripcioncortat)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.revision)
        Me.Controls.Add(Me.nombrexrec)
        Me.Controls.Add(Me.gestionimagen)
        Me.Controls.Add(Me.enviar_modelo)
        Me.Controls.Add(Me.guardar_imagen)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.abre_imagen)
        Me.Controls.Add(Me.Editor)
        Me.Controls.Add(Me.editormodelos)
        Me.Controls.Add(Me.Modificar)
        Me.Controls.Add(Me.Buscar)
        Me.Controls.Add(Me.Estado)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Categoria)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.nombre_ampliado)
        Me.Controls.Add(Me.Cerrar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.idmodelo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.guardar)
        Me.Controls.Add(Me.nombremodelo)
        Me.Name = "Modelo"
        Me.Text = "Modelo"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Editor As System.Windows.Forms.Button
    Friend WithEvents editormodelos As System.Windows.Forms.RichTextBox
    Friend WithEvents Modificar As System.Windows.Forms.Button
    Friend WithEvents Buscar As System.Windows.Forms.Button
    Friend WithEvents Estado As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Categoria As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents nombre_ampliado As System.Windows.Forms.TextBox
    Friend WithEvents Cerrar As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents idmodelo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents guardar As System.Windows.Forms.Button
    Friend WithEvents nombremodelo As System.Windows.Forms.TextBox
    Friend WithEvents abre_imagen As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents guardar_imagen As System.Windows.Forms.Button
    Friend WithEvents enviar_modelo As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents gestionimagen As System.Windows.Forms.Button
    Friend WithEvents nombrexrec As System.Windows.Forms.TextBox
    Friend WithEvents revision As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents descripcioncortat As System.Windows.Forms.TextBox
    Friend WithEvents nombreampliacionrev As System.Windows.Forms.TextBox
    Friend WithEvents nombrecortoxrec As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
End Class
