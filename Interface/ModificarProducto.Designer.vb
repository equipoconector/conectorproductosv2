﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ModificarProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_Editor = New System.Windows.Forms.Button()
        Me.btn_Modificar = New System.Windows.Forms.Button()
        Me.ComboEstado = New System.Windows.Forms.ComboBox()
        Me.txtIdCategoria = New System.Windows.Forms.TextBox()
        Me.txtDescripcionCorta = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.btn_Cerrar = New System.Windows.Forms.Button()
        Me.lbl_Producto_Configurable = New System.Windows.Forms.Label()
        Me.lbl_Estado = New System.Windows.Forms.Label()
        Me.lbl_Id_Categoria = New System.Windows.Forms.Label()
        Me.lbl_Desc_Larga = New System.Windows.Forms.Label()
        Me.lbl_Desc_Corta = New System.Windows.Forms.Label()
        Me.lbl_Nombre = New System.Windows.Forms.Label()
        Me.lbl_Id = New System.Windows.Forms.Label()
        Me.DataGridProductosSimples = New System.Windows.Forms.DataGridView()
        Me.btn_Buscar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSku = New System.Windows.Forms.TextBox()
        Me.btn_Nuevo = New System.Windows.Forms.Button()
        Me.btnBuscarSku = New System.Windows.Forms.Button()
        Me.lbl_Sku = New System.Windows.Forms.Label()
        Me.txt_Sku = New System.Windows.Forms.TextBox()
        Me.txtNombre_Anterior = New System.Windows.Forms.TextBox()
        Me.txtNombre_Rev = New System.Windows.Forms.TextBox()
        Me.txtDescripcionCorta_Anterior = New System.Windows.Forms.TextBox()
        Me.txtDescripcionCorta_Rev = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btn_Ver = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txt_Ids_Categorias = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_Ids_Principales = New System.Windows.Forms.TextBox()
        CType(Me.DataGridProductosSimples, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_Editor
        '
        Me.btn_Editor.Enabled = False
        Me.btn_Editor.Location = New System.Drawing.Point(173, 380)
        Me.btn_Editor.Name = "btn_Editor"
        Me.btn_Editor.Size = New System.Drawing.Size(83, 23)
        Me.btn_Editor.TabIndex = 35
        Me.btn_Editor.Text = "Editor"
        Me.btn_Editor.UseVisualStyleBackColor = True
        '
        'btn_Modificar
        '
        Me.btn_Modificar.Enabled = False
        Me.btn_Modificar.Location = New System.Drawing.Point(737, 623)
        Me.btn_Modificar.Name = "btn_Modificar"
        Me.btn_Modificar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Modificar.TabIndex = 34
        Me.btn_Modificar.Text = "Modificar"
        Me.btn_Modificar.UseVisualStyleBackColor = True
        '
        'ComboEstado
        '
        Me.ComboEstado.FormattingEnabled = True
        Me.ComboEstado.Items.AddRange(New Object() {"OCULTO", "VISIBLE"})
        Me.ComboEstado.Location = New System.Drawing.Point(173, 307)
        Me.ComboEstado.Name = "ComboEstado"
        Me.ComboEstado.Size = New System.Drawing.Size(121, 21)
        Me.ComboEstado.TabIndex = 33
        '
        'txtIdCategoria
        '
        Me.txtIdCategoria.Enabled = False
        Me.txtIdCategoria.Location = New System.Drawing.Point(173, 255)
        Me.txtIdCategoria.Name = "txtIdCategoria"
        Me.txtIdCategoria.Size = New System.Drawing.Size(70, 20)
        Me.txtIdCategoria.TabIndex = 31
        '
        'txtDescripcionCorta
        '
        Me.txtDescripcionCorta.Location = New System.Drawing.Point(173, 178)
        Me.txtDescripcionCorta.Multiline = True
        Me.txtDescripcionCorta.Name = "txtDescripcionCorta"
        Me.txtDescripcionCorta.Size = New System.Drawing.Size(731, 57)
        Me.txtDescripcionCorta.TabIndex = 30
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(173, 138)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(731, 20)
        Me.txtNombre.TabIndex = 29
        '
        'txtId
        '
        Me.txtId.Location = New System.Drawing.Point(173, 101)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(100, 20)
        Me.txtId.TabIndex = 28
        '
        'btn_Cerrar
        '
        Me.btn_Cerrar.Location = New System.Drawing.Point(829, 623)
        Me.btn_Cerrar.Name = "btn_Cerrar"
        Me.btn_Cerrar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cerrar.TabIndex = 27
        Me.btn_Cerrar.Text = "Cerrar"
        Me.btn_Cerrar.UseVisualStyleBackColor = True
        '
        'lbl_Producto_Configurable
        '
        Me.lbl_Producto_Configurable.AutoSize = True
        Me.lbl_Producto_Configurable.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Producto_Configurable.Location = New System.Drawing.Point(27, 32)
        Me.lbl_Producto_Configurable.Name = "lbl_Producto_Configurable"
        Me.lbl_Producto_Configurable.Size = New System.Drawing.Size(216, 24)
        Me.lbl_Producto_Configurable.TabIndex = 26
        Me.lbl_Producto_Configurable.Text = "Producto configurable"
        '
        'lbl_Estado
        '
        Me.lbl_Estado.AutoSize = True
        Me.lbl_Estado.Location = New System.Drawing.Point(74, 310)
        Me.lbl_Estado.Name = "lbl_Estado"
        Me.lbl_Estado.Size = New System.Drawing.Size(40, 13)
        Me.lbl_Estado.TabIndex = 25
        Me.lbl_Estado.Text = "Estado"
        '
        'lbl_Id_Categoria
        '
        Me.lbl_Id_Categoria.AutoSize = True
        Me.lbl_Id_Categoria.Location = New System.Drawing.Point(70, 257)
        Me.lbl_Id_Categoria.Name = "lbl_Id_Categoria"
        Me.lbl_Id_Categoria.Size = New System.Drawing.Size(66, 13)
        Me.lbl_Id_Categoria.TabIndex = 23
        Me.lbl_Id_Categoria.Text = "ID Categoria"
        '
        'lbl_Desc_Larga
        '
        Me.lbl_Desc_Larga.AutoSize = True
        Me.lbl_Desc_Larga.Location = New System.Drawing.Point(70, 385)
        Me.lbl_Desc_Larga.Name = "lbl_Desc_Larga"
        Me.lbl_Desc_Larga.Size = New System.Drawing.Size(89, 13)
        Me.lbl_Desc_Larga.TabIndex = 22
        Me.lbl_Desc_Larga.Text = "Descripción larga"
        '
        'lbl_Desc_Corta
        '
        Me.lbl_Desc_Corta.AutoSize = True
        Me.lbl_Desc_Corta.Location = New System.Drawing.Point(70, 181)
        Me.lbl_Desc_Corta.Name = "lbl_Desc_Corta"
        Me.lbl_Desc_Corta.Size = New System.Drawing.Size(90, 13)
        Me.lbl_Desc_Corta.TabIndex = 21
        Me.lbl_Desc_Corta.Text = "Descripción corta"
        '
        'lbl_Nombre
        '
        Me.lbl_Nombre.AutoSize = True
        Me.lbl_Nombre.Location = New System.Drawing.Point(70, 141)
        Me.lbl_Nombre.Name = "lbl_Nombre"
        Me.lbl_Nombre.Size = New System.Drawing.Size(44, 13)
        Me.lbl_Nombre.TabIndex = 20
        Me.lbl_Nombre.Text = "Nombre"
        '
        'lbl_Id
        '
        Me.lbl_Id.AutoSize = True
        Me.lbl_Id.Location = New System.Drawing.Point(70, 104)
        Me.lbl_Id.Name = "lbl_Id"
        Me.lbl_Id.Size = New System.Drawing.Size(64, 13)
        Me.lbl_Id.TabIndex = 19
        Me.lbl_Id.Text = "ID Producto"
        '
        'DataGridProductosSimples
        '
        Me.DataGridProductosSimples.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridProductosSimples.Location = New System.Drawing.Point(74, 450)
        Me.DataGridProductosSimples.Name = "DataGridProductosSimples"
        Me.DataGridProductosSimples.Size = New System.Drawing.Size(830, 150)
        Me.DataGridProductosSimples.TabIndex = 36
        '
        'btn_Buscar
        '
        Me.btn_Buscar.Location = New System.Drawing.Point(293, 100)
        Me.btn_Buscar.Name = "btn_Buscar"
        Me.btn_Buscar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Buscar.TabIndex = 37
        Me.btn_Buscar.Text = "Buscar"
        Me.btn_Buscar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(72, 423)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(181, 24)
        Me.Label1.TabIndex = 38
        Me.Label1.Text = "Productos simples"
        '
        'txtSku
        '
        Me.txtSku.Enabled = False
        Me.txtSku.Location = New System.Drawing.Point(804, 423)
        Me.txtSku.Name = "txtSku"
        Me.txtSku.Size = New System.Drawing.Size(100, 20)
        Me.txtSku.TabIndex = 39
        Me.txtSku.Visible = False
        '
        'btn_Nuevo
        '
        Me.btn_Nuevo.Enabled = False
        Me.btn_Nuevo.Location = New System.Drawing.Point(74, 604)
        Me.btn_Nuevo.Name = "btn_Nuevo"
        Me.btn_Nuevo.Size = New System.Drawing.Size(75, 23)
        Me.btn_Nuevo.TabIndex = 40
        Me.btn_Nuevo.Text = "Nuevo"
        Me.btn_Nuevo.UseVisualStyleBackColor = True
        '
        'btnBuscarSku
        '
        Me.btnBuscarSku.Location = New System.Drawing.Point(723, 101)
        Me.btnBuscarSku.Name = "btnBuscarSku"
        Me.btnBuscarSku.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscarSku.TabIndex = 43
        Me.btnBuscarSku.Text = "Buscar"
        Me.btnBuscarSku.UseVisualStyleBackColor = True
        '
        'lbl_Sku
        '
        Me.lbl_Sku.AutoSize = True
        Me.lbl_Sku.Location = New System.Drawing.Point(448, 106)
        Me.lbl_Sku.Name = "lbl_Sku"
        Me.lbl_Sku.Size = New System.Drawing.Size(83, 13)
        Me.lbl_Sku.TabIndex = 44
        Me.lbl_Sku.Text = "Referencia/Sku"
        '
        'txt_Sku
        '
        Me.txt_Sku.Location = New System.Drawing.Point(541, 102)
        Me.txt_Sku.Name = "txt_Sku"
        Me.txt_Sku.Size = New System.Drawing.Size(163, 20)
        Me.txt_Sku.TabIndex = 45
        '
        'txtNombre_Anterior
        '
        Me.txtNombre_Anterior.Location = New System.Drawing.Point(908, 138)
        Me.txtNombre_Anterior.Name = "txtNombre_Anterior"
        Me.txtNombre_Anterior.Size = New System.Drawing.Size(12, 20)
        Me.txtNombre_Anterior.TabIndex = 46
        Me.txtNombre_Anterior.Visible = False
        '
        'txtNombre_Rev
        '
        Me.txtNombre_Rev.Location = New System.Drawing.Point(926, 138)
        Me.txtNombre_Rev.Name = "txtNombre_Rev"
        Me.txtNombre_Rev.Size = New System.Drawing.Size(10, 20)
        Me.txtNombre_Rev.TabIndex = 47
        Me.txtNombre_Rev.Visible = False
        '
        'txtDescripcionCorta_Anterior
        '
        Me.txtDescripcionCorta_Anterior.Location = New System.Drawing.Point(908, 178)
        Me.txtDescripcionCorta_Anterior.Name = "txtDescripcionCorta_Anterior"
        Me.txtDescripcionCorta_Anterior.Size = New System.Drawing.Size(12, 20)
        Me.txtDescripcionCorta_Anterior.TabIndex = 48
        Me.txtDescripcionCorta_Anterior.Visible = False
        '
        'txtDescripcionCorta_Rev
        '
        Me.txtDescripcionCorta_Rev.Location = New System.Drawing.Point(925, 178)
        Me.txtDescripcionCorta_Rev.Name = "txtDescripcionCorta_Rev"
        Me.txtDescripcionCorta_Rev.Size = New System.Drawing.Size(10, 20)
        Me.txtDescripcionCorta_Rev.TabIndex = 49
        Me.txtDescripcionCorta_Rev.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(322, 387)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 51
        Me.Label3.Text = "Imágenes"
        '
        'btn_Ver
        '
        Me.btn_Ver.Enabled = False
        Me.btn_Ver.Location = New System.Drawing.Point(390, 382)
        Me.btn_Ver.Name = "btn_Ver"
        Me.btn_Ver.Size = New System.Drawing.Size(75, 23)
        Me.btn_Ver.TabIndex = 52
        Me.btn_Ver.Text = "Ver"
        Me.btn_Ver.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(829, 257)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 53
        Me.Button1.Text = "Arbol"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txt_Ids_Categorias
        '
        Me.txt_Ids_Categorias.Enabled = False
        Me.txt_Ids_Categorias.Location = New System.Drawing.Point(344, 257)
        Me.txt_Ids_Categorias.Name = "txt_Ids_Categorias"
        Me.txt_Ids_Categorias.Size = New System.Drawing.Size(479, 20)
        Me.txt_Ids_Categorias.TabIndex = 54
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(260, 259)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 55
        Me.Label2.Text = "ID's Categorias"
        '
        'txt_Ids_Principales
        '
        Me.txt_Ids_Principales.Enabled = False
        Me.txt_Ids_Principales.Location = New System.Drawing.Point(344, 284)
        Me.txt_Ids_Principales.Name = "txt_Ids_Principales"
        Me.txt_Ids_Principales.Size = New System.Drawing.Size(479, 20)
        Me.txt_Ids_Principales.TabIndex = 56
        Me.txt_Ids_Principales.Visible = False
        '
        'ModificarProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 668)
        Me.ControlBox = False
        Me.Controls.Add(Me.txt_Ids_Principales)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_Ids_Categorias)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btn_Ver)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDescripcionCorta_Rev)
        Me.Controls.Add(Me.txtDescripcionCorta_Anterior)
        Me.Controls.Add(Me.txtNombre_Rev)
        Me.Controls.Add(Me.txtNombre_Anterior)
        Me.Controls.Add(Me.txt_Sku)
        Me.Controls.Add(Me.lbl_Sku)
        Me.Controls.Add(Me.btnBuscarSku)
        Me.Controls.Add(Me.btn_Nuevo)
        Me.Controls.Add(Me.txtSku)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_Buscar)
        Me.Controls.Add(Me.DataGridProductosSimples)
        Me.Controls.Add(Me.btn_Editor)
        Me.Controls.Add(Me.btn_Modificar)
        Me.Controls.Add(Me.ComboEstado)
        Me.Controls.Add(Me.txtIdCategoria)
        Me.Controls.Add(Me.txtDescripcionCorta)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.txtId)
        Me.Controls.Add(Me.btn_Cerrar)
        Me.Controls.Add(Me.lbl_Producto_Configurable)
        Me.Controls.Add(Me.lbl_Estado)
        Me.Controls.Add(Me.lbl_Id_Categoria)
        Me.Controls.Add(Me.lbl_Desc_Larga)
        Me.Controls.Add(Me.lbl_Desc_Corta)
        Me.Controls.Add(Me.lbl_Nombre)
        Me.Controls.Add(Me.lbl_Id)
        Me.Name = "ModificarProducto"
        Me.Text = "Modificar Producto"
        CType(Me.DataGridProductosSimples, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Editor As System.Windows.Forms.Button
    Friend WithEvents btn_Modificar As System.Windows.Forms.Button
    Friend WithEvents ComboEstado As System.Windows.Forms.ComboBox
    Friend WithEvents txtIdCategoria As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcionCorta As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents btn_Cerrar As System.Windows.Forms.Button
    Friend WithEvents lbl_Producto_Configurable As System.Windows.Forms.Label
    Friend WithEvents lbl_Estado As System.Windows.Forms.Label
    Friend WithEvents lbl_Id_Categoria As System.Windows.Forms.Label
    Friend WithEvents lbl_Desc_Larga As System.Windows.Forms.Label
    Friend WithEvents lbl_Desc_Corta As System.Windows.Forms.Label
    Friend WithEvents lbl_Nombre As System.Windows.Forms.Label
    Friend WithEvents lbl_Id As System.Windows.Forms.Label
    Friend WithEvents DataGridProductosSimples As System.Windows.Forms.DataGridView
    Friend WithEvents btn_Buscar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSku As System.Windows.Forms.TextBox
    Friend WithEvents btn_Nuevo As System.Windows.Forms.Button
    Friend WithEvents btnBuscarSku As System.Windows.Forms.Button
    Friend WithEvents lbl_Sku As System.Windows.Forms.Label
    Friend WithEvents txt_Sku As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre_Anterior As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre_Rev As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcionCorta_Anterior As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcionCorta_Rev As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btn_Ver As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txt_Ids_Categorias As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_Ids_Principales As System.Windows.Forms.TextBox
End Class
