﻿Imports System
Imports System.IO
Imports MySql.Data.MySqlClient
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Data
Imports System.Deployment
Imports System.Xml
Imports System.Threading
Imports [Interface].MagentoService
Imports System.Drawing.Imaging.ImageFormat
Imports WinSCP


Public Class Images
    Dim incrementop As String
    Dim y As Integer = 0
    'Dim small As String = "\\192.168.2.6\images\tsmall\"
    'Dim big As String = "\\192.168.2.6\images\tbig\"
    'Dim large As String = "\\192.168.2.6\images\tlarge\"
    'Dim medium As String = "\\192.168.2.6\images\tmedium\"
    Dim directorio_principal As String = "\\192.168.2.6\imagenes\img_agrupado\"
    Public mageconect As String = "https://quirumed.on-dev.com/scripts/pre/"
    Public referencia As String
    Public incremento As String

    ' PROCESOS PARA EL MODELO --------------------------------------------------------------------------------------------------------------------

    Public Sub Crear_carpeta_modelo()
        Modelo.i = Modelo.i + 1
        Gestion_imagen_modelos.PictureBox1.Image.Save("\\192.168.2.6\imagenes\img_agrupado\" & incremento & ".jpg", Imaging.ImageFormat.Jpeg)
        Gestion_imagen_modelos.PictureBox2.Image = Image.FromFile(directorio_principal & "\" & incremento & ".jpg")
        Gestion_imagen_modelos.nombreimgen.Text = incremento
        subir_producto_agrupado()
    End Sub

    Public Sub imagen_big()
        'Dim original As Image = Image.FromFile(directorio_principal & "\" & incremento & ".jpg")
        'Dim resized As Image = ResizeImage(original, New Size(800, 800))
        'Dim memStream As MemoryStream = New MemoryStream()
        'resized.Save(memStream, ImageFormat.Jpeg)
        'Dim bmp2 As New Bitmap(memStream)
        'Gestion_imagen_modelos.PictureBox2.Image = New Bitmap(bmp2)
        'Gestion_imagen_modelos.PictureBox2.Image.Save(big & incremento & ".jpg", Imaging.ImageFormat.Jpeg)
        'memStream.Dispose()
    End Sub

    Public Sub imagen_large()
        'Dim original As Image = Image.FromFile(directorio_principal & "\" & incremento & ".jpg")
        'Dim resized As Image = ResizeImage(original, New Size(400, 400))
        'Dim memStream As MemoryStream = New MemoryStream()
        'resized.Save(memStream, ImageFormat.Jpeg)
        'Dim bmp2 As New Bitmap(memStream)
        'Gestion_imagen_modelos.PictureBox2.Image = New Bitmap(bmp2)
        'Gestion_imagen_modelos.PictureBox2.Image.Save(large & incremento & ".jpg", Imaging.ImageFormat.Jpeg)
        'memStream.Dispose()
    End Sub

    Public Sub imagen_medium()
        'Dim original As Image = Image.FromFile(directorio_principal & "\" & incremento & ".jpg")
        'Dim resized As Image = ResizeImage(original, New Size(125, 125))
        'Dim memStream As MemoryStream = New MemoryStream()
        'resized.Save(memStream, ImageFormat.Jpeg)
        'Dim bmp2 As New Bitmap(memStream)
        'Gestion_imagen_modelos.PictureBox2.Image = New Bitmap(bmp2)
        'Gestion_imagen_modelos.PictureBox2.Image.Save(medium & incremento & ".jpg", Imaging.ImageFormat.Jpeg)
        'memStream.Dispose()
    End Sub

    Public Sub imagen_small()
        'Dim original As Image = Image.FromFile(directorio_principal & "\" & incremento & ".jpg")
        'Dim resized As Image = ResizeImage(original, New Size(65, 65))
        'Dim memStream As MemoryStream = New MemoryStream()
        'resized.Save(memStream, ImageFormat.Jpeg)
        'Dim bmp2 As New Bitmap(memStream)
        'Gestion_imagen_modelos.PictureBox2.Image = New Bitmap(bmp2)
        'Gestion_imagen_modelos.PictureBox2.Image.Save(small & incremento & ".jpg", Imaging.ImageFormat.Jpeg)
        'memStream.Dispose()
    End Sub

    Public Shared Function ResizeImage(ByVal image As Image,
         ByVal size As Size, Optional ByVal preserveAspectRatio As Boolean = True) As Image
        Dim newWidth As Integer
        Dim newHeight As Integer
        If preserveAspectRatio Then
            Dim originalWidth As Integer = image.Width
            Dim originalHeight As Integer = image.Height
            Dim percentWidth As Single = CSng(size.Width) / CSng(originalWidth)
            Dim percentHeight As Single = CSng(size.Height) / CSng(originalHeight)
            Dim percent As Single = If(percentHeight < percentWidth,
                    percentHeight, percentWidth)
            newWidth = CInt(originalWidth * percent)
            newHeight = CInt(originalHeight * percent)
        Else
            newWidth = size.Width
            newHeight = size.Height
        End If
        Dim newImage As Image = New Bitmap(newWidth, newHeight)
        Using graphicsHandle As Graphics = Graphics.FromImage(newImage)
            graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic
            graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight)
        End Using
        Return newImage
    End Function

    Public Sub ftp_modelos_small()
        ' My.Computer.Network.UploadFile("\\192.168.2.6\images\tsmall\" & incremento & ".jpg", "ftp://ftp.95.39.14.35/public_html/images/productos/tsmall/" & incremento & ".jpg", "qfiles", "5%tG.S10dm3", True, 500)
        'My.Computer.FileSystem.DeleteFile(small & incremento & ".jpg")
    End Sub
    Public Sub ftp_modelos_big()
        ' My.Computer.Network.UploadFile("\\192.168.2.6\images\tbig\" & incremento & ".jpg", "ftp://ftp.95.39.14.35/public_html/images/productos/tbig/" & incremento & ".jpg", "qfiles", "5%tG.S10dm3", True, 500)
        'My.Computer.FileSystem.DeleteFile(big & incremento & ".jpg")
    End Sub
    Public Sub ftp_modelos_large()
        ' My.Computer.Network.UploadFile("\\192.168.2.6\images\tlarge\" & incremento & ".jpg", "ftp://ftp.95.39.14.35/public_html/images/productos/tlarge/" & incremento & ".jpg", "qfiles", "5%tG.S10dm3", True, 500)
        'My.Computer.FileSystem.DeleteFile(large & incremento & ".jpg")
    End Sub
    Public Sub ftp_modelos_medium()
        ' My.Computer.Network.UploadFile("\\192.168.2.6\images\tmedium\" & incremento & ".jpg", "ftp://ftp.95.39.14.35/public_html/images/productos/tmedium/" & incremento & ".jpg", "qfiles", "5%tG.S10dm3", True, 500)
        'My.Computer.FileSystem.DeleteFile(medium & incremento & ".jpg")
    End Sub

    'FIN PROCESOS PARA EL MODELO -------------------------------------------------------------------------------------------------------------------------------------------------------------

    'PROCESOS PARA EL PRODUCTO----------------------------------------------------------------------------------------------------------------------------------------------------------------

    Public Sub Abrir_carpeta_Producto()
        ENLACE.z = ENLACE.z + 1
        Gestion_imagen_productos.PictureBox1.Image.Save("\\192.168.2.6\imagenes\img_simple\" & incremento & ".jpg", Imaging.ImageFormat.Jpeg)
        Gestion_imagen_productos.PictureBox2.Image = Image.FromFile("\\192.168.2.6\imagenes\img_simple\" & incremento & ".jpg")
        Gestion_imagen_productos.nombreimgen.Text = incremento
        subir_producto_simple()
    End Sub

    Public Sub imagen_big_producto()
        'Dim original As Image = Image.FromFile("\\192.168.2.6\imagenes\img_simple\" & incremento & ".jpg")
        'Dim resized As Image = ResizeImage(original, New Size(800, 800))
        'Dim memStream As MemoryStream = New MemoryStream()
        'resized.Save(memStream, ImageFormat.Jpeg)
        'Dim bmp2 As New Bitmap(memStream)
        'ENLACE.PictureBox2.Image = New Bitmap(bmp2)
        'ENLACE.PictureBox2.Image.Save(big & incremento & ".jpg", Imaging.ImageFormat.Jpeg)
        'memStream.Dispose()
    End Sub

    Public Sub imagen_large_producto()
        'Dim original As Image = Image.FromFile("\\192.168.2.6\imagenes\img_simple\" & incremento & ".jpg")
        'Dim resized As Image = ResizeImage(original, New Size(400, 400))
        'Dim memStream As MemoryStream = New MemoryStream()
        'resized.Save(memStream, ImageFormat.Jpeg)
        'Dim bmp2 As New Bitmap(memStream)
        'ENLACE.PictureBox2.Image = New Bitmap(bmp2)
        'ENLACE.PictureBox2.Image.Save(large & incremento & ".jpg", Imaging.ImageFormat.Jpeg)
        'memStream.Dispose()
    End Sub

    Public Sub imagen_medium_producto()
        'Dim original As Image = Image.FromFile("\\192.168.2.6\imagenes\img_simple\" & incremento & ".jpg")
        'Dim resized As Image = ResizeImage(original, New Size(125, 125))
        'Dim memStream As MemoryStream = New MemoryStream()
        'resized.Save(memStream, ImageFormat.Jpeg)
        'Dim bmp2 As New Bitmap(memStream)
        'ENLACE.PictureBox2.Image = New Bitmap(bmp2)
        'ENLACE.PictureBox2.Image.Save(medium & incremento & ".jpg", Imaging.ImageFormat.Jpeg)
        'memStream.Dispose()
    End Sub

    Public Sub imagen_small_producto()
        'Dim original As Image = Image.FromFile("\\192.168.2.6\imagenes\img_simple\" & incremento & ".jpg")
        'Dim resized As Image = ResizeImage(original, New Size(65, 65))
        'Dim memStream As MemoryStream = New MemoryStream()
        'resized.Save(memStream, ImageFormat.Jpeg)
        'Dim bmp2 As New Bitmap(memStream)
        'ENLACE.PictureBox2.Image = New Bitmap(bmp2)
        'ENLACE.PictureBox2.Image.Save(small & incremento & ".jpg", Imaging.ImageFormat.Jpeg)
        'memStream.Dispose()
    End Sub

    Public Sub imagen_producto_agrupado_magento()
        'Using proxy As New MagentoService.MagentoService
        'Dim sessionId As String = proxy.login("jose", "Quirumed2012")
        'Dim productIdentifierType As String = "sku" 'Define si es el id o sku el que se envia como parametro
        'Dim sku As String = Modelo.idmodelo.Text
        'Dim path As String = Modelo.idmodelo.Text & "_0.jpg"
        'Dim imageStream = New MemoryStream()

        'Using i = Image.FromFile("\\192.168.2.6\imagenes\img_agrupado\ " & path)
        'i.Save(imageStream, ImageFormat.Jpeg)
        'End Using

        'Dim encbuff As Array = imageStream.ToArray()
        'Dim enc As String = Convert.ToBase64String(CType(encbuff, Byte()), 0, encbuff.Length)
        'Dim imageEntity = New catalogProductImageFileEntity()
        'imageEntity.name = sku
        'imageEntity.content = enc
        'imageEntity.mime = "image/jpeg"
        'imageStream.Close()

        'Dim entityP = New catalogProductAttributeMediaCreateEntity()
        'entityP.file = imageEntity
        'entityP.types = {"image", "small_image", "thumbnail"}
        'entityP.position = "0"
        'entityP.exclude = "0"

        'Dim imagen As String = proxy.catalogProductAttributeMediaCreate(sessionId, sku, entityP, "default", productIdentifierType)
        'MessageBox.Show(imagen)
        'proxy.endSession(sessionId)
        'End Using
    End Sub

    Public Sub imagen_producto_simple_magento()
        'Using proxy As New MagentoService.MagentoService
        'Dim sessionId As String = proxy.login("jose", "Quirumed2012")
        'Dim productIdentifierType As String = "sku" 'Define si es el id o sku el que se envia como parametro
        'Dim sku As String = ENLACE.Referencia.Text
        'Dim path As String = ENLACE.Referencia.Text & "_0.jpg"
        'Dim imageStream = New MemoryStream()

        'Using i = Image.FromFile("\\192.168.2.6\imagenes\img_simple\" & path)
        'i.Save(imageStream, ImageFormat.Jpeg)
        'End Using

        'Dim encbuff As Array = imageStream.ToArray()
        'Dim enc As String = Convert.ToBase64String(CType(encbuff, Byte()), 0, encbuff.Length)
        'Dim imageEntity = New catalogProductImageFileEntity()
        'imageEntity.name = sku
        'imageEntity.content = enc
        'imageEntity.mime = "image/jpeg"
        'imageStream.Close()

        'Dim entityP = New catalogProductAttributeMediaCreateEntity()
        'entityP.file = imageEntity
        'entityP.types = {"image", "small_image", "thumbnail"}
        'entityP.position = "0"
        'entityP.exclude = "0"

        'Dim imagen As String = proxy.catalogProductAttributeMediaCreate(sessionId, sku, entityP, "default", productIdentifierType)
        'MessageBox.Show(imagen)
        'proxy.endSession(sessionId)
        'End Using
    End Sub
    'FIN PROCESO IMAGENES PRODUCTOS

    'PROCESO CATEGORIAS
    Public Sub guardar_categorias()
        Gestion_imagen_categorias.PictureBox1.Image.Save("\\192.168.2.6\imagenes\categorias\" & Gestion_imagen_categorias.nombreimgen.Text, Imaging.ImageFormat.Jpeg)
        MessageBox.Show("Guardado en el servidor local")

    End Sub

    Public Sub subir_categorias()
        Dim sessionOptions As New SessionOptions
        With sessionOptions
            .Protocol = Protocol.Sftp
            .HostName = "185.105.223.9"
            .PortNumber = CInt("22")
            .UserName = "customer"
            .Password = "354de9eadd25433aa51245f8795b4de0"
            .SshHostKeyFingerprint = "ssh-rsa 2048 e5:53:12:a1:f0:de:b7:87:c1:d9:98:36:cd:0f:d2:3a"
        End With

        Using session As Session = New Session
            ' Connect
            session.Open(sessionOptions)
            ' Upload files
            Dim transferOptions As New TransferOptions

            transferOptions.TransferMode = TransferMode.Binary

            Dim transferResult As TransferOperationResult
            transferResult = session.PutFiles("\\192.168.2.6\imagenes\categorias\" & Gestion_imagen_categorias.nombreimgen.Text, "media/catalog/category/", False, transferOptions)

            ' Throw on any error
            transferResult.Check()
        End Using
    End Sub

    '----------------------------NUEVO GESTOR DE CATEGORIAS ---------------------------------------------
    Public Sub actualizar_categorias()
        'SERVIDOR
        My.Computer.FileSystem.DeleteFile("\\192.168.2.6\imagenes\categorias\" & Gestion_imagen_categorias.nombreimgen.Text)
        Gestion_imagen_categorias.PictureBox1.Image.Save("\\192.168.2.6\imagenes\categorias\" & Gestion_imagen_categorias.nombreimgen.Text, Imaging.ImageFormat.Jpeg)
        'MAGENTO
        Dim carpeta As String = Mid(Gestion_imagen_categorias.nombreimgen.Text, 1, 1)
        Dim subcarpeta As String = Mid(Gestion_imagen_categorias.nombreimgen.Text, 2, 1)
        Dim sessionOptions As New SessionOptions
        With sessionOptions
            .Protocol = Protocol.Sftp
            .HostName = "185.105.223.9"
            .PortNumber = CInt("22")
            .UserName = "customer"
            .Password = "354de9eadd25433aa51245f8795b4de0"
            .SshHostKeyFingerprint = "ssh-rsa 2048 e5:53:12:a1:f0:de:b7:87:c1:d9:98:36:cd:0f:d2:3a"
        End With

        Using session As Session = New Session
            ' Connect
            session.Open(sessionOptions)

            Dim transferResult As RemovalOperationResult
            transferResult = session.RemoveFiles("media/catalog/category/" & Gestion_imagen_categorias.nombreimgen.Text)
            Dim transferOptions As New TransferOptions

            transferOptions.TransferMode = TransferMode.Binary

            Dim transferResult1 As TransferOperationResult
            transferResult1 = session.PutFiles("\\192.168.2.6\imagenes\categorias\" & Gestion_imagen_categorias.nombreimgen.Text, "media/catalog/category/", False, transferOptions)
        End Using
    End Sub

    Public Sub eliminar_imagen_categoria()
        'SERVIDOR
        My.Computer.FileSystem.DeleteFile("\\192.168.2.6\imagenes\categorias\" & Gestion_imagen_categorias.nombreimgen.Text)
        'MAGENTO
        Dim carpeta As String = Mid(Gestion_imagen_productos.nombreimgen.Text, 1, 1)
        Dim subcarpeta As String = Mid(Gestion_imagen_productos.nombreimgen.Text, 2, 1)

        Dim sessionOptions As New SessionOptions
        With sessionOptions
            .Protocol = Protocol.Sftp
            .HostName = "185.105.223.9"
            .PortNumber = CInt("22")
            .UserName = "customer"
            .Password = "354de9eadd25433aa51245f8795b4de0"
            .SshHostKeyFingerprint = "ssh-rsa 2048 e5:53:12:a1:f0:de:b7:87:c1:d9:98:36:cd:0f:d2:3a"
        End With

        Using session As Session = New Session
            ' Connect
            session.Open(sessionOptions)

            Dim transferResult As RemovalOperationResult
            transferResult = session.RemoveFiles("\\192.168.2.6\imagenes\categorias\" & Gestion_imagen_categorias.nombreimgen.Text)
        End Using
    End Sub

    '----------------------------FIN NUEVO GESTOR DE CATEGORIAS -----------------------------------------
    'FIN PROCESO CATEGORIAS
    Public Sub subir_producto_agrupado()
        Dim sessionOptions As New SessionOptions
        With sessionOptions
            .Protocol = Protocol.Sftp
            .HostName = "185.105.223.9"
            .PortNumber = CInt("22")
            .UserName = "customer"
            .Password = "354de9eadd25433aa51245f8795b4de0"
            .SshHostKeyFingerprint = "ssh-rsa 2048 e5:53:12:a1:f0:de:b7:87:c1:d9:98:36:cd:0f:d2:3a"
        End With

        Using session As Session = New Session
            ' Connect
            session.Open(sessionOptions)

            ' Upload files
            Dim transferOptions As New TransferOptions

            transferOptions.TransferMode = TransferMode.Binary

            Dim transferResult As TransferOperationResult
            transferResult = session.PutFiles("\\192.168.2.6\imagenes\img_agrupado\" & Gestion_imagen_modelos.nombreimgen.Text & ".jpg", "media/import/", False, transferOptions)

            ' Throw on any error
            transferResult.Check()
        End Using
    End Sub

    Public Sub subir_producto_simple()
        Dim sessionOptions As New SessionOptions
        With sessionOptions
            .Protocol = Protocol.Sftp
            .HostName = "185.105.223.9"
            .PortNumber = CInt("22")
            .UserName = "customer"
            .Password = "354de9eadd25433aa51245f8795b4de0"
            .SshHostKeyFingerprint = "ssh-rsa 2048 e5:53:12:a1:f0:de:b7:87:c1:d9:98:36:cd:0f:d2:3a"
        End With

        Using session As Session = New Session
            ' Connect
            session.Open(sessionOptions)

            ' Upload files
            Dim transferOptions As New TransferOptions

            transferOptions.TransferMode = TransferMode.Binary

            Dim transferResult As TransferOperationResult
            transferResult = session.PutFiles("\\192.168.2.6\imagenes\img_simple\" & Gestion_imagen_productos.nombreimgen.Text & ".jpg", "media/import/", False, transferOptions)

            ' Throw on any error
            transferResult.Check()
        End Using
    End Sub

    Public Sub actualizar_img_modelo()
        'SERVIDOR
        My.Computer.FileSystem.DeleteFile("\\192.168.2.6\imagenes\img_agrupado\" & Gestion_imagen_modelos.nombreimgen.Text)

        Gestion_imagen_modelos.PictureBox1.Image.Save("\\192.168.2.6\imagenes\img_agrupado\" & Gestion_imagen_modelos.nombreimgen.Text, Imaging.ImageFormat.Jpeg)

        'MAGENTO
        Dim carpeta As String = Mid(Gestion_imagen_modelos.nombreimgen.Text, 1, 1)
        Dim subcarpeta As String = Mid(Gestion_imagen_modelos.nombreimgen.Text, 2, 1)

        Dim sessionOptions As New SessionOptions
        With sessionOptions
            .Protocol = Protocol.Sftp
            .HostName = "185.105.223.9"
            .PortNumber = CInt("22")
            .UserName = "customer"
            .Password = "354de9eadd25433aa51245f8795b4de0"
            .SshHostKeyFingerprint = "ssh-rsa 2048 e5:53:12:a1:f0:de:b7:87:c1:d9:98:36:cd:0f:d2:3a"
        End With

        Using session As Session = New Session
            ' Connect
            session.Open(sessionOptions)

            Dim transferResult As RemovalOperationResult
            transferResult = session.RemoveFiles("media/catalog/product/" & carpeta & "/" & subcarpeta & "/" & Gestion_imagen_modelos.nombreimgen.Text)
            Dim transferOptions As New TransferOptions

            transferOptions.TransferMode = TransferMode.Binary

            Dim transferResult1 As TransferOperationResult
            transferResult1 = session.PutFiles("\\192.168.2.6\imagenes\img_agrupado\" & Gestion_imagen_modelos.nombreimgen.Text, "media/catalog/product/" & carpeta & "/" & subcarpeta & "/", False, transferOptions)
        End Using
    End Sub

    Public Sub eliminar_imagen_modelo()
        Dim conexionmag2 As New MySqlConnection("server = 185.105.223.9; user id= quirumed;password = debc7139448d440a905f1613ed0fdc9f;database = quirumed; port = 3306;Convert Zero Datetime=True")
        'SERVIDOR
        My.Computer.FileSystem.DeleteFile("\\192.168.2.6\imagenes\img_agrupado\" & Gestion_imagen_modelos.nombreimgen.Text)
        'MAGENTO
        Dim carpeta As String = Mid(Gestion_imagen_modelos.nombreimgen.Text, 1, 1)
        Dim subcarpeta As String = Mid(Gestion_imagen_modelos.nombreimgen.Text, 2, 1)

        Dim sessionOptions As New SessionOptions
        With sessionOptions
            .Protocol = Protocol.Sftp
            .HostName = "185.105.223.9"
            .PortNumber = CInt("22")
            .UserName = "customer"
            .Password = "354de9eadd25433aa51245f8795b4de0"
            .SshHostKeyFingerprint = "ssh-rsa 2048 e5:53:12:a1:f0:de:b7:87:c1:d9:98:36:cd:0f:d2:3a"
        End With

        Using session As Session = New Session
            ' Connect
            session.Open(sessionOptions)

            Dim transferResult As RemovalOperationResult
            transferResult = session.RemoveFiles("media/catalog/product/" & carpeta & "/" & subcarpeta & "/" & Gestion_imagen_modelos.nombreimgen.Text)
        End Using

        Try
            Dim imagenmag As String
            imagenmag = "/" & carpeta & "/" & subcarpeta & "/" & Gestion_imagen_modelos.nombreimgen.Text

            conexionmag2.Open()
            Dim mydelete As New MySqlCommand("delete from `catalog_product_entity_media_gallery` where value = '" & imagenmag & "'", conexionmag2)

            mydelete.ExecuteNonQuery()

            conexionmag2.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
            conexionmag2.Close()
        End Try
    End Sub

    Public Sub actualizar_img_articulo()
        'SERVIDOR
        My.Computer.FileSystem.DeleteFile("\\192.168.2.6\imagenes\img_simple\" & Gestion_imagen_productos.nombreimgen.Text)

        Gestion_imagen_productos.PictureBox1.Image.Save("\\192.168.2.6\imagenes\img_simple\" & Gestion_imagen_productos.nombreimgen.Text, Imaging.ImageFormat.Jpeg)

        'MAGENTO
        Dim carpeta As String = Mid(Gestion_imagen_productos.nombreimgen.Text, 1, 1)
        Dim subcarpeta As String = Mid(Gestion_imagen_productos.nombreimgen.Text, 2, 1)

        Dim sessionOptions As New SessionOptions
        With sessionOptions
            .Protocol = Protocol.Sftp
            .HostName = "185.105.223.9"
            .PortNumber = CInt("22")
            .UserName = "customer"
            .Password = "354de9eadd25433aa51245f8795b4de0"
            .SshHostKeyFingerprint = "ssh-rsa 2048 e5:53:12:a1:f0:de:b7:87:c1:d9:98:36:cd:0f:d2:3a"
        End With

        Using session As Session = New Session
            ' Connect
            session.Open(sessionOptions)

            Dim transferResult As RemovalOperationResult
            transferResult = session.RemoveFiles("media/catalog/product/" & carpeta & "/" & subcarpeta & "/" & Gestion_imagen_productos.nombreimgen.Text)
            Dim transferOptions As New TransferOptions

            transferOptions.TransferMode = TransferMode.Binary

            Dim transferResult1 As TransferOperationResult
            transferResult1 = session.PutFiles("\\192.168.2.6\imagenes\img_simple\" & Gestion_imagen_productos.nombreimgen.Text, "media/catalog/product/" & carpeta & "/" & subcarpeta & "/", False, transferOptions)
        End Using
    End Sub

    Public Sub eliminar_imagen_articulo()
        Dim conexionmag2 As New MySqlConnection("server = 185.105.223.9; user id= quirumed;password = debc7139448d440a905f1613ed0fdc9f;database = quirumed; port = 3306;Convert Zero Datetime=True")
        'SERVIDOR
        My.Computer.FileSystem.DeleteFile("\\192.168.2.6\imagenes\img_simple\" & Gestion_imagen_productos.nombreimgen.Text)
        'MAGENTO
        Dim carpeta As String = Mid(Gestion_imagen_productos.nombreimgen.Text, 1, 1)
        Dim subcarpeta As String = Mid(Gestion_imagen_productos.nombreimgen.Text, 2, 1)

        Dim sessionOptions As New SessionOptions
        With sessionOptions
            .Protocol = Protocol.Sftp
            .HostName = "185.105.223.9"
            .PortNumber = CInt("22")
            .UserName = "customer"
            .Password = "354de9eadd25433aa51245f8795b4de0"
            .SshHostKeyFingerprint = "ssh-rsa 2048 e5:53:12:a1:f0:de:b7:87:c1:d9:98:36:cd:0f:d2:3a"
        End With

        Using session As Session = New Session
            ' Connect
            session.Open(sessionOptions)

            Dim transferResult As RemovalOperationResult
            transferResult = session.RemoveFiles("media/catalog/product/" & carpeta & "/" & subcarpeta & "/" & Gestion_imagen_productos.nombreimgen.Text)
        End Using

        Try
            Dim imagenmag As String
            imagenmag = "/" & carpeta & "/" & subcarpeta & "/" & Gestion_imagen_productos.nombreimgen.Text

            conexionmag2.Open()
            Dim mydelete As New MySqlCommand("delete from `catalog_product_entity_media_gallery` where value = '" & imagenmag & "'", conexionmag2)

            mydelete.ExecuteNonQuery()

            conexionmag2.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            conexionmag2.Close()
        End Try
    End Sub
End Class
