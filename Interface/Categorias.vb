﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class FCategorias
    Public enviar As Images = New Images
    Public rev_descripcion As Integer
    Public cat_padre_magento As String = ""
    Dim conect As New web
    Public busquedacat As busqueda = New busqueda

    Private Sub Categorias_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Catpadre.Enabled = True
        Me.nivelcat.Enabled = True
    End Sub

    Private Sub modcat_Click(sender As Object, e As EventArgs) Handles modcat.Click
        rev_descripcion = rev_descripcion + 1
        cat_padre_magento = catpadremag.Text
        conect.modificarCategoria(Me.categoriaid.Text)
        conect.savelogs(ENLACE.txtUsuario.Text, categoriaid.Text, "Modificacion", "Se ha modificado la categoria ID: " & categoriaid.Text)
    End Sub

    Private Sub buscarcat_Click(sender As Object, e As EventArgs) Handles buscarcat.Click
        If buscarcat.Text <> "" Then
            busquedacat.categorias_modificar_referencia()
        End If
        If nombrecat.Text <> "" And categoriaid.Text = "" Then
            busquedacat.categorias_modificar_nombre()
        End If
    End Sub

    Private Sub nuevacat_Click(sender As Object, e As EventArgs) Handles nuevacat.Click
        If Me.nombrecat.Text = "" Then
            MessageBox.Show("EL NOMBRE DE LA CATEGORIA NO PUEDE QUEDAR EN BLANCO")
        Else
            conect.nuevaCategoria()
            conect.savelogs(ENLACE.txtUsuario.Text, categoriaid.Text, "Creacion", "Se ha creado la categoría ID: " & categoriaid.Text)
        End If
    End Sub

    Private Sub cerrar_Click(sender As Object, e As EventArgs) Handles cerrar.Click
        Me.Close()
    End Sub

    Private Sub abre_imagen_Click(sender As Object, e As EventArgs) Handles abre_imagen.Click
        Dim OpenFile As New OpenFileDialog()
        If OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
            PictureBox1.Image = Image.FromFile(OpenFile.FileName)
        End If
    End Sub

    Private Sub guardar_imagen_Click(sender As Object, e As EventArgs) Handles guardar_imagen.Click
        enviar.guardar_categorias()
    End Sub

    Private Sub enviar_modelo_Click(sender As Object, e As EventArgs) Handles enviar_modelo.Click
        enviar.subir_categorias()
    End Sub

    Private Sub gestionimg_Click(sender As Object, e As EventArgs) Handles gestionimg.Click
        Gestion_imagen_categorias.Show()
        Gestion_imagen_categorias.nombreimgen.Text = Me.categoriaid.Text & ".jpg"
        Gestion_imagen_categorias.idcatweb.Text = Me.categoriaid.Text
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.categoriaid.Text = ""
        Me.nombrecat.Text = ""
        Me.Catpadre.Text = ""
        Me.nivelcat.Text = ""
        Me.Idmagento.Text = ""
    End Sub

    Private Sub Catpadre_TextChanged(sender As Object, e As EventArgs) Handles Catpadre.TextChanged
        conect.obtiene_padre_magento()
    End Sub
End Class