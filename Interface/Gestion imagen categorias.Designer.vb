﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Gestion_imagen_categorias
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.enviar = New System.Windows.Forms.Button()
        Me.guardar_imagen = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Eliminar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.abredirectorio = New System.Windows.Forms.Button()
        Me.actualizar = New System.Windows.Forms.Button()
        Me.nombreimgen = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.buscar = New System.Windows.Forms.Button()
        Me.categoriabusqueda = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.idcatweb = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(459, 64)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(37, 50)
        Me.PictureBox2.TabIndex = 82
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Controls.Add(Me.enviar)
        Me.GroupBox3.Controls.Add(Me.guardar_imagen)
        Me.GroupBox3.Location = New System.Drawing.Point(215, 120)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(271, 81)
        Me.GroupBox3.TabIndex = 81
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Añadir imagen"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(6, 21)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 52)
        Me.Button1.TabIndex = 70
        Me.Button1.Text = "Seleccionar imagen"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'enviar
        '
        Me.enviar.Location = New System.Drawing.Point(190, 20)
        Me.enviar.Name = "enviar"
        Me.enviar.Size = New System.Drawing.Size(75, 54)
        Me.enviar.TabIndex = 71
        Me.enviar.Text = "Enviar"
        Me.enviar.UseVisualStyleBackColor = True
        Me.enviar.Visible = False
        '
        'guardar_imagen
        '
        Me.guardar_imagen.Location = New System.Drawing.Point(99, 20)
        Me.guardar_imagen.Name = "guardar_imagen"
        Me.guardar_imagen.Size = New System.Drawing.Size(75, 54)
        Me.guardar_imagen.TabIndex = 66
        Me.guardar_imagen.Text = "Guardar Imagen"
        Me.guardar_imagen.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Eliminar)
        Me.GroupBox2.Location = New System.Drawing.Point(215, 328)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(108, 81)
        Me.GroupBox2.TabIndex = 80
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Eliminar"
        '
        'Eliminar
        '
        Me.Eliminar.Location = New System.Drawing.Point(14, 19)
        Me.Eliminar.Name = "Eliminar"
        Me.Eliminar.Size = New System.Drawing.Size(88, 43)
        Me.Eliminar.TabIndex = 68
        Me.Eliminar.Text = "Eliminar"
        Me.Eliminar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.abredirectorio)
        Me.GroupBox1.Controls.Add(Me.actualizar)
        Me.GroupBox1.Location = New System.Drawing.Point(215, 230)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(189, 81)
        Me.GroupBox1.TabIndex = 79
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Actualizar Imagenes"
        '
        'abredirectorio
        '
        Me.abredirectorio.Location = New System.Drawing.Point(6, 19)
        Me.abredirectorio.Name = "abredirectorio"
        Me.abredirectorio.Size = New System.Drawing.Size(75, 46)
        Me.abredirectorio.TabIndex = 13
        Me.abredirectorio.Text = "Seleccionar imagen"
        Me.abredirectorio.UseVisualStyleBackColor = True
        '
        'actualizar
        '
        Me.actualizar.Location = New System.Drawing.Point(99, 21)
        Me.actualizar.Name = "actualizar"
        Me.actualizar.Size = New System.Drawing.Size(84, 46)
        Me.actualizar.TabIndex = 15
        Me.actualizar.Text = "Actualizar"
        Me.actualizar.UseVisualStyleBackColor = True
        '
        'nombreimgen
        '
        Me.nombreimgen.Location = New System.Drawing.Point(69, 415)
        Me.nombreimgen.Name = "nombreimgen"
        Me.nombreimgen.Size = New System.Drawing.Size(129, 20)
        Me.nombreimgen.TabIndex = 78
        Me.nombreimgen.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(537, 64)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(769, 521)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 77
        Me.PictureBox1.TabStop = False
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.Location = New System.Drawing.Point(69, 90)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(129, 319)
        Me.CheckedListBox1.Sorted = True
        Me.CheckedListBox1.TabIndex = 76
        '
        'buscar
        '
        Me.buscar.Location = New System.Drawing.Point(175, 51)
        Me.buscar.Name = "buscar"
        Me.buscar.Size = New System.Drawing.Size(75, 23)
        Me.buscar.TabIndex = 75
        Me.buscar.Text = "Buscar"
        Me.buscar.UseVisualStyleBackColor = True
        '
        'categoriabusqueda
        '
        Me.categoriabusqueda.Location = New System.Drawing.Point(69, 54)
        Me.categoriabusqueda.Name = "categoriabusqueda"
        Me.categoriabusqueda.Size = New System.Drawing.Size(100, 20)
        Me.categoriabusqueda.TabIndex = 74
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Coral
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(1312, 64)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(106, 98)
        Me.Button2.TabIndex = 83
        Me.Button2.Text = "Cerrar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'idcatweb
        '
        Me.idcatweb.Location = New System.Drawing.Point(69, 441)
        Me.idcatweb.Name = "idcatweb"
        Me.idcatweb.Size = New System.Drawing.Size(100, 20)
        Me.idcatweb.TabIndex = 84
        Me.idcatweb.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(65, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(289, 24)
        Me.Label7.TabIndex = 85
        Me.Label7.Text = "IMÁGENES DE CATEGORÍAS"
        '
        'Gestion_imagen_categorias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1445, 636)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.idcatweb)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.nombreimgen)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.CheckedListBox1)
        Me.Controls.Add(Me.buscar)
        Me.Controls.Add(Me.categoriabusqueda)
        Me.Name = "Gestion_imagen_categorias"
        Me.Text = "Gestion_imagen_categorias"
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents enviar As System.Windows.Forms.Button
    Friend WithEvents guardar_imagen As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Eliminar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents abredirectorio As System.Windows.Forms.Button
    Friend WithEvents actualizar As System.Windows.Forms.Button
    Friend WithEvents nombreimgen As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents CheckedListBox1 As System.Windows.Forms.CheckedListBox
    Friend WithEvents buscar As System.Windows.Forms.Button
    Friend WithEvents categoriabusqueda As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents idcatweb As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
