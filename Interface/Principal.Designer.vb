﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class principal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btn_categorias_crear = New System.Windows.Forms.Button()
        Me.btn_productos_crear = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btn_categorias_modificar = New System.Windows.Forms.Button()
        Me.btn_productos_modificar = New System.Windows.Forms.Button()
        Me.btn_Cerrar = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btn_pedidos = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.ControlDark
        Me.GroupBox1.Controls.Add(Me.btn_categorias_crear)
        Me.GroupBox1.Controls.Add(Me.btn_productos_crear)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(57, 138)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(415, 464)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "CREAR"
        '
        'btn_categorias_crear
        '
        Me.btn_categorias_crear.Location = New System.Drawing.Point(43, 266)
        Me.btn_categorias_crear.Name = "btn_categorias_crear"
        Me.btn_categorias_crear.Size = New System.Drawing.Size(331, 155)
        Me.btn_categorias_crear.TabIndex = 1
        Me.btn_categorias_crear.Text = "Categorias"
        Me.btn_categorias_crear.UseVisualStyleBackColor = True
        '
        'btn_productos_crear
        '
        Me.btn_productos_crear.Location = New System.Drawing.Point(40, 66)
        Me.btn_productos_crear.Name = "btn_productos_crear"
        Me.btn_productos_crear.Size = New System.Drawing.Size(331, 155)
        Me.btn_productos_crear.TabIndex = 0
        Me.btn_productos_crear.Text = "Productos configurables"
        Me.btn_productos_crear.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.GroupBox2.Controls.Add(Me.btn_categorias_modificar)
        Me.GroupBox2.Controls.Add(Me.btn_productos_modificar)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(554, 138)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(415, 464)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "MODIFICAR"
        '
        'btn_categorias_modificar
        '
        Me.btn_categorias_modificar.Location = New System.Drawing.Point(41, 273)
        Me.btn_categorias_modificar.Name = "btn_categorias_modificar"
        Me.btn_categorias_modificar.Size = New System.Drawing.Size(331, 155)
        Me.btn_categorias_modificar.TabIndex = 1
        Me.btn_categorias_modificar.Text = "Categorias"
        Me.btn_categorias_modificar.UseVisualStyleBackColor = True
        '
        'btn_productos_modificar
        '
        Me.btn_productos_modificar.Location = New System.Drawing.Point(41, 74)
        Me.btn_productos_modificar.Name = "btn_productos_modificar"
        Me.btn_productos_modificar.Size = New System.Drawing.Size(331, 155)
        Me.btn_productos_modificar.TabIndex = 0
        Me.btn_productos_modificar.Text = "Productos configurables y simples"
        Me.btn_productos_modificar.UseVisualStyleBackColor = True
        '
        'btn_Cerrar
        '
        Me.btn_Cerrar.Location = New System.Drawing.Point(894, 617)
        Me.btn_Cerrar.Name = "btn_Cerrar"
        Me.btn_Cerrar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cerrar.TabIndex = 2
        Me.btn_Cerrar.Text = "Cerrar"
        Me.btn_Cerrar.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(352, 19)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(387, 85)
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'btn_pedidos
        '
        Me.btn_pedidos.Location = New System.Drawing.Point(0, 0)
        Me.btn_pedidos.Name = "btn_pedidos"
        Me.btn_pedidos.Size = New System.Drawing.Size(75, 23)
        Me.btn_pedidos.TabIndex = 4
        Me.btn_pedidos.Text = "Pedidos"
        Me.btn_pedidos.UseVisualStyleBackColor = True
        Me.btn_pedidos.Visible = False
        '
        'principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 652)
        Me.ControlBox = False
        Me.Controls.Add(Me.btn_pedidos)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.btn_Cerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "principal"
        Me.Text = "Conector de categorias y productos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_categorias_crear As System.Windows.Forms.Button
    Friend WithEvents btn_productos_crear As System.Windows.Forms.Button
    Friend WithEvents btn_categorias_modificar As System.Windows.Forms.Button
    Friend WithEvents btn_productos_modificar As System.Windows.Forms.Button
    Friend WithEvents btn_Cerrar As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btn_pedidos As System.Windows.Forms.Button

End Class
