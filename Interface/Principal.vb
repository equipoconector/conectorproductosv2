﻿Imports MySql.Data.MySqlClient
Imports [Interface].PRODUCTOS
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.IO
Imports System.Net
'Imports CKEditor.NET
Imports System.Web
Imports System.Text
Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Xml
Imports MSXML2
Imports System.Threading

Public Class principal

    Private Sub btn_Cerrar_Click(sender As Object, e As EventArgs) Handles btn_Cerrar.Click
        Me.Close()
    End Sub

    Private Sub btn_productos_crear_Click(sender As Object, e As EventArgs) Handles btn_productos_crear.Click
        CrearProducto.ShowDialog()
    End Sub

    Private Sub btn_categorias_crear_Click(sender As Object, e As EventArgs) Handles btn_categorias_crear.Click
        CrearCategoria.ShowDialog()
    End Sub

    Private Sub btn_productos_modificar_Click(sender As Object, e As EventArgs) Handles btn_productos_modificar.Click
        ModificarProducto.ShowDialog()
    End Sub

    Private Sub btn_categorias_modificar_Click(sender As Object, e As EventArgs) Handles btn_categorias_modificar.Click
        ModificarCategorias.ShowDialog()
    End Sub

    Private Sub principal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.PictureBox1.ImageLocation = "http://quirumed.dev.onestic.com/skin/frontend/default/quirumed_responsive/images/logo.png"
    End Sub

    Private Sub btn_pedidos_Click(sender As Object, e As EventArgs) Handles btn_pedidos.Click
        Pedidos.Dispose()
        Pedidos.ShowDialog()
    End Sub
End Class
