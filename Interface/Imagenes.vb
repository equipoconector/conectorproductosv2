﻿Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.IO
Imports System.Drawing.Imaging
Imports System
Imports System.Data
Imports System.Deployment
Imports System.Xml


'OBTENCION IMAGEN PRODUCTO ----------------------------------------------------------------------------------------------------------------------
Public Class Imagenes
    Public Sub Localizar_imagen()
        Dim carpeta As String = Mid(ENLACE.Referencia.Text, 1, 1)
        Dim subcarpeta As String = Mid(ENLACE.Referencia.Text, 2, 1)
        Dim original As Image = Image.FromFile("\\192.168.2.6\imagenes\img_simple\009-AW-601\" & ENLACE.Referencia.Text & "_0.jpg")
        Dim resized As Image = ResizeImage(original, New Size(350, 203))
        Dim memStream As MemoryStream = New MemoryStream()
        resized.Save(memStream, ImageFormat.Jpeg)
        Dim bmp2 As New Bitmap(memStream)
        ENLACE.PictureBox1.Image = New Bitmap(bmp2)
        memStream.Dispose()
    End Sub

    Public Shared Function ResizeImage(ByVal image As Image,
      ByVal size As Size, Optional ByVal preserveAspectRatio As Boolean = True) As Image
        Dim newWidth As Integer
        Dim newHeight As Integer
        If preserveAspectRatio Then
            Dim originalWidth As Integer = image.Width
            Dim originalHeight As Integer = image.Height
            Dim percentWidth As Single = CSng(size.Width) / CSng(originalWidth)
            Dim percentHeight As Single = CSng(size.Height) / CSng(originalHeight)
            Dim percent As Single = If(percentHeight < percentWidth,
                    percentHeight, percentWidth)
            newWidth = CInt(originalWidth * percent)
            newHeight = CInt(originalHeight * percent)
        Else
            newWidth = size.Width
            newHeight = size.Height
        End If
        Dim newImage As Image = New Bitmap(newWidth, newHeight)
        Using graphicsHandle As Graphics = Graphics.FromImage(newImage)
            graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic
            graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight)
        End Using
        Return newImage
    End Function

    'FIN OBTENCION IMAGEN PRODUCTO --------------------------------------------------------------------------------------------------------------------------
End Class



