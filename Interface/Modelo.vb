﻿Imports MySql.Data.MySqlClient
Imports System.Xml
Imports MSXML2
Imports System.Threading

Public Class Modelo
    Public usuario As String = ENLACE.txtUsuario.Text
    Public i As Integer = Gestion_imagen_modelos.i
    Public enviar As Images = New Images
    Public busquedaconfig As busqueda = New busqueda
    Public numero_rev As Integer
    Public mageconect As String = "https://quirumed.on-dev.com/scripts/pre/"

    Private Sub Modelo_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub guardar_Click(sender As Object, e As EventArgs) Handles guardar.Click
        'Inserta y recupera id del modelo
        With ENLACE
            .conect.myinsertar_mod()
            .conect.idmodelo()
        End With
        'Inserta en Navision
        With ENLACE
            .navconectar.navconectar()
            .navconectar.INSERTAR_MODELO()
            .navconectar.MODIFICAR_MODELO()
            .conect.revision()
            .conect.savelogs(ENLACE.txtUsuario.Text, ENLACE.modeloid.Text, "Creacion", "Se ha creado el producto configurable con sku: " & ENLACE.modeloid.Text)
        End With
        Me.Categoria.Text = ""
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Cerrar.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Buscar.Click
        Me.editormodelos.Text = ""
        If Me.idmodelo.Text <> "" Then
            busquedaconfig.busqueda_configurable_referencia()
        End If

        If Me.nombremodelo.Text <> "" And Me.idmodelo.Text = "" Then
            busquedaconfig.busqueda_configurable_nombre()
        End If
    End Sub

    Private Sub Button1_Click_2(sender As Object, e As EventArgs) Handles Modificar.Click
        If nombrexrec.Text <> "" Then
            If nombremodelo.Text <> nombrexrec.Text Then
                Me.revision.Text = CStr(CDbl(revision.Text) + 1)
            End If
        End If
        If descripcioncortat.Text <> nombrecortoxrec.Text Then
            nombreampliacionrev.Text = CStr(CDbl(nombreampliacionrev.Text) + 1)
        End If
        Dim ent As CONECTOR_NAVISION = New CONECTOR_NAVISION
        ent.entrada = Me.nombremodelo.Text
        With ENLACE
            .Descripcion.Text = Me.nombremodelo.Text & " " & ENLACE.Valor.Text
            'navision
            .navconectar.navconectar()
            .navconectar.MODIFICAR_MODELO()
            'mysql
            .conect.myactualizar_mod()
            .conect.savelogs(ENLACE.txtUsuario.Text, idmodelo.Text, "Creacion", "Se ha Creado el producto configurable en Mysql/navision con sku: " & idmodelo.Text)
        End With
    End Sub

    Private Sub Editor_Click(sender As Object, e As EventArgs) Handles Editor.Click
        Editorweb.Show()
    End Sub

    'PROCESO DE GESTION DE IMAGENES
    Private Sub abre_imagen_Click(sender As Object, e As EventArgs) Handles abre_imagen.Click
        Dim OpenFile As New OpenFileDialog()
        If OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
            PictureBox1.Image = Image.FromFile(OpenFile.FileName)
        End If
    End Sub

    Private Sub guardar_imagen_Click(sender As Object, e As EventArgs) Handles guardar_imagen.Click
        Dim conexion As web = New web
        enviar.referencia = idmodelo.Text
        enviar.incremento = Me.idmodelo.Text & "_" & i
        enviar.Crear_carpeta_modelo()
        'enviar.imagen_big()
        'enviar.imagen_large()
        'enviar.imagen_medium()
        'enviar.imagen_small()
        conexion.imagen_modelo() 'magento
    End Sub

    Private Sub idmodelo_TextChanged(sender As Object, e As EventArgs) Handles idmodelo.TextChanged
        i = 0
    End Sub

    Private Sub enviar_modelo_Click(sender As Object, e As EventArgs) Handles enviar_modelo.Click
        'enviar.imagen_producto_agrupado_magento()
        'enviar.ftp_modelos_small()
        'enviar.ftp_modelos_big()
        'enviar.ftp_modelos_large()
        'enviar.ftp_modelos_medium() 'magento
    End Sub

    'FIN PROCESO DE GESTION DE IMAGENES
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim strHtml As String = ""
        Dim UrlStr As String
        Dim llamada_arbol As gestion_simple = New gestion_simple()
        Dim DomDoc As MSXML2.XMLHTTP
        Dim params As String
        UrlStr = mageconect & "mageProductoModificar.php"
        DomDoc = New XMLHTTP
        params = "producto_id=" & Me.idmodelo.Text
        DomDoc.open("POST", UrlStr, False)
        DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
        DomDoc.setRequestHeader("Connection", "close")
        DomDoc.send(params)

        ENLACE.conect.savelogs(ENLACE.txtUsuario.Text, idmodelo.Text, "Modificacion", "Se ha modificado el producto configurable en Magento con sku: " & idmodelo.Text)
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        'LLamada a código Mage para definir el atributo configurable en el producto
        Dim strHtml As String = ""
        Dim UrlStr As String
        Dim DomDoc As MSXML2.XMLHTTP
        Dim params As String
        UrlStr = mageconect & "mageProductoNuevo.php"
        DomDoc = New XMLHTTP
        params = "producto_id=" & Me.idmodelo.Text
        DomDoc.open("POST", UrlStr, False)
        DomDoc.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        DomDoc.setRequestHeader("Content-length", CStr(Len(params)))
        DomDoc.setRequestHeader("Connection", "close")
        DomDoc.send(params)

        '************************************************
        ' Traducciones

        Dim strHtml2 As String = ""
        Dim UrlStr2 As String

        Dim DomDoc2 As MSXML2.XMLHTTP

        Dim params2 As String
        UrlStr2 = mageconect & "mageTranslate.php"
        DomDoc2 = New XMLHTTP
        params2 = "producto_id=" & Me.idmodelo.Text
        DomDoc2.open("POST", UrlStr2, False)
        DomDoc2.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        DomDoc2.setRequestHeader("Content-length", CStr(Len(params2)))
        DomDoc2.setRequestHeader("Connection", "close")
        DomDoc2.send(params2)

        ENLACE.conect.savelogs(ENLACE.txtUsuario.Text, idmodelo.Text, "Creacion", "Se ha creado el producto configurable en Magento con sku: " & idmodelo.Text)
        'fin traducciones
    End Sub

    Private Sub gestionimagen_Click(sender As Object, e As EventArgs) Handles gestionimagen.Click
        Gestion_imagen_modelos.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ARBOL_CATEGORIAS.Show()
        ARBOL_CATEGORIAS.id.Text = Me.idmodelo.Text
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs)
        MessageBox.Show(idmodelo.Text)
        Dim numero As Decimal = Convert.ToDecimal(idmodelo.Text)
        MessageBox.Show(numero.ToString)
    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs)
        idmodelo.Text = Replace(idmodelo.Text, ".", ",")
        Dim numero As Decimal = Convert.ToDecimal(idmodelo.Text)
        MessageBox.Show(numero.ToString)
    End Sub
End Class