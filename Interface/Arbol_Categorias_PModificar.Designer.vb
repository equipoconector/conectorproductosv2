﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Arbol_Categorias_PModificar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_nLevel = New System.Windows.Forms.TextBox()
        Me.txt_Categorias = New System.Windows.Forms.TextBox()
        Me.Tree = New System.Windows.Forms.TreeView()
        Me.btn_Cerrar = New System.Windows.Forms.Button()
        Me.lst_Categorias = New System.Windows.Forms.ListView()
        Me.txt_Path_Anterior = New System.Windows.Forms.TextBox()
        Me.txt_Categorias_Nuevas = New System.Windows.Forms.TextBox()
        Me.txt_Categorias_Borradas = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(845, 468)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Nivel"
        Me.Label2.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(660, 449)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Path"
        Me.Label1.Visible = False
        '
        'txt_nLevel
        '
        Me.txt_nLevel.Enabled = False
        Me.txt_nLevel.Location = New System.Drawing.Point(886, 465)
        Me.txt_nLevel.Name = "txt_nLevel"
        Me.txt_nLevel.Size = New System.Drawing.Size(68, 20)
        Me.txt_nLevel.TabIndex = 10
        Me.txt_nLevel.Visible = False
        '
        'txt_Categorias
        '
        Me.txt_Categorias.Enabled = False
        Me.txt_Categorias.Location = New System.Drawing.Point(660, 468)
        Me.txt_Categorias.Multiline = True
        Me.txt_Categorias.Name = "txt_Categorias"
        Me.txt_Categorias.Size = New System.Drawing.Size(164, 77)
        Me.txt_Categorias.TabIndex = 9
        Me.txt_Categorias.Visible = False
        '
        'Tree
        '
        Me.Tree.CheckBoxes = True
        Me.Tree.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tree.Location = New System.Drawing.Point(19, 35)
        Me.Tree.Name = "Tree"
        Me.Tree.Size = New System.Drawing.Size(623, 563)
        Me.Tree.TabIndex = 8
        '
        'btn_Cerrar
        '
        Me.btn_Cerrar.Location = New System.Drawing.Point(1163, 575)
        Me.btn_Cerrar.Name = "btn_Cerrar"
        Me.btn_Cerrar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cerrar.TabIndex = 7
        Me.btn_Cerrar.Text = "Cerrar"
        Me.btn_Cerrar.UseVisualStyleBackColor = True
        '
        'lst_Categorias
        '
        Me.lst_Categorias.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lst_Categorias.Location = New System.Drawing.Point(660, 35)
        Me.lst_Categorias.Name = "lst_Categorias"
        Me.lst_Categorias.Size = New System.Drawing.Size(578, 243)
        Me.lst_Categorias.TabIndex = 13
        Me.lst_Categorias.UseCompatibleStateImageBehavior = False
        Me.lst_Categorias.View = System.Windows.Forms.View.List
        '
        'txt_Path_Anterior
        '
        Me.txt_Path_Anterior.Location = New System.Drawing.Point(663, 367)
        Me.txt_Path_Anterior.Multiline = True
        Me.txt_Path_Anterior.Name = "txt_Path_Anterior"
        Me.txt_Path_Anterior.Size = New System.Drawing.Size(100, 59)
        Me.txt_Path_Anterior.TabIndex = 14
        Me.txt_Path_Anterior.Visible = False
        '
        'txt_Categorias_Nuevas
        '
        Me.txt_Categorias_Nuevas.Location = New System.Drawing.Point(776, 367)
        Me.txt_Categorias_Nuevas.Multiline = True
        Me.txt_Categorias_Nuevas.Name = "txt_Categorias_Nuevas"
        Me.txt_Categorias_Nuevas.Size = New System.Drawing.Size(100, 59)
        Me.txt_Categorias_Nuevas.TabIndex = 15
        Me.txt_Categorias_Nuevas.Visible = False
        '
        'txt_Categorias_Borradas
        '
        Me.txt_Categorias_Borradas.Location = New System.Drawing.Point(888, 367)
        Me.txt_Categorias_Borradas.Multiline = True
        Me.txt_Categorias_Borradas.Name = "txt_Categorias_Borradas"
        Me.txt_Categorias_Borradas.Size = New System.Drawing.Size(100, 59)
        Me.txt_Categorias_Borradas.TabIndex = 16
        Me.txt_Categorias_Borradas.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(663, 348)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Anterior"
        Me.Label3.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(776, 347)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Nuevas"
        Me.Label4.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(885, 347)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "Borrar"
        Me.Label5.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(913, 521)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'Arbol_Categorias_PModificar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1271, 624)
        Me.ControlBox = False
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_Categorias_Borradas)
        Me.Controls.Add(Me.txt_Categorias_Nuevas)
        Me.Controls.Add(Me.txt_Path_Anterior)
        Me.Controls.Add(Me.lst_Categorias)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_nLevel)
        Me.Controls.Add(Me.txt_Categorias)
        Me.Controls.Add(Me.Tree)
        Me.Controls.Add(Me.btn_Cerrar)
        Me.Name = "Arbol_Categorias_PModificar"
        Me.Text = "Arbol Categorias"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_nLevel As System.Windows.Forms.TextBox
    Friend WithEvents txt_Categorias As System.Windows.Forms.TextBox
    Friend WithEvents Tree As System.Windows.Forms.TreeView
    Friend WithEvents btn_Cerrar As System.Windows.Forms.Button
    Friend WithEvents lst_Categorias As System.Windows.Forms.ListView
    Friend WithEvents txt_Path_Anterior As System.Windows.Forms.TextBox
    Friend WithEvents txt_Categorias_Nuevas As System.Windows.Forms.TextBox
    Friend WithEvents txt_Categorias_Borradas As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
