﻿Public Class Calculo_pvp_distribuidor
    Public Sub precio_distribuidor()
        Dim porcentaje As Decimal
        Dim resultado As Decimal

        If ENLACE.importe_oferta.Text = "0" Then
            With ENLACE
                If CDec(.Precio.Text) <> 0 And CDec(.Coste.Text) <> 0 Then
                    porcentaje = (CDec(.Precio.Text) - CDec(.Coste.Text)) * 100 / CDec(.Precio.Text)

                    If (porcentaje >= 37.5) And (porcentaje < 41.1764706) Then
                        resultado = 5
                    End If

                    If (porcentaje >= 41.1764706) And (porcentaje < 44.44) Then
                        resultado = 10
                    End If

                    If (porcentaje >= 44.44) And (porcentaje < 50) Then
                        resultado = 15
                    End If

                    If (porcentaje >= 50) And (porcentaje < 60) Then
                        resultado = 20
                    End If

                    If (porcentaje >= 60) And (porcentaje < 66.6666667) Then
                        resultado = 25
                    End If

                    If porcentaje >= 66.67 Then
                        resultado = 30
                    End If

                    .pvp_distribuidor.Text = CStr(CDec(.Precio.Text) - ((CDec(.Precio.Text) * resultado)) / 100)
                End If
            End With
        End If

        If ENLACE.importe_oferta.Text <> "0" Then
            With ENLACE
                If CDec(.importe_oferta.Text) <> 0 And CDec(.Coste.Text) <> 0 Then
                    porcentaje = (CDec(.importe_oferta.Text) - CDec(.Coste.Text)) * 100 / CDec(.importe_oferta.Text)

                    If (porcentaje >= 37.5) And (porcentaje < 41.1764706) Then
                        resultado = 5
                    End If

                    If (porcentaje >= 41.1764706) And (porcentaje < 44.44) Then
                        resultado = 10
                    End If

                    If (porcentaje >= 44.44) And (porcentaje < 50) Then
                        resultado = 15
                    End If

                    If (porcentaje >= 50) And (porcentaje < 60) Then
                        resultado = 20
                    End If

                    If (porcentaje >= 60) And (porcentaje < 66.6666667) Then
                        resultado = 25
                    End If

                    If porcentaje >= 66.67 Then
                        resultado = 30
                    End If

                    .pvp_distribuidor.Text = CStr(CDec(.importe_oferta.Text) - ((CDec(.importe_oferta.Text) * resultado)) / 100)
                End If
            End With
        End If
    End Sub
End Class
