﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CrearCategoria
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_Crear = New System.Windows.Forms.Button()
        Me.txtNivel = New System.Windows.Forms.TextBox()
        Me.txt_Descripcion = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txt_Id = New System.Windows.Forms.TextBox()
        Me.btn_Cerrar = New System.Windows.Forms.Button()
        Me.lbl_Producto_Configurable = New System.Windows.Forms.Label()
        Me.lbl_Id_Magento = New System.Windows.Forms.Label()
        Me.lbl_Nivel = New System.Windows.Forms.Label()
        Me.lbl_Descripcion = New System.Windows.Forms.Label()
        Me.lbl_Nombre = New System.Windows.Forms.Label()
        Me.lbl_Id = New System.Windows.Forms.Label()
        Me.txt_Id_magento = New System.Windows.Forms.TextBox()
        Me.lbl_Categoria_Padre = New System.Windows.Forms.Label()
        Me.txt_Categoria_Padre = New System.Windows.Forms.TextBox()
        Me.lbl_Categoria_Padre_Magento = New System.Windows.Forms.Label()
        Me.txt_Categoria_Padre_Magento = New System.Windows.Forms.TextBox()
        Me.lbl_Meta_Title = New System.Windows.Forms.Label()
        Me.txtMetaTitle = New System.Windows.Forms.TextBox()
        Me.txt_Meta_Keywords = New System.Windows.Forms.TextBox()
        Me.lbl_Meta_Keywords = New System.Windows.Forms.Label()
        Me.txt_Meta_Description = New System.Windows.Forms.TextBox()
        Me.lbl_Meta_Description = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txt_Path = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SuspendLayout()
        '
        'btn_Crear
        '
        Me.btn_Crear.Location = New System.Drawing.Point(736, 588)
        Me.btn_Crear.Name = "btn_Crear"
        Me.btn_Crear.Size = New System.Drawing.Size(75, 23)
        Me.btn_Crear.TabIndex = 34
        Me.btn_Crear.Text = "Crear"
        Me.btn_Crear.UseVisualStyleBackColor = True
        '
        'txtNivel
        '
        Me.txtNivel.Enabled = False
        Me.txtNivel.Location = New System.Drawing.Point(365, 271)
        Me.txtNivel.Name = "txtNivel"
        Me.txtNivel.Size = New System.Drawing.Size(28, 20)
        Me.txtNivel.TabIndex = 31
        '
        'txt_Descripcion
        '
        Me.txt_Descripcion.Location = New System.Drawing.Point(177, 194)
        Me.txt_Descripcion.Multiline = True
        Me.txt_Descripcion.Name = "txt_Descripcion"
        Me.txt_Descripcion.Size = New System.Drawing.Size(731, 57)
        Me.txt_Descripcion.TabIndex = 30
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(177, 154)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(731, 20)
        Me.txtNombre.TabIndex = 29
        '
        'txt_Id
        '
        Me.txt_Id.Enabled = False
        Me.txt_Id.Location = New System.Drawing.Point(177, 117)
        Me.txt_Id.Name = "txt_Id"
        Me.txt_Id.Size = New System.Drawing.Size(100, 20)
        Me.txt_Id.TabIndex = 28
        '
        'btn_Cerrar
        '
        Me.btn_Cerrar.Location = New System.Drawing.Point(833, 588)
        Me.btn_Cerrar.Name = "btn_Cerrar"
        Me.btn_Cerrar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cerrar.TabIndex = 27
        Me.btn_Cerrar.Text = "Cerrar"
        Me.btn_Cerrar.UseVisualStyleBackColor = True
        '
        'lbl_Producto_Configurable
        '
        Me.lbl_Producto_Configurable.AutoSize = True
        Me.lbl_Producto_Configurable.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Producto_Configurable.Location = New System.Drawing.Point(31, 48)
        Me.lbl_Producto_Configurable.Name = "lbl_Producto_Configurable"
        Me.lbl_Producto_Configurable.Size = New System.Drawing.Size(99, 24)
        Me.lbl_Producto_Configurable.TabIndex = 26
        Me.lbl_Producto_Configurable.Text = "Categoría"
        '
        'lbl_Id_Magento
        '
        Me.lbl_Id_Magento.AutoSize = True
        Me.lbl_Id_Magento.Location = New System.Drawing.Point(75, 320)
        Me.lbl_Id_Magento.Name = "lbl_Id_Magento"
        Me.lbl_Id_Magento.Size = New System.Drawing.Size(63, 13)
        Me.lbl_Id_Magento.TabIndex = 25
        Me.lbl_Id_Magento.Text = "ID Magento"
        Me.lbl_Id_Magento.Visible = False
        '
        'lbl_Nivel
        '
        Me.lbl_Nivel.AutoSize = True
        Me.lbl_Nivel.ForeColor = System.Drawing.Color.Red
        Me.lbl_Nivel.Location = New System.Drawing.Point(312, 274)
        Me.lbl_Nivel.Name = "lbl_Nivel"
        Me.lbl_Nivel.Size = New System.Drawing.Size(52, 13)
        Me.lbl_Nivel.TabIndex = 23
        Me.lbl_Nivel.Text = "Nivel Hijo"
        '
        'lbl_Descripcion
        '
        Me.lbl_Descripcion.AutoSize = True
        Me.lbl_Descripcion.Location = New System.Drawing.Point(74, 197)
        Me.lbl_Descripcion.Name = "lbl_Descripcion"
        Me.lbl_Descripcion.Size = New System.Drawing.Size(63, 13)
        Me.lbl_Descripcion.TabIndex = 21
        Me.lbl_Descripcion.Text = "Descripción"
        '
        'lbl_Nombre
        '
        Me.lbl_Nombre.AutoSize = True
        Me.lbl_Nombre.ForeColor = System.Drawing.Color.Red
        Me.lbl_Nombre.Location = New System.Drawing.Point(74, 157)
        Me.lbl_Nombre.Name = "lbl_Nombre"
        Me.lbl_Nombre.Size = New System.Drawing.Size(44, 13)
        Me.lbl_Nombre.TabIndex = 20
        Me.lbl_Nombre.Text = "Nombre"
        '
        'lbl_Id
        '
        Me.lbl_Id.AutoSize = True
        Me.lbl_Id.Location = New System.Drawing.Point(74, 120)
        Me.lbl_Id.Name = "lbl_Id"
        Me.lbl_Id.Size = New System.Drawing.Size(68, 13)
        Me.lbl_Id.TabIndex = 19
        Me.lbl_Id.Text = "ID Categoría"
        '
        'txt_Id_magento
        '
        Me.txt_Id_magento.Location = New System.Drawing.Point(177, 317)
        Me.txt_Id_magento.Name = "txt_Id_magento"
        Me.txt_Id_magento.Size = New System.Drawing.Size(100, 20)
        Me.txt_Id_magento.TabIndex = 36
        Me.txt_Id_magento.Visible = False
        '
        'lbl_Categoria_Padre
        '
        Me.lbl_Categoria_Padre.AutoSize = True
        Me.lbl_Categoria_Padre.ForeColor = System.Drawing.Color.Red
        Me.lbl_Categoria_Padre.Location = New System.Drawing.Point(75, 272)
        Me.lbl_Categoria_Padre.Name = "lbl_Categoria_Padre"
        Me.lbl_Categoria_Padre.Size = New System.Drawing.Size(97, 13)
        Me.lbl_Categoria_Padre.TabIndex = 37
        Me.lbl_Categoria_Padre.Text = "ID Categoria Padre"
        '
        'txt_Categoria_Padre
        '
        Me.txt_Categoria_Padre.Location = New System.Drawing.Point(179, 269)
        Me.txt_Categoria_Padre.Name = "txt_Categoria_Padre"
        Me.txt_Categoria_Padre.Size = New System.Drawing.Size(62, 20)
        Me.txt_Categoria_Padre.TabIndex = 38
        '
        'lbl_Categoria_Padre_Magento
        '
        Me.lbl_Categoria_Padre_Magento.AutoSize = True
        Me.lbl_Categoria_Padre_Magento.Location = New System.Drawing.Point(330, 320)
        Me.lbl_Categoria_Padre_Magento.Name = "lbl_Categoria_Padre_Magento"
        Me.lbl_Categoria_Padre_Magento.Size = New System.Drawing.Size(142, 13)
        Me.lbl_Categoria_Padre_Magento.TabIndex = 39
        Me.lbl_Categoria_Padre_Magento.Text = "ID Categoria Padre Magento"
        Me.lbl_Categoria_Padre_Magento.Visible = False
        '
        'txt_Categoria_Padre_Magento
        '
        Me.txt_Categoria_Padre_Magento.Location = New System.Drawing.Point(477, 320)
        Me.txt_Categoria_Padre_Magento.Name = "txt_Categoria_Padre_Magento"
        Me.txt_Categoria_Padre_Magento.Size = New System.Drawing.Size(100, 20)
        Me.txt_Categoria_Padre_Magento.TabIndex = 40
        Me.txt_Categoria_Padre_Magento.Visible = False
        '
        'lbl_Meta_Title
        '
        Me.lbl_Meta_Title.AutoSize = True
        Me.lbl_Meta_Title.Location = New System.Drawing.Point(74, 386)
        Me.lbl_Meta_Title.Name = "lbl_Meta_Title"
        Me.lbl_Meta_Title.Size = New System.Drawing.Size(54, 13)
        Me.lbl_Meta_Title.TabIndex = 41
        Me.lbl_Meta_Title.Text = "Meta Title"
        '
        'txtMetaTitle
        '
        Me.txtMetaTitle.Location = New System.Drawing.Point(177, 386)
        Me.txtMetaTitle.Name = "txtMetaTitle"
        Me.txtMetaTitle.Size = New System.Drawing.Size(731, 20)
        Me.txtMetaTitle.TabIndex = 42
        '
        'txt_Meta_Keywords
        '
        Me.txt_Meta_Keywords.Location = New System.Drawing.Point(177, 427)
        Me.txt_Meta_Keywords.Multiline = True
        Me.txt_Meta_Keywords.Name = "txt_Meta_Keywords"
        Me.txt_Meta_Keywords.Size = New System.Drawing.Size(731, 45)
        Me.txt_Meta_Keywords.TabIndex = 44
        '
        'lbl_Meta_Keywords
        '
        Me.lbl_Meta_Keywords.AutoSize = True
        Me.lbl_Meta_Keywords.Location = New System.Drawing.Point(74, 427)
        Me.lbl_Meta_Keywords.Name = "lbl_Meta_Keywords"
        Me.lbl_Meta_Keywords.Size = New System.Drawing.Size(80, 13)
        Me.lbl_Meta_Keywords.TabIndex = 43
        Me.lbl_Meta_Keywords.Text = "Meta Keywords"
        '
        'txt_Meta_Description
        '
        Me.txt_Meta_Description.Location = New System.Drawing.Point(177, 493)
        Me.txt_Meta_Description.Multiline = True
        Me.txt_Meta_Description.Name = "txt_Meta_Description"
        Me.txt_Meta_Description.Size = New System.Drawing.Size(731, 58)
        Me.txt_Meta_Description.TabIndex = 46
        '
        'lbl_Meta_Description
        '
        Me.lbl_Meta_Description.AutoSize = True
        Me.lbl_Meta_Description.Location = New System.Drawing.Point(75, 493)
        Me.lbl_Meta_Description.Name = "lbl_Meta_Description"
        Me.lbl_Meta_Description.Size = New System.Drawing.Size(87, 13)
        Me.lbl_Meta_Description.TabIndex = 45
        Me.lbl_Meta_Description.Text = "Meta Description"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(249, 268)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(55, 23)
        Me.Button1.TabIndex = 55
        Me.Button1.Text = "Arbol"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txt_Path
        '
        Me.txt_Path.Enabled = False
        Me.txt_Path.Location = New System.Drawing.Point(438, 271)
        Me.txt_Path.Name = "txt_Path"
        Me.txt_Path.Size = New System.Drawing.Size(470, 20)
        Me.txt_Path.TabIndex = 56
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(403, 274)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 13)
        Me.Label1.TabIndex = 57
        Me.Label1.Text = "Path"
        '
        'GroupBox1
        '
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.GroupBox1.Location = New System.Drawing.Point(56, 361)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(870, 209)
        Me.GroupBox1.TabIndex = 58
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Metas"
        '
        'CrearCategoria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(952, 634)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_Path)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txt_Meta_Description)
        Me.Controls.Add(Me.lbl_Meta_Description)
        Me.Controls.Add(Me.txt_Meta_Keywords)
        Me.Controls.Add(Me.lbl_Meta_Keywords)
        Me.Controls.Add(Me.txtMetaTitle)
        Me.Controls.Add(Me.lbl_Meta_Title)
        Me.Controls.Add(Me.txt_Categoria_Padre_Magento)
        Me.Controls.Add(Me.lbl_Categoria_Padre_Magento)
        Me.Controls.Add(Me.txt_Categoria_Padre)
        Me.Controls.Add(Me.lbl_Categoria_Padre)
        Me.Controls.Add(Me.txt_Id_magento)
        Me.Controls.Add(Me.btn_Crear)
        Me.Controls.Add(Me.txtNivel)
        Me.Controls.Add(Me.txt_Descripcion)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.txt_Id)
        Me.Controls.Add(Me.btn_Cerrar)
        Me.Controls.Add(Me.lbl_Producto_Configurable)
        Me.Controls.Add(Me.lbl_Id_Magento)
        Me.Controls.Add(Me.lbl_Nivel)
        Me.Controls.Add(Me.lbl_Descripcion)
        Me.Controls.Add(Me.lbl_Nombre)
        Me.Controls.Add(Me.lbl_Id)
        Me.Controls.Add(Me.GroupBox1)
        Me.ForeColor = System.Drawing.SystemColors.Desktop
        Me.Name = "CrearCategoria"
        Me.Text = "Crear Categoría"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Crear As System.Windows.Forms.Button
    Friend WithEvents txtNivel As System.Windows.Forms.TextBox
    Friend WithEvents txt_Descripcion As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txt_Id As System.Windows.Forms.TextBox
    Friend WithEvents btn_Cerrar As System.Windows.Forms.Button
    Friend WithEvents lbl_Producto_Configurable As System.Windows.Forms.Label
    Friend WithEvents lbl_Id_Magento As System.Windows.Forms.Label
    Friend WithEvents lbl_Nivel As System.Windows.Forms.Label
    Friend WithEvents lbl_Descripcion As System.Windows.Forms.Label
    Friend WithEvents lbl_Nombre As System.Windows.Forms.Label
    Friend WithEvents lbl_Id As System.Windows.Forms.Label
    Friend WithEvents txt_Id_magento As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Categoria_Padre As System.Windows.Forms.Label
    Friend WithEvents txt_Categoria_Padre As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Categoria_Padre_Magento As System.Windows.Forms.Label
    Friend WithEvents txt_Categoria_Padre_Magento As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Meta_Title As System.Windows.Forms.Label
    Friend WithEvents txtMetaTitle As System.Windows.Forms.TextBox
    Friend WithEvents txt_Meta_Keywords As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Meta_Keywords As System.Windows.Forms.Label
    Friend WithEvents txt_Meta_Description As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Meta_Description As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txt_Path As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
