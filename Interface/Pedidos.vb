﻿Public Class Pedidos

    Public navconectar As CONECTOR_NAVISION = New CONECTOR_NAVISION
    Public conect As web = New web
    Public sqlconexion As SQLCONECTAR = New SQLCONECTAR

    Private Sub btn_Buscar_Click(sender As Object, e As EventArgs) Handles btn_Buscar.Click
        If Me.txtNumPedido.Text <> "" Then
            conect.buscarPedido(Me.txtNumPedido.Text)
        Else
            MsgBox("Debes de introducir el número de pedido")
        End If
    End Sub

    Private Sub btn_Cerrar_Click(sender As Object, e As EventArgs) Handles btn_Cerrar.Click
        Me.Close()
    End Sub

    Private Sub btn_Cliente_Click(sender As Object, e As EventArgs) Handles btn_Cliente.Click        
        conect.buscarCliente(Me.txtClienteID.Text)
        Clientes.ShowDialog()
    End Sub

End Class