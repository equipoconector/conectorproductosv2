﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CrearProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_Id = New System.Windows.Forms.Label()
        Me.lbl_Nombre = New System.Windows.Forms.Label()
        Me.lbl_Desc_Corta = New System.Windows.Forms.Label()
        Me.lbl_Desc_Larga = New System.Windows.Forms.Label()
        Me.lbl_Id_Categoria = New System.Windows.Forms.Label()
        Me.lbl_Nombre_Categoria = New System.Windows.Forms.Label()
        Me.lbl_Estado = New System.Windows.Forms.Label()
        Me.lbl_Producto_Configurable = New System.Windows.Forms.Label()
        Me.btn_Cerrar = New System.Windows.Forms.Button()
        Me.txt_Id = New System.Windows.Forms.TextBox()
        Me.txt_Nombre = New System.Windows.Forms.TextBox()
        Me.txt_Descripcion_Corta = New System.Windows.Forms.TextBox()
        Me.txt_Id_categoria = New System.Windows.Forms.TextBox()
        Me.txt_Nombre_Categoria = New System.Windows.Forms.TextBox()
        Me.combo_Estado = New System.Windows.Forms.ComboBox()
        Me.btn_Crear = New System.Windows.Forms.Button()
        Me.btn_Editor = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btnCambiarIva = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.txt_precio = New System.Windows.Forms.TextBox()
        Me.txt_pcoste = New System.Windows.Forms.TextBox()
        Me.txt_pdistribuidor = New System.Windows.Forms.TextBox()
        Me.txt_referencia = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_Examinar = New System.Windows.Forms.Button()
        Me.txtFile = New System.Windows.Forms.TextBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.txt_Email = New System.Windows.Forms.TextBox()
        Me.btn_Traducir = New System.Windows.Forms.Button()
        Me.txtTO = New System.Windows.Forms.TextBox()
        Me.txtFrom = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.result = New System.Windows.Forms.TextBox()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.txtOrigen = New System.Windows.Forms.TextBox()
        Me.txtIdioma = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_Idioma = New System.Windows.Forms.Button()
        Me.txt_Contador = New System.Windows.Forms.TextBox()
        Me.txt_Inicio = New System.Windows.Forms.TextBox()
        Me.txt_Fin = New System.Windows.Forms.TextBox()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.txtIdiomaValor = New System.Windows.Forms.TextBox()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_Id
        '
        Me.lbl_Id.AutoSize = True
        Me.lbl_Id.Location = New System.Drawing.Point(61, 98)
        Me.lbl_Id.Name = "lbl_Id"
        Me.lbl_Id.Size = New System.Drawing.Size(64, 13)
        Me.lbl_Id.TabIndex = 0
        Me.lbl_Id.Text = "ID Producto"
        '
        'lbl_Nombre
        '
        Me.lbl_Nombre.AutoSize = True
        Me.lbl_Nombre.ForeColor = System.Drawing.Color.Red
        Me.lbl_Nombre.Location = New System.Drawing.Point(61, 135)
        Me.lbl_Nombre.Name = "lbl_Nombre"
        Me.lbl_Nombre.Size = New System.Drawing.Size(44, 13)
        Me.lbl_Nombre.TabIndex = 1
        Me.lbl_Nombre.Text = "Nombre"
        '
        'lbl_Desc_Corta
        '
        Me.lbl_Desc_Corta.AutoSize = True
        Me.lbl_Desc_Corta.Location = New System.Drawing.Point(61, 175)
        Me.lbl_Desc_Corta.Name = "lbl_Desc_Corta"
        Me.lbl_Desc_Corta.Size = New System.Drawing.Size(90, 13)
        Me.lbl_Desc_Corta.TabIndex = 2
        Me.lbl_Desc_Corta.Text = "Descripción corta"
        '
        'lbl_Desc_Larga
        '
        Me.lbl_Desc_Larga.AutoSize = True
        Me.lbl_Desc_Larga.Location = New System.Drawing.Point(61, 390)
        Me.lbl_Desc_Larga.Name = "lbl_Desc_Larga"
        Me.lbl_Desc_Larga.Size = New System.Drawing.Size(89, 13)
        Me.lbl_Desc_Larga.TabIndex = 3
        Me.lbl_Desc_Larga.Text = "Descripción larga"
        Me.lbl_Desc_Larga.Visible = False
        '
        'lbl_Id_Categoria
        '
        Me.lbl_Id_Categoria.AutoSize = True
        Me.lbl_Id_Categoria.ForeColor = System.Drawing.Color.Red
        Me.lbl_Id_Categoria.Location = New System.Drawing.Point(62, 252)
        Me.lbl_Id_Categoria.Name = "lbl_Id_Categoria"
        Me.lbl_Id_Categoria.Size = New System.Drawing.Size(66, 13)
        Me.lbl_Id_Categoria.TabIndex = 4
        Me.lbl_Id_Categoria.Text = "ID Categoria"
        '
        'lbl_Nombre_Categoria
        '
        Me.lbl_Nombre_Categoria.AutoSize = True
        Me.lbl_Nombre_Categoria.Location = New System.Drawing.Point(304, 254)
        Me.lbl_Nombre_Categoria.Name = "lbl_Nombre_Categoria"
        Me.lbl_Nombre_Categoria.Size = New System.Drawing.Size(29, 13)
        Me.lbl_Nombre_Categoria.TabIndex = 5
        Me.lbl_Nombre_Categoria.Text = "Path"
        '
        'lbl_Estado
        '
        Me.lbl_Estado.AutoSize = True
        Me.lbl_Estado.ForeColor = System.Drawing.Color.Red
        Me.lbl_Estado.Location = New System.Drawing.Point(65, 304)
        Me.lbl_Estado.Name = "lbl_Estado"
        Me.lbl_Estado.Size = New System.Drawing.Size(40, 13)
        Me.lbl_Estado.TabIndex = 6
        Me.lbl_Estado.Text = "Estado"
        '
        'lbl_Producto_Configurable
        '
        Me.lbl_Producto_Configurable.AutoSize = True
        Me.lbl_Producto_Configurable.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Producto_Configurable.Location = New System.Drawing.Point(21, 9)
        Me.lbl_Producto_Configurable.Name = "lbl_Producto_Configurable"
        Me.lbl_Producto_Configurable.Size = New System.Drawing.Size(216, 24)
        Me.lbl_Producto_Configurable.TabIndex = 7
        Me.lbl_Producto_Configurable.Text = "Producto configurable"
        '
        'btn_Cerrar
        '
        Me.btn_Cerrar.Location = New System.Drawing.Point(820, 389)
        Me.btn_Cerrar.Name = "btn_Cerrar"
        Me.btn_Cerrar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Cerrar.TabIndex = 8
        Me.btn_Cerrar.Text = "Cerrar"
        Me.btn_Cerrar.UseVisualStyleBackColor = True
        '
        'txt_Id
        '
        Me.txt_Id.Enabled = False
        Me.txt_Id.Location = New System.Drawing.Point(164, 95)
        Me.txt_Id.Name = "txt_Id"
        Me.txt_Id.Size = New System.Drawing.Size(100, 20)
        Me.txt_Id.TabIndex = 9
        '
        'txt_Nombre
        '
        Me.txt_Nombre.Location = New System.Drawing.Point(164, 132)
        Me.txt_Nombre.Name = "txt_Nombre"
        Me.txt_Nombre.Size = New System.Drawing.Size(731, 20)
        Me.txt_Nombre.TabIndex = 10
        '
        'txt_Descripcion_Corta
        '
        Me.txt_Descripcion_Corta.Location = New System.Drawing.Point(164, 172)
        Me.txt_Descripcion_Corta.Multiline = True
        Me.txt_Descripcion_Corta.Name = "txt_Descripcion_Corta"
        Me.txt_Descripcion_Corta.Size = New System.Drawing.Size(731, 57)
        Me.txt_Descripcion_Corta.TabIndex = 11
        '
        'txt_Id_categoria
        '
        Me.txt_Id_categoria.Enabled = False
        Me.txt_Id_categoria.Location = New System.Drawing.Point(164, 252)
        Me.txt_Id_categoria.Name = "txt_Id_categoria"
        Me.txt_Id_categoria.Size = New System.Drawing.Size(56, 20)
        Me.txt_Id_categoria.TabIndex = 13
        '
        'txt_Nombre_Categoria
        '
        Me.txt_Nombre_Categoria.Enabled = False
        Me.txt_Nombre_Categoria.Location = New System.Drawing.Point(339, 251)
        Me.txt_Nombre_Categoria.Name = "txt_Nombre_Categoria"
        Me.txt_Nombre_Categoria.Size = New System.Drawing.Size(556, 20)
        Me.txt_Nombre_Categoria.TabIndex = 14
        '
        'combo_Estado
        '
        Me.combo_Estado.FormattingEnabled = True
        Me.combo_Estado.Items.AddRange(New Object() {"OCULTO", "VISIBLE"})
        Me.combo_Estado.Location = New System.Drawing.Point(164, 301)
        Me.combo_Estado.Name = "combo_Estado"
        Me.combo_Estado.Size = New System.Drawing.Size(121, 21)
        Me.combo_Estado.TabIndex = 15
        '
        'btn_Crear
        '
        Me.btn_Crear.Location = New System.Drawing.Point(723, 389)
        Me.btn_Crear.Name = "btn_Crear"
        Me.btn_Crear.Size = New System.Drawing.Size(75, 23)
        Me.btn_Crear.TabIndex = 17
        Me.btn_Crear.Text = "Crear"
        Me.btn_Crear.UseVisualStyleBackColor = True
        '
        'btn_Editor
        '
        Me.btn_Editor.Enabled = False
        Me.btn_Editor.Location = New System.Drawing.Point(164, 385)
        Me.btn_Editor.Name = "btn_Editor"
        Me.btn_Editor.Size = New System.Drawing.Size(83, 23)
        Me.btn_Editor.TabIndex = 18
        Me.btn_Editor.Text = "Editor"
        Me.btn_Editor.UseVisualStyleBackColor = True
        Me.btn_Editor.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(226, 250)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(55, 23)
        Me.Button1.TabIndex = 54
        Me.Button1.Text = "Arbol"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Enabled = False
        Me.Button2.Location = New System.Drawing.Point(339, 390)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(172, 23)
        Me.Button2.TabIndex = 55
        Me.Button2.Text = "Poner categorias principales"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'btnCambiarIva
        '
        Me.btnCambiarIva.Location = New System.Drawing.Point(536, 388)
        Me.btnCambiarIva.Name = "btnCambiarIva"
        Me.btnCambiarIva.Size = New System.Drawing.Size(135, 23)
        Me.btnCambiarIva.TabIndex = 56
        Me.btnCambiarIva.Text = "Cambiar IVA"
        Me.btnCambiarIva.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(536, 356)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(135, 23)
        Me.Button3.TabIndex = 57
        Me.Button3.Text = "Cambiar Tarifas"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'txt_precio
        '
        Me.txt_precio.Location = New System.Drawing.Point(686, 331)
        Me.txt_precio.Name = "txt_precio"
        Me.txt_precio.Size = New System.Drawing.Size(57, 20)
        Me.txt_precio.TabIndex = 58
        '
        'txt_pcoste
        '
        Me.txt_pcoste.Location = New System.Drawing.Point(749, 331)
        Me.txt_pcoste.Name = "txt_pcoste"
        Me.txt_pcoste.Size = New System.Drawing.Size(68, 20)
        Me.txt_pcoste.TabIndex = 59
        '
        'txt_pdistribuidor
        '
        Me.txt_pdistribuidor.Location = New System.Drawing.Point(833, 331)
        Me.txt_pdistribuidor.Name = "txt_pdistribuidor"
        Me.txt_pdistribuidor.Size = New System.Drawing.Size(62, 20)
        Me.txt_pdistribuidor.TabIndex = 60
        '
        'txt_referencia
        '
        Me.txt_referencia.Location = New System.Drawing.Point(739, 305)
        Me.txt_referencia.Name = "txt_referencia"
        Me.txt_referencia.Size = New System.Drawing.Size(95, 20)
        Me.txt_referencia.TabIndex = 61
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(71, 358)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 77
        Me.Label1.Text = "Fichero"
        '
        'btn_Examinar
        '
        Me.btn_Examinar.Location = New System.Drawing.Point(456, 356)
        Me.btn_Examinar.Name = "btn_Examinar"
        Me.btn_Examinar.Size = New System.Drawing.Size(75, 23)
        Me.btn_Examinar.TabIndex = 76
        Me.btn_Examinar.Text = "Examinar"
        Me.btn_Examinar.UseVisualStyleBackColor = True
        '
        'txtFile
        '
        Me.txtFile.Location = New System.Drawing.Point(126, 356)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.Size = New System.Drawing.Size(324, 20)
        Me.txtFile.TabIndex = 75
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(698, 360)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(119, 23)
        Me.Button4.TabIndex = 78
        Me.Button4.Text = "Estado Clientes"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'txt_Email
        '
        Me.txt_Email.Location = New System.Drawing.Point(824, 363)
        Me.txt_Email.Name = "txt_Email"
        Me.txt_Email.Size = New System.Drawing.Size(10, 20)
        Me.txt_Email.TabIndex = 79
        '
        'btn_Traducir
        '
        Me.btn_Traducir.Location = New System.Drawing.Point(307, 29)
        Me.btn_Traducir.Name = "btn_Traducir"
        Me.btn_Traducir.Size = New System.Drawing.Size(75, 23)
        Me.btn_Traducir.TabIndex = 80
        Me.btn_Traducir.Text = "Traducir"
        Me.btn_Traducir.UseVisualStyleBackColor = True
        '
        'txtTO
        '
        Me.txtTO.Location = New System.Drawing.Point(456, 26)
        Me.txtTO.Name = "txtTO"
        Me.txtTO.Size = New System.Drawing.Size(439, 20)
        Me.txtTO.TabIndex = 81
        '
        'txtFrom
        '
        Me.txtFrom.Location = New System.Drawing.Point(456, 52)
        Me.txtFrom.Name = "txtFrom"
        Me.txtFrom.Size = New System.Drawing.Size(439, 20)
        Me.txtFrom.TabIndex = 82
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(456, 317)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(176, 23)
        Me.Button5.TabIndex = 83
        Me.Button5.Text = "Campo valor Inglés"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(456, 288)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(176, 23)
        Me.Button6.TabIndex = 84
        Me.Button6.Text = "Borrar productos"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(316, 293)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(134, 23)
        Me.Button7.TabIndex = 85
        Me.Button7.Text = "Precios distribuidor"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(686, 278)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(170, 23)
        Me.Button8.TabIndex = 86
        Me.Button8.Text = "Poner EANS a vacio"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(486, 88)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(100, 23)
        Me.Button9.TabIndex = 87
        Me.Button9.Text = "Traducir con API"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'result
        '
        Me.result.Location = New System.Drawing.Point(592, 105)
        Me.result.Name = "result"
        Me.result.Size = New System.Drawing.Size(303, 20)
        Me.result.TabIndex = 88
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(316, 322)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 23)
        Me.Button10.TabIndex = 89
        Me.Button10.Text = "PVP"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'txtOrigen
        '
        Me.txtOrigen.Location = New System.Drawing.Point(592, 81)
        Me.txtOrigen.Name = "txtOrigen"
        Me.txtOrigen.Size = New System.Drawing.Size(303, 20)
        Me.txtOrigen.TabIndex = 90
        '
        'txtIdioma
        '
        Me.txtIdioma.Location = New System.Drawing.Point(62, 63)
        Me.txtIdioma.Name = "txtIdioma"
        Me.txtIdioma.Size = New System.Drawing.Size(38, 20)
        Me.txtIdioma.TabIndex = 91
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 92
        Me.Label2.Text = "Idioma"
        '
        'btn_Idioma
        '
        Me.btn_Idioma.Location = New System.Drawing.Point(110, 62)
        Me.btn_Idioma.Name = "btn_Idioma"
        Me.btn_Idioma.Size = New System.Drawing.Size(98, 23)
        Me.btn_Idioma.TabIndex = 93
        Me.btn_Idioma.Text = "Importar idioma"
        Me.btn_Idioma.UseVisualStyleBackColor = True
        '
        'txt_Contador
        '
        Me.txt_Contador.Location = New System.Drawing.Point(226, 62)
        Me.txt_Contador.Name = "txt_Contador"
        Me.txt_Contador.Size = New System.Drawing.Size(38, 20)
        Me.txt_Contador.TabIndex = 94
        '
        'txt_Inicio
        '
        Me.txt_Inicio.Location = New System.Drawing.Point(62, 31)
        Me.txt_Inicio.Name = "txt_Inicio"
        Me.txt_Inicio.Size = New System.Drawing.Size(100, 20)
        Me.txt_Inicio.TabIndex = 95
        '
        'txt_Fin
        '
        Me.txt_Fin.Location = New System.Drawing.Point(169, 31)
        Me.txt_Fin.Name = "txt_Fin"
        Me.txt_Fin.Size = New System.Drawing.Size(100, 20)
        Me.txt_Fin.TabIndex = 96
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(307, 62)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(130, 23)
        Me.Button11.TabIndex = 97
        Me.Button11.Text = "URL's producto nueva"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(307, 95)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(130, 23)
        Me.Button12.TabIndex = 98
        Me.Button12.Text = "Nombres Checo"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CheckBox6)
        Me.GroupBox1.Controls.Add(Me.CheckBox5)
        Me.GroupBox1.Controls.Add(Me.CheckBox4)
        Me.GroupBox1.Controls.Add(Me.CheckBox3)
        Me.GroupBox1.Controls.Add(Me.CheckBox2)
        Me.GroupBox1.Controls.Add(Me.CheckBox1)
        Me.GroupBox1.Location = New System.Drawing.Point(1031, 175)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(358, 100)
        Me.GroupBox1.TabIndex = 99
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.Location = New System.Drawing.Point(204, 57)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(81, 17)
        Me.CheckBox6.TabIndex = 5
        Me.CheckBox6.Text = "CheckBox6"
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.Location = New System.Drawing.Point(116, 57)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(81, 17)
        Me.CheckBox5.TabIndex = 4
        Me.CheckBox5.Text = "CheckBox5"
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.Location = New System.Drawing.Point(28, 57)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(81, 17)
        Me.CheckBox4.TabIndex = 3
        Me.CheckBox4.Text = "CheckBox4"
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Location = New System.Drawing.Point(204, 20)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(81, 17)
        Me.CheckBox3.TabIndex = 2
        Me.CheckBox3.Text = "CheckBox3"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(116, 20)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(81, 17)
        Me.CheckBox2.TabIndex = 1
        Me.CheckBox2.Text = "CheckBox2"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(28, 20)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(81, 17)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "CheckBox1"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(1314, 293)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(75, 23)
        Me.Button13.TabIndex = 100
        Me.Button13.Text = "Button13"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(1175, 388)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(75, 23)
        Me.Button14.TabIndex = 101
        Me.Button14.Text = "Valor"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'txtIdiomaValor
        '
        Me.txtIdiomaValor.Location = New System.Drawing.Point(1130, 390)
        Me.txtIdiomaValor.Name = "txtIdiomaValor"
        Me.txtIdiomaValor.Size = New System.Drawing.Size(29, 20)
        Me.txtIdiomaValor.TabIndex = 102
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(1059, 28)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(230, 50)
        Me.Button15.TabIndex = 103
        Me.Button15.Text = "Borrar productos"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'CrearProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1469, 439)
        Me.ControlBox = False
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.txtIdiomaValor)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.txt_Fin)
        Me.Controls.Add(Me.txt_Inicio)
        Me.Controls.Add(Me.txt_Contador)
        Me.Controls.Add(Me.btn_Idioma)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtIdioma)
        Me.Controls.Add(Me.txtOrigen)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.result)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.txtFrom)
        Me.Controls.Add(Me.txtTO)
        Me.Controls.Add(Me.btn_Traducir)
        Me.Controls.Add(Me.txt_Email)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_Examinar)
        Me.Controls.Add(Me.txtFile)
        Me.Controls.Add(Me.txt_referencia)
        Me.Controls.Add(Me.txt_pdistribuidor)
        Me.Controls.Add(Me.txt_pcoste)
        Me.Controls.Add(Me.txt_precio)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.btnCambiarIva)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btn_Editor)
        Me.Controls.Add(Me.btn_Crear)
        Me.Controls.Add(Me.combo_Estado)
        Me.Controls.Add(Me.txt_Nombre_Categoria)
        Me.Controls.Add(Me.txt_Id_categoria)
        Me.Controls.Add(Me.txt_Descripcion_Corta)
        Me.Controls.Add(Me.txt_Nombre)
        Me.Controls.Add(Me.txt_Id)
        Me.Controls.Add(Me.btn_Cerrar)
        Me.Controls.Add(Me.lbl_Producto_Configurable)
        Me.Controls.Add(Me.lbl_Estado)
        Me.Controls.Add(Me.lbl_Nombre_Categoria)
        Me.Controls.Add(Me.lbl_Id_Categoria)
        Me.Controls.Add(Me.lbl_Desc_Larga)
        Me.Controls.Add(Me.lbl_Desc_Corta)
        Me.Controls.Add(Me.lbl_Nombre)
        Me.Controls.Add(Me.lbl_Id)
        Me.Name = "CrearProducto"
        Me.Text = "Crear Producto Configurable"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_Id As System.Windows.Forms.Label
    Friend WithEvents lbl_Nombre As System.Windows.Forms.Label
    Friend WithEvents lbl_Desc_Corta As System.Windows.Forms.Label
    Friend WithEvents lbl_Desc_Larga As System.Windows.Forms.Label
    Friend WithEvents lbl_Id_Categoria As System.Windows.Forms.Label
    Friend WithEvents lbl_Nombre_Categoria As System.Windows.Forms.Label
    Friend WithEvents lbl_Estado As System.Windows.Forms.Label
    Friend WithEvents lbl_Producto_Configurable As System.Windows.Forms.Label
    Friend WithEvents btn_Cerrar As System.Windows.Forms.Button
    Friend WithEvents txt_Id As System.Windows.Forms.TextBox
    Friend WithEvents txt_Nombre As System.Windows.Forms.TextBox
    Friend WithEvents txt_Descripcion_Corta As System.Windows.Forms.TextBox
    Friend WithEvents txt_Id_categoria As System.Windows.Forms.TextBox
    Friend WithEvents txt_Nombre_Categoria As System.Windows.Forms.TextBox
    Friend WithEvents combo_Estado As System.Windows.Forms.ComboBox
    Friend WithEvents btn_Crear As System.Windows.Forms.Button
    Friend WithEvents btn_Editor As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents btnCambiarIva As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents txt_precio As System.Windows.Forms.TextBox
    Friend WithEvents txt_pcoste As System.Windows.Forms.TextBox
    Friend WithEvents txt_pdistribuidor As System.Windows.Forms.TextBox
    Friend WithEvents txt_referencia As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_Examinar As System.Windows.Forms.Button
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents txt_Email As System.Windows.Forms.TextBox
    Friend WithEvents btn_Traducir As System.Windows.Forms.Button
    Friend WithEvents txtTO As System.Windows.Forms.TextBox
    Friend WithEvents txtFrom As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents result As System.Windows.Forms.TextBox
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents txtOrigen As System.Windows.Forms.TextBox
    Friend WithEvents txtIdioma As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btn_Idioma As System.Windows.Forms.Button
    Friend WithEvents txt_Contador As System.Windows.Forms.TextBox
    Friend WithEvents txt_Inicio As System.Windows.Forms.TextBox
    Friend WithEvents txt_Fin As System.Windows.Forms.TextBox
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents txtIdiomaValor As System.Windows.Forms.TextBox
    Friend WithEvents Button15 As System.Windows.Forms.Button
End Class
