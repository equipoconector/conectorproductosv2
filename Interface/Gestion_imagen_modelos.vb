﻿Imports System
Imports System.IO
Imports WinSCP

Public Class Gestion_imagen_modelos
    Public carpeta As New DirectoryInfo("\\192.168.2.6\imagenes\img_agrupado")
    Public act_imagen As New Images
    Public i As Integer = 0
    Public n As Integer

    Private Sub buscar_Click(sender As Object, e As EventArgs) Handles buscar.Click
        CheckedListBox1.Items.Clear()

        For Each f As FileInfo In carpeta.GetFiles
            If f.Name.Contains(modelobusqueda.Text) Then
                CheckedListBox1.Items.Add(f.Name)
            End If
        Next
    End Sub

    Private Sub CheckedListBox1_Click(sender As Object, e As EventArgs) Handles CheckedListBox1.Click
        Dim cadena As String = CStr(CheckedListBox1.SelectedItem)
        Using stream As New StreamReader("\\192.168.2.6\imagenes\img_agrupado\" & cadena)
            PictureBox1.Image = Image.FromStream(stream.BaseStream)
            nombreimgen.Text = cadena
        End Using
    End Sub

    Private Sub Gestion_imagen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For Each f As FileInfo In carpeta.GetFiles
            If f.Name.StartsWith(Modelo.idmodelo.Text & "_") Then
                CheckedListBox1.Items.Add(f.Name)
                i = i + 1
                'TextBox1.Text = CStr(i)
            End If
        Next

        If i = 1 Then
            i = 0
        End If
    End Sub

    Private Sub abredirectorio_Click(sender As Object, e As EventArgs) Handles abredirectorio.Click
        PictureBox1.Image = Nothing
        CheckedListBox1.ClearSelected()
        Dim OpenFile As New OpenFileDialog()
        If OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
            PictureBox1.Image = Image.FromFile(OpenFile.FileName)
        End If
    End Sub

    Private Sub actualizar_Click(sender As Object, e As EventArgs) Handles actualizar.Click
        act_imagen.actualizar_img_modelo()
    End Sub

    Private Sub Eliminar_Click(sender As Object, e As EventArgs) Handles Eliminar.Click
        act_imagen.eliminar_imagen_modelo()
    End Sub

    Private Sub guardar_imagen_Click(sender As Object, e As EventArgs) Handles guardar_imagen.Click
        Dim conexion As web = New web

        act_imagen.referencia = Modelo.idmodelo.Text
        act_imagen.incremento = Modelo.idmodelo.Text & "_" & n
        act_imagen.Crear_carpeta_modelo()
        conexion.imagen_modelo() 'magento
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        For Me.n = 0 To 19
            If (File.Exists("\\192.168.2.6\imagenes\img_agrupado\" & Modelo.idmodelo.Text & "_" & n & ".jpg")) Then
            Else : Exit For
            End If
        Next
        nombreimgen.Text = Modelo.idmodelo.Text & "_" & n & ".jpg"

        PictureBox1.Image = Nothing
        CheckedListBox1.ClearSelected()
        Dim OpenFile As New OpenFileDialog()
        If OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
            PictureBox1.Image = Image.FromFile(OpenFile.FileName)
        End If
    End Sub

    Private Sub enviar_Click(sender As Object, e As EventArgs) Handles enviar.Click

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)
        '    Dim n As Integer
        '    For n = 0 To 19 Step 1
        '        If File.Exists("\\192.168.2.6\imagenes\img_agrupado\56125_5.jpg") = True Then
        '            MessageBox.Show("encontrado")
        '            Exit For
        '        End If
        '    Next n
        '    MessageBox.Show(CStr(n))
    End Sub
End Class